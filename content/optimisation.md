Title: Optimisation des fonctions différentiables
Date: 2018-06-24
Category: L3
Tags: Analyse
# Objectifs pédagogiques:


Optimisation des fonctions différentiables sous contraintes d'égalités et d'inégalités.


# Fichiers:
### Cours
[Programation linéaire]({attach}docs/PL.pdf)

[Le cas des fonctions de deux variables]({attach}docs/optimisation_deux_var.pdf)

[Le cas des fonctions de $n$ variables]({attach}docs/optimisation_plus_vars.pdf)

### TD

[TD fiche n°1]({attach}docs/fiche_n1.pdf)

[TD fiche n°2]({attach}docs/fiche_n2.pdf)

[TD fiche n°3]({attach}docs/fiche_n3.pdf)

[TD fiche n°4]({attach}docs/fiche_n4.pdf)

[TD fiche n°5]({attach}docs/fiche_n5.pdf)

[TD fiche n°6]({attach}docs/fiche_n6.pdf)

[TD fiche n°7]({attach}docs/fiche_n7.pdf)




# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Optimisation/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/Optimisation/TD)