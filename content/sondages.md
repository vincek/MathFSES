Title: Les sondages
Date: 2018-06-24
Category: L3
Tags: Statistiques
# Objectifs pédagogiques:

Quelques apports sur les enquêtes par sondage.


# Fichiers:
### Cours
[Présentation]({attach}docs/sondage.pdf)

[Biliographie]({attach}docs/sondage_bibliographie.pdf)


# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Sondages)
