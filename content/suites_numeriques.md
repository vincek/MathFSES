Title: Suites numériques
Date: 2018-06-24
Category: L1
Tags: Analyse
# Objectifs pédagogiques:

Introduction aux suites numériques.

* Raisonnement par récurrence
* Suites arithmétiques
* Suites géométriques
* Suites arithmético-géométrique
* Suite récurrente double
* Étude des suites du type $u_{n+1}=f(u_n)$


# Fichiers:
[Cours]({attach}docs/Les_suites_numeriques.pdf)

[TD]({attach}docs/TD.pdf)

# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Suites_numeriques/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/Suites_numeriques/TD)