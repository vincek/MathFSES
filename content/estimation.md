Title: Estimation et théorie des tests
Date: 2018-06-24
Category: L3
Tags: Statistiques
# Objectifs pédagogiques:


* théorie de l'estimation
* introduction à la théorie des tests statistiques


# Fichiers:
### Cours
[Estimation]({attach}docs/estimation.pdf)

[Tests]({attach}docs/tests.pdf)

### TD
[Fiche n°1]({attach}docs/fiche_n_1.pdf)
[Fiche n°2]({attach}docs/fiche_n_2.pdf)
[Fiche n°3]({attach}docs/fiche_n_3.pdf)
[Fiche n°4]({attach}docs/fiche_n_4.pdf)
[Fiche n°5]({attach}docs/fiche_n_5.pdf)





### TP

[TP n°1]({attach}docs/TP_n1.pdf) [Données]({attach}docs/données.ods)


[TP n°2]({attach}docs/TP_n2.pdf) [Données]({attach}docs/donnees.csv) [Données]({attach}docs/Répartition des compétences commerciales.ods) 

[TP n°3]({attach}docs/tp3.pdf) [Données]({attach}docs/hdv2003.ods)


# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Estimation_Tests/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/Estimation_Tests/TD)
[TP](https://framagit.org/vincek/MathFSES/tree/master/Estimation_Tests/TP)