Title: À propos
Date: 2018-06-24


# Présentation

Ce site présente des documents utilisés pour les enseignements de mathématiques en licence de Sciences Économiques et Sociales de l'université de Lille entre 2016 et 2018.




# Outils utilisés

Tous les documents ont été réalisés à l'aide de logiciels libres.

* LaTeX avec la classe [cueep](https://framagit.org/vincek/cueepdotcls)
* [asymptote](https://sourceforge.net/projects/asymptote/)
* [GeoGebra](https://www.geogebra.org/)
* [TikZ/PGF](http://pgf.sourceforge.net/)
* [Pdfadd](http://www.xm1math.net/pdfadd/index.html)
* [maxima](http://maxima.sourceforge.net/)

# Licence

L'ensemble du contenu de ce site est placé (sauf mention expresse du contraire) sous la licence [Creative Commons BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).
