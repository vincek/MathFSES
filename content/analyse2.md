Title: Fonctions de deux variables et intégration
Date: 2018-06-24
Category: L2
Tags: Analyse
# Objectifs pédagogiques:


* Intégration
* Fonction de deux variables
* Optimisation sous contrainte d'égalité des fonctions de deux variables 


# Fichiers:

[Cours]({attach}docs/analyse2.pdf)

[TD]({attach}docs/td.pdf)


[Quiz moodle]({attach}docs/quiz-Analyse2.xml)

# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Analyse2/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/Analyse2/TD)
[Moodle](https://framagit.org/vincek/MathFSES/tree/master/Analyse2/Cours/moodle)
