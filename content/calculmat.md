Title: Calcul matriciel
Date: 2018-06-24
Category: L2
Tags: Algèbre
# Objectifs pédagogiques:

Présentation du calcul matriciel sans le language des espaces vectoriels.

# Fichiers:

[Cours]({attach}docs/Calculmat_2017.pdf)

[TD]({attach}docs/td_matrices_2017.pdf)

[Quiz Moodle]({attach}docs/quiz-matrices.xml)

# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/Calcul_matriciel/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/Calcul_matriciel/TD)
[moodle](https://framagit.org/vincek/MathFSES/tree/master/Calcul_matriciel/Cours/moodle)
