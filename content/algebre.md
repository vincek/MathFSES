Title: Algèbre linéaire
Date: 2018-06-24
Category: L3
Tags: Algèbre
# Objectifs pédagogiques:


* Sous-espace vectoriels de $R^n$ 
* Applications linéaires
* Matrices d'une application linéaire
* Réduction des endomorphismes
* Espaces euclidens


# Fichiers:
### Cours
[L’espace vectoriel $R^n$]({attach}docs/AL_ch1.pdf)

[Base d’un espace vectoriel]({attach}docs/AL_ch2.pdf)

[Applications linéaires]({attach}docs/AL_ch3.pdf)

[Matrice d’une application linéaire]({attach}docs/AL_ch4.pdf)

[Matrice inversible et déterminants]({attach}docs/AL_ch5.pdf)

[Réduction des endomorphismes]({attach}docs/AL_ch6.pdf)

[Espaces euclidiens]({attach}docs/AL_ch7.pdf)
### TD
[TD]({attach}docs/td_al_2017.pdf)

# Sources
[Cours](https://framagit.org/vincek/MathFSES/tree/master/AL/Cours)
[TD](https://framagit.org/vincek/MathFSES/tree/master/AL/TD)