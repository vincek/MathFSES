#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Vincek'
SITENAME = u'Math FSES Université de Lille'
SITEURL = u'http://vincek.frama.io/MathFSES'

#SITEURL='http://localhost:8000'
PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'
#THEME='attila'
THEME='theme'
DEFAULT_LANG = u'fr'
STATIC_PATHS=['docs','images']
OG_LOCALE=u'fr_FR'
PLUGIN_PATHS = ["plugins"]
PLUGINS = [ "sitemap","footer_insert","tag_cloud","global_license"]
#ORG_READER_EMACS_LOCATION='/usr/bin/emacs25'

LICENCE="CC BY-NC-SA"
SITEMAP = {
            'format': 'xml',
                'priorities': {
                            'articles': 0.5,
                                    'indexes': 0.5,
                                            'pages': 0.5
                                                },
                    'changefreqs': {
                                'articles': 'monthly',
                                        'indexes': 'daily',
                                                'pages': 'monthly'
                                                    }
                    }

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml' 
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Faculté de Sciences Économiques et Sociales', 'http://ses.univ-lille1.fr/'))

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

SOCIAL= (('rss','http://vincek.frama.io/MathFSES/feeds/all.atom.xml'),)
DEFAULT_PAGINATION = 10 

DISPLAY_CATEGORIES_ON_MENU = True
TYPOGRIFY = True
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


TAG_CLOUD_STEPS=4 #number of different sizes of fonts in the tag cloud
TAG_CLOUD_MAX_ITEMS=20 #number of different tags that can appear in tag cloud
TAG_CLOUD_SORTING='random' #how tags will be ordered in the tag cloud. Valid values: random, alphabetically, alphabetically-rev, size and size-rev
TAG_CLOUD_BADGE=True #If True, displays the number of articles in each tag

DISQUS_SITENAME = "maths-fses"
