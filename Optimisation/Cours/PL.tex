%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Cours,11pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\titre{Programmation linéaire}
\auteur{V. Ledda}
\reference{Chapitre 1}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 MISEG \\
Optimisation}
\usepackage{draftwatermark}
\motsclefs{Programme linéaire, simplexe, matrice}
\usepackage{fancybox}
\newcommand{\titledframe}[2]{%
\boxput*(0,1){#1}%
{\ovalbox{#2}}}
\usepackage{tikz}
\usetikzlibrary{matrix}
\usepackage{algo,enumerate,ulem}
\newcommand{\ps}{(\cdot|\cdot )}

\usepackage[tikz]{bclogo}
\usepackage{tabularx}
\etudiant=0
\begin{document}

\maketitle
\tableofcontents
\newpage

\section{Introduction}
\label{sec:introduction}

\subsection{Présentation du problème}
\label{sec:pres-du-probl}

On se donne une grandeur économique $M$ qui dépend linéairement de
plusieurs paramètres $X=(x_1;x_2;\cdots;x_n)$. On cherche à optimiser cette grandeur, c'est à
dire que l'on cherche les valeurs  $X^m=(x_1^m;x_2^m;\cdots;x_n^m)$ qui rendent
$M$ maximale (ou minimale). Bien évidement, les valeurs de $X$ sont
contraintes! Premièrement, les $x_i$ sont positifs; de plus on suppose
qu'il existe $k$ relation du type
$a_{i1}x_1+a_{i2}x_2+\cdots+a_{in}x_n\leq k_i$.
 
\begin{exem}
  Une entreprise vend trois produits différents. On note  $P_1, P_2,
  P_3$ les quantités produites par l'entreprise de chacun de ces
  produits. Le prix de vente de chaque produit est fixé à
  l'année. L'entreprise cherche à maximiser la fonction:

  \begin{equation*}
    (P_1;P_2;P_3)\longmapsto 4P_1+12P_2+3P_3
  \end{equation*}


  Cette fonction est appelée la \textbf{fonction économique}.

  Ces articles sont produits sur une chaîne qui <<tourne>> 45~h par
  semaine. Sur cette chaîne, l'entreprise peut produire 50 articles
  $P_1$ par heure, 25 articles  $P_2$ par heure et 75 articles $P_3$
  par heure. De plus les études de marché, montre que les possibilités
  de vente des 3 produits ne dépassent pas respectivement 1\,000, 500 et
  1\,500 unités par semaine. Quelle répartition hebdomadaire de la
  production assure un chiffre d'affaire maximum?
\end{exem}
\subsection{Le cas de deux variables}
\label{sec:le-cas-de}

Une usine produit deux ciments Portland:
\begin{itemize}
\item le ciment <<OPTIM>> rapportant 30~€ la tonne;
\item le ciment <<EXTREM>> rapportant 40~€ la tonne.
\end{itemize}
 

Pour réaliser une tonne de ciment <<OPTIM>> 40 minutes de calcination dans un four à
chaux et 20 minutes de broyage sont nécessaires.


Pour réaliser une tonne de ciment <<EXTREM>> 30 minutes de calcination dans un four à
chaux et 30 minutes de broyage sont nécessaires. 

Le four et l’atelier de broyage sont disponibles respectivement 12
heures et 8 heures par jour. Pour des raisons de stockage,
l'entreprise ne peut stocker que 19 tonnes de ciment par jour.


Combien de ciment de chaque type peut-on produire par jour pour maximiser le
bénéfice?

\section{La méthode du simplexe}
\label{sec:la-methode-du}

\subsection{Écriture matricielle}
\label{sec:ecriture-matricielle}


Dans la partie précédente \ref{sec:le-cas-de} on a obtenu le système de contraintes suivant:

\begin{equation}
\label{contraintes}
   \systt{4x+3y\leq 72}{x+y\leq 19}{2x+3y\leq 48}{0\leq x\text{
       et }0\leq y}
\end{equation}

représenté par le polygone des contraintes:
\begin{center}
  \includegraphics[width=13cm]{images/retour_cimenterie}
\end{center}


La fonction économique est:
\begin{equation*}
  B=30x+40y
\end{equation*}



Dans cette partie, nous allons présenter le même problème de manière
matricielle. Cette démarche nous permettra de généraliser au cas de
plusieurs variables. Introduisons, trois variables positives,
appelées \textbf{variables d'écart}, $e_1$, $e_2$ et  $e_3$ pour
écrire les inégalités du système \eqref{contraintes} sous forme
d'égalités.


\begin{equation}
\label{contraintes_e}
   \systt{4x+3y+e_1= 72}{x+y+e_2= 19}{2x+3y+e_3=48}{0\leq x, 0\leq y,
     0\leq e_1, 0\leq e_2, 0\leq e_3}
\end{equation}



On peut écrire matriciellement ce système:


\begin{equation*}
\label{sys}\tag{S}
  \begin{pmatrix}
4&3&1&0&0\\
1&1&0&1&0\\
2&3&0&0&1\\
\end{pmatrix}\times
\begin{pmatrix}
  x\\
y\\
e_1\\
e_2\\
e_3\\
\end{pmatrix}=
\begin{pmatrix}
  72\\
  19\\
  48\\
  \end{pmatrix}
\end{equation*}

\subsection{Solutions de base}
\label{sec:solutions-de-base}



Lorsque $x=y=0$, le système admet une unique solution. Mais $B=0$, on
est loin du maximum!! C'est une \textbf{solution de base réalisable}
du système.


Lorsque $y=e_2=0$, le système admet pour solution $x=19$, $e_1=-4<0$
et $e_3=4$. C'est une \textbf{solution de base} du système. Mais, ici, cette solution
  n'est pas réalisable.



  \begin{exem}
    Reprendre le travail précédent: annuler 2 variables, résoudre,
  calculer $B$,  interpréter graphiquement, dire s'il s'agit d'une solution de base réalisable; pour tous les cas possibles. (Il y a $\binom{5}{2}
  =10$ cas à traiter)
  \end{exem}

  
\subsection{Le tableau du simplexe}
\label{sec:le-tableau-du}
 Considérons le tableau suivant:

  \begin{eqnarray*}
    \begin{array}[h]{rlc}
\text{Hors base}&\text{En base}&\\
 \overbrace{\hphantom{87}}&\overbrace{\hphantom{8664587}}&\hphantom{7}\\
\end{array}\\
    \begin{array}[h]{cccccc}
           x&y&\!e_1&\!e_2&\!e_3&\hphantom{7}\\
    \end{array}\\
   \begin{bmatrix}
4&3&1&0&0&72\\
1&1&0&1&0&19\\
2&3&0&0&1&48\\
\end{bmatrix}
\end{eqnarray*}


Lors que $x=y=0$, $e_1$, $e_2$ et $e_3$ forment une base de
$\R^3$. (Il s'agit de la base canonique de $\R^3$). On dit que $e_1$,
$e_2$ et $e_3$ sont <<en base>> et que $x$ et $y$ sont <<hors base>>.


À l'aide du pivot de Gauss, remplacer la deuxième colonne du tableau
par la colonne:
\begin{equation*}
\begin{bmatrix}
  0\\
  1\\
  0
\end{bmatrix}
\end{equation*}



Que remarque-t-on? Interpréter géométriquement. Cette opération s'appelle un <<pivotement>>, $y$
est entré en base et $e_2$ est sorti, il est maintenant hors base.

En général parcourir tous les sommets n'est pas réalisable car leur
nombre croît très vite avec le nombre de contraintes et de
variables\footnote{Pour $n$ variable et $m$ contraintes, il est inférieur ou égal à $\binom{n}{m}$}.

L'idée de l'algorithme du simplexe\footnote{ George Dantzig (1951)}
est de parcourir  un chemin parmi  les solutions de bases réalisables
en augmentant à chaque étape la valeur de la fonction économique pour
arriver à une solution de base réalisable optimale.


L'algorithme du simplexe se présente généralement sous la forme d'une
succession de tableaux, dans le cas présent le tableau initial
pourrait être:

\begin{equation}
\label{S}
   \begin{bmatrix}
4&3&1&0&0&72\\
1&1&0&1&0&19\\
2&3&0&0&1&48\\
30&40&0&0&0&0\\
\end{bmatrix}
\end{equation}

\subsection{La méthode de Dantzig}
\label{sec:la-methode-de}


Il s'agit, à l'aide de <<pivotements>>,   de faire entrer et sortir judicieusement des vecteurs de l'ensemble des
vecteurs de base. On
parcourt ainsi le
polygone des contraintes et l'on s'arrête une fois que l'on a  trouvé une solution de base réalisable optimale.

\paragraph{L'algorithme du simplexe}
\label{sec:lalg-du-simpl}

On appelle $S$ le tableau à $n+1$ lignes et $m+n+1$ colonnes issue d'un
problème de programmation linéaire à $m$ variables et $n$
contraintes. La dernière ligne représente la fonction économique que
l'on cherche à maximiser.

 
\begin{algo}[numbered]
  \ALGO{Simplexe}
  \WHILE{La dernière ligne contient des coefficients strictement
    positifs}
  \STATE{j\recoit{}$\{j|S_{n+1,j}=\max_{i\in\llbracket
      1,\cdots,m+n\rrbracket}S_{n+1,i}\}$}
  \STATE{i\recoit{} $\{i|\frac{S_{i,n+m+1}}{S_{i,j}}=\min_{k\in\llbracket
      1,\cdots,n\rrbracket,S_{i,j}>0} \frac{S_{k,n+m+1}}{S_{k,j}}\}$}
  \STATE{Par pivotement, faire entrer la colonne $j$ et sortir la
    colonne $i$ de la liste des colonnes <<en base>>}
  \ENDWHILE
  \STATE{Dans le tableau, on lit les variables en base}
  \STATE{Si la variable $i$ est <<en base>> alors $S_{i,n+m+1}$ donne
    la valeur à donner à la variable $i$ pour obtenir une solution
    optimale}
  \STATE{$-S_{n+1,n+m+1}$ est le maximum de la fonction économique}
\end{algo}

\paragraph{Quelques explications}
\label{sec:quelq-expl}

\begin{Itemize}
\item \textit{À la ligne 3}, Pour déterminer la variable qui entre en
  base, on choisit parmi les variables celle qui  <<rapporte>> le
  plus. Autrement dit, on choisit la variable associée au plus grand
  gain marginal. S'il y a plusieurs colonnes où le coefficient  est maximal, on en choisit une.
\item \textit{À la ligne 4}, Pour déterminer la variable qui sort de  la base, on prend la ligne où le rapport  $\frac{S_{k,n+m+1}}{S_{i,j}}$ est le plus petit possible pour ne pas  sortir du polygone des contraintes. En effet, se faisant, lors du  pivotement on a pour $k\neq i$:
  \begin{center}
    L$_{k}\leftarrow $ L$_{k}-\frac{S_{k,j}}{S_{i,j}}$L$_{i}$
  \end{center}

  Plus précisément pour la dernière colonne:
  \begin{equation*}
    S_{k,n+m+1}\leftarrow  S_{k,n+m+1}-\frac{S_{k,j}}{S_{i,j}}S_{i,n+m+1}
  \end{equation*}

Donc $S_{k,n+m+1}\leq 0$ car par le choix de $i$
,$\frac{S_{k,n+m+1}}{S_{k,j}}-\frac{S_{i,n+m+1}}{S_{i,j}}\geq 0$.

  S'il y a plusieurs lignes où le rapport
  est minimal, on en choisit une.
\item \textit{À la ligne 2}, dès que tous les coefficients sur la
  dernière ligne sont tous négatifs ou nuls, on s'arrête. 
\end{Itemize}

\begin{remarque}
  À côté des tableaux simplexes, il peut être intéressant de tenir à
  jour les listes des variables en base et des variables hors base.
\end{remarque}


\begin{remarque}
  Les coefficients $S_{i,j}$ et $S_{n+1,j}$ sont par construction de  l'algorithme strictement positifs. Lors du pivotement, la dernière  ligne est transformée par L$_{n+1}\leftarrow $  L$_{n+1}-\frac{S_{n+1,j}}{S_{i,j}}$L$_{j}$. Pour le coefficient  $S_{n+1,n+m+1}$ étant donné que les coefficients $S_{k,n+m+1}$ sont  positifs pour $1\leq k\leq n$, on est assuré qu'à chaque étape le  coefficient sera négatif et plus petit que le précédent. C'est à  dire, que la nouvelle solution de base est équivalente ou meilleure que la précédente.
\end{remarque}




\begin{exem}
  Appliquer l'algorithme du simplexe au tableau \eqref{S} et retrouver les résultats obtenus graphiquement lors d'une séance précédente.
\end{exem}
\pagebreak
\section{Formalisme et démonstration}
\label{sec:form-et-demonstr}

\begin{Def}
  On appelle programme linéaire, $(P)$, la donnée:
  \begin{enumerate}
  \item d'un système d'équations ou d'inéquations linéaires:

    \begin{equation}
      \label{eq:1}\tag{I}
      \left\lbrace\begin{array}[h]{c}
                    X\geq0\\
                    a_{11}x_1+a_{12}x_2+\cdots+a_{1n}x_n \lessgtr_1 b_1\\
                    \vdots\qquad \vdots\\
                    a_{p1}x_1+a_{p2}x_2+\cdots+a_{pn}x_n \lessgtr_p b_p\\
                  \end{array}\right.
              \end{equation}

              avec $X=\left(
                \begin{array}[h]{c}
                  x_1\\x_2\\\vdots\\ x_n
                \end{array}\right)$ et $\lessgtr_i$ est soit le symbole ``$=$'', soit ``$\leq$'',
              soit ``$\geq$'';
            \item d'une forme linéaire $m=\transp C\cdot X$ où $C=\left(
                \begin{array}[h]{c}
                  c_1\\c_2\\\vdots\\ c_n
                \end{array}\right)$.
            \end{enumerate}


\end{Def}

Résoudre un programme linéaire consiste à trouver le ou les valeurs de
$X$ qui optimisent $m$, c'est à dire le ou les valeurs de $X$ qui
rende(nt) $m$ maximale ou minimale selon les cas.

\begin{Def}
  Une solution \uline{réalisable} est une solution de
  \eqref{eq:1}. Une solution \uline{optimale} est une solution
  réalisable qui optimise $m$. La \uline{fonction objectif}, ou la
  \uline{fonction économique}, est la forme linéaire $m$. La
  \uline{valeur de $(P)$} est le nombre  $\transp C\cdot X_o$ où
  $X_o$ est une solution optimale.
\end{Def}

\begin{Def}
  On appelle programme linéaire canonique de type \textbf{max} tout programme
  linéaire de la forme:
  \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        AX\leq B\\
        m=\transp C\cdot X \quad\textrm{(max)}
      \end{array}\right.
    \end{equation*}

    où $A\in\mathcal{M}_{p,n}$, $B\in\mathcal{M}_{p,1}$ et $C\in\mathcal{M}_{n,1}$.
  \end{Def}


En remplaçant $ AX\leq B$ par $ AX\geq B$ et $ m=\transp C\cdot X
\quad\textrm{(max)}$ par  $m=\transp C\cdot X \quad\textrm{(min)}$ on
obtient un \uline{programme linéaire canonique de type \textbf{min}}. Un
\uline{programme linéaire standard} est un P.L. canonique où les contraintes
(hormis celles de positivitées) sont des égalités.

\begin{pro}
  Tout programme linéaire est équivalent à un programme linéaire
  canonique de type max. Tout programme linéaire est équivalent à un
  programme linéaire standard de type max.
\end{pro}

\begin{exem}
  Considérons le programme linéaire suivant:

  \begin{equation}
    \label{eq:2}\tag{P}
    \left\lbrace
      \begin{array}[h]{l}
        X \geq 0\\
          2x+y-z\leq 4\\
          -3x+y+z\geq 2\\
          2x+7y=3\\
          m=x+y-z\qquad \textrm{(min)}\\
        \end{array}\right.
    \end{equation}

    \begin{prof}
      \eqref{eq:2} est équivalent à
\begin{equation}
    \label{eq:3}\tag{P'}
    \left\lbrace
        \begin{array}[h]{l}   X \geq 0\\
          2x+y-z\leq 4\\
          3x-y-z\leq -2\\
          2x+7y\leq 3\\
           -2x-7y\leq -3\\
          m=-x-y+z\qquad \textrm{(max)}\\
        \end{array}\right.
    \end{equation}


 \eqref{eq:2} est équivalent à 
    \begin{equation}
    \label{eq:4}\tag{P''}
    \left\lbrace
        \begin{array}[h]{l}   X' \geq 0\\
          2x+y-z+e_1= 4\\
          3x-y-z+e_2= -2\\
          2x+7y+e_3= 3\\
           -2x-7y+e_4= -3\\
          m=-x-y+z\qquad \textrm{(max)}\\
        \end{array}\right.
    \end{equation}

    où $X'=\left(
      \begin{array}[h]{c}
        x\\ y\\z\\e_1\\e_2\\e_3\\e_4\\
      \end{array}\right)$.


    Les variables $e_i$ sont appelées variables d'écarts.
    \end{prof}
    
  \end{exem}


  \begin{pro}
    L'ensemble des solutions réalisables d'un programme linéaire est une partie
    convexe de $\R^n$.
  \end{pro}

  \begin{prof}
    \begin{preuve}
      Soit $t\in[0;1]$, $X=tX_0+(1-t)X_1$ où $X_0$ et $X_1$ sont deux
      solutions réalisable d'un programme linéaire, que l'on peut supposer sous la
      forme canonique de type max. Comme $t$ et $1-t$ sont positifs,


    \begin{equation*}
      AX=tAX_0+(1-t)X_1\leq t B+(1-t)B=B
    \end{equation*}
    Ainsi $X$ est une solution réalisable.
    
  \end{preuve}
\end{prof}

\begin{Def} Soit $(P)$ un programme linéaire.  On dit qu'une solution réalisable est un
  \uline{sommet} de $(P)$ s'il n'existe pas deux solutions réalisables
  \textbf{distinctes} $X_1$ et $X_2$ de $(P)$ telles que $X$ soit le
  milieu du segment $[X_1;X_2]$.
\end{Def}

\begin{pro}
  Si un programme linéaire $(P)$ admet une unique solution maximale $X$ alors $X$ est
  un sommet de $P$.
\end{pro}



\begin{prof}

  \begin{preuve}
    Procédons par l'absurde. Supposons qu'il existe deux solutions réalisables
  \textbf{distinctes} $X_1$ et $X_2$ de $(P)$ telles que $X$ soit le
  milieu du segment $[X_1;X_2]$. Sans perte de généralité, on peut
  supposer également que le programme linéaire $(P)$ est sous la forme canonique de
  type max.

Considérons la fonction
\begin{equation*}
\fonc{f}{[0;1]}{\R}{x}{\transp C (xX_1+(1-x)X_2).}
\end{equation*}

La fonction $f$ est une fonction affine, $f(x)=\transp C (X_1-X_2)x
+\transp C X_2$; cette fonction est soit croissante soit
décroissante. Supposons qu'elle soit croissante. $f(0,5)=\transp C
X\leq f(1)=\transp C X_1$. Comme $X$ est l'unique solution optimale,
$X=X_1$. Or $X$ est le milieu du segment $[X_1;X_2]$, donc
$X_1=X=X_2$; ceci contredit l'hypothèse. Donc $X$ est un sommet de
l'ensemble des contraintes.
  \end{preuve}
\end{prof}


De la même manière on peut démontrer la proposition suivante:

\begin{pro}
  Si $X_1$ et $X_2$ sont deux solutions optimales distinctes d'un programme linéaire
  $(P)$ alors  tous les points du segment $[X_1;X_2]$ sont des solutions optimales
  de $(P)$.
\end{pro}

Dans la suite on suppose que $(P)$ est un programme linéaire  standard


 \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        AX= B\\
        m=\transp C\cdot X \quad\textrm{(max)}
      \end{array}\right.
    \end{equation*}
  où $A\in\mathcal{M}_{p,n}$, $B\in\mathcal{M}_{p,1}$ et $C\in\mathcal{M}_{n,1}$;
tel que $p\leq
n$ et $\rg{A}=p$. Ces deux conditions sont raisonnables car:

\begin{itemize}
\item On peut toujours rajouter des variables
\item Si $\rg{A}<p$ alors soit le système n'a pas de solution! soit il
  en a et certaines équations sont redondantes (combinaisons
  linéaires des autres)
\end{itemize}

\begin{Def}
  On appelle base réalisable de $(P)$ toute famille $\mathcal{B}$ de vecteurs colonnes
  extraites de $A$ telle que:
  \begin{enumerate}[i)]
  \item $\mathcal{B}$ est une base de $\R^p$
  \item les coordonnées de $B$ dans la base $\mathcal{B}$ sont toutes
    positives ou nulles.
  \end{enumerate}
\end{Def}



\begin{Def} Soit $\mathcal{B}$ une base réalisable de $(P)$. Notons $i_1, i_2,
  \cdots, i_p$ les indices des colonnes de $A$ sélectionnées pour
  former $\mathcal{B}$. En particulier
  $B=x_{i_1}A_{i_1}+x_{i_2}A_{i_2}+\cdots+x_{i_p}A_{i_p}=x_{i_1}\mathcal{B}_1+x_{i_2}\mathcal{B}_2+\cdots+x_{i_p}\mathcal{B}_p$.
  
  On appelle solution de base réalisable associée à la base réalisable
  $B$, le vecteur de $\R^n$ dont la coordonnée de rang $i_k$ vaut
  $x_k$ et 0 sinon.
\end{Def}

\begin{exem}
  Dans l'exemple de la cimenterie, $A= \begin{bmatrix}
4&3&1&0&0\\
1&1&0&1&0\\
2&3&0&0&1\\
\end{bmatrix}$ les 3 dernières colonnes forment une base de $\R^3$. $B=
\begin{pmatrix}
  72\\19\\48
\end{pmatrix}$ ainsi
$B=72A_3+19A_4+48A_5=72\mathcal{B}_1+19\mathcal{B}_2+48\mathcal{B}_3$. Le
vecteur $
\begin{pmatrix}
  0\\0\\72\\19\\48
\end{pmatrix}$ est la solution de base réalisable associé à la base
réalisable $\mathcal{B}$.


 

\end{exem}


On dit que les variables $x_{i_k}$ sont les \uline{variables de base} (ou  en
base), les autres sont les \uline{variables hors-base}.


\begin{remarque}
  Lorsque l'on transforme un programme linéaire canonique en un programme linéaire standard en
  ajoutant $k$ variables d'écart, les $k$ dernières colonnes forment
  une base réalisable.
\end{remarque}


\begin{thm}[admis]\label{th1}
  Soit $X$ une solution réalisable de $(P)$. Notons $i_1$, $i_2$, $\cdots$, $i_r$ les indices des coordonnées strictement positives.

  On a l'équivalence suivante:

  $X$ est un sommet de $(P)$ si et seulement si le système $(A_{i_1};A_{i_2};\cdots;A_{i_r})$ est libre.
\end{thm}

% \begin{preuve}
  
% \end{preuve}


En conséquence toute solution de base réalisable de $(P)$ est un
sommet de $(P)$. Réciproquement, si $X$ est un sommet, en
sélectionnant les colonnes de $A$ dont les indices sont ceux des
coordonnées strictement positives de X, on
obtient une famille libre. On obtient une base de $\R^p$ en complétant
si besoin cette famille.

\begin{bclogo}[arrondi=0.1,logo=\bcdanger]{Attention}
  En général pour un sommet donné, il existe plusieurs bases réalisables correspondantes.
\end{bclogo}


\begin{remarque}
  Comme il y a au plus ${n \choose m}$ bases de $m$ vecteurs
  sélectionnés parmi les $n$ colonnes de $A$, le programme linéaire $(P)$ possède au plus
  ${n \choose m}$ sommets.
\end{remarque}


\begin{remarque}
  Dans le cas où un sommet à $r<p$ coordonnées strictement positive,
  il y a autant de base réalisable associée à ce sommet que de façon
  de compléter la famille libre en une base. On dit qu'il s'agit d'un
  \uline{sommet dégénéré}. Géométriquement, il s'agit de situation où
  au moins $r+1$ hyperplan ``se coupent'' au même point.


  \begin{center}
    \includegraphics[width=10cm]{images/degenerescence}
  \end{center}

  
\end{remarque}
\begin{thm}
  Soit $(P)$ un programme linéaire. Si $(P)$
  possède des solutions optimales, alors au moins un des sommets de
  $(P)$ est solution optimale.\label{th2}
\end{thm}

\begin{preuve}
  Soit $X$ une solution optimale de $(P)$. On suppose que $X$ possède
  $r$ coordonnées strictement positives. On peut supposer, sans perte
  de généralité, qu'il s'agit des $r$ premières coordonnées.

  Si $X$ est un sommet, il n'y a rien à démontrer. Sinon, d'après le
  théorème \ref{th1}, $A_1;A_2;\cdots;A_r$ forment une famille liée de
  $\R^p$. C'est à dire qu'il existe un vecteur non nul $W\in\R^n$
  dont les $n-r$ coordonnées sont nulles (comme $X$) tel que $AW=0$. Nous allons
  construire une solution optimale de $(P)$ qui possède au plus $r-1$
  coordonnées strictement positives. (C'est à dire un zéro de plus que
  $X$).


  \paragraph{Construction de solutions réalisables}

 
  Soit $t$ un réel strictement positif et cherchons notre vecteur sous
  la forme $X+tW$. Posons $X_t=X+tW$ et $X_{-t}=X-tW$. Par
  construction ces deux vecteurs sont des solutions
  du système ($A(X_t)=A(X_{-t})=A(X)=B$). Cherchons les valeurs de
  $t$ qui rendent réalisable ses solutions. Pour cela, il est
  nécessaire que:

  \begin{equation*}
    \forall i\in \llbracket 1;r\rrbracket\quad
    \sys{x_i+tw_i\geq0}{x_i-tw_i\geq 0}\Leftrightarrow \sys{\textrm{si
      } w_i>0 \quad t\leq \frac{x_i}{w_i}}{\textrm{si
      } w_i<0 \quad t\leq -\frac{x_i}{w_i}}\Leftrightarrow t\leq\tau=\min_{
      i\in \llbracket 1;r\rrbracket}\left(\frac{x_i}{w_i}, w_i>0\quad;\quad -\frac{x_i}{w_i}, w_i<0\right)      
  \end{equation*}


  Lorsque $t=\tau$, non seulement $X_t$ et $X_{-t}$ sont des solutions
  réalisables (coordonnées positives) mais de plus soit $X_t$ soit
  $X_{-t}$ a une coordonnées nulles supplémentaires.

  \paragraph{Montrons que  $X_t$ et $X_{-t}$ sont optimales.}

  Par définition de $X$, $\transp C X_t\leq \transp C X$ et $\transp C
  X_{-t}\leq \transp C X$
  
  \begin{equation*}
    \sys{\transp C X_t =\transp C X+t\transp C W\leq \transp C
      X}{\transp C X_{-t} =\transp C X-t\transp C W\leq \transp C
      X}\Leftrightarrow \sys{t\transp C W\leq 0}{-t\transp C W\leq 0}
  \end{equation*}

  Or $t>0$, donc $\transp C W=0$ et $\transp C X_t =\transp C
  X_{-t}=\transp C X$.


  Ainsi $X_t$ et $X_{-t}$ sont optimales.


  \paragraph{Conclusion}


  Nous avons donc une nouvelle solution optimale qui possède au plus
  $k-1$ coordonnées non nulles, d'après le
  théorème \ref{th1} soit il s'agit d'un sommet soit nous avons une
  famille d'au plus $k-1$ vecteurs liés parmi les colonnes de $A$. On
  peut reprendre le raisonnement précédent et diminuer encore le
  nombre de colonnes. Or $\rg(A)=m$, donc nécessairement après un
  certain nombre d'itération, on obtiendra
  une famille libre et donc un sommet de $(P)$
\end{preuve}


\begin{remarque}
  Une conséquence du théorème \ref{th2} est la suivante: tout programme linéaire
  réalisable possède au moins un sommet.
\end{remarque}



\begin{Def}
  Deux bases réalisables sont dites voisines si elles ne diffèrent que
  d'un seul vecteur.
\end{Def}

Une base $\mathcal{B}$ peut être \uline{améliorable} s'il existe une
meilleure base voisine. Plus précisément, $\mathcal{B}$ est meilleur
que $\mathcal{B'}$ si $\transp C X_{\mathcal{B}}\leq\transp C
X_{\mathcal{B'}}$. Elle peut être \uline{optimale} dans le cas où il n'y a pas
de meilleur base. Il se peut également que le programme linéaire $(P)$ ne possède pas
de solution optimale.


\begin{Def}
  Mettre la base $\mathcal{B}$ en position test c'est construire le programme linéaire
  $(P')=(P_{\mathcal{B}})$ équivalent à $P$ où:
  \begin{enumerate}[i)]
  \item les colonnes de $A'$ correspondant aux colonnes de $B$ forment
    (à l'ordre près) la matrice $I_p$;
  \item les coefficients de $C'$ dont les indices sont ceux des
    colonne de $B$ sont nulles.
  \end{enumerate}
\end{Def}


Lorsque le système standard provient d'un système canonique. La
matrice identité se lit sur les dernières colonne de $A$.




\begin{pro} Considérons $(P_{\mathcal{B}})$ un programme linéaire
  standard à second membre positif où la base
  réalisable $\mathcal{B}$ est en position test. Notons
  $X_{\mathcal{B}}$ la solution de base réalisable associée.
  \begin{enumerate}
  \item Si tous les coefficients de $C$ sont négatifs ou nul alors
    $X_{\mathcal{B}}$ est une solution optimale.
  \item S'il existe $c_i$ un coefficient de $C$ strictement positif
    alors en posant $X_{\mathcal{B}}(t)= X_{\mathcal{B}}+t\mathbf{e_i}$ on a pour $t>0$, $m(X_{\mathcal{B}}(t))=tc_i$
    \begin{enumerate}
    \item Si $\forall k\in\llbracket 1;p \rrbracket$, $a_{ki}<0$ alors
      $(P)$ n'a pas de solution optimale.
    \item S'il existe des indices $k$ tels que $a_{ki}>0$ alors
      $\mathcal{B}$ est améliorable en
      remplaçant dans le $\mathcal{B}$ le vecteur d'indice $i$ de $A$ par le
      vecteur d'indice $j=\min_{k\in\llbracket
        1;p\rrbracket}(\frac{b_k}{a_{ik}}|a_{ik}>0)$ de $A$.
    \end{enumerate}
  \end{enumerate}

\end{pro}


\begin{preuve}
  \begin{enumerate}
  \item Quel que soit $X\geq 0$, $\transp C X\leq 0$, comme $\transp C
    X_{\mathcal{B}}=0$, $\forall X\geq 0,\quad \transp C X\leq
    X_{\mathcal{B}}$.
  \item $m(X_{\mathcal{B}}(t))=tc_i$ car d'une part $\transp C
    X_{\mathcal{B}}=0$ et d'autre part $\transp C \mathbf{e_i}=c_i$.
    \begin{enumerate}
    \item Observons que
      $AX_{\mathcal{B}}(t)=A(X_{\mathcal{B}}+t\mathbf{e_i})=B+tA_i\leq
      B$. Donc $X_{\mathcal{B}}(t)$ est une solution réalisable
      quelque soit $t>0$ et $m(X_{\mathcal{B}}(t))=tc_i$ est aussi
      grand que l'on veut: $(P)$ n'a pas de solution optimale.
    \item Sans sacrifier à la généralité, on peut supposer que $A$ est
      de la forme


      \definecolor{parme}{rgb}{0.81,0.62,0.91}
\definecolor{bleunuit}{rgb}{0,0,0.99}
\tikzset{ image/.style={above,draw,thin,minimum height=2em,fill=parme!15}}
      \begin{center}
        \begin{tikzpicture}
          \tikzset{bloc/.style={fill=parme,rounded corners,fill
              opacity=0.2}};
          \tikzset{fleche/.style={thick,bleunuit,>=latex}}
        
          \matrix(M)[matrix of math nodes, left delimiter=(,right
          delimiter=),column sep =2mm, row sep=2mm] {
            \vphantom{O}\quad&a_{1i}&& \quad&\vphantom{O}\quad&&&&&&& \vphantom{O} \\
            \quad&a_{2i}&& \quad&&&&&&&& \\
            \quad&\vdots&& \quad&&&\quad&\quad&&\quad&& \\
            \quad&\vdots&& \quad&&&&&&&& \\
            \quad&a_{pi}&& \quad&\quad&&&&&&&\quad \\};
          \draw[bloc] (M-1-1.north west) rectangle (M-5-4.south east);
          \draw[bloc] (M-1-5.north west) rectangle (M-5-12.south east);
          \node[scale=1.2,above=0mm] at (M-3-8) {$I_p$};
          \draw[fleche,<-](M-1-2.north)--++(0,1)node[image] {$A_i$};
        \end{tikzpicture}
      \end{center}


     
Cherchons un vecteur $X(t)$ de la forme $\begin{pmatrix}
  0\\
  \vdots\\
  t\\
  \vdots\\
  0\\
  x_1\\
  x_2\\
  \vdots\\
  x_p  
\end{pmatrix}$ où $t$ est à la $i$\up{ème} position.


Pour que la solution soit réalisable il est nécessaire que

\begin{equation}
  \forall k \in\llbracket
        1;p\rrbracket\quad a_{ki}t+x_k=b_k \Leftrightarrow
        b_k-a_{ki}t=x_k\geq 0
        \label{cond}
\end{equation}

Si $a_{ki}<0$ alors $b_k-a_{ki}t\geq0$ quelque soit $t$.

Donc \eqref{cond} est équivalent à
\begin{equation*}
   \forall k \in\llbracket
        1;p\rrbracket\quad \text{tel que } a_{ki}>0 \quad, t\leq \frac{b_k}{a_{ki}}
\end{equation*}


La plus grande solution que l'on peut obtenir est atteinte pour $t=\min_{
      i\in \llbracket 1;r\rrbracket,
      a_{ki}>0}\left(\frac{b_k}{a_{ki}}\right)$.


    Soit $j$ un indice pour lequel $\min_{
      i\in \llbracket 1;r\rrbracket,
      a_{ki}>0}\left(\frac{b_k}{a_{ki}}\right)$ est atteint.


    On montre en utilisant le théorème \ref{th1} qu'avec de tels choix
    de $i$ et de $j$, $\mathcal{B}$ est améliorable et que cette
    solution réalisable est un sommet du programme linéaire.
    \end{enumerate}

  \end{enumerate}
\end{preuve}
\pagebreak
\section{Quelques prolongements}
\label{sec:quelq-prol}


\subsection{Cas où la base de départ n'est pas évidente}
\label{sec:cas-ou-la}



\subsection{Dualité}
\label{sec:dualite}


\begin{exem}
  Une entreprise fabrique deux types produits, ces produits requièrent
  3 matières premières. On note $c_1$ et $c_2$ les bénéfices obtenus
  par la vente des produits fabriqués et l'on suppose que les
  quantités $x_1$ et $x_2$ vérifient le système des contraintes
  suivant:

  \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        x_1+3x_2\leq 18\\
        x_1+x_2\leq 8\\
        2x_1+x_2\leq 14\\
             \end{array}\right.
  \end{equation*}

  Que l'on peut écrire:

   \begin{equation}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        AX\leq B\\
        m=\transp C\cdot X \quad\textrm{(max)}
      \end{array}\right.\tag{P}\label{eq:6}
  \end{equation}

  La résolution de ce problème par la méthode du simplexe donne:

\begin{equation*}
\left[\begin{matrix}1 & 3 & 1 & 0 & 0 & 18\\1 & 1 & 0 & 1 & 0 & 8\\2 & 1 & 0 & 0 & 1 & 14\\1 & 1 & 0 & 0 & 0 & 0\end{matrix}\right]\end{equation*}
\begin{equation*}
\begin{array}[h]{l}
L_{1}\leftarrow L_{1}{- \frac{1}{2}}L_{3}\\L_{2}\leftarrow L_{2}{- \frac{1}{2}}L_{3}\\L_{3}\leftarrow {\frac{1}{2}}L_{3}\\L_{4}\leftarrow L_{4}{- \frac{1}{2}}L_{3}\\\end{array}\left[\begin{matrix}0 & \frac{5}{2} & 1 & 0 & - \frac{1}{2} & 11\\0 & \frac{1}{2} & 0 & 1 & - \frac{1}{2} & 1\\1 & \frac{1}{2} & 0 & 0 & \frac{1}{2} & 7\\0 & \frac{1}{2} & 0 & 0 & - \frac{1}{2} & -7\end{matrix}\right]\end{equation*}
\begin{equation*}
\begin{array}[h]{l}
L_{1}\leftarrow L_{1}{-5}L_{2}\\L_{2}\leftarrow {2}L_{2}\\L_{3}\leftarrow L_{3}{-1}L_{2}\\L_{4}\leftarrow L_{4}{-1}L_{2}\\\end{array}\left[\begin{matrix}0 & 0 & 1 & -5 & 2 & 6\\0 & 1 & 0 & 2 & -1 & 2\\1 & 0 & 0 & -1 & 1 & 6\\0 & 0 & 0 & -1 & 0 & -8\end{matrix}\right]\end{equation*}

En donnant les valeurs: 6 à la variable $x_1$, 2 à la variable $x_2$, on obtient un maximum de 8.
Dans cette situation, les variables d'écart $e_2$ et $e_3$ sont
nulles, donc les deux dernières contraintes sont serrées et la
première contrainte est lâche, il reste 6 unités de la première
matière première. L'entreprise peut être intéressée par la revente de
ces 6 unités de matière première inutilisée.




Notons $y_1$, $y_2$ et $y_3$ le prix de revente des matières
premières. Si $18y_1+8y_2+14y_3>8$ produire n'est pas
rentable.


Notons $\transp{}Y=(y_1;y_2;y_3)$. Le bénéfice pour l'entreprise
s'écrit donc:

\begin{equation*}
  \transp{}CX+\transp{}Y(B-AX)= \transp{}CX+\transp{}Y(B-AX)=\transp{}YB+(\transp{}C-\transp{}YA)X
\end{equation*}


Lorsque $(\transp{}C-\transp{}YA)X$ est négatif, la production des
quantités $X$ n'est pas rentable.





Plaçons nous maintenant du point de vue d'une entreprise qui
rachète les matières premières, si elle souhaite dissuader
l'entreprise de produire elle doit proposer un système de prix
$\transp{}Y=(y_1;y_2;y_3)$ tel que  $\transp{}C-\transp{}YA\leq 0$
tout en minimisant son coût $\transp{}Y B$.

Donc l'entreprise qui rachète les matières premières doit résoudre le
programme linéaire suivant:

 \begin{equation}
    \left\lbrace
      \begin{array}[h]{l}
        Y\geq 0\\
        \transp{}AY\geq C\\
        m=\transp B\cdot Y \quad\textrm{(min)}
      \end{array}\right. \tag{D}\label{eq:5}
  \end{equation}

  Où de manière explicite:


    \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        y_1+y_2+2y_3\geq 1\\
        3y_1+y_2+y_3\geq 2\\
        18y_1+8y_2+14y_3=m\quad\textrm{(min)}
                    \end{array}\right.
  \end{equation*}

  Le programme linéaire \eqref{eq:5} est appelé programme linéaire dual
  de \eqref{eq:6}.

\end{exem}



\begin{exem}
  La cimenterie. Le dual du système \eqref{contraintes} est:

   \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        a\geq 0, b\geq 0, c\geq 0\\
        4a+b+2c\geq 30\\
        3a+b+3c\geq 40\\
        
        72a+19b+48c \quad\textrm{(min)}
      \end{array}\right.
  \end{equation*}

On peut résoudre ce problème qui a pour solution $a=0$, $b=10$ et
$c=10$. À l'optimum $72a+19b+48c$ vaut 670 comme pour le primal.


  

  Si l'on augmente la capacité de stockage d'une unité, le primal
  devient:


  \begin{equation}
\label{contraintes_a}
   \systt{4x+3y\leq 72}{x+y\leq 20}{2x+3y\leq 48}{0\leq x\text{
       et }0\leq y}
\end{equation}


Sa résolution donne:

\begin{center}
  \includegraphics[width=10cm]{images/xcas}
\end{center}

une augmentation du bénéfice de 10 unités. Ce qui correspond à la
solution pour la deuxième variable du dual. Les solutions du dual sont
les élasticités du bénéfice par rapport à chacune des contraintes.
\end{exem}
\begin{Def}
  À tout programme linéaire canonique de type \textbf{max}  de la forme:
  \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        AX\leq B\\
        m=\transp C\cdot X \quad\textrm{(max)}
      \end{array}\right.
  \end{equation*}

  on associe le  programme linéaire canonique de type \textbf{min}  de la forme:
  \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        Y\geq 0\\
        \transp{}AY\geq C\\
        m=\transp B\cdot Y \quad\textrm{(min)}
      \end{array}\right.
  \end{equation*}

  appelé \underline{programme linéaire dual}.
\end{Def}

\begin{pro}
  Le dual du dual est le primal.
\end{pro}

\begin{thm}
  Soit $X$ et $Y$ des solutions réalisables respectivement du primal
  et du dual. On a l'inégalité suivante:
\label{thm:inedual}
  \begin{equation*}
    \transp{}C X\leq \transp{}B Y.
  \end{equation*}
\end{thm}

\begin{preuve}
  \begin{eqnarray*}
    AX\leq B\\
    \transp{}Y AX\leq \transp{}YB\\
    \transp{}Y AX\leq \transp{}B Y\\
     \transp{}(\transp{}AY)X\leq \transp{}B Y\\
     \transp{}CX\leq \transp{}Y AX\leq \transp{}B Y\\
  \end{eqnarray*}
\end{preuve}
\begin{cor}
   Soit $X$ et $Y$ des solutions réalisables respectivement du primal
   et du dual. Si $\transp{}C X\geq \transp{}B Y$ alors

   \begin{equation*}
    \transp{}C X = \transp{}B Y
  \end{equation*}

  et $X$ et $Y$ sont respectivement des solutions réalisables
  optimales du primal et du dual.
\end{cor}
\begin{preuve}
  Soit $X'$ une autre solution réalisable de $(P)$, d'après le
  théorème \eqref{thm:inedual}:
  \begin{equation*}
    \transp{}C X'\leq \transp{}B Y= \transp{}C X 
  \end{equation*}

  Donc $X$ est optimale. En reprenant le même raisonnement  avec une
  solution réalisable quelconque  du dual on obtient le résultat annoncé.
\end{preuve}

\begin{thm}[Théorème fondamental de la programmation linéaire]
  Soit un programme linéaire. Alors
  \begin{enumerate}
  \item s'il admet une solution réalisable alors il admet une solution
    réalisable de base;

    
  \item s'il admet une solution optimale, alors il admet une solution optimale de base;
  \item s'il admet une solution réalisable et si la valeur de la  fonction objectif est bornée, alors il admet une solution optimale
    de base.
  \end{enumerate}
\end{thm}

\begin{thm}
  \begin{enumerate}
  \item Si le programme linéaire primal et le PL dual admettent des
    solutions réalisable, alors ils admettent tous les deux une
    solution optimale et les fonctions objectifs sont égales à
    l'optimum.
  \item Si le programme linéaire primal n'est pas borné alors le PL
    dual n'admet pas de solution de base réalisable.
  \end{enumerate}

\end{thm}


\begin{exem}
   \begin{equation}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        2x_1-x_2\leq 2\\
        3x_1-2x_2\leq 6\\
        z=2x_1+x_2 \quad\textrm{(max)}
      \end{array}\right.
    \label{ex:primal}
  \end{equation}


  \begin{prof}

    Le PL \eqref{ex:primal} n'est pas borné, on peut faire croître
    $x_2$ indéfiniment.

    Examinons le dual:

  \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        Y\geq 0\\
        2y_1+3y_2\geq 2\\
        -y_1-2y_2\geq 1\\
z=2y_1+6y_2        \quad\textrm{(min)}
      \end{array}\right.
    \label{ex:dual}
  \end{equation*}


  La seconde contrainte ne peut être satisfaite.
  \end{prof}
\end{exem}






\begin{exem}
   \begin{equation*}
    \left\lbrace
      \begin{array}[h]{l}
        X\geq 0\\
        x_1+2x_2\geq 12\\
        2x_1+x_2\geq 9\\
z=-3x_1-4x_2        \quad\textrm{(min)}
      \end{array}\right.
    \label{ex:dualsimplex}
  \end{equation*}



  \begin{prof}
    
  \end{prof}
\end{exem}
















\end{document}

\vspace{10mm}
\titledframe{\includegraphics[width=15mm]{images/icon-outil}}{%
  \begin{minipage}[h]{1\linewidth}
    \vspace{10mm}



On peut utiliser \texttt{Xcas} pour résoudre ce système. Pour mémoire,
si $A$ est une matrice carrée de taille $n\times n$ et $B$ une matrice
colonne de taille $n\times 1$, lorsque le système $A\cdot X=B$ a une
solution unique alors $X=A^{-1}\cdot B$.

Dans \texttt{Xcas}, \texttt{A:=S[0..2,[1,3,4,5]]} permet de
sélectionner pour les  3 premières lignes de la matrice S, les
colonnes 2, 4, 5 et 6  pour former la matrice carrée A.
\vspace{2mm}
\end{minipage}}

 Cherchons $X^t$ sous la forme
      \begin{pmatrix}
        x_1=0\\
        \vdots\\
        x_i=t\\
        \vdots\\
        x_n=0\\
       
      \end{pmatrix}