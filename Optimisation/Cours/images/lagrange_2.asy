//settings.outformat="png";
//settings.render=0;


import graph3;
import contour;
import grid3;
import palette;
settings.tex="pdflatex";
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
size(12cm,IgnoreAspect);
defaultpen(fontsize(14pt));

settings.outformat="pdf";
settings.prc=true;
settings.render = 0;
settings.antialias = 1;

currentprojection=orthographic(-10,-10,8);
limits((0,0,0),(5,10,10));

real f(pair z) {return z.x^(1/3)*z.y^(2/3);}

real[] lignesniveaux={2,4,6};

surface s=surface(f,(0,0),(5,10),10,Spline);



  real x(real t){return t;};
  real y(real t){return 9-2*t;};
real z(real t){return f((t,y(t)));};



path3 p;

p=graph(x,y,z,0,4.5,operator ..);
pen pe=blue;

draw(s, lightblue+opacity(0.8),meshpen=pe+thick());



grid3(new grid3routines [] {XYXgrid, ZXgrid(10), ZYgrid(5)},
      Step=2,
      step=1,
      pGrid=new pen[] {red, blue, black},
      pgrid=new pen[] {0.5red, lightgray, lightgray});
xaxis3(Label("$x$",position=MidPoint,align=SE), 
       Bounds(Min,Min), 
       OutTicks());
yaxis3(Label("$y$",position=MidPoint,align=SW), 
       Bounds(Min,Min), 
       OutTicks(Step=2));
zaxis3(Bounds(Max,Both));
zaxis3(Label("$z=\sqrt[3]{xy^2}$",position=EndPoint,align=N+W),
       XYEquals(0,10),
       InTicks(beginlabel=false,endlabel=false,Label(align=Y))
       );
draw(lift(f,contour(f,(0,0),(5,10),lignesniveaux)),0.5bp+red);
real[] lignesniveaux={8*2^(1/3)/3,8};
draw(lift(f,contour(f,(0,0),(5,10),lignesniveaux)),1bp+red);
draw(p, 1.5bp+green);

triple m[];
m[0]=(4/3,16/3,8*2^(1/3)/3);
m[1]=(1.5,6,3*2^(1/3));
dot((4/3,16/3,8*2^(1/3)/3),orange);
dot(m[1],red);
draw(m[0]--m[1],deepmagenta,Arrow3);

