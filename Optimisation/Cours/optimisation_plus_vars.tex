%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[A4,Cours,11pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\titre{Optimisation des fonctions de plusieurs variables}
\auteur{V. Ledda}
\reference{Chapitre 3}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 MISEG \\
Optimisation}

\motsclefs{différentielle,lagrangien}
\usepackage{fancybox}
\newcommand{\titledframe}[2]{%
\boxput*(0,1){#1}%
{\ovalbox{#2}}}
\usepackage{pgf,tikz}
\usetikzlibrary{matrix}
\usetikzlibrary{positioning}
\usepackage{draftwatermark}
\usepackage{algo,enumitem,ulem}
\newcommand{\ps}{(\cdot|\cdot )}

\usepackage[tikz]{bclogo}
\usepackage{tabularx}
\etudiant=1
\usepackage[backend=biber,   style=science,    sortlocale=fr_FR, natbib=true,    url=false,     doi=true,    eprint=false]{biblatex}

\addbibresource{../../biblio.bib}



\begin{document}

\maketitle
\tableofcontents
\newpage


Le cas de d'un optimum libre d'une  fonction de deux variables et le
cas d'un optimum d'une fonction de deux variables soumis à une contrainte a été précédemment.  Dans ce chapitre on généralise les
résultats obtenus au chapitre 2. La généralisation se fait sans problème au prix d'un alourdissement des notations et du remplacement
des conditions de non nullité de la différentielle par des conditions sur le rang de la matrice jacobienne des contraintes.

\section{Rappels sur les fonctions différentiables  de $n$ variables}
\label{sec:rappels-sur-les}

On travaille avec la norme euclidienne sur $\R^n$: $||X||=\sqrt{x_1^2+x_2^2+\cdots+x_n^2}$.

\begin{Def}
Soit $f$ une fonction réelle de $n$ variables réelles définie
sur l'ensemble $\mathcal{D}$ à valeur réelle et $A$ un point  de $\R^n$.

\noindent On dit que $f$ tend vers  $\ell$ lorsque $X$ tend vers $A$,
($X$  restant dans $\mathcal{D}$) si : $$\underset{X\in \mathcal{D}}{\underset{d(X,A)\rightarrow 0}{\lim }}\vert f(X)-\ell\vert=0 
\text{ ou } \underset{X\in \mathcal{D}}{\underset{\Vert X-A\Vert \rightarrow 0}{\lim }}\vert f(X)-\ell\vert=0$$

\end{Def}
\begin{Def}
Soit $f$ une fonction  définie sur un sous-ensemble $\mathcal{D}$ de $\R^n$ et $A\in \mathcal{D}$.

\noindent On dit que $f$ \textbf{est continue en $A$} si $f(X)$ tend vers $f(A)$ quand $X$ tend vers $A$,  soit encore $\underset{X\in \mathcal{D}}{\underset{X\rightarrow A}{\lim }}f(X)=f(A)$.\\

\noindent On dit que $f$ est \textbf{continue sur $\mathcal{D}$} si $f$ est continue en tout point de $\mathcal{D}$.
\end{Def}

\begin{Def}
Soit $f$ une fonction définie sur un ensemble $\mathcal{D}$ de $\R^n$ et $A\in\mathcal{D}$.\\

\noindent $f$ est\textbf{ différentiable en $A$} si il existe une
forme linéaire sur $\R^n$, $L$,  telle que:

\[\forall H\in \R^n \text{ tel que } A+H\in\mathcal{D}, \;
  f(A+H)=f(A)+L\cdot H+\Vert H \Vert \varepsilon (H)
\text{ où }\displaystyle \lim_{\Vert H \Vert\to 0} \varepsilon (H)=0\]

\noindent La forme linéaire $L$ est appelée \textbf{``différentielle de $f$ en $A$''}. On la note $\D{f}_{A}$.\\

\noindent On a donc $f(A+H)=f(A)+\D{f}_{A}(H)+\Vert H \Vert \varepsilon (H)$ où $\displaystyle \lim_{\Vert H \Vert\to 0} \varepsilon (H)=0$.
\end{Def}


\begin{Def} Soit $f$ une fonction de $\mathcal{D}\subset\R^n$ dans
  $\R$.

  Pour tout indice $i\in\llbracket 1;n\rrbracket$, on note, sous
  réserve d'existence, $\frac{\partial f}{\partial x_i}(A)=\lim_{h\to
    0}\frac{f(a_1,a_2,\cdots,a_i+h,\cdots, a_n)-f(A)}{h}$. Ce nombre
  est la \underline{différentielle partielle de $f$ en $A$} suivant la variable
  $x_i$. Lorsque les $n$ dérivées partielles existent en $A$ ($f$ est
  alors \underline{dérivable} en $A$), on
  appelle \underline{gradient} de $f$ en $A$ le vecteur colonne formé par les $n$
  dérivées partielles en $A$.

  \begin{equation*}
    \grad{f}_A=
    \begin{pmatrix}
      \frac{\partial f}{\partial x_1}(A)\\
      \frac{\partial f}{\partial x_2}(A)\\
      \vdots\\
      \frac{\partial f}{\partial x_n}(A)
    \end{pmatrix}
  \end{equation*}
  
\end{Def}

\begin{pro}
  Soit $f$ une fonction différentiable en
  $A\in\mathcal{D}\subset\R^n$. \fonction{\D{f}_A}{\R^n}{\R}{H}{\grad{f}_A\cdot\transp{}H}
\end{pro}

\begin{Def}
  On dit qu'une fonction $f$ est de classe $\mathcal{C}^1$ sur
  $\mathcal{D}\subset \R^n$ si l'application
  \fonction{\D{f}}{\mathcal{D}}{\R}{X}{\D{f}_X} est continue. 
\end{Def}
\begin{thm}
  Soit $f$ une application de $\mathcal{D}\subset\R^n$ dans $\R$ et  $A\in\mathcal{D}$ . Si
  toutes les dérivées partielles de $f$ en existent  et sont continues en $A$ alors $f$ est de classe $\mathcal{C}^1$ en $A$. 
\end{thm}

\begin{Def}
Soit $f$ une fonction définie sur un
ensemble  $\mathcal{D} $ de $\mathbb{R}^{n}$.\\
Si $f$ admet des dérivées partielles
secondes continues en un point $A$ de $\mathcal{D} $, on dit que $f$ est deux
fois contin\^{u}ment différentiable en $A$ ou encore que $f$ est de
classe $C^{2}$ en $A$.\\
\end{Def}


\begin{thm}[Théorème de Schwartz]

  Soit $f$ une fonction de classe $\mathcal{C}^2$ en $A$.

  \begin{equation*}
    \label{eq:4}
    \forall (i;j)\in\llbracket 1;n\rrbracket^2;\quad i\neq j;\quad
    \dfrac{\partial ^{2}f}{\partial x_i\partial x_j}\left( A\right) =\dfrac{\partial ^{2}f}{\partial x_j\partial x_i}\left( X\right)
  \end{equation*}
  
\end{thm}


\begin{Def}
  Soit $f$ une fonction de classe $\mathcal{C}^2$ en $A$. La matrice carrée
  de $\mathcal{M}_n$  de terme général:

  \begin{equation*}
    \dfrac{\partial ^{2}f}{\partial x_i\partial x_j}\left( A\right)
  \end{equation*}

  est appelée la matrice hessienne de $f$ en $A$. On la note $\mathbf{D}_f^2(A)$.  D'après le théorème
  de Schwartz cette matrice est symétrique.
\end{Def}


   \definecolor{parme}{rgb}{0.81,0.62,0.91}
   \definecolor{bleunuit}{rgb}{0,0,0.99}
   \definecolor{bleuciel}{rgb}{0.466,0.709,0.996}
\newcommand{\aaa}{\hphantom{\hspace{1.1cm}}}
\begin{center}
  \begin{tikzpicture}[scale =0.6]
    \matrix(M)[matrix of math nodes, left delimiter=(, right
    delimiter=), nodes={minimum size=1.2cm,scale=1.1}] %row sep=0.1cm,
    {  \dfrac{\partial ^{2}f}{\partial x_1^2}\left( A\right) & \cdots & \quad &  \cdots & \dfrac{\partial ^{2}f}{\partial x_1\partial x_n}\left( A\right) \\
      \vdots&&&&\vdots\\
      \vdots&& \dfrac{\partial ^{2}f}{\partial x_i\partial x_j}\left( A\right)&&\vdots\\
      \vdots&&&&\vdots\\
      \dfrac{\partial ^{2}f}{\partial x_n\partial x_1}\left( A\right) & \cdots &  &  \cdots & \dfrac{\partial ^{2}f}{\partial x_n^2}\left( A\right) \\
    };

    \tikzset{fleche/.style={thick,bleunuit,>=latex}};
    \tikzset{image/.style={above,draw,thin,minimum
        height=2em,fill=bleuciel!15}};
    \tikzset{image2/.style={left,draw,thin,minimum
        width=2em,fill=bleuciel!15}};
    \tikzset{image3/.style={left,thin,minimum width=2em}};

    \draw[fleche,<-](M-3-1.west)--++(-1.5,0) node[image2]{$i$};
    \draw[fleche,<-](M-1-3.north)--++(0,1.5) node[image]{$j$} ;

    \draw (M-3-3.east)++(8,0) node {\large $=\mathbf{D}_f^2(A)$} ;

  \end{tikzpicture}
\end{center}


\begin{remarque}
  Les définitions et résultats précédents ont été donnés localement en
  $A$. Ils s'étendent de manière naturelle à un sous ensemble de
  $\mathcal{D}$.
\end{remarque}





\begin{pro}$\quad$
  
  \begin{itemize}
  \item Le caractère continue, $\mathcal{C}^1$, $\mathcal{C}^2$,
    etc. se conserve par somme, produit, quotient et composée.
  \item Toutes les fonctions polynômes et quotients de polynômes sont de
classe $C^{2}$ sur leur domaine de définition.
\end{itemize}

\end{pro}
\begin{thm}[Formule de Taylor à l'ordre $2$ pour une fonction de $n$
  variables]
  
Soit $f$ une fonction définie un ensemble $\mathcal{D}$ de $\R^n$ et
$A$ un point de $\mathcal{D}$. Si $f$ est de classe $C^{2}$ en $A$, alors 
 pour tout $H\in\R^n$, tel que $A+H\in \mathcal{D}$, on a :

 \[f(A+H)=f(A)+\D{f}_A\cdot H + \dfrac{1}{2}\transp{}H\cdot
   \mathbf{D}_f^2(A)\cdot H+||H||^2\epsilon(H)\]

     
$\text{avec }\underset{H \rightarrow 0}{\lim }\varepsilon \left( H\right) =0$

\end{thm}



\section{Optimisation libre}
\label{sec:optimisation-libre}

\subsection{Condition nécessaire}
\label{sec:condition-necessaire}


\begin{thm}
  Soit $f$ une fonction différentiable sur ouvert
  $\mathcal{D}\subset\R^n$ et $A$ un élément de $\mathcal{D}$.

Si  $f$ admet un extremum en $A$ alors $\D{f}_A=0$. On dit que $A$ est
un point critique de $f$. 
\end{thm}

\subsection{Conditions du second ordre}
\label{sec:conditions-du-second-1}


\begin{thm}\label{thm:cso}
  Soit $f$ une fonction de classe $\mathcal{C}^2$ sur un ouvert
  $\mathcal{D}\subset\R^n$ et $A\in \mathcal{D}$ un point critique de
  $f$.

  \begin{enumerate}
  \item Si $f$ admet un maximum en $A$ alors $\forall H\in \R^n,\quad
    \transp{}H\cdot \mathrm{D}_f^2(A)\cdot H\leq 0$
  \item Si $\forall H\in \R^n\backslash\{0\},\quad  \transp{}H\cdot
    \mathrm{D}_f^2(A)\cdot H< 0$ alors $f$ admet un maximum local
    strict en $A$.
  \end{enumerate}
\end{thm}

\begin{remarque}
  Le théorème peut s'écrire dans les mêmes conditions pour un minimum
  local en renversant les inégalités.
\end{remarque}


Le théorème \eqref{thm:cso} indique que l'étude du  point critique $A$
de $f$ peut se ramener à l'étude de la forme
quadratique:
\fonction{Q}{R^n}{\R}{H}{\transp{}H\cdot \mathrm{D}_f^2(A)\cdot H}




L'algèbre linéaire permet de répondre de la manière suivante:

\begin{itemize}
\item Si les valeurs propres de $ \mathrm{D}_f^2(A)$ sont toutes
  strictement positives alors $f$ admet un minimum strict en $A$
\item Si les valeurs propres de $ \mathrm{D}_f^2(A)$ sont toutes
  strictement négatives alors $f$ admet un maximum strict en $A$
\item Si $ \mathrm{D}_f^2(A)$ admet deux valeurs propres de signe
  opposés alors $f$ admet un point selle en $A$.
\item Si les valeurs propres sont toutes de même signe mais qu'au
  moins une est nulle alors dans le cas où il existe $H_0$ tel que $H_0\cdot
\mathrm{D}_f^2(A)\cdot H_0$ les conditions du second ordre ne
permettent pas de conclure.
\end{itemize}


\begin{Def}
  Soit $M=(a_{i,j})$ un éléments de $\mathcal{M}_n$ et
  $i\in\llbracket1;n\rrbracket$. On note $M_i$ la matrice carrée formée
  des $i$ premières lignes de $M$ et des $i$ premières colonnes de
  $M$. On appelle \underline{mineurs principaux} de $M$ la suite des
  $n$ déterminants suivants:
  \begin{equation*}
    |M_1|, \;|M_2|, \cdots ,\; |M_n|
  \end{equation*}
\end{Def}


On peut alors reformuler le théorème \eqref{thm:cso} de la manière
suivante:


\begin{pro}
   Soit $f$ une fonction de classe $\mathcal{C}^2$ sur un ouvert
  $\mathcal{D}\subset\R^n$ et $A\in \mathcal{D}$ un point critique de
  $f$. On note $m_1$, $m_2$, $\cdots$, $m_n$ les mineurs principaux de $\mathrm{D}_f^2(A)$.

  \begin{enumerate}
  \item
    \begin{enumerate}
    \item Si $f$ admet un minimum en $A$ alors  $\forall
      i\in\llbracket1;n\rrbracket,\quad  m_i\geq 0$
    \item Si $f$ admet un maximum en $A$ alors  $m_1\leq 0,\, m_2\geq 0,\,
      \cdots,\, (-1)^nm_n\geq 0$.
    \end{enumerate}

  \item
    \begin{enumerate}
    \item Si $m_1< 0,\, m_2> 0,\,
      \cdots,\, (-1)^nm_n> 0$ alors $f$ admet un maximum local strict
      en $A$.
    \item Si $\forall
      i\in\llbracket1;n\rrbracket,\quad  m_i> 0$ alors $f$ admet un minimum local strict en $A$.
    \end{enumerate}

  \end{enumerate}
\end{pro}



\section{Optimisation d'une fonction sous contrainte d'égalités}
\label{sec:optim-dune-fonct}

\subsection{Présentation du problème}
\label{sec:pres-du-probl}


Dans cette partie on s'intéresse aux programmes de la forme:



\begin{equation*}
  \sys{\text{ext}\quad f(X)}{\text{s.c.}\quad \forall
    i\in\ii{k}\quad g_i(X)=0}
\end{equation*}
où les fonctions $f, g_1,g_2,\cdots, g_k$ sont de classe
$\mathcal{C}^1$ sur un ouvert $\mathcal{D}\subset\R^n$.

On pose
\fonction{L}{\R^{n+k}}{\R}{(x_1;x_2;\cdots;x_n;\lambda_1;\lambda_2;\cdots;\lambda_k)}{f(x_1;x_2;\cdots;x_k)+\sum_{i=1}^k\lambda_ig(x_1;x_2;\cdots;x_k)=f(X)+\transp{}\Lambda\cdot
  G(X)}

où $\Lambda=\transp{}(\lambda_1,\lambda_2,\cdots\lambda_k)$ et \fonction{G}{\R^n}{\R^k}{X}{
  \begin{pmatrix}
    g_1(X)\\g_2(X)\\\vdots\\g_k(X)
  \end{pmatrix}}

La fonction $L$ est appelée le lagrangien de $f$ et de $G$. Notons
$\partial G(A)$
la jacobienne de $G$ en un point $A$ de $\mathcal{D}$.

\begin{equation*}
  \partial G(A)=
  \begin{pmatrix}
    \frac{\partial g_1}{\partial x_1}(A)&\cdots&&\cdots&\frac{\partial
      g_1}{\partial x_n}(A)\\
    \vdots&&&&\vdots\\
    &&\frac{\partial g_i}{\partial x_j}(A)&&\\
     \vdots&&&&\vdots\\ \frac{\partial g_k}{\partial x_1}(A)&\cdots&&\cdots&\frac{\partial
      g_k}{\partial x_n}(A)\\
  \end{pmatrix}
\end{equation*}




\subsection{Condition du premier ordre}
\label{sec:condition-du-premier}


\begin{thm}[Théorème de Lagrange]
  Soit $f$ une fonction de classe $\mathcal{C}^1$ sur un ouvert
  $\mathcal{D}$ de $\R^n$. Soit $G$ une application de classe
  $\mathcal{C}^1$ de $\mathcal{D}$ vers $\R^k$. On pose \fonction{L}{\R^{n}\times\R^k}{\R}{(X;\Lambda)}{f(X)+\transp{}\Lambda\cdot
    G(X)}
  
On suppose qu'il existe un point $A\in \mathcal{D}$ tel que:

\begin{itemize}
\item $G(A)=0$
\item $\rg(\partial G(A))=k$
\end{itemize}



Si $f$ admet un extremum sous la contrainte $G(X)=0$ en $A$ alors il
existe un élément $\Lambda_A\in\R^k$ tel que $\D{L}_{(A;\Lambda_A)}=0$

\end{thm}



\subsection{Conditions du second ordre}
\label{sec:conditions-du-second-2}


\begin{thm}\label{thm:csosc}

  Soit $f$ une fonction de classe $\mathcal{C}^2$ sur un ouvert
  $\mathcal{D}$ de $\R^n$. Soit $G$ une application de classe
  $\mathcal{C}^2$ de $\mathcal{D}$ vers $\R^k$. On pose \fonction{L}{\R^{n}\times\R^k}{\R}{(X;\Lambda)}{f(X)+\transp{}\Lambda\cdot
  G(X)}
  On suppose qu'il existe un point $A\in \mathcal{D}$ et un élément
  $\Lambda_A$ dans $\R^k$ tels que:
  \begin{itemize}
  \item $\rg(\partial G(A))=k$
  \item $\D{L}_{(A;\Lambda_A)}=0$
  \end{itemize}
  Dans ces conditions, en notant $L_A:X\longmapsto L(X,\Lambda_A)$  on a les résultats suivants:
  \begin{enumerate}
  \item Si $f$ admet un maximum local en $A$ sous la contrainte
    $G(X)=0$ alors
    \begin{equation}
      \label{eq:7}
      \forall H\in \R^k;\quad  \partial G(A)\cdot H=0\;\Rightarrow\;
      \transp{}H\cdot \mathrm{D}^2_{L_A}\cdot H \leq 0
    \end{equation}
  \item Si 
\begin{equation}
      \label{eq:7}
      \forall H\in \R^k\backslash\{0\};\quad  \partial G(A)\cdot H=0\;\Rightarrow\;
      \transp{}H\cdot \mathrm{D}^2_{L_A}\cdot H < 0
    \end{equation}
    alors $f$ admet un maximum local strict  en $A$ sous la contrainte
    $G(X)=0$ 
  \end{enumerate}
\end{thm}

En notant $L_{\Lambda}:X\longmapsto L(X,\Lambda)$  posons \fonction{B}{\R^k\times \mathcal{D}}{R^{n+k}}{(\Lambda;X)}{(G(X);\D{L}_{\Lambda}(X))=
  \transp{}\left(g_1(X), g_2(X),\cdots
    g_k(X)\;,\;f'_{x_1}(X)+\transp{}\Lambda
    G'_{x_1}(X),\;\cdots ,\;f'_{x_n}(X)+\transp{}\Lambda G'_{x_n}(X)\right)}

Sous les hypothèse du théorème \eqref{thm:csosc}, l'application $B$
est de classe $\mathcal{C}^1$ et sa matrice jacobienne en $(A;\Lambda_A)$ à la forme
suivante:

\begin{center}
  \begin{tikzpicture}[thick,scale=0.6]

    \tikzset{bloc/.style={fill =bleuciel,rounded corners,fill opacity=0.2}}
    \matrix(M)[matrix of math nodes, left delimiter=(, right
    delimiter=), nodes={minimum size=0.8cm}] %row sep=0.1cm,
    { 0&\cdots&0 &{g_1}'_{x_1}\qquad\hspace{12mm}&&\cdots&&\hspace{2cm}\qquad{g_1}'_{x_n}\\
      \vdots&&\vdots &\vdots\qquad\hspace{12mm}&&\partial G(A)&&\hspace{2cm}\qquad\vdots\\
      0&\cdots&0& {g_k}'_{x_1}\qquad\hspace{12mm}&&\cdots&&\hspace{2cm}\qquad{g_k}'_{x_n}\\
      {g_1}'_{x_1}&\cdots&{g_k}'_{x_1} & f''_{x_1^2}(A)+\transp{}\Lambda     G''_{x_1^2}(A)&&\cdots&&f''_{x_1x_n}(A)+\transp{}\Lambda   G''_{x_1x_n}(A)\\
 && & \vdots&&&&\vdots\\
 \vdots& \transp{}\partial G(A)&\vdots & && \mathrm{D}^2_f(A)+\transp{}\Lambda\mathrm{D}^2_g(A)&&\\

       && & \vdots&&&&\vdots\\
      {g_1}'_{x_n}&\cdots& {g_k}'_{x_n}& f''_{x_1^2}(A)+\transp{}\Lambda  G''_{x_1^2}(A)&&\cdots&&f''_{x_1x_n}(A)+\transp{}\Lambda  G''_{x_1x_n}(A)\\
    };

    \draw[bloc](M-4-4.north west) rectangle (M-1-8.north east);
    \draw[bloc](M-4-1.north west) rectangle (M-8-3.south east);
    \draw[bloc](M-4-4.north west) rectangle (M-8-8.south east);
    \draw (M-4-1.west)++(-3,0) node {\large $H_b(L,\partial G)=$} ;
      \end{tikzpicture}
\end{center}

Cette matrice est la hessienne bordée de $L$ en $A$.

\begin{pro}
  Sous les hypothèses du théorème \eqref{thm:csosc}, en notant $m_i$
  les $n-k$ derniers mineurs principaux de la  hessienne bordée de $L$ en $A$, pour
  $i\in\ii[k+1]{n}$.


  \begin{enumerate}

    \item Conditions suffisantes
    \begin{enumerate}
    \item Si $\forall   i\in\ii[k+1]{n},\quad  (-1)^km_i>0$ alors $f$ admet un maximum local strict en $A$.
    \item Si$\forall   i\in\ii[k+1]{n},\quad  (-1)^im_i> 0$  alors
      $f$ admet un maximum local strict   en $A$ sous la contrainte $g(X)=0$.
  
    \end{enumerate}
  \item Conditions nécessaires
    \begin{enumerate}
    \item Si $f$ admet un minimum local en $A$ sous la contrainte
    $G(X)=0$  alors  $\forall   i\in\ii[k+1]{n},\quad  (-1)^km_i\geq
    0$ (les $n-k$ derniers mineurs sont du même signe que $(-1)^k$.)
    \item Si $f$ admet un maximum local en $A$ sous la contrainte
    $G(X)=0$ alors $\forall   i\in\ii[k+1]{n},\quad  (-1)^im_i\geq
    0$ (les $n-k$ derniers mineurs sont de signes alternés et le
    dernier est du signe de $(-1)^{n}$)
    \end{enumerate}

  

  \end{enumerate}
\end{pro}



\begin{remarque}
  \begin{equation*}
    m_{k+1}=|H_{2k+1}|,\,  m_{k+2}=|H_{2k+2}|,\, \cdots ,\, m_n=|H_{n+k}|
  \end{equation*}

  Par exemple pour $k=3$ et $m=5$, il y a deux mineurs principaux à calculer.
  \begin{center}
    \begin{tikzpicture}[thick,scale=0.3]
      \tikzset{bloc/.style={fill =bleuciel,rounded
          corners,fill opacity=0.2}}
      \tikzset{bloc2/.style={draw=red}} \matrix(M)[matrix of math
      nodes, nodes={minimum size=0.6cm}] %row sep=0.1cm,
      {
        % 1 %%% 2 %%% 3 %%% 4 %%% 5 %%% 6 %%% 7 %%% 8
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %1
        \quad& 0 &\quad&\quad&\quad&\partial G&\quad&\quad\\ %2
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %3
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %4
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %5
        \quad&\transp{}\partial
        G&\quad&\quad&\quad&\mathrm{D}^2_f+\transp{}\Lambda\mathrm{D}^2_g
        &\quad&\quad\\ %6
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %7
        \quad&\quad&\quad&\quad&\quad&\quad&\quad&\quad\\ %8
      };

      \draw[bloc](M-1-4.north west) rectangle (M-3-8.south east);
      \draw[bloc](M-4-4.north west) rectangle (M-8-8.south east);
      \draw[bloc](M-4-1.north west) rectangle (M-8-3.south east);

      \draw[bloc2](M-1-1.north west) rectangle (M-7-7.south east);
      \draw[bloc2](M-1-1.north west) rectangle (M-8-8.south east);

    \end{tikzpicture}
  \end{center}

\end{remarque}


\section{Optimisation d'une fonction sous contrainte d'inégalités}
\label{sec:optim-dune-fonct}

Dans cette partie on s'intéresse aux programmes de la forme:



\begin{equation}
  \sys{\text{ext}\quad f(X)}{\text{s.c.}\quad \forall
    i\in\ii{k}\quad g_i(X)\geq 0}\label{eq:3}
\end{equation}
où les fonctions $f, g_1,g_2,\cdots, g_k$ sont de classe
$\mathcal{C}^1$ sur un ouvert $\mathcal{D}\subset\R^n$.





\subsection{Vocabulaire}
\label{sec:vocabulaire}
L'ensemble des valeurs admissibles est
l'ensemble des points de $\R^n$ où $G(X)\geq 0$. On cherchera donc
des solutions dans l'ensemble $E=\mathcal{D}\cap \{(x;y)\in\R^n|G(X)\geq 0\}$.


\begin{Def}
  La contrainte $g(X)\geq 0$ est dite \underline{serrée}, ou
  effective ou encore active au point $A\in \mathcal{D}$ si $g(A)=0$. Lorsque
  $g(A)>0$, on dit que la contrainte est
  \underline{relâchée} en $A$. Enfin, la contrainte $G(X)\geq 0$ est dite
  qualifiée en $A$ si:
  \begin{itemize}
  \item soit $G(A)>0$
  \item soit $G(A)=0$ et $\rg(\partial G)(A)=k$
  \end{itemize}
\end{Def}

\subsection{Conditions nécessaires pour un optimum local}
\label{sec:cond-necess-pour}

\begin{thm}[Kuhn et Tucker]
\label{thm:kt}
  On considère le programme \eqref{eq:3}.
  Si $f$ admet un maximum local en $A$ sur $E$ et si la contrainte
  est qualifiée en $A$ alors il existe un élément $\Lambda_A\in\R^k$ tel que:

  \begin{enumerate}
  \item $\Lambda_A\geq 0$
  \item $\forall i\in\ii{k},\quad (\Lambda_A)_i\times g_i(A)=0$ dites relations d'exclusion
  \item $\D{f}_A+\Lambda_A\D{G}_A=0$
  \end{enumerate}
\end{thm}




\begin{remarque}
  Lorsque l'on cherche un minimum local la condition (1.) devient:
  $\Lambda_A\leq 0$.
\end{remarque}

\begin{remarque}
  \begin{enumerate}
  \item Si $(\Lambda_A)_i \neq 0$ la relation d'exclusion implique que
    $g_i(A)=0$ donc la contrainte est serrée en $A$.
  \item Si $g_i(A)>0$ alors $(\Lambda_A)_i = 0$ donc dans
    $\Lambda_A\D{G}_A$ seules interviennent les contraintes serrées.
  \end{enumerate}

\end{remarque}

\subsection{Condition suffisante d'optimalité globale}
\label{sec:cond-suff-dopt}


Dans cette partie on suppose que les fonctions  $g_i$ ($i\in\ii{k}$) sont concaves sur $\mathcal{D}$ un
ouvert convexe de $\R^n$.

Si de plus $f$ est convexe (resp. concave) sur $\mathcal{D}$ les
conditions du théorème \eqref{thm:kt} sont suffisantes pour montrer
que $f$ admet un minimum (resp. un maximum) sous la contrainte
$G(X)\geq 0$.

\begin{pro}
  Soit le programme 
\begin{equation}\label{eq:5}
  \sys{\min{f(X)}}{\text{s.c.}\quad G(X)\geq 0}
\end{equation}

où $f$ est une fonction convexe et de classe $\mathcal{C}^1$ sur un ouvert convexe, $\mathcal{D}$,
de $\R^n$ et $g$ est une fonction concave et de classe $\mathcal{C}^1$
sur $\mathcal{D}$.
S'il existe un point $A$ de $\mathcal{D}$ et un vecteur $\Lambda_A\in\R^k$ tel que

\begin{itemize}
\item la contrainte soit qualifiée en $A$
\item $\Lambda_A\leq 0$
\item $\forall i\in\ii{k},\quad (\Lambda_A)_i\times g_i(A)=0$
  \item $\D{f}_A+\Lambda_A\cdot \D{G}_A=0$
  \end{itemize}

  alors $f$ admet un minimum global sous la contrainte $g(X)\geq 0$
  en $A$.
\end{pro}

\begin{pro}
  Soit le programme \label{pro:tkconcave}
\begin{equation}\label{eq:6}
  \sys{\max{f(x;y)}}{\text{s.c.}\quad g(x;y)\geq 0}
\end{equation}

où $f$ est une fonction concave et de classe $\mathcal{C}^1$ sur un ouvert convexe, $\mathcal{D}$,
de $\R^n$ et les fonctions $g_i$ sont des fonctions concaves et de classe $\mathcal{C}^1$
sur $\mathcal{D}$.
S'il existe un point $A$ de $\mathcal{D}$ et un vecteur $\Lambda_A\in\R^k$ tel que

\begin{itemize}
\item la contrainte soit qualifiée en $A$
\item $\Lambda_A\geq 0$
\item $\forall i\in\ii{k},\quad (\Lambda_A)_i\times g_i(A)=0$
  \item $\D{f}_A+\Lambda_A\D{g}_A=0$
  \end{itemize}

  alors $f$ admet un maximum global sous la contrainte $g(x;y)\geq 0$
  en $A$.
\end{pro}
\begin{exem}
  \begin{equation*}
  \syst{\max{\;\ln(x)+\ln(y+5)}}{\text{s.c.}\quad x+y\leq 4}{\quad\quad
    y\geq 0}
\end{equation*}

\begin{prof}
  

  Posons $f(x;y)=\ln(x)+\ln(y+5)$, $f$ est de classe $\mathcal{C}^2$
  sur $\mathcal{D}=]0;+\infty[\times]-5;+\infty[$.

  \begin{itemize}
  \item $\grad{f}=\transp{}
    \begin{pmatrix}
      \frac{1}{x}&\frac{1}{y+5}
    \end{pmatrix}$
  \item $\mathbf{D}^2_f=
    \begin{pmatrix}
      -\frac{1}{x^2}&0\\0& -\frac{1}{(y+5)^2}
    \end{pmatrix}$
  \end{itemize}

  Donc $f$ est concave sur $\mathcal{D}$.

  
  Posons $g(x;y)=4-x-y$, $g$ est affine donc $g$ est concave et de
  classe $\mathcal{C}^1$ sur
  $\mathcal{D}$. La différentielle $\D{g}$ est constante et non nulle, donc les éléments
  tels que $g(X)\geq 0$ sont qualifiés. Le théorème \eqref{thm:kt}
  s'applique, comme $\grad{f}$ n'est
  jamais nul sur $\mathcal{D}$ alors $\lambda\neq 0$.
  
    La seconde contrainte est également qualifiée sur $\R^2$ et on a
    également $\lambda\neq 0$.

Cherchons donc
  $\lambda_1>0$ et $\lambda_2>0$ tels que $\grad{L}=\overrightarrow{0}$ où


  
  $L(x;y;\lambda_1;\lambda_2)=f(x;y)+\lambda_1 g(x;y)+\lambda_2y$.


  \begin{eqnarray*}
    \systt{\frac{1}{x}-\lambda_1=0}{\frac{1}{y+5}-\lambda_1-\lambda_2=0}{x+y=4}{y=0}\Leftrightarrow
    \systt{x=4}{y=0}{\lambda_1=\frac{1}{4}}{\lambda_2=\frac{1}{20}}
  \end{eqnarray*}

  D'après la proposition \eqref{pro:tkconcave}, $f$ admet un maximum
  global en $(4; 0)$ sous la contrainte $\sys{x+y\leq 4}{y\geq0}$.



  \begin{center}
    \includegraphics[width=8cm]{images/exemple_1}
  \end{center}
\end{prof}
\end{exem}


\begin{exem}
  \begin{equation*}
    \sys{\max\left\lbrace-(x-4)^2-(y-4)^2\right\rbrace}{\text{s.c.} \quad
      \sys{x+y\leq 4}{x+3y\leq 9}}
  \end{equation*}


  \begin{prof} On pose $g_1(x,y)=4-x-y$ et $g_2(x,y)=9-x-3y$.
    $f$ est concave sur $\R^2$ et les $g_i$ sont des fonctions concave
    également.

    $f, g_1, g_2$ sont de classe $\mathcal{C}^2$ sur $\R^2$ 

\paragraph{Condition de qualification}


\begin{equation*}
  \partial G=
  \begin{pmatrix}
    -1&-1\\
    -1&-3
  \end{pmatrix}
\end{equation*}

$\rg(\partial G)=2$, donc tous les points de $\R^2$ sont qualifiés.

\paragraph{Conditions d'exclusion}

\begin{equation*}
  \lambda_1(4-x-y)=0 \text{ et }\lambda_2(9-x-3y)=0
\end{equation*}


  \begin{description}
  \item[$\lambda_1=0$ et $\lambda_2=0$ ]
    Dans ce cas $\D{f}(A)=0$, et $x=y=4$
  \item[$\lambda_1=0$ et $\lambda_2>0$]

    Dans ce cas $x+3y=9$, et $\D{f}(A)+\lambda_2(-\D{x}-3\D{y})=0$

    \begin{eqnarray*}
      \syst{x+3y=9}{-2(x-4)-\lambda_2 =0}{-2(y-4)-3\lambda_2 =0}\Leftrightarrow\syst{x=3,3}{y=1,9}{\lambda_2=1,4}
    \end{eqnarray*}

    Cette solution est exclue car elle ne vérifie par $G(A)\geq 0$
  \item[$\lambda_1>0$ et $\lambda_2=0$]
    Dans ce cas $x+y=4$, et $\D{f}(A)+\lambda_1(-\D{x}-\D{y})=0$
  \begin{eqnarray*}
      \syst{x+y=4}{-2(x-4)-\lambda_1 =0}{-2(y-4)-\lambda_1 =0}\Leftrightarrow\syst{x=2}{y=2}{\lambda_2=4}
    \end{eqnarray*}
    
  \item[$\lambda_1>0$ et $\lambda_2>0$]
    \begin{eqnarray*}
    \sys{x+y=4}{x+3y=9}\Leftrightarrow \sys{x=\frac{3}{2}}{y=\frac{5}{2}}
    \end{eqnarray*}

    et
    $\D{f}(A)+\lambda_1(-\D{x}-\D{y})+\lambda_2(-\D{x}-3\D{y})=0$

    \begin{eqnarray*}
    \sys{5-\lambda_1-\lambda_2=0}{3-\lambda_1-\lambda_2
      =0}\Leftrightarrow \sys{\lambda_1=6}{\lambda_2=-1     \text{ exclu car }\lambda_2>0} 
    \end{eqnarray*}

  \end{description}
  Obtient 2 points candidats:
  \begin{equation*}
    A(4;4)\quad B(2;2)
  \end{equation*}

  L'un correspond au maximum 0 en $A(4;4)$ et le second à un minimum sous
  contrainte -8 en $B(2;2)$.



  \end{prof}
  
\end{exem}
\subsection{Conditions du second ordre}

\label{sec:conditions-du-second}
\begin{thm}
  On considère le programme \eqref{eq:3}.
  Si  la contrainte est qualifiée en $A$ et qu'il existe un élément $\Lambda_A\in\R^k$ tel que:

  \begin{enumerate}
  \item $\Lambda_A\geq 0$
  \item $\forall i\in\ii{k},\quad (\Lambda_A)_i\times g_i(A)=0$ dites relations d'exclusion
  \item $\D{f}_A+\Lambda_A\D{G}_A=0$
  \end{enumerate}

  On suppose en outre, sans perte de généralité,  qu'uniquement les $s$ premières contraintes sont
  serrées ($s\in\ii{k}$).

  On désigne par $H$ la matrice hessienne du lagrangien bordée par les dérivées premières des seules
  contraintes saturées évaluée en $(A;\Lambda_A)$. Cette matrice est symétrique d’ordre $s+n$. Et
  l'on note $m_i$ les $n-s$ derniers mineurs principaux de $H$, $i\in\ii[s+1]{n}$.


  \begin{enumerate}
  \item Conditions suffisantes \begin{enumerate}
    \item Si $(-1)^im_i>0$ pour $i\in\ii[s+1]{n}$ alors $f$ admet un
      maximum local sous la contrainte $G(X)\geq 0$.
    \item Si $(-1)^sm_i>0$ pour $i\in\ii[s+1]{n}$ alors $f$ admet un
      minimum local sous la contrainte $G(X)\geq 0$.
    \end{enumerate}
  \item Conditions nécessaires
\begin{enumerate}
    \item Si $f$ admet un
      maximum local sous la contrainte $G(X)\geq 0$ alors
      $(-1)^im_i\geq 0$ pour $i\in\ii[s+1]{n}$.
    \item Si $f$ admet un
      minimum local sous la contrainte $G(X)\geq 0$ alors
      $(-1)^sm_i\geq 0$ pour $i\in\ii[s+1]{n}$.
    \end{enumerate}
    
  \end{enumerate}

  
\end{thm}


\begin{exem}
  

Les points $A(\frac{1}{3};\frac{1}{3})$ et $B(0;-1)$ sont-ils des
solutions du programme ci-dessous:

\begin{equation*}
  \sys{\text{ext}\quad x^2+2y^2}{\text{s.c.}\quad \sys{x^2+y^2\leq
      1}{1+x-2y\geq 0}}
\end{equation*}

\end{exem}

\begin{exem}
  Résoudre le programme suivant:

  \begin{equation*}
  \sys{\text{ext}\quad xy+zt}{\text{s.c.}\quad \sys{x^2+y^2\leq
      2}{z^2+t^2\geq 8}}
\end{equation*}
\end{exem}
% \begin{exem}
%   $\min\limits_{\mathbf{x}\in\R^n} -\sum \ln(\alpha_i+x_i)$ tels que
%   $\mathbf{x}\in(\R^{+})^n$ et $\sum x_i=1$.
% \end{exem}

\end{document}

