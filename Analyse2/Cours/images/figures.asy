settings.outformat="png";
settings.render=0;
import graph3;
import palette;
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
//unitsize(0.0125cm);
//import fontsize;
defaultpen(fontsize(14pt));

import animation;

settings.tex="pdflatex";


animation A=animation("chapitre7_fig_", global=false);

size(7.5cm,0);

currentprojection=orthographic(4,2,3);
currentlight=(5,-2,4);

real f(pair z) {return z.y^2-z.x^2;}

draw(surface(f,(-1,-1),(1,1),nx=32,Spline),
                   lightblue+opacity(0.8),blue);
A.add();
erase();


real f(pair z) {return z.y^2+z.x^2;}

draw(surface(f,(-1,-1),(1,1),nx=32,Spline),
                   lightblue+opacity(0.8),blue);
A.add();
erase();


real f(pair z) {return -z.y^2-z.x^2;}

draw(surface(f,(-1,-1),(1,1),nx=32,Spline),
                   lightblue+opacity(0.8),blue);
A.add();
erase();

currentprojection=orthographic(4,2,6);
real f(pair z) {return z.y^3+z.x^3-3*z.x*z.y;}

draw(surface(f,(-1,-1),(1,1),nx=32,Spline),
                   lightblue+opacity(0.8),blue);
A.add();
