size(12cm,0,false);
import graph3;
import contour;
import palette;
import graph;
settings.tex="pdflatex";
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
import three;
texpreamble("\usepackage{icomma}");
import animation;

animation A=animation("utilite_fig_", global=false);

real f(pair z) {return z.x^0.3*z.y^0.4;}
real g(pair z) {return 0;}

currentprojection=orthographic(-2.5,-5,1);

draw(surface(f,(0,0),(5,10),10,Spline),palegray,bp+rgb(0.2,0.5,0.7));

scale(true);

xaxis3(Label("$x$",MidPoint),OutTicks());
yaxis3(Label("$y$",MidPoint),OutTicks(Step=2));
zaxis3(Label("$U$",Relative(1),align=2E),Bounds(Min,Max),OutTicks);

real[] datumz={2,2.5,3,3.5};

Label[] L=sequence(new Label(int i) {
    return YZ()*(Label(format("$U=%g$",datumz[i]),
                       align=2currentprojection.vector()-1.5Z,Relative(1)));
  },datumz.length);

pen fontsize=bp+fontsize(10);
draw(L,lift(f,contour(f,(0,0),(5,10),datumz)),
     palette(datumz,Gradient(fontsize+red,fontsize+black)));
A.add();
erase();


currentprojection=orthographic(0,-5,0);

draw(surface(f,(0,0),(5,10),10,Spline),lightgrey,bp+rgb(0.2,0.5,0.7),nolight);

scale(true);

xaxis3(Label("$x$",MidPoint),OutTicks());
yaxis3(Label("$y$",MidPoint));
zaxis3(Label("$U$",Relative(1),align=2E),Bounds(Min,Max));




//real f(real x) { return 4^0.3*x^0.4; }
//path s = graph(f, 0, 2, operator..);
//Draw the graph and the axes.
//draw(path3(s,plane=YZplane),red);


A.add();
erase();

currentprojection=orthographic(-2.5,-5,1);

draw(surface(f,(0,0),(5,10),10,Spline),palegray+opacity(0.7),bp+rgb(0.2,0.5,0.7));
draw(surface(g,(0,0),(5,10),10,Spline),paleblue);

real x(real t) {return t;}
real y(real t) {return -2*t+8;}
real z(real t) {return t^0.3*(-2*t+8)^0.4;}
real zz(real t) {return 0;}

path3 p=graph(x,y,z,0,4,n=13);

draw(p,3bp+red);
path3 p=graph(x,y,zz,0,4,n=13);


draw(p,2bp+blue);

scale(true);

xaxis3(Label("$x$",MidPoint),OutTicks());
yaxis3(Label("$y$",MidPoint),OutTicks(Step=2));
zaxis3(Label("$U$",Relative(1),align=2E),Bounds(Min,Max));



A.add();
erase();
