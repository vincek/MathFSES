%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
% rubber: module biblatex
\documentclass[Cours,12pt,sol]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\usepackage[tikz]{bclogo}
\titre{Tests statistiques}
\auteur{V. Ledda}
\reference{L3}
\composante{Faculté des Sciences Économiques et Sociales - Université de Lille\\
Licence 3 / Économie - Gestion \\
Techniques Mathématiques de l'Économiste - Estimation et tests}

\motsclefs{tests}
\usepackage{array}
\usepackage{slashbox}
\usepackage{frcursive}
\newcommand{\s}{\text{\textcursive{s}}}

\usepackage{draftwatermark}
%\SetWatermarkScale{1.5}
%\SetWatermarkAngle{305}
%\SetWatermarkLightness{0.99}
%\SetWatermarkLightness{0.8}
%\SetWatermarkText{$\quad$Université de Lille}

%\usepackage{pgf,tikz}
%\usepackage{mathrsfs}
%\usetikzlibrary{arrows}
%\usepackage{slashbox}
\usepackage{pgf,tikz}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\etudiant=0
\nosolout=1
\affichepreuve=0

\usepackage[backend=biber,   style=science,    sortlocale=fr_FR, natbib=true,    url=false,     doi=true,    eprint=false]{biblatex}

\addbibresource{../../biblio.bib}


%\usepackage[xcas]{pro-tablor}
%\usepackage{pro-tablor}

\setcounter{tocdepth}{2}
\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Introduction}
\label{sec:introduction}

\subsection{Le vocabulaire des tests}
\label{sec:le-vocabulaire-des}


Soit $\vec{X}$ un échantillon dont la loi parente dépend d'un
paramètre $\theta\in\Theta\in\R$.


\begin{Def}
  Toute assertion relative au paramètre $\theta\in\Theta$ est dite être
  une \underline{hypothèse statistique}. L'hypothèse testée est
  qualifiée d'hypothèse nulle, notée H$_0:\,\theta\in\Theta_0$. On
  note $\Theta_1$ le complémentaire de $\Theta_0$ dans $\Theta$, et
  l'on appelle hypothèse alternative à H$_0$, l'hypothèse, notée
  H$_1$: $\theta\in\Theta_1$.
\end{Def}


Si $\Theta_0$ (resp. $\Theta_1$) l'hypothèse est dite simple, sinon
elle est qualifiée de composée.

Un test statistique est une procédure de décision dont l'objectif est
d'accepter H$_0$ ou de rejeter  H$_0$.


Il y a deux risques d'erreur:
\begin{itemize}
\item rejeter H$_0$ à tort, c'est le risque de première espèce;
\item accepter H$_0$ à tort, c'est le risque de seconde espèce.
\end{itemize}


On note $\alpha$ la probabilité de rejeter à tort H$_0$ et $\beta$ la
probabilité d'accepter à tort H$_0$.


Notons $\mathcal{X}\subset\R^n$ l'ensemble des résultats
possibles. Réaliser un test de H$_0$ contre H$_1$, c'est partitionner $\mathcal{X}$ en deux
régions:
\begin{itemize}
\item une région d'acceptation $A$, si $\vec{x}\in A$ alors on accepte
  H$_0$;
\item  une région de rejet (ou région critique) $R$, si $\vec{x}\in R$ alors on rejette
  H$_0$.
\end{itemize}



Enfin on appelle puissance d'un test la quantité $1-\beta$.

\begin{bclogo}[logo=\bcdanger,arrondi=0.1]{Attention}
  $\alpha$ et $\beta$ dépendent de $\theta$. 
\end{bclogo}

\begin{Def}
  La fonction $\pi:\theta
  \longmapsto 1-\beta(\theta)$ est la \underline{fonction puissance} du test.  
\end{Def}

\begin{Def}
  On appelle seuil de signification (ou niveau) d'un test le maximum
  de $\pi(\theta)$ lorsque $\theta$ décrit $\Theta_0$.
  \begin{equation*}
    \alpha = \max_{\theta \in \Theta_0}\p(\text{rejeter à tort H$_0$})=\max_{\theta \in \Theta_0}\pi(\theta)
  \end{equation*}
\end{Def}

\begin{exem}
  Si $X\rightsquigarrow \mathcal{N}(\theta;1)$ et que l'on teste:

  \begin{equation*}
    \syst{\text{H}_0:\, \theta\leq 0}{\text{contre}}{\text{H}_1: \theta>0}
  \end{equation*}

  prenons par exemple $A=]-\infty;\,0,5]$.

  \begin{eqnarray*}
    \pi(\theta)=1-\p(X\leq 0,5)=\p(X-\theta>0,5-\theta)=\p(X-\theta<\theta-0,5)=\phi(\theta-0,5)
  \end{eqnarray*}

  \begin{center}
    \includegraphics[width=10cm]{images/exemple_test_1}
  \end{center}


Et $\alpha=\pi(0)=1-\phi(0,5)\simeq 0,31$.
\end{exem}


\subsection{L'approche de Neyman-Pearson}
\label{sec:lapproche-de-neyman}


Elle consiste à fixer $\alpha$ et construire la région d'acceptation
(et de rejet) en fonction de $\alpha$.


Plus précisément:


\begin{enumerate}
\item Énoncer les hypothèses à tester
\item Établir la statistique, $T$, utilisée pour le test
\item Déterminer la région critique en fonction de $\alpha$ et de la
  statistique du test.
\item Appliquer la règle de décision:

  \begin{itemize}
  \item si $T(\vec{x}) \in R$ on rejette H$_0$ avec une probabilité
    inférieur à $\alpha$ de se tromper;
  \item sinon, on accepte H$_0$ au seuil de signification $\alpha$.
  \end{itemize}
\item Calculer la fonction puissance. (ou la puissance)
\end{enumerate}
\begin{exemple}
  Soit $(X_1;X_2;\cdots;X_n)$ un échantillon de taille $n$ de la loi
  normale de paramètre $\mathcal{N}(\mu;\sigma^2)$ où $\sigma$ est connue.

  On pose:
  \begin{itemize}
  \item H$_0:\,\mu=\theta_0$;
  \item H$_1:\,\mu=\theta_1$ (avec $\theta_1>\theta_0$).
  \end{itemize}


  On estime $\mu$ par la moyenne empirique
  $\overline{X}=\frac{1}{n}\sum_{k=1}^nX_k$. Soit $m$ un réel et
  considérons la règle de décision suivante:
  \begin{itemize}
  \item si $\overline{x}<\ell$ alors on accepte H$_0$;\footnote{ou plus  précisément on ne rejette pas H$_0$}
  \item sinon on rejette H$_0$.
  \end{itemize}


  \begin{center}
    \includegraphics[width=16cm]{images/seance1_fig1.png}
  \end{center}

  
  On s'aperçoit que:
  \begin{itemize}
  \item plus $\ell$ est grand, plus $\alpha$ faible mais plus la
    puissance du test diminue (l'erreur $\beta$ augmente).
  \item plus $n$ est grand, plus $\alpha$ diminue et $p$
    augmente. Plus l'échantillon est grand, plus il est facile de
    décider entre H$_0$ et H$_1$.
  \end{itemize}
 \begin{center}
    \includegraphics[width=16cm]{images/seance1_fig2.png}
  \end{center}

  Fixons $\alpha=0,05$. On
  sait que si H$_0$ est vraie
  $\p(\sqrt{n}\frac{\overline{X}-\theta_0}{\sigma}\leq
  1,64)=0,95$. Donc on en déduit la règle de décision suivante: si
  $\overline{x}>\theta_0+1,64\frac{\sigma}{\sqrt{n}}=\ell$ alors on rejette
  H$_0$.

  Dans ce cas on peut calculer la puissance du test: $1-\int_{-\infty}^m\frac{\sqrt{n}}{2\sqrt{\pi}\sigma}\exp(-n\frac{(x-\theta_1)^2}{2\sigma^2})\D{x}$.



  \begin{eqnarray*}
    \beta=\p(\overline{X}\leq \ell)\text{ où } \mu=\theta_1\\
    \beta=\p(\sqrt{n}\frac{\overline{X}-\theta_1}{\sigma}\leq
    \sqrt{n}\frac{\ell -\theta_1}{\sigma})=\phi(\sqrt{n}\frac{\ell
    -\theta_1}{\sigma})\\
    \pi=1-\phi(\sqrt{n}\frac{\ell
    -\theta_1}{\sigma})\\
  \end{eqnarray*}
\end{exemple}

La puissance du test dépend de $n$, plus $n$ est grand plus le test
est puissant. Si le test n'est pas assez puissant, il ne permet pas de
décider de manière satisfaisante. On considère qu'en général pour que
le test soit utilisable que sa puissance doit être égale à 0,8.

\subsection{Les différents types de tests}
\label{sec:les-differents-types}

Les tests statistiques permettent de répondre à des questions très
diverses par exemple:
\begin{enumerate}
\item test d'indépendance entre variable aléatoire;
  
\item test d'adéquation à une loi théorique;
\item test de conformité d'un paramètre d'un échantillon à une valeur
  donnée;
\item test de comparaison d'un paramètre issu de plusieurs échantillons.
\end{enumerate}


On distingue deux grandes familles de test:
\begin{itemize}
\item les tests paramétriques qui repose sur le ou les paramètres de la
  distribution connue ou supposée des données observées;
\item les tests non paramétriques où l'on ne s'intéresse pas à la
  distribution des données observées.
\end{itemize}

Il est a noté que lorsque les variables sont qualitatives seuls les
tests non paramétriques sont opérants.
\section{Tests paramétriques}
\label{sec:tests-parametriques}


\subsection{Test de conformité d'une moyenne à une valeur standard}
On se place dans le cas d'un échantillon de taille $n$ de loi parente
une loi normale de paramètre $(\mu;\sigma^2)$. La valeur de référence
étant notée $\mu_0$. On se donne un seuil $\alpha$ (en général
$\alpha=0,1$ ou $\alpha=0,95$. On construit la
fonction discriminante à partir de l'estimateur habituel de la
moyenne:

\begin{equation*}
  \xi=\sqrt{n}\frac{\overline{X}-\mu_0}{\widehat{S}}.
\end{equation*}


Avec $\overline{X}=\frac{1}{n}\sum_{i=1}^nX_i$ et
$\widehat{S}=\sqrt{\frac{1}{n-1}\sum_{i=1}^n(X_i-\overline{X})^2}$. Lorsque
$X_i$ suit $\mathcal{N}(\mu_0;\sigma^2)$, l'estimateur $\xi$ suit une loi de {\sc Student} à $n-1$ degrés de liberté.

\subsubsection{Test unilatéral}
\label{sec:test-unilateral}

\begin{equation*}
  \sys{\textrm{H}_0:~\mu=\mu_0}{\textrm{H}_1:~\mu>\mu_0}
\end{equation*}

On cherche le seuil $m$ tel que
\begin{equation*}
  \alpha=\p(\overline{X}\geq m|\mu=\mu_0)\\
\end{equation*}


On obtient:
\begin{equation*}
  \p(\xi\geq \sqrt{n}\frac{m-\mu_0}{\widehat{S}}|\mu=\mu_0)=\alpha\Leftrightarrow m=\mu_0+t_{\alpha}\frac{\widehat{S}}{\sqrt{n}}
\end{equation*}

où $t_{\alpha}$ est le quantile d'ordre $1-\alpha$ de la loi de {\sc Student}
à $n-1$ degrés de liberté.


On obtient la règle de décision suivante:
\begin{bclogo}[logo=\bcbook,arrondi=0.1]{Test unilatéral de conformité d'une moyenne}

  \begin{itemize}
  \item Si $\overline{X}\leq \mu_0+t_{\alpha}\frac{\widehat{S}}{\sqrt{n}}$ on
    accepte H$_0$;
  
  \item sinon on rejette H$_0$.
  \end{itemize} où $t_{\alpha}$ est le quantile d'ordre $1-\alpha$ de la loi de {\sc Student}
à $n-1$ degrés de liberté.
\end{bclogo}



\subsubsection{Test bilatéral}
\label{sec:test-bilateral}
\begin{equation*}
  \sys{\textrm{H}_0:~\mu=\mu_0}{\textrm{H}_1:~\mu\neq\mu_0}
\end{equation*}


On cherche les seuils $m_1$ et $m_2$ tel que
\begin{equation*}
  \alpha=\p(m_1\leq \overline{X}\leq m_2|\mu=\mu_0)\\
\end{equation*}

En utilisant la symétrie de la loi de {\sc Student}, on pose
$m_1=\mu_0-e$ et $m_2=\mu_0+e$ on obtient:
\begin{equation*}
\p(|\overline{X}-\mu_0|\leq e|\mu=\mu_0)=  \p(|\xi|\leq \sqrt{n}\cfrac{e}{\widehat{S}}\,|\,\mu=\mu_0)=\alpha\Leftrightarrow e=t_{\frac{1-\alpha}{2}}\frac{\widehat{S}}{\sqrt{n}}
\end{equation*}

On obtient la règle de décision suivante:
\begin{bclogo}[logo=\bcbook,arrondi=0.1]{Test bilatéral de conformité d'une moyenne}

  \begin{itemize}
  \item Si $\overline{X}\in\left[
      \mu_0-t_{\frac{1-\alpha}{2}}\frac{\widehat{S}}{\sqrt{n}};\mu_0+t_{\frac{1-\alpha}{2}}\frac{\widehat{S}}{\sqrt{n}}\right]$
    on ne rejette pas H$_0$;
  
  \item sinon on rejette H$_0$.
  \end{itemize} où $t_{\frac{1-\alpha}{2}}$ est le quantile d'ordre $\frac{1+\alpha}{2}$ de la loi de {\sc Student}
à $n-1$ degrés de liberté.
\end{bclogo}

\begin{remarque}
  \begin{enumerate}
  \item Si l'on connaît $\sigma$, on utilise comme statistique la
    moyenne empirique et les quantiles de la loi normale.
  \item On peut adapter facilement ce qui précède pour un test
    unilatéral à gauche.
  % \item Lorsque $\alpha\geq 0,05$, on peut approcher la loi de {\sc
  %     Student} par la loi normale. Par exemple pour $\alpha=0,05$.

  %   \begin{tabular}[h]{c|cc|c}
  %     &$t$&$z$&erreur relative\\\hline
  %     unilatéral&1,697261&1,644854&3\%\\
  %     bilatéral&2,042272&1,959964&4\%\\
  %   \end{tabular}

    
  \end{enumerate}
\end{remarque}


\begin{exemple}
On pèse 15 gélules à l'issue de la chaîne de fabrication, on obtient
les résultats suivants (en gramme):

\begin{center}
  \begin{tabular}[h]{*5c}
    1,41 &1,67& 1,51& 1,72& 1,28\\
    1,33& 1,45& 1,33& 1,76& 1,28\\
    1,31& 1,45 &1,29& 1,58& 1,23\\
  \end{tabular}
\end{center}


Ce résultat est-il conforme à la spécification de 1,50 grammes?
\end{exemple}

\subsection{Test de signification du degré de corrélation}
\label{sec:test-de-sign}

On se place dans la situation d'un échantillon $((X_i;Y_i))_{i\in
  \llbracket 1;n\rrbracket}$, on suppose que les variables sont
indépendantes et que les $X_i$ ont pour loi parente une loi normale et
les $Y_i$ également. On rappelle que le coefficient de corrélation
linéaire vaut:

\begin{equation*}
  r=\frac{\cov(X;Y)}{\sqrt{\var{(X)}\times\var(Y)}}
\end{equation*}

 On pose:
  \begin{itemize}
  \item H$_0:\,r=0$;
  \item H$_1:\,r\neq 0$.
  \end{itemize}


On peut montrer que $T=\cfrac{r\sqrt{n-2}}{\sqrt{1-r^2}}$ suit une loi
de {\sc Student} à $n-2$ degrés de liberté.

\begin{equation*}
  \p(|r|>m\,|\text{H}_0)\leq\alpha\Leftrightarrow
  \cfrac{m\sqrt{n-2}}{\sqrt{1-m^2}}>t_{\alpha}\Leftrightarrow m\geq \cfrac{t_{\alpha}}{\sqrt{n-2+t_{\alpha}^2}}
\end{equation*}

On obtient la règle de décision suivante:
\begin{bclogo}[logo=\bcbook,arrondi=0.1]{Test de signification du degré de corrélation}

  \begin{itemize}
  \item Si $r\leq \cfrac{t_{\alpha}}{\sqrt{n-2+t_{\alpha}^2}}$ on ne
      rejette pas H$_0$;
  
  \item sinon on rejette H$_0$.
  \end{itemize} où 
 $t_{\alpha}$ est le quantile d'ordre $1-\alpha$ de la loi de {\sc Student}
à $n-2$ degrés de liberté.
  
\end{bclogo}

\subsection{Test relatif à la variance d'une population normale}
\label{sec:test-relatif-la}

\subsubsection{Test bilatéral}
\label{sec:test-bilateral-1}

\begin{equation*}
  \sys{\textrm{H}_0:~\sigma^2=\sigma^2_0}{\textrm{H}_1:~\sigma^2\neq\sigma^2_0}
\end{equation*}



Lorsque $\textrm{H}_0$ est vraie, $\frac{nS^2}{\sigma^2_0}$ suit une
loi du $\chi^2$ à $n-1$ degrés de liberté. 

On cherche les seuils $m_1$ et $m_2$ tel que
\begin{equation*}
  \alpha=\p(m_1\leq \frac{nS^2}{\sigma^2_0} \leq m_2)\\
\end{equation*}


Ici la loi du $\chi^2$ n'est pas symétrique.


On obtient la règle de décision suivante:
\begin{bclogo}[logo=\bcbook,arrondi=0.1]{Test bilatéral de conformité d'une moyenne}

  \begin{itemize}
  \item Si $S^2\in\left[
      \frac{\sigma^2_0}{n}\chi^2_{n-1;\frac{\alpha}{2}};\frac{\sigma^2_0}{n}\chi^2_{n-1;1-\frac{\alpha}{2}}\right]$
    on ne rejette pas H$_0$;
  
  \item sinon on rejette H$_0$.
  \end{itemize} où $t_{\frac{1-\alpha}{2}}$ est le quantile d'ordre $\frac{1+\alpha}{2}$ de la loi de {\sc Student}
à $n-1$ degrés de liberté.
\end{bclogo}
\begin{exem}
   La durée d'écoulement d'un sablier suit une loi normale de paramètre
  $\mathcal{N}(m;\sigma^2)$. On le retourne 12 fois de suite et l'on
  obtient une durée moyenne de 4~minutes et un écart-type de 10
  secondes. Tester l'hypothèse $\sigma^2=0,025$.
  \label{sablier}
\end{exem}

\subsubsection{Cas où l'hypothèse alternative est simple}
\label{sec:cas-ou-lhypothese}

Dans ce cas, on peut facilement calculer la puissance du test.


\begin{exem}
  Reprenons l'exemple du sablier (\ref{sablier}) et reprendre le test dans le cas où
  $\textrm{H}_1:~\sigma^2=\frac{1}{36}$. Calculer alors la puissance
  de ce test.
\end{exem}


\subsection{Tests de comparaison de moyenne}

 Soit $\{x_1^k,x_2^k,\cdots,x_{n_k}^k\}$ un échantillon aléatoire de
 taille $n_k$ prélevé dans une population $U_k$ de moyenne $\mu_k$ et
 de variance $\sigma_k^2$. ($k=1,2$)

 On se donne un seuil $\alpha$, on pose $\Delta= \mu_1-\mu_2$ et l'on teste

 
  \begin{itemize}
  \item H$_0$: $\mu_1=\mu_2 \Leftrightarrow \Delta=0$
    \begin{center}
      contre
    \end{center}
  \item H$_1$:$\mu_1\neq\mu_2\Leftrightarrow \Delta\neq 0$
  \end{itemize}
 


  On considère $\overline{x_1}-\overline{x_2}$, c'est un estimateur
  sans biais de $\Delta$.

\subsubsection{ Cas (théorique) où $\sigma_1$ et $\sigma_2$ sont
  connues.}
\label{sec:cas-theorique-ou}



    Dans ce cas la variable
    $T=D/\sqrt{\frac{\sigma_1^2}{n_1}+\frac{\sigma_2^2}{n_2}}$ suit
    (ou suit approximativement) une loi normale centrée réduite.


\subsubsection{    Cas (pratique) où $\sigma_1$ et $\sigma_2$ ne sont
  pas   connues.}
\label{sec:cas-pratique-ou}


    \begin{enumerate}
    \item Si $n\geq 30$ alors on approche $\sigma_i^2$ par
      ${S_i}^2=\frac{n_i}{n_i-1}\s_i^2$ et l'on montre que
      $T=D/\sqrt{\frac{S_1^2}{n_1-1}+\frac{S_2^2}{n_2-1}}$ suit
      approximativement une loi normale centrée réduite.
    \item Si $n<30$ les approches précédentes ne fonctionnent pas car
      on ne connaît pas $\var(D)$. Dans le cas particulier où les
      populations sont normales et $\sigma_1=\sigma_2$ alors

      \begin{equation*}
        T^{*}=\frac{D}{\sqrt{\frac{n_1S_1^2+n_2S_2^2}{n_1+n_2-2}\left(\frac{1}{n_1}+\frac{1}{n_2}\right)}}
          \end{equation*}

          suit une loi de Student à $n_1+n_2-2$ degrés de liberté.
    \end{enumerate}


\subsubsection{Homoscédasticité}

    Précédemment, dans le dernier cas ($n<30$ et $\sigma_i$  inconnues), nous devons nous assurer que $\sigma_1=\sigma_2$ pour
    comparer les moyennes des échantillons. C'est l'hypothèse
    d'homoscédasticité.

    Le test de Fisher permet de tester cette hypothèse. Si \underline{les
    échantillons suivent des lois normales} alors

    \begin{equation*}
      F=\frac{S_1^2}{S_2^2}\rightsquigarrow F_{n_1-1,n_2-1}
    \end{equation*}

    On place au numérateur la plus grande variance.


    \begin{exem}
      On dispose de deux
\href{https://fr.wikipedia.org/wiki/Luminance}{luminancemètres}. On
réalise deux séries de mesures (exprimées en cd/m\up{2})
avec ces deux appareils d'une même surface lumineuse:



\begin{center}  \begin{tabular}[h]{|c|*{10}{|c}|}\hline
      l$_1$&101,1&101,9 &101&99,2&103,3&99,5&99&100,2&
      102,4&98,2\\\hline
      l$_2$&99,1&96,8&100,2&96,3&96,9&99,5&100,3&100,8&102,5&98,5\\\hline
                      \end{tabular}
                  \end{center}
                  
Est-ce que les  deux appareils donnent la même mesure?
  


\paragraph{Données complémentaires}
  On prend comme mesure de la surface lumineuse la moyenne des 10
  mesures.\\
  On suppose que les deux échantillons ont pour loi parente une loi
  normale. On peut reformuler (de manière simplifiée) le problème de la façon
  suivante:


  \begin{center}
\textbf{ Est-ce que les échantillons ont la même moyenne?}
  \end{center}

    \end{exem}

\subsection{Analyse de variance (ANOVA)} 
\label{sec:analyse-de-variance}

\subsubsection{Aspects théoriques}
\label{sec:aspects-theoriques}


Il s'agit de comparer les moyennes de plusieurs populations.


On considère $k$ populations distribuées selon des lois dont les
moyennes sont $m_1; m_2;\cdots ;m_k$.

On suppose en outre que ces $k$ populations sont distribuées selon des
lois normales de même variance (hypothèse d'homoscédasticité).

 On pose:
  \begin{itemize}
  \item H$_0:\, m_1=m_2=\cdots=m_k=m$;
  \item H$_1:\, \exists j\neq i \quad m_i\neq m_j$.
  \end{itemize}

  Le test repose sur la formule de décomposition de la variance:

  \begin{pro} Soit $k$ populations d'effectifs $n_i$, d'espérance
    $m_i$ et de variance $\sigma^2_i$. On pose $N=\sum_{i=1}^kn_i$. 
    \begin{align*}
  s^2= \frac{1}{N}\sum_{i=1}^{N}\big(X_i-\overline{X}\big)^2 = \var\, \mathrm{inter}(X)+\var\, \mathrm{intra}(X),\\
  \mbox{où : }\var \,\mathrm{inter}(X) = \frac{1}{N}\sum_{i=1}^kn_i(\overline{X}_k-\overline{X})^2\\
\mbox{ et }\quad \var\, \mathrm{intra}(X)  =   \frac{1}{N}\sum_{i=1}^k\sum_{j=1}^{n_i}(X_{i,j}-\overline{X_i})^2
\end{align*}
  \end{pro}

  \begin{preuve} Dans le cas où $k=2$.
    
    \begin{align*}
  Ns^2= &\sum_{i=1}^{N}\big(X_i-\bar{X}\big)^2 = \sum_{X_i\in
    G_1}\big(X_i-\overline{X}\big)^2+\sum_{X_i\in
    G_2}\big(X_i-\overline{X}\big)^2\\
  =&\sum_{X_i\in
    G_1}\big(X_i-\overline{X_1}\big)^2+\sum_{X_i\in
    G_1}\big(\overline{X_1}-\overline{X}\big)^2+\sum_{X_i\in
    G_2}\big(X_i-\overline{X_2}\big)^2+\sum_{X_i\in
    G_2}\big(\overline{X_2}-\overline{X}\big)^2\\
  &+2\big(\sum_{X_i\in   G_1}\big(X_i-\overline{X_1}\big)\big(\overline{X_1}-\overline{X}\big)+\sum_{X_i\in G_2}\big(X_i-\overline{X_2}\big)\big(\overline{X_2}-\overline{X}\big)\big)\\
   =&  N_1s^2_1+N_1\big(\overline{X_1}-\overline{X}\big)^2+N_2s^2_2+N_2\big(\overline{X_2}-\overline{X}\big)^2\\
   &+2\big(\big(\overline{X_1}-\overline{X}\big)\sum_{X_i\in
     G_1}\big(X_i-\overline{X_1}\big)+\big(\overline{X_2}-\overline{X}\big)\sum_{X_i\in
     G_2}\big(X_i-\overline{X_1}\big)\big)\\
   =&N\var
   \,\mbox{inter}(X)+N\var\,\mbox{intra}(X)+2\big(\big(\overline{X_1}-\overline{X}\big)\times
   0+\big(\overline{X_2}-\overline{X}\big)\times 0\big)\\
   =&N\var \,\mbox{inter}(X)+N\var\,\mbox{intra}(X)\\
   \end{align*}
 \end{preuve}

$R=\frac{\var\, \mbox{inter}(X)}{s^2}$ est la part de la variance
totale expliquée par la variance issue des différences entre les groupes.


La statistique utilisée est:

\begin{equation}
  \label{eq:1}
  F=\frac{\var \,\mathrm{inter}(X)}{\var \,\mathrm{intra}(X)}\times\frac{N-k}{k-1}
\end{equation}


\begin{pro}
  Lorsque H$_0$ est vraie, $F$ suit une loi de Fisher de degrés de
  liberté $(k-1;N-k)$. \label{pro:f}
\end{pro}

\begin{preuve}
  \begin{enumerate}
  \item L'indépendance des variables et leur normalité implique que
    \[\forall i\in \llbracket 1;k\rrbracket \quad
    \frac{(n_i-1)S_i^2}{\sigma^2_i} \rightsquigarrow
    \chi^2_{n_i-1}.\] Par additivité de la loi du $\chi^2$:
    \begin{equation*}
      \sum_{i=1}^k\frac{(n_i-1)S_i^2}{\sigma^2_i}= \sum_{i=1}^k\frac{\sum_{j=1}^{n_i}(X_{i,j}-\overline{X})^2}{\sigma^2_i}\rightsquigarrow \chi^2_{N-k}
    \end{equation*}

    Comme les variances sont égales, on obtient: $N\frac{\var
      \,\mathrm{intra}(X)}{\sigma^2}\rightsquigarrow \chi^2_{N-k}$
  \item Lorsque H$_0$ est vrai toutes les variables suivent une loi
    normale de paramètre $(m;\sigma^2)$. Donc
    $N\frac{\var(X)}{\sigma^2}\rightsquigarrow \chi^2_{N-1}$

    Or $\frac{N\var(X)}{\sigma^2}=\frac{N\var\,
      \mathrm{inter}(X)}{\sigma^2}+\frac{N\var\,
      \mathrm{intra}(X)}{\sigma^2}$. Donc $\frac{N\var\,
      \mathrm{inter}(X)}{\sigma^2}\rightsquigarrow
    \chi^2_{(N-1)-(N-k)}$
  \item Ainsi

    \begin{equation*}
      F=\frac{\frac{\var\,
      \mathrm{inter}(X)}{k-1}}{\frac{\var\,
      \mathrm{intra}(X)}{N-k}}=\frac{\var \,\mathrm{inter}(X)}{\var \,\mathrm{intra}(X)}\times\frac{N-k}{k-1}
\end{equation*}

suit une loi de Fisher de degrés de
  liberté $(k-1;N-k)$.
  \end{enumerate}
\end{preuve}


On déduit de la proposition \eqref{pro:f} la règle de décision de
manière habituelle.

\subsubsection{Présentation classique de l'analyse de variance: le
  tableau ANOVA}
\label{sec:pres-class-de}

Le test d'analyse de la variance est souvent présenté à l'aide d'un
tableau appelé <<tableau d'analyse de la variance>>.


\begin{table}[h]
  \centering
  \renewcommand{\arraystretch}{1.8}
  \begin{tabular}{cccc}
   variation&Somme des carrés&Degré de liberté&Carrés
                                                            moyens\\\hline\hline
    inter&SCE=$\sum_{i=1}^kn_i(\overline{X_i}-\overline{X})^2=\sum_{i=1}^kn_i\overline{X_i}^2-N\overline{X}^2$&$k-1$&$\cfrac{\textrm{SCE}}{k-1}$\\
    intra&SCD=$\sum_{i=1}^k\sum_{j=1}^{n_i}(X_{i,j}-\overline{X_i})^2$=SCT-SCE&$N-k$&$\cfrac{\textrm{SCD}}{N-k}$\\
total       &SCT=$\sum_{i=1}^k\sum_{j=1}^{n_i}(X_{i,j}-\overline{X})^2=\sum_{i=1}^k\sum_{j=1}^{n_i}X_{i,j}^2-N\overline{X}^2$&$N-1$&
  \end{tabular}
  \caption{Tableau ANOVA}
  \label{tab:anova}
\end{table}

Ensuite, on calcule $F$ comme le rapport des nombres placés dans la
dernière colonne.

\begin{exem}
  Douze parcelles de terrains sont réparties au hasard en trois
  groupes. Les engrais $A$ et $B$ sont employés dans les des premiers
  groupes et le groupe $C$ ne reçoit pas d'intrants.

  Les rendements sont les suivants:

  \begin{center}
    \begin{tabular}[h]{ccc}
      A&B&C\\\hline
      75&74&60\\70&78&64\\66&72&65\\69&68&55
    \end{tabular}
  \end{center}


  Le tableau anova est le suivant:
  
  \begin{center}
    \begin{tabular}{cccc}
      variation&Somme des carrés&Degré de liberté&Carrés
                                                   moyens\\\hline\hline
      inter&312&2&156\\
      intra&156&9&17,33\\
      total       &468&11&
    \end{tabular}
  \end{center}

  $F=\frac{156}{17,33}=9$ or le quantile d'ordre 0,95 de la loi de Fisher à (2;9) degré de
  liberté est 4,25. Donc on rejette H$_0$ au seuil d'acceptation de 5\%.
  
\end{exem}

\subsubsection{Analyse de la variance à deux facteurs}
\label{sec:analyse-deux-facteurs}

Voir en  TD

\begin{bclogo}[logo=\bcdanger,arrondi=0.1]{Attention}
Le test d'analyse de la variance pour la comparaison de moyenne,
repose sur l'hypothèse d'homoscédasticité. C'est ce qui motive le
paragraphe qui suit.
\end{bclogo}
\subsection{Comparaison simultanée de variances}
\label{sec:comp-simult-de}

Présentons le test de
\footnote{\href{https://lemakistatheux.wordpress.com/2013/08/04/le-test-de-levene/}{voir
    le makistatheux}}{Levene}.

On pose:
  \begin{itemize}
  \item H$_0:\, \sigma^2_1=\sigma^2_2=\cdots=\sigma^2_k=m$;
  \item H$_1:\, \exists j\neq i \quad \sigma^2_i\neq \sigma^2_j$.
  \end{itemize}

La statistique utilisée est 
\[W = \frac{N - k}{k - 1} \times \frac{\sum_{i = 1} ^k n_i (\overline{Z_i} - \overline{Z}) ^2}{\sum_{i = 1} ^k \sum_{j = 1} ^{n_i} (Z_{i,j} - \overline{Z_i}) ^2}\]

Avec
\begin{Itemize}
\item 
  $Z_{i,j} = | X_{i,j}- \overline{X_i} |$ pour
  $i\in\llbracket 1;k\rrbracket$ et $j\in \llbracket 1;n_i\rrbracket$
\item  $\overline{Z} =\frac{1}{N}\sum_{i = 1} ^k \sum_{j = 1} ^{n_i}
  Z_{i,j}$
\end{Itemize}



Lorsque H$_0$ est vraie, $W$ suit une loi de Fisher-Snedecor de paramètres $(k-1, N - k)$.


\begin{remarque}
  La statistique $W$ ressemble à la statistique $F$: on  a remplacé
  $X_{i,j}$ par $Z_{i,j} = | X_{i,j}- \overline{X_i} |$. Ce test peut
  être utilisé sans supposer que les $X_{i,j}$ suivent une loi
  normale. Parfois il peut être intéressant de remplacer
  $\overline{X_i}$ par la médiane du sous-groupe lorsque la
  distribution est asymétrique.
\end{remarque}

\section{Tests non paramétriques}
\label{sec:tests-non-param}


\subsection{Test d'adéquation}
\label{sec:test-dadequation}


On lance un dé à 6 faces 100 fois et on obtient les résultats suivant:

\begin{center}
  \begin{tabular}[h]{|*6{c|}}
    \hline
    1&2&3&4&5&6\\\hline
    15&17&20&14&13&21\\\hline
  \end{tabular}
\end{center}

On va présenter la méthodologie des tests statistiques pour essayer de
répondre à la question suivante :<< Le dé est-il truqué?>>. 

On confronte deux hypothèses:


\begin{Itemize}
\item   H$_0$:<<Le dé est équilibré>>
  \begin{center}
    contre
  \end{center}
\item  H$_1$:<<Le dé n'est pas équilibré>>
\end{Itemize}

L'hypothèse H$_0$ est appelée hypothèse nulle et  H$_1$ l'hypothèse
alternative.

On commence par fixer un seuil de tolérance $\alpha$ ( Prenons
$\alpha=0,05$) et l'on suppose  H$_0$ vraie. Dans ce cas, les valeurs
théoriques pour les résultats de 100 lancers sont $\hat{N_i}=100/6$.

On calcule ce qu'on appelle la distance\footnote{ce n'est pas une
  distance au sens mathématique du terme} du $\chi^2$:

\begin{equation*}
  \xi^2=\sum \frac{(N_i-\hat{N_i})^2}{\hat{N_i}}
\end{equation*}



On peut montrer que lorsque  H$_0$ est vraie $D$ suit asymptotiquement
la loi du  $\chi^2$ à 5 degré de liberté.

On détermine la région critique:

\begin{equation*}
  W=\{\xi^2\geq k\}
\end{equation*}

où $k$ est obtenu à l'aide de la loi du $\chi^2$ de telle sorte que:
\begin{equation*}
  \alpha=\p(\xi^2\geq k|\textrm{H}_0)
\end{equation*}


Enfin, on applique la règle de décision suivante:

\begin{Itemize}

\item Si $d^2\in W$ on rejette l'hypothèse H$_0$
\item Si $d^2\not\in W$ on accepte H$_0$
\end{Itemize}



\subsubsection{Test d'indépendance}
\begin{exem} {\textbf{CC 2010}}
  Une expérience a été menée dans le but de mettre en évidence un éventuel effet de la fumée de papier à
cigarette sur la génèse du cancer du poumon. Au cours de cette expérience, 74 souris ont été utilisées :
38 sont placées dans une cage enfumée par une machine à fumer, 36 servent de témoin et sont placées
dans une cage séparée et non enfumée. La machine a produit la fumée de 108 papiers à cigarette par
jour, six jours par semaine et cela pendant un an. À la fin de l'expérience, les animaux sont sacrifiés
et on compte 13 tumeurs parmi les souris expérimentales et 11 parmi les témoins.
\end{exem}


\begin{exem}
  Dans le cadre d'une étude de médicaments pour soulager les symptômes
  du rhume, on considère trois type de médicaments (X=A, B ou C). On
  étudie leur action Y (qui va de 0 : "aucune action" à 10 : "très
  efficace"). On donne à chaque individu malade l'un des trois
  médicaments, et chaque patient note le médicament.



\begin{tabular}[h]{|*4{c|}}
  \hline
  \backslashbox{Y}{X}&A& B& C\\\hline
  [0, 3[&42 &18& 31\\\hline
  [3, 8[&25& 20& 30\\\hline
  [8, 10]&20& 37 &15\\\hline
\end{tabular}

Les deux caractères X et Y peuvent-ils être considérés comme
indépendants? On réalisera un test du $\chi^2$ d'indépendance au
risque de 5\%, en précisant H$_0$, H$_1$ et la région critique.
\end{exem}

\subsection{Test d'ajustement Smirnov-Kolmogorov}
\label{sec:test-de-smirnov}
Soit $(X_1;X_2;\cdots;X_n)$ un échantillon de loi parente $X$ et
$(Y_1;Y_2;\cdots;Y_m)$ un échantillon de loi parente $Y$. Est-ce que
$X$ et $Y$ ont la même loi?

Soit $F_{X}$ la fonction de répartition de X et $F_{Y}$  celle de $Y$.


 On pose:
  \begin{itemize}
  \item H$_0:\,F_X=F_Y$;
  \item H$_1:\,F_X\neq F_Y$.
  \end{itemize}

  Naturellement on s'intéresse à la fonction de répartition empirique:

  \begin{equation*}
    F_n^X(x)=\frac{1}{n}\sum_{k=1}^n\I_{]-\infty;x]}(X_k) \quad\text{ et }\quad F_m^Y(x)=\frac{1}{m}\sum_{k=1}^m\I_{]-\infty;x]}(Y_k)
  \end{equation*}
L'écart entre la distribution de $X$ et celle de $Y$ peut se mesurer
par la variables aléatoire:
\begin{equation*}
  D=\sup_{x\in\R}|F_n^X(x)-F_m^Y(x)|
\end{equation*}

Soit $t>0$, on peut démontrer que lorsque H$_0$ est vraie que 
$\p(\sqrt{\frac{nm}{n+m}}D \geq t)$ converge lors que $n$ et $m$ tendent vers
l'infini vers la série absolument convergente
$S(t)=2\sum_{k=1}^{+\infty}(-1)^{k+1}\e^{-2k^2t^2}$.


On cherche le seuil $s$ tel que $\p(D\geq s)=\alpha$.


\begin{equation*}
  \p(D\geq s)=\alpha\Leftrightarrow \p(\sqrt{\frac{nm}{n+m}}D \geq \sqrt{\frac{nm}{n+m}}s)=\alpha
  \end{equation*}

  En utilisant la table suivante:
 
    \begin{table}[h]
      \centering
      \begin{tabular}[h]{|*6{c|}}
        \hline
        $\alpha$&0,2&0,15&0,1&0,05&0,01\\\hline
        $s_{\alpha}$ tel que $S(s_{\alpha})=\alpha$&1,073&1,138&1,224&1,358&1,628\\\hline 
      \end{tabular}
      
      \caption{Valeurs particulières des solutions de l'équation
        $S(t)=\alpha$}
      
      \label{tab:s}
    \end{table}


    On en déduit la règle de décision suivante:

    \begin{bclogo}[logo=\bcbook,arrondi=0.1]{Test d'ajustement de {\sc
          Kolmogorov-Smirnov}}

  \begin{itemize}
  \item Si $D\leq s_{\alpha}\sqrt{\frac{n+m}{nm}}$ on ne rejette pas H$_0$;
  \item sinon on rejette H$_0$.
  \end{itemize}

  où $s_{\alpha}$ s'obtient à l'aide de la table \ref{tab:s}.
\end{bclogo}

\section{Approche baysienne}
\label{sec:approche-baysienne}

\pagebreak

\nocite{Wonnacott1991,Delsart2011,Droesbeke1997,Saporta1990,Phan2012}



\printbibliography
\end{document}


\begin{Def}
  \begin{Itemize}

  \item Le risque de première espèce est le risque de rejeter à tort
    l'hypothèse nulle (faux négatif) on la note $\alpha$
  \item Le risque de seconde espèce est le rique d'accepter à tort
    l'hypothèse nulle (faux positif) on la note $\beta$
  \item On appelle puissance d'un test la quantité $1-\beta$
  \end{Itemize}
\end{Def}
 