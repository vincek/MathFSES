%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
% rubber: module biblatex
\documentclass[Cours,12pt,sol]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\usepackage[tikz]{bclogo}
\titre{Estimation}
\auteur{V. Ledda}
\reference{L3}
\composante{Faculté des Sciences Économiques et Sociales - Université de Lille\\
Licence 3 / Économie - Gestion \\
Techniques Mathématiques de l'Économiste - Estimation et tests}

\motsclefs{estimateur, maximum de vraisemblance, tests}
\usepackage{array}

\usepackage{frcursive}
\newcommand{\s}{\text{\textcursive{s}}}

\usepackage{draftwatermark}
%\SetWatermarkScale{1.5}
%\SetWatermarkAngle{305}
%\SetWatermarkLightness{0.99}
%\SetWatermarkLightness{0.8}
%\SetWatermarkText{$\quad$Université de Lille}

%\usepackage{pgf,tikz}
%\usepackage{mathrsfs}
%\usetikzlibrary{arrows}
%\usepackage{slashbox}
\usepackage{pgf,tikz}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\etudiant=0
\nosolout=1
\affichepreuve=0

\usepackage[backend=biber,   style=science,    sortlocale=fr_FR, natbib=true,    url=false,     doi=true,    eprint=false]{biblatex}

\addbibresource{../../biblio.bib}


%\usepackage[xcas]{pro-tablor}
%\usepackage{pro-tablor}

\setcounter{tocdepth}{2}
\begin{document}
\maketitle
\tableofcontents
\newpage


\section{Introduction}
\label{sec:introduction}


\begin{Def} Soit $X$ une variable aléatoire réelle.
  On appelle \emph{échantillon} de taille $n$ de $X$ le vecteur aléatoire
  $(X_1;X_2;\cdots;X_n)$ où les $X_i$ sont des variables aléatoires réelles indépendantes de
  même loi que $X$.
  \end{Def}

On notera $(x_1;x_2;\cdots;x_n)$ une réalisation de l'échantillon et la loi de \(X\) est la loi \emph{parente} de l'échantillon.

\begin{exem}\label{echan}
  Pour $n=5$, on dira que $(1,0,0,1,1)$ est une réalisation de
  l'échantillon $(X_1;X_2;\cdots;X_5)$ où les $X_i\rightsquigarrow
  \mathcal{B}(p)$.
\end{exem}


\begin{exem}\label{exem:sondage}
  Lorsque l'on réalise un sondage sur une population de taille \(N\), on cherche la proportion \(p\) d'individus [intéressé par un nouveau service, ayant l'intention de voter pour le candidat C, présentant une défaut, etc. ]. On sélection un échantillon de taille \(n\). Si l'on suppose \(n\) très petit devant \(N\) on peut considérer qu'il s'agit d'un échantillon de taille \(n\) de la loi de Bernoulli de paramètre \(p\) et  que le nombre d'individus de l'échantillon qui vérifie la propriété étudiée suit une loi binomiale de paramètre \((n;p)\).

\end{exem}

Une \emph{statistique} $T_n$ de $X$ est une fonction mesurable de
$(X_1;X_2;\cdots;X_n)$.

\begin{exem} En reprenant l'exemple \ref{echan},
  $T=\frac{1}{5}(X_1+X_2+X_3+X_4+X_5)$ est une statistique de l'échantillon $(X_1;X_2;\cdots;X_5)$.

\end{exem}

Le problème est le suivant, on se donne un ensemble de données que l'on souhaite étudier. On suppose que ces données sont la réalisation d'un échantillon issue d'une loi donnée dont les paramètres sont
inconnus. Peut-on estimer les paramètres inconnus à partir des données? Si oui, quel crédit peut-on donner à ces estimations?

\begin{exem}
  Dans \autoref{exem:sondage}; quelle est notre capacité à estimer le paramètre inconnu \(p\) à partir des résultats du sondage? C'est-à-dire, à partir de la réalisation de l'échantillon de loi parente \(\mathcal{B}(p)\), quelle information peut-on déduire sur la valeur de \(p\)? Une réponse rapide: on effectue le sondage, à partir de la réalisation de l'échantillon, on note \(x\) le nombre d'individus qui possède la propriété étudiée. La loi des grands nombres affirme que \(x/n\) est proche de \(p\) pour de grandes valeurs de \(n\), on a ainsi une estimation de \(p\).
\end{exem}

Commençons par préciser la notion d'estimation.

\section{Notion d'estimateur - Estimation ponctuelle}
\label{estimateur}


\subsection{Estimateur convergent}
\label{sec:estim-conv}


On suppose que la loi d'une variable aléatoire réelle $X$ dépend d'un paramètre $\theta$,
avec $\theta\in\Theta \subset \R^d$. On note $m$ l'espérance de $X$ et
$\sigma$ son écart-type. Soit $(X_1;X_2;\cdots;X_n)$ un échantillon
de $X$ et $T_n$ une statistique sur cet échantillon.



\begin{exem}
  Si $X\rightsquigarrow \mathcal{B}(p)$ alors $\theta=p$ et $\Theta=[0;1]$.
\end{exem}


\begin{Def}
  On dit que $T_n$ est \emph{estimateur convergent} de $\theta$ si $T_n$
  converge (en probabilité) vers $\theta$ lorsque $n$ tend vers $+\infty$.
\end{Def}

La proposition suivante donne une condition suffisante de convergence.

\begin{pro}
\label{CSCVpro}
  Soit $(X_n)$ une suite de variables aléatoires réelles intégrables. Si $\E(X_n)$ converge
  vers le réel $a$ et si $\var(X_n)$ converge vers 0 alors $(
X_n)$  converge en probabilité vers $a$.
\end{pro}


\begin{preuve}
  Soit $\epsilon>0$.

  \begin{eqnarray*}
    \epsilon\leq|X_n-a|\leq |X_n-\E(X_n)|+|\E(X_n)-a|\\
     \epsilon-|\E(X_n)-a|\leq |X_n-\E(X_n)|
  \end{eqnarray*}

Soit $\xi\in]0;\epsilon[$, il existe un réel $\eta>0$ tel que pour
$n>\eta$, $|\E(X_n)-a|\leq \xi$.

Ainsi \[\epsilon\leq|X_n-a|\Rightarrow \epsilon-\xi\leq
|X_n-\E(X_n)|\] donc $\p(|X_n-a|\geq \epsilon)\leq
\p(|X_n-\E(X_n)|\geq \epsilon-\xi)$.

D'après l'inégalité de Bienaymé-Chebyshev,
\[\p(|X_n-a|\geq \epsilon)\leq\frac{\var(X_n)}{(\epsilon-\xi)^2}\]

Comme $\var(X_n)$ converge vers 0, $(
X_n)$  converge en probabilité vers $a$.


\end{preuve}

Attention, la convergence de la variance vers $0$ n'est pas une
condition suffisante de convergence.

\begin{exem}
  Soit $X_n\rightsquigarrow B(n,p)$ on pose
  $Y_n=\frac{(-1)^n}{n}X_n$. $\var(Y_n)\to 0$ mais $(Y_n)$ ne converge pas.
\end{exem}
\begin{preuve}
  $x_n=Y_{2n}=\frac{X_{2n}}{2n}$. $\E(x_n)=p$.


  $y_n=Y_{2n+1}=\frac{X_{2n+1}}{2n+1}.$ $\E(y_n)=-p$.


  $\var(x_n)=\frac{\var(X_{2n})}{4n^2}=\frac{p(1-p)}{2n}\to 0$, idem pour
  $y_n$. Donc $(x_n)$ et $(y_n)$ convergent vers des limites
  différentes, se sont des suites extraites de $(Y_n)$. Ainsi $(Y_n)$ diverge.
\end{preuve}

\begin{exem}
  $\overline{X}=\frac{1}{n}\sum_{i=1}^nX_i$ est un estimateur
  convergent de $m$. C'est une conséquence de la loi des grands nombres.
\end{exem}

\begin{exem}
  $\s^2=\frac{1}{n}\sum (X_i-m)^2$ est un estimateur convergent de
  $\sigma^2$.


  $\E(\s^2)=\frac{1}{n}\sum\E((X_i-m)^2)=\frac{1}{n}\sum
  \E(X_i^2)-2\E(X_i)m+m^2=\frac{1}{n}\sum\E(X_i^2)-m^2=\E(X_1)^2-m^2=\sigma^2$.

  D'après la loi des grands nombres $\s^2$ tend vers  $\sigma^2$.
\end{exem}



\begin{exem}

Supposons que $X$ possède un moment d'ordre 4 (\(\mu_4=\E(|X-m|^4)<+\infty\)).

   $\s^2=\frac{1}{n}\sum (X_i-\overline{X})^2$ est un estimateur convergent de
  $\sigma^2$.

  $\E(\s^2)=\frac{1}{n}\sum\left[\E(X_i)^2-2\E(X_i\overline{X})+\E(\overline{X})^2\right]$.

  \begin{eqnarray*}
    \E(X_i\overline{X})&=&\frac{1}{n}\sum
    \E(X_iX_j)=\frac{1}{n}\left[\E(X_i^2)+\sum_{i\neq j}\E(X_iX_j)\right]\\
    &=&\frac{1}{n}\E(X_i^2)+\frac{1}{n}\sum_{i\neq j}\E(X_i)\E(X_j)\text{ car les va sont indépendantes}\\
    \E(X_i\overline{X})&=&\frac{1}{n}\E(X_i^2)+\frac{n-1}{n} m^2
  \end{eqnarray*}

De même

\begin{eqnarray*}
  \E(\overline{X}^2)&=&\frac{1}{n^2}\left(n \E(X_1^2)+n(n-1)m^2\right)
\end{eqnarray*}



Ainsi

\begin{eqnarray*}
  \E(\s^2)&=&\frac{1}{n}\sum\left[\E(X_i)^2-2\left(\frac{1}{n}\E(X_i^2)+\frac{n-1}{n} m^2\right)+\frac{1}{n^2}\left(n\E(X_1^2)+n(n-1)m^2\right)\right]\\
  &=&\frac{1}{n}\sum\left[\frac{n-1}{n}\E(X_i)^2-\frac{n-1}{n}m^2\right]=\frac{1}{n}\sum\frac{n-1}{n}\sigma^2\\
  \E(\s^2)&=&\frac{n-1}{n}\sigma^2
\end{eqnarray*}






% \begin{flushright}
%   \begin{minipage}[h]{0.7\linewidth}
%     \begin{eqnarray*}
%       \E(S^2)=\frac{1}{n}\sum \E((X_i-\overline{X})^2)
%     \end{eqnarray*}


%   \end{minipage}
% \end{flushright}



Calculons $\var(\s^2)=\E((\s^2)^2)-\E(\s^2)^2$.



\begin{eqnarray*}
\E((\s^2)^2)&=&\E\left(\left[\frac{1}{n}\sum (X_i-\overline{X})^2\right]^2\right)\\
&=&\E\left( \left[\sum (X_i-\overline{X})^2\right]^2\right)\\
&=& \frac{1}{n^2} \left[\sum  \E(X_i-\overline{X})^4+\sum_{i\neq j} \E((X_i-\overline{X})^2 (X_j-\overline{X})^2 ) \right]
\end{eqnarray*}


Au prix d'un calcul simple mais fastidieux on obtient \footnote{Voir
  par exemple \cite[p 45]{Phan2012}}:


\begin{equation}
 \var(\s^2)=\frac{n-1}{n^3}\left((n-1)\mu_4-(n-3)\sigma^4\right)\label{eq:varscarre}
 \end{equation}






Ainsi $\lim\var(\s^2)=0$ et $\lim\E(\s^2)=\sigma^2$ donc $\s^2$ tend en
probabilité vers $\sigma^2$.


Donc $\s^2$ est un estimateur de $\sigma^2$.

\end{exem}


\subsection{Biais d'un estimateur}
\label{sec:biais-dun-estimateur}


Soit $T_n$ est estimateur convergent de $\theta$.

 \begin{eqnarray*}
\E(T_n-\theta)= \E(T_n)-\theta\\
\end{eqnarray*}




\begin{Def}
  Le nombre $\E(T_n)-\theta$ est appelé le \emph{biais} de l'estimateur. Si
  $\E(T_n)=\theta$, on dit que l'estimateur est sans biais; sinon on
  parlera d'estimateur biaisé. Si $\lim E(T_n)=\theta$ on dira que
  l'estimateur $T_n$ est \emph{asymptotiquement sans biais}.
\end{Def}


\begin{exem}
   $\overline{X}$ est un estimateur sans biais de $m$.
\end{exem}


\begin{exem}
   $\mathcal{S}^2$ est un estimateur sans biais de $\sigma^2$.
\end{exem}


\begin{exem}
  $\s^2=\frac{1}{n}\sum (X_i-\overline{X})^2$ est un estimateur biaisé  de
  $\sigma^2$.


  $\E(\s^2)=\frac{n-1}{n}\sigma^2$. On lui préférera donc $S^{2}=\frac{1}{n-1}\sum (X_i-\overline{X})^2$, qui est l'estimateur sans biais de la variance.
\end{exem}

\begin{remarque}
  \begin{equation}
\var(S^{2})=\frac{\mu_4}{n}-\frac{n-3}{n(n-1)}\sigma^4
\end{equation}

Avec $\mu_4=\E((X-m)^4)$.
\end{remarque}


\begin{Def}
  Soient $T_1$ et $T_2$ deux estimateurs de $\theta$. On dira que
  $T_1$ est \emph{plus efficace} que $T_2$ si $\E((T_1-\theta)^2) \leq
  \E((T_2-\theta)^2)$. La quantité $\E((T_1-\theta)^2)$ est appelée
  \emph{erreur quadratique moyenne} de $T_1$.
\end{Def}


Lorsque $T$ est sans biais, $\E((T-\theta)^2)=
\E(T^2)-\theta\E(T)+\theta^2=\E(T^2)-\theta^2$. Ainsi comparer deux
estimateurs sans biais revient à comparer leurs variances.




\begin{exem}
  Soit $(X_1;X_2)$ un échantillon dont la loi parente est la loi de
  Bernoulli de paramètre $p$.


  On considère les statistiques suivantes:
  \begin{itemize}
  \item $T_1=\overline{X}$
  \item $T_2=X_1$
  \item $T_3=\frac{1}{2}$
  \end{itemize}

  Calculer les EQM. Quelles conclusions peut-on en tirer?
\end{exem}

\begin{pro}
  Soit $T$ une statistique telle que $\var(T)<+\infty$.

  \begin{equation*}
    \E((T-\theta)^2)=\var(T)+(\E(T)-\theta)^2
  \end{equation*}
\end{pro}

  \begin{preuve}
    \begin{eqnarray*}
       \E((T-\theta)^2)&= \E((T-\E(T)\quad +\quad \E(T)-\theta)^2)\\
         \E((T-\theta)^2)&=                \E((T-\E(T))^2)+2\E((T-\E(T))(\E(T)-\theta))+\E((\E(T)-\theta)^2)\\
      \E((T-\theta)^2)&=\var(T)+0+(\E(T)-\theta)^2
    \end{eqnarray*}
  \end{preuve}


\section{Méthode d'estimation}
\label{sec:methode-estimation}


Soit $X$ une v.a.r. sur $(\Omega;\mathcal{A};\p)$ on suppose que la loi de $X$
dépend d'un paramètre $\theta\in\Theta$.

On s'intéresse à la question suivante:  Comment trouver un estimateur
du paramètre $\theta$? Il existe deux méthodes classiques.

\subsection{Méthode des moments}
\label{sec:methode-des-moments}


Soit $f$ une fonction mesurable, en général $\E(f(X))$ dépend de
$\theta$ car la loi de $X$ dépend de $\theta$. La méthode des moments consiste à
exprimer $\theta$ en fonction de $\E(f(X))$, comme $\E(f(X))$ peut
être estimé par la moyenne empirique ($\frac{1}{n}\sum_{1\leq i\leq n}
f(X_i)$), on obtient un estimateur de $\theta$.

\begin{remarque}
  \begin{enumerate}
  \item En général, on prend $f:x\longmapsto x^n$, ainsi $\E(f(X))$
    est un moment de $X$, d'où le nom de la méthode.
  \item En pratique, $\E(f(X))$ est une fonction de $\theta$. Si
    \begin{equation}
\E(f(X))=g(\theta)\label{eq:1}
\end{equation}
, dans cette méthode, on doit  donc être en mesure de résoudre
l'équation \eqref{eq:1} dont l'inconnue est $\theta$.
  \end{enumerate}
\end{remarque}


\begin{exem}[Loi géométrique] Soit $X$ une v.a.r. dont la loi est
  une loi géométrique de paramètre $\lambda$.  

  \begin{equation}
    \label{eq:2}
    \E(X)=\frac{1}{\lambda}\Leftrightarrow \lambda=\frac{1}{\E(X)}
  \end{equation}


  Ainsi, $\frac{n}{\sum_{i=1}^nX_i}$ est l'estimateur de $\lambda$ par
  la méthode des moments.
\end{exem}


\begin{exem}[Loi Gamma] \label{ex:gamma}
  Soit $X$ une v.a.r. dont la loi est
  une loi gamma de paramètre $(a;\lambda)$.

  $\E(X)=\frac{a}{\lambda}$ et
  $\var(X)=\E(X^2)-\E(X)^2=\frac{a}{\lambda^2}$.

  \begin{eqnarray*}
    a=\lambda\E(X)\text{ et } \var(X)=\frac{\E(X)}{\lambda}\\
    \lambda=\frac{\E(X)}{\var(X)} \text{ et } \frac{\E(X)^2}{\var(X)}
  \end{eqnarray*}

  Ainsi $\frac{\overline{X}}{S^2}$ est un estimateur de $a$ et
  $\frac{\overline{X}^2}{S^2}$ est un estimateur de $\lambda$.
\end{exem}
\begin{remarque}
  Dans l'exemple  \ref{ex:gamma}, il y a deux paramètres à estimer. On
  a donc cherché les moments d'ordre 1 et 2, puis exprimé les
  paramètres $a$ et $\lambda$ en fonction de ces moments. De manière
  générale, on écrit autant d'équations que de paramètres à
  déterminer. Parfois, il est impossible de résoudre le système
  d'équations formé sans l'aide des méthodes numériques.
\end{remarque}
\subsection{Méthode du maximum de vraisemblance}

\begin{Def}
  La vraisemblance d'un $n$-échantillon d'une loi $X$ est la fonction
  $L$ définie sur $\R^n\times\Theta$ par:

  \begin{enumerate}
  \item si $X$ est discrète $L(\overrightarrow{x};\theta)=\prod_{i=1}^n\p(X_i=x_i)$
  \item si $X$ est à densité $L(\overrightarrow{x};\theta)=\prod_{i=1}^nf_\theta(x_i)$
  \end{enumerate}
 
\end{Def}


\begin{exem}
\label{vraissbern}
   Si $X\rightsquigarrow \mathcal{B}(p)$ alors $X$ est à densité par
   rapport à la mesure de comptage et
   $f_\theta(x_i)=\p(X=x_i)=p^{x_i}(1-p)^{1-x_i}$.
   \begin{equation*}
     L(\overrightarrow{x};p)=\prod_{i=1}^np^{x_i}(1-p)^{1-x_i}=p^{\sum x_i}(1-p)^{\sum(1-x_i)}
   \end{equation*}


   Si l'on reprend les données de l'exemple \ref{echan}, on obtient:


   $L(\overrightarrow{x};p)=p^3(1-p)^2$.
\end{exem}
\begin{Def}
  On appelle log-vraisemblance la fonction:
  \fonction{l}{\R^n\times \Theta}{\R}{(\overrightarrow{x};\theta)}{\ln(L(\overrightarrow{x};\theta))}
\end{Def}

La fonction $\ln$ étant strictement croissante chercher le maximum de
$\theta\longmapsto L(\overrightarrow{x};\theta)$ revient à chercher le
maximum de $\theta\longmapsto l(\overrightarrow{x};\theta)$. 


\begin{exem}
  À partir de l'exemple \ref{vraissbern}.


$l(\overrightarrow{x};p)=3\ln(p)+2\ln(1-p)$. Soit 

\fonction{f}{]0;1[}{\R}{p}{3\ln(p)+2\ln(1-p)}



$f$ est deux fois dérivable sur $]0;1[$;
$f'(p)=\frac{3}{p}-\frac{2}{1-p}=\frac{3-5p}{p(1-p)}$ et
$f''(\frac{3}{5})=-\frac{3}{(\frac{3}{5})^2}-\frac{2}{(\frac{2}{5})^2}<0$,
donc $f$ admet un  maximum pour $p_0=\frac{3}{2}$.


$\frac{3}{5}$ maximise la probabilité d'observer $(1,0,0,1,1)$, donc
$\frac{3}{5}$ est l'estimation de $p$ par la méthode du maximum de
vraisemblance.


On remarquera qu'ici $\frac{3}{5}$ est la moyenne empirique.
\end{exem}




\begin{Def}
  Un estimateur $T$ est appelé estimateur du maximum de vraisemblance
  de $\theta$ si
  \begin{equation}
    \label{eq:estimateurmaxvrai}
    L(\overrightarrow{X};T(\overrightarrow{X}))=\sup_{\theta\in\Theta}L(\overrightarrow{X};\theta)
  \end{equation}
\end{Def}


\begin{exem}
 Estimateur du maximum de vraisemblance pour un $n$-échantillon de la  loi exponentielle.
\end{exem}




\begin{exem}
  $X\rightsquigarrow\mathcal{N}(m;\sigma^2)$


  \begin{eqnarray*}
    \label{eq:5}
L(\vec{x};
    (m;\sigma^2))=\frac{1}{(\sqrt{2\pi}\sigma)^n}\exp(-\frac{\sum
    (x_i-m)^2}{2\sigma^2})\\
\mathcal{L}(\vec{x};(m;\sigma^2))=-n\ln(\sqrt{2\pi}\sigma)-\frac{\sum(x_i-m)^2}{2\sigma^2}\\
\mathcal{L}(\vec{x};(m;\sigma^2))=-n\ln(\sqrt{2\pi}) -\frac{n}{2}\ln(\sigma^2)-\frac{\sum(x_i-m)^2}{2\sigma^2}\\
\sys{\frac{\partial \mathcal{L}(\vec{x};(m;\sigma^2))}{\partial m}=
    \frac{1}{\sigma^2} \sum (x_i-m)=\frac{n}{\sigma^2}(\overline{x}-m)}{\frac{\partial
    \mathcal{L}(\vec{x};(m;\sigma^2))}{\partial \sigma^2}=
    -\frac{n}{2}\frac{1}{\sigma^2}+ \frac{1}{2}\sum(x_i-m)^2
    \frac{1}{\sigma^4}=-\frac{n}{2\sigma^2}+\frac{n}{2\sigma^4}v_e}\\
\nabla \mathcal{L}(\vec{x};(m;\sigma^2))=\vec{0}\Leftrightarrow
    \sys{m=\overline{x}}{\sigma^2=\frac{1}{n}\sum(x_i-m)^2=v_e}
 \end{eqnarray*}


 \begin{eqnarray*}
   \mathcal{H}=
   \begin{pmatrix}
     -\frac{n}{\sigma^2}&-\frac{n}{\sigma^4}(\overline{x}-m)\\
   -\frac{n}{\sigma^4}(\overline{x}-m)&  \frac{n}{2}\frac{1}{\sigma^4}-nv_e\frac{1}{\sigma^6}\\

   \end{pmatrix}=-\frac{n}{\sigma^2} \begin{pmatrix}
    1&\frac{1}{\sigma^2}(\overline{x}-m)\\
    \frac{1}{\sigma^2}(\overline{x}-m)&  -\frac{1}{2}\frac{1}{\sigma^2}+\frac{v_e}{\sigma^4}\\
   \end{pmatrix}\\
 \end{eqnarray*}


Lorsque $m=\overline{x}$ et $\sigma^2=v_e$ on a 

\begin{eqnarray*}
\mathcal{H}=-\frac{n}{v_e}
  \begin{pmatrix}
    1&0\\
0&\frac{1}{2v_e}
  \end{pmatrix}\\
|\mathcal{H}|=\frac{n^2}{2v_e^3}>0
\end{eqnarray*}

Donc $L(\vec{x}; (m;\sigma^2))$ admet un maximum (car $\frac{\partial^2
  \mathcal{L}(\vec{x};(m;\sigma^2))}{\partial m^2}<0$)  en $(\overline{x};\frac{1}{n}\sum(x_i-m)^2)$.
\end{exem}

\begin{exem}[Loi Gamma]
  Soit $X$ une v.a.r. suivant une loi Gamma de paramètre $(a;\lambda)$.
  
  \begin{equation*}
    f_X:t\longmapsto \frac{t^{a-1}\exp(-\frac{t}{\lambda})}{\lambda^a\Gamma(a)}
  \end{equation*}

  \begin{eqnarray*}
   L(\vec{x};a;\lambda)=\frac{(\Pi x_i)^{a-1}\exp(-\frac{\sum
    x_i}{\lambda})}{(\lambda^a\Gamma(a))^n}\\
\mathcal{L}(\vec{x};a;\lambda)= (a-1)\sum\ln(x_i)-\frac{\sum
    x_i}{\lambda}- na\ln(\lambda) -n\ln(\Gamma(a))\\
  \end{eqnarray*}


Cherchons les dérivées partielles:


\begin{eqnarray*}
  \frac{\partial\mathcal{L}}{\partial
  a}&=&\sum\ln(x_i)-n\ln(\lambda)-n\frac{\Gamma'(a)}{\Gamma(a)}\\
\frac{\partial\mathcal{L}}{\partial
  \lambda}&=&\frac{\sum
    x_i}{\lambda^2}-\frac{na}{\lambda}
\end{eqnarray*}
\end{exem}
\pagebreak
\section{Quelques propriétés des estimateurs}
\label{sec:quelq-propr-des}

\subsection{Information de Fisher}
\label{sec:inform-de-fish}

\begin{Def} Soit $\vec{X}$ un échantillon de taille $n$ dont la loi parente dépend
  d'un paramètre $\theta\in\Theta$ où $\Theta$ est un intervalle de $\R$.
  On appelle information de Fisher la quantité positive ou nulle:

  \begin{equation}
    \label{eq:3}
    I_n(\theta)=\E\left(\left(\frac{\partial L(\vec{X};\theta)}{\partial \theta}\right)^2\right)
  \end{equation}

\end{Def}


\begin{pro}
  Si le domaine de définition de $X$ ne dépend pas de $\theta$ et si  la vraisemblance est deux fois dérivable \footnote{d'autres hypothèses
    techniques sont nécessaires} alors
\label{info}
  \begin{equation*}
    I_n(\theta)=-\E\left(\frac{\partial^2 \mathcal{L}(\vec{X};\theta)}{\partial^2 \theta}\right)
  \end{equation*}
\end{pro}

\begin{preuve}

$\vec{X}\longmapsto L(\vec{X};\theta)$ est une densité de
probabilité. Donc $\int_{\R^n}L(\vec{X};\theta)\D{\vec{x}}=1$. En
dérivant par rapport à $\theta$ cette inégalité, d'après le théorème
de dérivation sous le signe intégral.

\begin{eqnarray*}
  \int_{\R^n}\frac{\partial L(\vec{X};\theta)}{\partial
  \theta}\D{\vec{x}}=0\\
 \int_{\R^n}\frac{\partial \ln(L(\vec{X};\theta))}{\partial
  \theta}L(\vec{X};\theta)\D{\vec{x}}=0\\
\E(\frac{\partial \ln(L(\vec{X};\theta))}{\partial
  \theta})=0
\end{eqnarray*}
  
Dérivons une seconde fois:

\begin{eqnarray*}
  \int_{\R^n}\frac{\partial^2 \ln(L(\vec{X};\theta))}{\partial
  \theta^2}L(\vec{X};\theta)\D{\vec{x}}+\int_{\R^n}\frac{\partial \ln(L(\vec{X};\theta))}{\partial
  \theta}\frac{\partial \ln(L(\vec{X};\theta))}{\partial
  \theta}L(\vec{X};\theta)\D{\vec{x}}=0\\
\E\left(\frac{\partial^2 \ln(L)(\vec{X};\theta)}{\partial^2
 \theta}\right)+I_n(\theta)=0\\
I_n(\theta)=-\E\left(\frac{\partial^2 \mathcal{L}(\vec{X};\theta)}{\partial^2 \theta}\right)
\end{eqnarray*}
\end{preuve}



\subsection{Estimateur efficace}
\label{sec:estimateur-efficace}



\begin{thm}[admis] \textbf{Inégalité de Fréchet-Darmois-Cramer-Rao}

Sous les hypothèses de la proposition \ref{info}
  Soit $T$ un estimateur sans biais de $\theta$. 

  \begin{equation}
    \label{eq:4}
    \var(T)\geq \frac{1}{I_n(\theta)}
  \end{equation}
\end{thm}

\begin{Def}
  Un estimateur $T$ est dit efficace s'il atteint la borne de Fréchet-Darmois-Cramer-Rao.
\end{Def}


\begin{exem}
  Si $X\rightsquigarrow \mathcal{P}(\lambda)$.

$L(\vec{X};\lambda)=\prod
\frac{\lambda^{x_i}}{x_i!}\exp(-\lambda)=\frac{\lambda^{\sum
    x_i}}{\prod x_i!}\exp(-n\lambda)$ et
$\mathcal{L}(\vec{X};\lambda)=(\sum x_i)\ln(\lambda)-\ln(\prod
x_i!)-n\lambda$


La fonction $f:\lambda \longmapsto (\sum x_i)\ln(\lambda)-\ln(\prod
x_i!)-n\lambda$ est définie sur $]0;+\infty[$ et deux fois dérivable.

$f''(\lambda)=-\frac{\sum x_i}{\lambda^2}$, donc la borne FDCR vaut
$-\frac{1}{\E(-\frac{\sum x_i}{\lambda^2})}=\frac{\lambda}{n}$; maintenant, calculons la variance de la moyenne
empirique qui est un estimateur sans biais de $\lambda$.

$\var(\overline{X})=\frac{\var{X}}{n}=\frac{\lambda}{n}$, ainsi la
borne FDCR est atteinte: la moyenne empirique est un estimateur
efficace pour le paramètre $\lambda$.
\end{exem}


\begin{pro}[admis]
  Si $T$ est l'estimateur du maximum de vraisemblance d'un paramètre
  $\theta$ alors:
  \begin{itemize}
  \item $T$ est asymptotiquement sans biais.
  \item $T$ est convergent.
  \item $T$ suit asymptotiquement une loi normale.
  \item $T$ est , au moins asymptotiquement, efficace.
  \end{itemize}
\end{pro}

\begin{remarque}
  Attention, l'estimateur du maximum de vraisemblance n'est pas
  toujours sans biais.
\end{remarque}

\subsection{Statistique exhaustive}
\label{sec:stat-exha}

Supposons que $X_i$ soit une var discrète avec $\p(X_i=x)>0$. D'après
la formule des probabilités totales:

\begin{equation*}
  \p(\vec{X}=\vec{x})=\sum \p(T=t)\times\p(\vec{X}=\vec{x}\,|\,T=t)
\end{equation*}


Ainsi si  $\p(\vec{X}=\vec{x}\,|\,T=t)$ ne dépend pas de $\theta$,
l'information contenu dans la connaissance de $T$ permet d'en déduire
complètement $\p(\vec{X}=\vec{x})$. Ceci motive la définition suivante:

\begin{Def}
  Une statistique $T$ est exhaustive relativement au paramètre
  $\theta\in\Theta\subset\R^d$ si 
  \begin{equation*}
\forall x\in\R^n; \forall t\in\R^d;  \p(\vec{X}=\vec{x}\,|\,T=t) \text{ ne
  dépend pas de }\theta
\end{equation*}

\end{Def}

\begin{thm}[Neyman-Fisher]
  $T$ est exhaustive si et seulement si la vraisemblance se factorise
  sous la forme:


  \begin{equation*}
    L( \theta;\vec{x})=g(T(\vec{x});\theta)\times h(\vec{x})
  \end{equation*}

  où $h$ ne dépend pas de $\theta$.
\end{thm}

\begin{exem}
  Si $X\rightsquigarrow \mathcal{P}(\lambda)$. Considérons la moyenne empirique.

  $L(\vec{X};\lambda)=\frac{\lambda^{\sum
    x_i}}{\prod x_i!}\exp(-n\lambda)=\lambda^{\sum
    x_i} \exp(-n\lambda)\times \frac{1}{\prod
    x_i!}=g(T(\vec{x});\theta)\times h(\vec{x})$ avec
  $g(t;\lambda)=\lambda^{nt}\exp(-nt)$ et $h(\vec{x})=\frac{1}{\prod
    x_i!}$. Donc la statistique est exhaustive.
\end{exem}
\pagebreak
\section{Intervalle de confiance}


On pose $\overline{X_n}=\frac{1}{n}\sum\limits_{k=1}^nX_k$ et l'on note
$z_{\alpha}$ le fractile d'ordre $\alpha$ de la loi normale centrée
réduite ($\Phi(z_{\alpha})=\alpha$).


\subsection{Un premier exemple: estimation d'une moyenne}
\label{sec:un-premier-exemple}

Soit $\vec{X}$ un échantillon dont la loi parente est une loi normale
de paramètre $(m;\sigma^2)$. On suppose que $\sigma$ est connue.


D'après la partie \ref{estimateur}, on sait que $\overline{X}$ est un
estimateur de $m$.




Lorsque l'on observe la réalisation $\vec{x}$, $\overline{x}$ est une
estimation de $m$. Quelle confiance peut-on apporter à cette
estimation?


Un raisonnement probabiliste élémentaire, nous apprend que
$\overline{X}$ suit une loi normale de paramètre
$(m;\frac{\sigma^2}{n})$. Ainsi, on peut calculer
$\p(|\overline{X}-m|\leq x)$ quelque soit $x$ et calculer la
probabilité que l'écart entre $m$ et son estimateur soit inférieur à
$x$.

En pratique, on souhaite que cette probabilité soit grande; mais il
faut être raisonnable, si l'on souhaite avoir une probabilité égale à
1, il suffit de prendre une valeur de $x$ très grande et dans ce cas
l'inégalité obtenue ne sera pas très utile.

Fixons $\alpha \in [0;1]$ le risque que nous sommes prêt à prendre.


\begin{equation*}
  \p\left(|\overline{X}-m|\leq x\right)=1-\alpha\Leftrightarrow \p\left(\sqrt{n}\frac{|\overline{X}-m|}{\sigma}\leq \sqrt{n}\frac{x}{\sigma}\right)=2\Phi\left(\sqrt{n}\frac{x}{\sigma}\right)-1=1-\alpha
\end{equation*}


Ainsi $x$ vérifie l'équation
$\Phi(\sqrt{n}\frac{x}{\sigma})=1-\alpha/2$, donc
$x=z_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}$.



On obtient
\begin{equation*}
   \p(\overline{X}-z_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}\leq m\leq \overline{X}+z_{1-\alpha/2}\frac{\sigma}{\sqrt{n}})=1-\alpha
\end{equation*}

On dira que
\begin{equation}
[\overline{X}-z_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}\quad;\quad
\overline{X}+z_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}]
 \label{iC}\tag{I}
\end{equation}
est un intervalle
de confiance symétrique de seuil $1-\alpha$  pour $m$.

\begin{bclogo}[arrondi=0.1,logo=\bcdanger]{Attention}
  Les bornes de l'intervalle \eqref{iC} sont des variables
  aléatoires. La probabilité que cet intervalle contienne $m$, $\p(I
  \ni m)$, est $1-\alpha$.
\end{bclogo}

\begin{exem}
   Pour déterminer l'âge moyen de ses clients, une grande entreprise
   de confection pour hommes prélève un échantillon aléatoire de 50
   clients dont l'âge moyen est de 36 ans. On suppose que la variance  $\sigma^2$ est connue et
est égale à 121. Donner un intervalle de confiance à 95\% pour l'âge moyen des clients de cette entreprise.
\label{exem:taille}

\begin{prof}

  L'intervalle de confiance à 95\% pour la moyenne est
  $[46,4\,;\,53,6]$. Cela signifie que l'on a une confiance à 95\% sur
  le fait que la véritable moyenne appartiennent à cet intervalle. Si
  la valeur réelle n'est pas dans cet intervalle, la probabilité a
  priori de cette observation ($\overline{X}=36$) était inférieure à
  5\%.
  
\end{prof}


\end{exem}
\subsection{Notion de région de confiance}
\label{sec:notion-de-region}
On suppose que la loi d'une v.a.r $X$ dépend d'un paramètre $\theta$,
avec $\theta\in\Theta \subset \R^d$. On note $m$ l'espérance de $X$ et
$\sigma$ son écart-type. Soit $\vec{X}=(X_1;X_2;\cdots;X_n)$ un échantillon
de $X$ et $T_n$ une statistique sur cet échantillon.


\begin{Def} Soit $\alpha \in [0;1]$. On considère une partie
  $C=C(\vec{X})$ de $\Theta$. On dit que $C$ est une région de
  confiance de seuil $1-\alpha$ si la probabilité que $C$ contienne
  $\theta$ est égale à $1-\alpha$.
\end{Def}


\begin{exem}
  Reprenons l'exemple \ref{exem:taille}. Cherchons une région de confiance pour
  $m$ de la forme $C=]-\infty;\overline{X}-a[\cup]\overline{X}+a;+\infty[$.

Calculons la probabilité que $C$ contienne $m$. Comme
$\sqrt{n}(\overline{X}-m)/\sigma$ suit la loi normale centrée réduite.

\begin{equation*}
\p(C\ni m)=\p(m\leq \overline{X}-a)+\p(m\geq
\overline{X}+a)=2\p(\overline{X}-m\leq -a)=2\p(\sqrt{n}\frac{\overline{X}-m}{\sigma}\leq -\sqrt{n}\frac{a}{\sigma})=2\phi(-\sqrt{n}\frac{a}{\sigma})
\end{equation*}


Ainsi:

\begin{equation*}
  \p(C\ni m)=1-\alpha\Leftrightarrow -\sqrt{n}\frac{a}{\sigma}=z_{\frac{1-\alpha}{2}}\Leftrightarrow a=-z_{\frac{1-\alpha}{2}}\frac{\sigma}{\sqrt{n}}
\end{equation*}

et 

\[]-\infty;\overline{X}+z_{\frac{1-\alpha}{2}}\frac{\sigma}{\sqrt{n}}[\cup]\overline{X}-z_{\frac{1-\alpha}{2}}\frac{\sigma}{\sqrt{n}};+\infty[\]
est une région de confiance de seuil $1-\alpha$ pour $m$

\paragraph{Application numérique}

$c=]-\infty;35,9[\cup]36,1;+\infty[$.

Bien sûr, cette région de confiance n'a aucun intérêt en pratique, car
elle ne contient pas l'estimation ponctuelle raisonnable de $m$ à
savoir $\overline{X}$. Aussi, en pratique on cherche des régions de
confiance connexe. Lorsque le paramètre est unidimensionnel ($d=1$),
on s'intéresse aux intervalles de confiance.
\label{exem:taille1}

\end{exem}


L'exemple précédent (\ref{exem:taille1}) nous enjoint à travailler
avec les intervalles de confiances dans le cadre où le paramètre est
unidimensionnel. Dans ce cas, remarquons que les bornes de
l'intervalle de confiance ne doivent pas dépendre de $\theta$ car
$\theta$ est inconnu.





\subsection{Intervalles de confiance classiques}
\label{sec:interv-de-conf}


\subsubsection{Estimation d'une proportion}

On s'intéresse à la proportion $p$ d'individus d'une population
possédant une certaine propriété. On considère un échantillon de taille
$n$ et $f$ la proportion d'individus de l'échantillon possédant la propriété.



La propriété peut être ``la pièce a un défaut en sortie de chaîne de
montage'' ou dans un contexte pré-électoral: ``la personne interrogée déclare qu'elle votera OUI au
référendum''.


\begin{pro}\label{icprop}
  L'intervalle  $[f-z_{1-\frac{\alpha}{2}}\frac{\sqrt{f(1-f)}}{\sqrt{n}}\;;\;f+z_{1-\frac{\alpha}{2}}\frac{\sqrt{f(1-f)}}{\sqrt{n}}]$
  est un intervalle de confiance \textbf{approché} symétrique en probabilité au niveau
  $\alpha$ pour $p$.
\end{pro}



\begin{preuve}
  On suppose que $n$ est très petit par rapport à l'effectif de la
  population, et que la situation  revient à effectuer \textit{un tirage
    aléatoire} avec remise. Donc si l'on désigne par $X$ la variable aléatoire qui vaut 1 si la propriété est
  vérifiée et 0 sinon, $X\rightsquigarrow \mathcal{B}(p)$. D'après ce qui précède la moyenne
  empirique fournit un estimateur convergent de $p$. 
  
  D'après le théorème central-limit \cite[195]{Delsart2010},
  $\frac{\sqrt{n}}{\sqrt{p(p-1)}}(\overline{X_n}-p)$ converge en loi vers $\mathcal{N}(0;1)$
  
  C'est à dire que pour $n$ suffisament grand et en remarquant que
  $f=\overline{X_n}$
  
  \begin{equation*}
    \p(\frac{\sqrt{n}}{\sqrt{p(p-1)}}|f-p|\leq z_{\frac{1+\alpha}{2}})=\alpha
  \end{equation*}

  Donc $p\in
  \left[f-z_{\frac{1+\alpha}{2}}\frac{\sqrt{p(1-p)}}{\sqrt{n}}\;;\;f+z_{\frac{1+\alpha}{2}}\frac{\sqrt{p(1-p)}}{\sqrt{n}}\right]$
  avec une probabilité égale à $\alpha$.

  En considérant que $p\simeq f$, on obtient le résultat énoncé.
\end{preuve}


 \begin{exem} 
  
En sortie de chaîne on récupère 200 pièces, 7 d'entre elles ne sont
pas conformes. Donner un intervalle de confiance au niveau 95\% de la
proportion $p$ de pièces défectueuses.



Soit $X$ une v.a.r distribuée suivant la loi de Bernoulli de paramètre
$p$. Soit $(X_1;X_2;X_3;\cdots; X_n)$ un échantillon de cette
variable. Posons
$\overline{X_n}=\frac{1}{n}(X_1+X_2+\cdots+X_n)$. $\E(\overline{X_n})=p$
et $\var(\overline{X_n})=\frac{1}{n}p(1-p)$.

\begin{prof}
  $I_{0,95}=[0,009\,;\,0,06]$
\end{prof}

\end{exem}


\begin{remarque}
  On peut également résoudre l'inéquation
  \begin{equation*}
    n(f-p)^2\leq z_{1-\alpha/2}^2p(1-p) 
  \end{equation*}
Pour trouver la valeur de $p$, en général le gain en précision de
cette méthode est négligeable.


Sur l'exemple, on obtient:

$I_{0,95}=[0,017\, ;\, 0.07]$.
\end{remarque}


\subsubsection{Estimation de $\mu$ pour la loi $\mathcal{N}(\mu;\sigma^2)$  avec $\sigma$ connu}


Soit $(X_1;X_2;\cdots;X_n)$ un n-échantillon de
$\mathcal{N}(\mu;\sigma^2)$, en utilisant la méthode développée dans
la preuve de la proposition  \ref{icprop} on démontre la 

\begin{pro}
  L'intervalle
  $[\overline{X_n}-z_{\frac{1+\alpha}{2}}\frac{\sigma}{\sqrt{n}}\;;\;\overline{X_n}+z_{\frac{1+\alpha}{2}}\frac{\sigma}{\sqrt{n}}]$
  est un intervalle de confiance symétrique en probabilité au niveau
  $1-\alpha$ pour $\mu$.
\end{pro}

\subsubsection{Estimation de $\mu$ pour la loi $\mathcal{N}(\mu;\sigma)$  avec $\sigma$ inconnu}

Il s'agit de la même situation que précédemment, mais l'on doit
estimer $\sigma^2$.


On pose ${S}^2=\frac{1}{n-1}\sum\limits_{k=1}^n(X_k-\overline{X_n})^2$ et l'on note
$t_{n,\alpha}$ le fractile d'ordre $\alpha$ de la loi de Student à $n$
degrés de liberté ($F_{T_n}(t_{n,\alpha})=\alpha$).

\begin{pro}
  L'intervalle
  $[\overline{X_n}-t_{n-1,\frac{1+\alpha}{2}}\frac{S^*}{\sqrt{n}}\;;\;\overline{X_n}+t_{n-1,\frac{1+\alpha}{2}}\frac{S^*}{\sqrt{n}}]$
  est un intervalle de confiance symétrique en probabilité au niveau
  $1-\alpha$ pour $\mu$.
\end{pro}


\begin{exem}
  Une usine produit des tôles métalliques ; la surface X des tôles
  suit une loi normale d’écart-type 4. Après une réorganisation de la chaîne de production, on cherche à déterminer la surface
moyenne des tôles. On prélève 28 tôles, on obtient une moyenne
empirique de 45,25~dm\up{2}.


\begin{enumerate}
\item Construire un intervalle de confiance au niveau 95\% pour la
  surface moyenne en supposant que l’écart-type n’a pas changé.
\item Tout en conservant le même niveau de confiance, on veut réduire
  l’amplitude de l’intervalle de confiance à 1~dm\up{2}. Combien faut-il
  prélever de tôles?  
\item  Cette fois-ci on ne suppose plus que l’écart-type n’a pas changé. L’écart-type empirique de l’échantillon  trouvé est $\sigma= 4$~dm\up{2} . Construire un nouvel intervalle de confiance au même niveau de confiance.
\end{enumerate}
On donne $t_{0,975,27}=2,05$ et $z_{0,975}=1,96$.
\end{exem}



\subsubsection{Estimation de $\sigma^2$ pour la loi $\mathcal{N}(\mu;\sigma)$  avec $\sigma$ inconnu}



On note $\chi^2_{\alpha}(n)$ le fractile d'ordre $\alpha$ de la loi du
$\chi^2$ à $n$ degrés de liberté.


On note
$S^2=\frac{1}{n-1}\sum_{i=1}^n(X_i-\overline{X_n})^2$ l'estimateur sans biais classique de la variance.


\begin{pro}
    L'intervalle
  $[\frac{nS^2}{\chi^2_{1-\alpha_2}(n-1)};\frac{nS^2}{\chi^2_{\alpha_1}(n-1)}]$
  est un intervalle de confiance  au niveau
  $1-\alpha$ pour $\sigma^2$. où $\alpha_1+\alpha_2=\alpha$.
\end{pro}
$\alpha_1$ et $\alpha_2$ mesurent respectivement les risques à gauche
et à droite.




\begin{preuve}
  On montre que $\frac{nS^2}{\sigma^2}$ suit une loi du $\chi^2$ à
  $n-1$ degré de liberté.


  \begin{eqnarray*}
    \p\left(\chi^2_{\alpha_1}(n-1)\leq \frac{nS^2}{\sigma^2}\leq
    \chi^2_{1-\alpha_2}(n-1)\right)=1-\alpha\\
    \p\left(\frac{nS^2}{\chi^2_{1-\alpha_2}(n-1)}\leq \sigma^2
    \leq \frac{nS^2}{\chi^2_{\alpha_1}(n-1)}\right)=1-\alpha
  \end{eqnarray*}

\end{preuve}
\begin{remarque}
  Lorsque $\mu$ est connue, l'intervalle de confiance est le même
  à ceci près le nombre de degrés de liberté de la loi du $\chi^2$
  suivit par $n\frac{\s^2}{\sigma^2}$ vaut $n$.
\end{remarque}

\begin{exem}
  La durée d'écoulement d'un sablier suit une loi normale de paramètre
  $\mathcal{N}(m;\sigma^2)$. On le retourne 12 fois de suite et l'on
  obtient une durée moyenne de 4~minutes et un écart-type de 10
  secondes. Donner un intervalle de confiance pour la variance
  $\sigma^2$.


  \begin{prof}
    $\chi^2_{0,025}(11)\simeq 3,81$ et    $\chi^2_{0,975}(11)\simeq
    21,92$ et $I_{0,95}=[0,015\,;\, 0,0874]$.
  \end{prof}
  
\end{exem}


\begin{remarque}
  La loi du $\chi^2$ n'étant pas symétrique, l'intervalle de confiance
  n'est pas forcément symétrique en probabilité.

  \begin{center}
    \includegraphics[width=8cm]{images/loi_chi2_ic}
  \end{center}
\end{remarque}

\pagebreak

\nocite{Wonnacott1991,Delsart2011,Droesbeke1997,Saporta1990,Phan2012}



\printbibliography
\end{document}
