%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[A4,12pt]{cueep}
\reference{L3\\SIAD - MISEG}
\titre{TP n°3: Échantillonage}
\usepackage{enumitem,ulem}\usepackage{frcursive}
\newcommand{\x}{\text{\textcursive{x}}}
\newcommand{\s}{\text{\textcursive{s}}}
\newcommand{\m}{\text{\textcursive{m}}}

\usepackage[tikz]{bclogo}
\motsclefs{échantillonage, sondage aléatoire simple, sondage aléatoire
  stratifié}

\newcommand\bclaptop{\includegraphics[width=\logowidth]{metalmarious_Laptop}}



\begin{document}

\section{Aspects théoriques}
\label{sec:aspects-theoriques}

On étudie une population $\mathcal{P}$ de taille $N$, on suppose ici
que $N$ est fini. On suppose que l'on a à notre disposition la liste
de tous les éléments de $\mathcal{P}$, cette liste est appelée
\uline{base de sondage}. Un sondage consiste à sélectionner un
échantillon d'éléments de $\mathcal{P}$ et de les
interroger. Les méthodes de sondage diffèrent sur la façon de
sélectionner un échantillon.

\begin{Def}
 Lorsque tous les échantillons ont la même probabilité d'être choisis, il 
 s'agit d'un sondage aléatoire simple.
\end{Def}

Dans ce cas tous les éléments de $\mathcal{P}$ ont la même probabilité
d'être choisis.

\begin{enumerate}[series=questions,label=\fbox{\arabic*}]
\item On suppose que la taille de la population est $N$, quel est le
  nombre d'échantillons de taille $n$ que l'on peut obtenir par un
  échantillonnage aléatoire simple sans remise? Quelle est la probabilité de choisir
  un échantillon particulier?
\item Qu'appelle-t-on le taux de sondage. On note $f$ le taux de sondage.
\end{enumerate}
 Supposons ici que le questionnaire porte (en autre) sur une
 variable $\x$ quantitative appelée \uline{variable d'intérêt}. On
 note $\m$  la moyenne et $\s^2$ la
 variance corrigée de   $\{\x_1;x_2;\cdots;\x_N\}$ 

Lorsque l'on <<tire>> un échantillon, le vecteur $(X_1;X_2;\cdots;X_n)$
est aléatoire et sa réalisation  $(x_1;x_2; \cdots; x_n)$ permet d'obtenir des
 informations sur des indicateurs statistiques de la population
 $\mathcal{P}$. En particulier, on peut obtenir un intervalle de
 confiance sur $\m$. Comme on le sait la précision de cet intervalle
 de confiance dépend de la variance de $(x_1;x_2; \cdots; x_n)$.

 \begin{enumerate}[series=questions,resume,label=\fbox{\arabic*}]
 \item Soit $\overline{X}=\frac{1}{n}\sum_{i=1}^nX_i$ montrer que
   $\E(X)=\m$. On pourra commencer par calculer la probabilité de  l'évènement \[A_i=\{\x_i\in
   \{X_1;X_2;\cdots;X_n\}\}\] pour $i\in\ii{N}$.
\end{enumerate}

On admet que \[\var(\overline{X})=\frac{\s^2}{n}(1-f).\]
Bien souvent la base de sondage comporte des informations
complémentaires sur chaque individu de la population. L'idée du
sondage stratifié est d'utiliser ces informations pour réduire la
taille de l'échantillon et (ou) augmenter la précision de l'estimation.

\begin{Def}
  Réaliser un sondage stratifié consiste à:
  \begin{enumerate}
  \item  subdiviser la population en groupes homogènes (strates);
\item extraire un échantillon aléatoire de chaque strate.
  \end{enumerate}
   
\end{Def}


Supposons que la base de sondage, nous permette de partitioner
$\mathcal{P}$ en $H$ sous-populations $\mathcal{P}_1; \mathcal{P}_2;
\cdots; \mathcal{P}_H$ d'effectifs respectifs $N_1;N_2;\cdots; N_H$ ($N_1+N_2+\cdots+N_H=N$).


On note $\m_1;\m_2;\cdots;\m_H$ les moyennes de $\x$
sur chaque strate et $\s^2_1;\s^2_2;\cdots;\s^2_H$ les variances de $\x$
sur chaque strate.

Dans chaque strate, on effectue un sondage aléatoire simple sans
remise et l'on note $n_1; n_2; \cdots; n_H$ la taille de chaque
échantillon (on a ainsi $n_1+n_2+\cdots+n_H=n$) et $\overline{X}_1;\overline{X}_2;\cdots;\overline{X}_H$ les moyennes
empiriques sur chaque strate.


\begin{enumerate}[resume=questions,label=\fbox{\arabic*}]
\item Montrer que
  \[\widehat{\mu}=\frac{1}{N}\sum_{h=1}^HN_h\overline{X}_h\] est un
  estimateur sans biais de $\m$.
  \item Que vaut le taux de sondage de la strate $\mathcal{P}_h$,
  ($h\in\ii{H}$)? On le notera $f_h$.
\item Montrer que
  \[\var(\widehat{\mu})=\sum_{h=1}^H\frac{N_h^2}{N^2}(1-f_h)\frac{\s_h^2}{n_h}\]
\item Que devient cette formule lorsque les tailles des échantillons
  sont  très petites par rapport aux tailles des strates? 
\item Pourquoi lors d'un sondage stratifié, cherche-t-on à avoir des
  strates les plus homogènes possibles?
\item On suppose que les $H$ taux de sondages sont égaux
  (\textbf{sondage stratifié proportionnel}).
  \begin{enumerate}
  \item Montrer que dans ce cas les taux de sondage de chaque strate
    sont égaux au taux de sondage global $f$.
  \item En déduire que
    \begin{equation*}
      n_h=f\times N_h
    \end{equation*}
  \item Comment réalise-t-on un sondage stratifié proportionnel?
  \item Montrer que

    \[\var(\widehat{\mu})=\frac{1}{n}(1-f)\sum_{h=1}^H\frac{N_h}{N}\s_h^2=\frac{1}{n}(1-f)\s^2_{\text{intra}}\]

    En déduire que la variance de $\widehat{\mu}$ pour un sondage
    stratifié est inférieur à la variance de  $\widehat{\mu}$ pour un
    sondage aléatoire simple.
  \end{enumerate}

\end{enumerate}
% Cette méthode suppose la connaissance de la structure de la population. Pour estimer les paramètres, les résultats doivent
% être pondérés par l’importance relative de chaque strate dans la population.

\section{Cas pratique}
\label{sec:cas-pratique}

On s'intéresse au nombre d'heures passées devant la télévision par
jour sur une population de 2\,000 personnes. On possède un
recensement de cette population (\texttt{hdv2003.ods}).


\begin{enumerate}[series=casp,label=\fbox{\arabic*}]


  \item À partir de la feuille de calcul, réaliser 10 sondages
    aléatoires simples avec un taux de sondage de 5~\%. Pour chaque sondage, on calculera l'estimation
    de la moyenne correspondante. Ainsi que la variance de ces 10
    moyennes.
  \item Sur le deuxième onglet, réaliser 10 sondages stratifiés
    proportionnels. On stratifiera par rapport à la fréquentation du
    cinéma. Pour chaque sondage, on calculera l'estimation
    de la moyenne correspondante. Ainsi que la variance de ces 10
    moyennes.
  \item Vérifie-t-on les conclusions de la première partie?
  \end{enumerate}


\begin{bclogo}[logo = \bclaptop,arrondi=0.1]{Fonctions utiles du tableur}
\texttt{INDIRECT, CONCAT, ALEA.ENTRE.BORNES}
\end{bclogo}

\end{document}
