%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Les_suites_numeriques.tex]{subfiles}


\begin{document}



\section{Suites récurrentes du type $u_{n+1}=f(u_n)$}
\label{sec:suit-recurr-du}


\subsection{Introduction}
\label{sec:introduction}



Le but de ce chapitre est d'étudier la suite définie par : $u_0\in I
\text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$. Où  $f$ est une
fonction numérique définie sur un intervalle $I$ de $\R$.


\begin{exemple}
  Au chapitre 3, on a étudié un cas simple où $f$ est une fonction
  affine.

  \begin{equation*}
    u_{n+1}=a\times u_n+b
  \end{equation*}
  La fonction $f$ est définie sur $\R$ par $x\longmapsto ax+b$.
\end{exemple}


\begin{exemple}
  Plaçons nous dans le modèle de production de Cobb-Douglas:
  \begin{equation*}
    \label{eq:1}
    Y=A\times K^{\alpha}L^{1-\alpha}
  \end{equation*}

  où $A>0$, et $0\leq\alpha\leq 1$.

  
On s'intéresse à l'évolution, à temps discret, du capital $(K)$ et du
travail $(L)$ en faisant les hypothèses suivantes:

\begin{equation*}
  \label{eq:2}
  \sys{K_n=\sigma Y_{n-1}\quad 0<\sigma<1}{L_{n}=(1+\lambda)L_{n-1}}
  \end{equation*}

  Posons $k_n=\frac{K_n}{L_n}$.


  \begin{equation*}
    k_{n+1}=\frac{ \sigma Y_{n}}{  (1+\lambda)L_{n}}=\frac{\sigma
      AK_n^{\alpha}L_n^{1-\alpha}}{
      (1+\lambda)L_{n}}=\frac{A\sigma}{1+\lambda}\times \frac{K_n^{\alpha}}{L_n^{\alpha}}=\frac{A\sigma}{1+\lambda}\times (k_n)^{\alpha}
  \end{equation*}

  La suite des ratios ($k_n$) est donc définie par récurrence.
  Ici la fonction $f$ est la fonction définie sur $\R^+$ par
  $x\longmapsto \frac{A\sigma}{1+\lambda}x^{\alpha}$
\end{exemple}


\begin{bclogo}[logo = \bcdanger,arrondi=0.1]{Attention }

La plupart du temps, on ne peut pas
exprimer $u_n$ directement en fonction de $n$. Il faut donc étudier la suite, sans connaître l'expression du terme général.

\end{bclogo}

\subsection{Existence et représentation graphique }
	\subsubsection{Intervalle stable}
\begin{exem}
 Soit la suite ``définie`` par : $u_0=2 \text{ et }
 u_{n+1}=\dfrac{1}{u_n-1}$.\\


 La fonction associée est la fonction $f:x\longmapsto \frac{1}{x-1}$ définie
 sur $\R\backslash\{1\}$. On a $u_1=\dfrac{1}{2-1}=1$ et on constate
 qu'il est impossible de calculer $u_2$. $\forall n\geq 2,\; u_n $ n'existe pas et donc la suite $(u_n)$ n'est pas définie.


On a $u_1=f(u_0)=1$. Comme $u_1\notin \R\backslash\{1\}$, on ne peut pas calculer $u_2=f(u_1)$...\\
\end{exem}



Soit $I$ un intervalle inclus dans $\mathcal{D}_f$. L'exemple
précédent nous montre que si $\forall n\in\N,\quad u_n\in I$ alors la
suite est bien définie (c'est une condition suffisante).

\begin{Def}
 On dira que $I$ est un intervalle stable par $f$ si $f(I)\subset I$.
\end{Def}

\begin{pro}
 Si $I$ est stable par $f$ et si $u_0\in I$, alors la suite $(u_n)$ est bien définie.
\end{pro}

\begin{preuve}
Nous allons démontrer par récurrence que : $\forall n\in \N,\; u_n \text{ existe et } u_n\in I$.\\
Initialisation : $u_0$ existe et $u_0\in I$.\\
Hérédité : Soit $n\in \N$, on suppose que $u_n \text{ existe et } u_n\in I$.\\
On sait que $u_n\in I$ donc $u_{n+1}=f(u_n)$ existe.\\
De plus, $f(I)\subset I$ donc $u_{n+1}\in I$.\\
Conclusion :  $\forall n\in \N,\; u_n \text{ existe et } u_n\in I$. La suite $(u_n)$ est donc bien définie.
\end{preuve}




\begin{bclogo}[logo = \bcdanger,arrondi=0.1]{Attention }
Le choix de $u_0$ est important.
\end{bclogo}

\begin{remarque}
 Comme $\forall n\in \N,\;  u_n\in I$, on en déduit que :
\begin{enumerate}
\item[-] Si l'intervalle $I$ est minoré, alors $(u_n)$ est minorée.
\item[-] Si l'intervalle $I$ est majoré, alors $(u_n)$ est majorée.
\item[-] Si l'intervalle $I$ est borné, alors $(u_n)$ est bornée.
\end{enumerate}

\end{remarque}


\subsubsection{Exemple}
Voici un exemple que nous étudierons tout au long du chapitre.
$$u_0=2 \text{ et } u_{n+1}=\dfrac{1}{5}(4+u_n^2),\; \forall n\in\N$$
Soit $f$ la fonction définie sur $\R$ par $f(x)=\dfrac{1}{5}(4+x^2)$.\\
Vérifions que l'intervalle $I=[0;4]$ est stable par $f$.\\

\noindent $f$ est dérivable sur $\R$ et $f'(x)=\dfrac{2}{5}x, \; \forall x\in \R$, d'où le tableau de variation suivant: 

\begin{figure}[!ht]

  
  \begin{TV*}{100}  
    TV([-infinity,+infinity],[],"f","x",(4+x^2)/5,1,n,\tv);
 \end{TV*}

\caption{\label{variations}Tableau de variations de $f$}
\end{figure}
   
\noindent De plus, $f(4)=4$. On a donc $f([0;4])=\left[ \dfrac{4}{5} ; 4\right] \subset [0;4]$.\\
Comme $u_0=2\in [0;4]$, la suite $(u_n)$ est bien définie.

\begin{remarque}
Dans la suite du chapitre, $I$ désigne un intervalle stable par $f$.
\end{remarque}

\subsubsection{Représentation graphique}
\noindent On munit le plan d'un repère orthonormé.\\
On note $\mathcal{C}$ la courbe représentative de $f$ et $\mathcal{D}$ la droite d'équation $y=x$.\\

\noindent Nous allons représenter graphiquement les termes de la suite $(u_n)$. Pour cela, nous allons procéder de la manière suivante :
\begin{enumerate}
\item[-] On place $u_0$ sur l'axe des abscisses.
\item[-] On lit $u_1$ sur l'axe des ordonnés en utilisant la courbe $\mathcal{C}$.
\item[-] On ramène $u_1$ sur l'axe des abscisses en utilisant la droite $\mathcal{D}$.
\item[-] On recommence \`a partir de $u_1$ pour construire $u_2$ et ainsi de suite \ldots
\end{enumerate}
%dessin escalier et escargot

\begin{figure}[!ht]
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=9cm,height=9cm]{images/escalier}   & \includegraphics[width=9cm,height=9cm]{images/escargot}
\end{tabular}
\end{center}
\caption{Construction des premiers termes de la suite $(u_n)$}
\end{figure}


\begin{remarque}
En général, cela permet assez vite de voir le comportement de la suite (monotonie, convergence,\ldots). Il reste ensuite à démontrer les résultats dont nous avons eu l'intuition grâce à cette construction.
\end{remarque}


\subsection{Monotonie de la suite $(u_n)$ }
	\subsubsection{Cas où $f$ est croissante sur l'intervalle $I$ }
\begin{pro}
Soient $f :I\to I$ et $(u_n)$ la suite définie par : $u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$.\\
Si $f$ est croissante sur $I$ alors $(u_n)$ est monotone. Plus précisément,
\begin{enumerate}
\item[$i)$] Si $u_0\geq u_1$, $(u_n)$ est décroissante.
\item[$ii)$] Si $u_0\leq u_1$, $(u_n)$ est croissante.
\end{enumerate}
\end{pro}

\begin{preuve}
Soit donc $f$ une fonction  croissante sur $I$.
 \begin{enumerate}
\item[$i)$] Supposons que  $u_0\geq u_1$ et démontrons que $u_{n+1}\leq u_{n},\; \forall n\in\N$.\\
\textit{Initialisation} : Par hypothèse $u_1\leq u_0$.\\
\textit{Hérédité }: Soit $n\in\N$. On suppose que $u_{n+1}\leq u_{n}$.\\
$f$ étant croissante sur $I$, elle préserve les inégalités. On a alors $f(u_{n+1})\leq f(u_{n})\Leftrightarrow \; u_{n+2}\leq u_{n+1}$\\
Conclusion : $u_{n+1}\leq u_{n},\; \forall n\in\N$. Donc la suite $(u_n)$ est décroissante.
\item[$ii)$] De la même manière, on montre que si $u_0\leq u_1$, $u_{n+1}\geq u_{n},\; \forall n\in\N$.
 \end{enumerate}
\end{preuve}

\begin{exem}
 Soit $(u_n)$ la suite définie par $u_0=2$ et $u_{n+1}=\sqrt{u_n+7}$.
\end{exem}
Introduisons la fonction $f$ définie sur $I=[0,+\infty[$ par $f(x)=\sqrt{x+7}$.\\
On remarque que $I$ est stable par $f$ et que $u_0\in I$ donc la suite est bien définie.\\

\noindent De plus $f$ est dérivable sur $I$ et $\forall x\geq 0,\; f'(x)=\dfrac{1}{2\sqrt{x+7}}\geq 0$ donc $f$ est croissante sur $I$.\\
Ce qui implique que $(u_n)$ est monotone. \\Comme $u_1=\sqrt{2+7}=3\geq u_0$, on en déduit que $(u_n)$ est croissante.

\begin{remarque}
 L'étude du signe de $f(x)-x$ sur l'intervalle $I$ permet de connaître le signe de $u_1-u_0$ (et donc le comportement de $(u_n)$)
 suivant les valeurs de $u_0$.
\end{remarque}

\subsubsection{Cas où $f$ est décroissante sur $I$ }
\begin{pro}
Soient $f$ une fonction définie sur un intervalle $I$ stable par $f$ et $(u_n)$ la suite définie par : $u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$.\\
Si $f$ est décroissante sur $I$ alors les suites extraites $(u_{2n})$ et $(u_{2n+1})$ sont de monotonies contraires (l'une est croissante, l'autre est décroissante) 
\end{pro}

\begin{preuve}
Remarquons tout d'abord que si $f$ est décroissante sur $I$ alors la fonction $g=f\circ f$ est croissante sur $I$.\\
Posons $v_n=u_{2n}\text{ et } w_n=u_{2n+1} \; \forall n\in \N$.
\begin{enumerate}
 \item[-]Démontrons que $(v_n)$ et $(w_n)$ sont monotones.

\noindent On a alors $v_{n+1}=u_{2(n+1)}=u_{2n+2}=f\circ f (u_{2n})=g(v_n)$. De même $w_{n+1}=g(w_n)$.\\
En appliquant le résultat précédent, on en déduit donc que les suites $(v_n)$ et $(w_n)$ sont monotones.\\
On détermine leur sens de variation en regardant le signe de $v_1-v_0 =u_2-u_0$ et le signe de $w_1-w_0=u_3-u_1$.

 \item[-] Vérifions \`a présent que $(v_n)$ et $(w_n)$ sont de monotonies contraires.\\
Pour cela supposons que $(v_n)$ est croissante.\\
On a en particulier $v_1\geq v_0 \Leftrightarrow u_2\geq u_0$.\\
Or $f$ est décroissante sur $I$ donc $f(u_2)\leq f(u_0)\Leftrightarrow u_3\leq u_1\Leftrightarrow w_1\leq w_0$.\\
On en déduit donc que $(w_n)$ est décroissante.

\noindent On montre de même que si $(v_n)$ est décroissante alors $(w_n)$ est croissante.
\end{enumerate}
\end{preuve}



\begin{exem}
 Soit $(u_n)$ la suite définie par $u_0=\dfrac32$ et $u_{n+1}=\dfrac{1}{u_n+1}$.
\end{exem}
Introduisons la fonction $f$ définie sur $I=]-1,+\infty[$ par $f(x)=\dfrac{1}{x+1}$.\\
On remarque que $I$ est stable par $f$ et que $u_0\in I$ donc la suite est bien définie.\\

\noindent De plus $f$ est dérivable sur $I$ et $\forall x>-1,\; f'(x)=-\dfrac{1}{(x+1)^2}\leq 0$ donc $f$ est décroissante sur $I$.\\
Comme $u_1=\dfrac25$ et $u_2=\dfrac57\leq u_0$, on en déduit que $(u_{2n})$ est décroissante et donc $(u_{2n+1})$ est croissante.

\subsubsection{Notre exemple}
\noindent Reprenons la suite $(u_n)$ définie par $u_0=2 \text{ et } u_{n+1}=\dfrac{1}{5}(4+u_n^2),\; \forall n\in\N$
et la fonction $f$  définie sur $I=[0;4]$ par $f(x)=\dfrac{1}{5}(4+x^2)$.\\
Nous avons déj\`a démontré que $I$ est stable par $f$ et que $f$ est croissante sur $I$. On en déduit que $(u_n)$ est monotone.\\
De plus $u_0=2$ et $u_1=\dfrac{1}{5}(4+2^2)=\dfrac{8}{5}$, donc $u_0 \geq u_1$. On en déduit que $(u_n)$ est décroissante.

\begin{remarque}
 On sait que $\forall n\in\N,\; u_n\in I$, en particulier $u_n\geq 0$.\\
La suite $(u_n)$ est donc décroissante, minorée par $0$.\\ 
D'après le théorème de convergence monotone, on en déduit que $(u_n)$ converge vers $\ell$ (avec $\ell\geq 0$).\\
Il nous reste encore \`a déterminer $\ell$.
\end{remarque}


\subsection{Convergence}
	\subsubsection{Limites éventuelles}
\begin{pro}(admise)\\
Soient $f :I\to I$ une fonction continue et $(u_n)$ la suite définie par : $u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$.\\ 
Si $(u_n)$ converge vers $\ell$ alors $f(\ell)=\ell$.
\end{pro}

\begin{preuve} (Une partie de preuve)\\
Supposons que $(u_n)$ converge vers $\ell$, $f$ étant continue sur $I$, on peut démontrer que $(f(u_n))$ converge vers $f(\ell)$.\\
Or $f(u_n)=u_{n+1}$ donc la suite $(u_{n+1})$ converge vers $f(\ell)$.\\
De plus $(u_{n+1})$ est une suite extraite de $(u_n)$ donc elle converge aussi vers $\ell$.\\
Par unicité de la limite de $(u_{n+1})$, on en déduit que $f(\ell)=\ell$.
 \end{preuve}


\begin{Def}
 Un réel $\ell$ vérifiant $f(\ell)=\ell$ est appelé
 \textbf{\textnormal{point fixe}} ou \textbf{\textnormal{point d'équilibre}} de la fonction $f$.
\end{Def}

\noindent La proposition ci-dessus se résume alors en disant que les limites \textbf{éventuelles}  de la suite $(u_n)$ sont 
les points fixes de la fonction $f$.\\

Par contre, elle ne permet pas de dire si la suite est convergente ou non!


\subsubsection{Notre exemple}
\noindent Reprenons la suite $(u_n)$ définie par $u_0=2 \text{ et } u_{n+1}=\dfrac{1}{5}(4+u_n^2),\; \forall n\in\N$
et la fonction $f$  définie sur $I=[0;4]$ par $f(x)=\dfrac{1}{5}(4+x^2)$.

\noindent Nous avons démontré que la suite $(u_n)$ est décroissante et minorée par $0$, donc elle converge 
vers un réel $\ell\geq 0$, d'après le théorème de convergence monotone.\\
On sait \`a présent que $\ell$ est un point fixe de $f$.

\noindent Résolvons l'équation $f(\ell)=\ell$ :\\
$f(\ell)=\ell \Leftrightarrow \dfrac{1}{5}(4+\ell^2)-\ell=0 \Leftrightarrow \dfrac{1}{5}(4-5\ell+\ell^2)=0\Leftrightarrow \ell=1 \text{ ou } \ell=4$.

\noindent Les seules limites possibles pour $(u_n)$ sont donc $1$ et $4$.\\


\noindent D'autre part, on sait que $(u_n)$ est décroissante donc $\forall n\in \N,\; u_n\leq u_0 \Leftrightarrow u_n\leq 2$. Donc $\ell\leq 2$.\\
On en déduit  que la suite $(u_n)$ converge vers $1$.

	\subsubsection{D'autres  exemples }
%ECS 1 page 370
\begin{exem}
Soit $(u_n)$ la suite  définie par $u_0=1 \text{ et } u_{n+1}=1+\dfrac{2}{u_n},\; \forall n\in\N$ 

Introduisons la fonction $f$ définie par $f(x)=1+\dfrac{2}{x}$.\\

\noindent On peut montrer que l'intervalle $I=[1;3]$ est stable par $f$ et que $f$ est décroissante sur $I$.\\

\noindent Posons $v_n=u_{2n}\text{ et } w_n=u_{2n+1} \; \forall n\in \N$.\\
On sait que les suites $(v_n)$ et $(w_n)$ sont monotones. De plus plus elles sont bornées (car tous les termes sont dans $I$), donc elles convergent respectivement vers $\ell$ et $\ell'$ ( avec $1\leq \ell \leq 3$ et $1\leq \ell' \leq 3$).\\

\noindent Posons $g=f\circ f$, c'est à dire $g(x)=f\circ f(x)=1+\dfrac{2}{1+\frac{2}{x}}=1+\dfrac{2x}{x+2}=\dfrac{3x+2}{x+2}$.\\
Comme $v_{n+1}=g(v_n)$ et $w_{n+1}=g(w_n)$, on sait que $\ell$ et $\ell'$ sont des points fixes de $g$.\\
Résolvons l'équation $g(x)=x$ :\\
$g(x)=x \Leftrightarrow \dfrac{3x+2}{x+2}-x=0 \Leftrightarrow x^2-x-2=0 \Leftrightarrow x=-1 \text{ ou } x=2$.\\
Donc $g$ admet un unique point fixe dans $I$ : $2$.\\
On en déduit que $\ell=\ell'=2$. Donc $(u_n)$ converge vers $2$ (car
les suites extraites $(u_{2n})$ et $(u_{2n+1})$ convergent vers $2$).

\begin{figure}[!ht]


\begin{center}
\includegraphics[width=9cm,height=9cm]{images/exemple5}  
\caption{\label{homogr}Les premiers termes de la suite définie par
  $u_{n+1}=f(u_n)$ et $u_0=1,1$}
\end{center} 

\end{figure}

\end{exem}
\begin{exem}
Soit $(u_n)$ la suite  définie par $u_0=2 \text{ et } u_{n+1}=\dfrac{1}{3}u_n^2-2u_n+3,\; \forall n\in\N$ 

On définit la fonction $f$ par $f(x)=\dfrac13 x^2-2x+3$.\\

\noindent On peut montrer que l'intervalle $I=[0;3]$ est stable par $f$ et que $f$ est décroissante sur $I$.\\

\noindent Posons $v_n=u_{2n}\text{ et } w_n=u_{2n+1} \; \forall n\in \N$.\\
On sait que les suites $(v_n)$ et $(w_n)$ sont monotones.\\
De plus plus elles sont bornées (car tous les termes sont dans $I$), donc elles convergent respectivement vers $\ell$ et $\ell'$
 ( avec $0\leq \ell \leq 3$ et $0\leq \ell' \leq 3$).\\

\noindent on a $v_0=u_0=2$, $w_0=u_1=\dfrac{1}{3}$, $v_1=u_2=\dfrac{64}{27}$ donc $v_0\leq v_1$.\\
On en déduit que $(v_n)$ est croissante et donc que $(w_n)$ est décroissante.\\
En particulier : $\forall n\in \N,\; v_n\geq v_0$ donc $\ell\geq 2$ et
$\forall n\in \N,\; w_n\leq w_0$ donc $\ell'\leq \dfrac{1}{3}$ d'où $\ell\neq \ell'$.\\

\noindent Les suites  $(u_{2n})$ et $(u_{2n+1})$ convergent vers deux limites distinctes donc $(u_n)$ diverge.




\begin{figure}[!ht]


\begin{center}
\includegraphics[width=9cm,height=9cm]{images/exemple6}  
\caption{\label{homogr}Les premiers termes de la suite définie par
  $u_{n+1}=f(u_n)$ et $u_0=2$}
\end{center} 

\end{figure}



\end{exem}
\subsection{Étude d'un exemple : une suite homographique}
\noindent Soit $(u_n)$ la suite définie par : $ u_0\in[0,1] \text{ et } u_{n+1}=\dfrac{2u_n+3}{u_n+4},\; \forall n\in\N$.\\
On définit la fonction $f$ par $f(x)=\dfrac{2x+3}{x+4}$.
\noindent Nous allons étudier la suite $(u_n)$.\\


\begin{enumerate}
 \item Vérifier que l'intervalle $I=[0;1]$ est stable par $f$ et donc que la suite $(u_n)$ est bien définie.
\item Étudier la monotonie de $(u_n)$.
\item Démontrer que $(u_n)$ est convergente et préciser sa limite.
\end{enumerate}


\begin{prof}

  
  Regardons tout d'abord les premiers termes de la suite: 
    \begin{center}
      \includegraphics[width=8cm,height=8cm]{images/homogr}
    \end{center}

    Il semble que la suite $(u_n)$ soit croissante et converge
  vers $1$. Il ne reste plus qu'à le démontrer\ldots


 La fonction $f$ est définie, dérivable sur $\R-\{-4\}$.\\
    $\forall x\neq -4,\; f'(x)=\dfrac{2(x+4)-(2x+3)}{(x+4)^2}=\dfrac{5}{(x+4)^2}>0$, donc $f$ est croissante\\

    De plus
    $\underset{x\to -\infty}{\lim f(x)}=\underset{x\to +\infty}{\lim
      f(x)}=2$ , $\underset{x\to -4^-}{\lim f(x)}=+\infty $ et
    $\underset{x\to -4^+}{\lim f(x)}=-\infty$.\\
    On obtient donc le tableau de variations suivant: 
      \begin{center}
  
 \begin{TV*}{102} 
   TV([-infinity,+infinity],[-4],"f","x",(2*x+3)/(x+4),1,n,\tv);
 \end{TV*}

 
\end{center}
\end{prof}

\begin{prof}
Et on a $f(0)=\dfrac{3}{4}$ et $f(1)=1$ donc $f([0;1])=\left[\dfrac{3}{4};1\right]\subset [0;1]$.\\
On en déduit que l'intervalle $I=[0;1]$ est stable par $f$.\\ Comme
$u_0\in I$, la suite $(u_n)$ est bien définie.
 
 On a vu que $f$ est croissante sur $I$, donc $(u_n)$ est monotone.\\
  Pour savoir si elle est croissante ou décroissante, nous allons étudier le signe de $f(x)-x$ sur l'intervalle $[0,1]$ :\\
  $f(x)-x=...=\dfrac{-x^2-2x+3}{x+4}$, or $x+4>0$ sur $[0,1]$ donc nous allons étudier le signe de $-x^2-2x+3$.\\
  $\Delta=16$, $x_1=-3$, $x_2=1$. Le polynôme est positif sur $[-3,1]$ donc sur $[0,1]$.\\
  On en déduit que : $\forall u_0\in[0,1]$, $u_1-u_0\geq 0$ donc
  $(u_n)$ est croissante.
 On sait que $(u_n)$ est croissante et que
  $\forall n\in\N,\; 0\leq u_n\leq 1$ donc, d'après le théorème de
  convergence monotone,
  $(u_n)$ converge vers $\ell$ (avec $0\leq \ell\leq 1$).\\

  \noindent Comme $f$ est dérivable (donc continue\ldots), $\ell$ est un point fixe de $f$. \\
  Résolvons l'équation $f(\ell)=\ell$ :\\
  $f(\ell)=\ell\Leftrightarrow \dfrac{2\ell+3}{\ell+4}=\ell \Leftrightarrow \ell^2+2\ell-3=0 \Leftrightarrow \ell=-3 \text{ ou } \ell=1$.\\
  Or $-3\notin I$ donc on en déduit que $\ell=1$.\\

  \noindent La suite $(u_n)$ converge vers $1$.

\end{prof}

	
\end{document}
