%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Les_suites_numeriques.tex]{subfiles}

\begin{document}

%\setcounter{section}{1}
%\input{pagedegarde}
\section{Limite d'une suite}
\label{sec:limites-dune-suite}


\subsection{Définitions et premières propriétés}
\noindent Dans ce chapitre, nous allons étudier le comportement asymptotique d'une suite.

 
Cela signifie que nous allons nous intéresser au comportement de $u_n$, pour les grandes valeurs de $n$. 




\subsubsection{Définitions}
\begin{Def}
Soit $\ell\in\R$. \\On dit que la suite $(u_{n})$ \textbf{tend vers
  $\ell$} (ou converge vers $\ell$) si << on peut 
rendre $|u_n-\ell|$ aussi petit que l'on veut dès que $n$ est
suffisamment grand>>.\\ Autrement dit, 

\begin{equation*}
\forall \varepsilon>0,\exists n_{0}\in \mathbb{N}\text{ tel que : }
\forall n\geqslant n_{0},\;\left\vert u_{n}-\ell\right\vert <\varepsilon
\text{.}
\end{equation*}
Dans ce cas, on notera : $\displaystyle\lim_{n\to +\infty} u_{n}=\ell$ ou encore $\lim u_{n}=\ell$ , $u_n \to \ell$. \\
Une telle suite est dite \textbf{convergente}.
\end{Def}


\begin{exemple}
  La suite définie par $u_n=\frac{1}{n}$ converge vers 0.
\end{exemple}


\begin{remarque}
$\left\vert u_{n}-\ell\right\vert <\varepsilon \Leftrightarrow \ell-\varepsilon
<u_{n}<\ell+\varepsilon \Longleftrightarrow u_{n}\in ]\ell-\varepsilon
,\ell+\varepsilon \lbrack $.\\
La définition se traduit alors en disant qu'à partir d'un certain
rang $n_{0}$ tous les termes de la suite sont dans l'intervalle $]\ell-\varepsilon ,\ell+\varepsilon \lbrack $.
\begin{figure}[!ht]

%\begin{minipage}{\linewidth}
\begin{center}
\includegraphics{images/limite.pdf} 
\caption{\label{limite}Suite $(u_n)$ convergente, de limite $\ell$}
\end{center} 
%\end{minipage}
\end{figure}
\end{remarque}


\begin{Def}
La suite $(u_{n})$ est \textbf{divergente} si elle n'est pas convergente.
\end{Def}
Il y a deux types de suites divergentes:

\begin{itemize}
\item les suites qui tendent vers l'infini;
  
\item les suites qui n'ont pas  de limite.
\end{itemize}

Voici la définition d'une suite qui tend vers l'infini.

\begin{Def}
\begin{enumerate}
\item[]
\item[-] On dit que la suite $\left( u_{n}\right) $ tend vers $+\infty
  $  si <<on peut rendre $u_n$ aussi grand que l'on veut dès que $n$ est suffisamment grand>>.
\begin{equation*}
\forall A>0,\exists n_{0}\in \mathbb{N}\text{ tel que  } \forall n\geq n_{0},u_{n}>A\text{.}
\end{equation*}
On notera dans ce cas :$\displaystyle\lim_{n\to +\infty} u_{n}=+\infty $ ou encore  $\lim u_{n}=+\infty $ , $u_n \to +\infty$.
\item[-] On dit que la suite $\left( u_{n}\right) $ tend vers $-\infty $ si la suite $\left(- u_{n}\right) $ tend vers $+\infty $
\begin{equation*}
\forall A>0,\exists n_{0}\in \mathbb{N}\text{ tel que  } \forall n\geq n_{0},u_{n}<-A\text{.}
\end{equation*}
On notera dans ce cas : $\displaystyle\lim_{n\to +\infty} u_{n}=-\infty $ ou encore  $\lim u_{n}=-\infty $ , $u_n \to -\infty$.
\end{enumerate}
\end{Def}

\begin{exemple} Suites divergentes.

  
  \begin{enumerate}
  \item Une suite arithmétique dont la raison est non nul diverge vers
    l'infini.
  \item La suite définie par $u_n=(-1)^n$ n'a pas de limite.
  \end{enumerate}
\end{exemple}


\subsubsection{Premières propriétés}
\begin{thm} \textnormal{(Unicité de la limite)}\\
La limite d'une suite $(u_{n})$ convergente est unique.
\end{thm}


\begin{figure}[!ht]
\begin{minipage}{\linewidth}
\begin{center}
\includegraphics{images/unicite.pdf} 
\end{center} 
\end{minipage}
\end{figure}

\begin{preuve}
La suite $(u_n)$ est convergente. Supposons qu'elle converge $\ell$ et $\ell'$ et montrons qu'alors $\ell=\ell'$.\\

\noindent Soit $\varepsilon>0$,\\
$u_n\to \ell$ donc $\exists n_{1}\in \mathbb{N}\text{ tel que : }
\forall n\geqslant n_{1},\;\left\vert u_{n}-\ell\right\vert <\varepsilon$ et\\
$u_n\to \ell'$ donc $\exists n_{2}\in \mathbb{N}\text{ tel que : }
\forall n\geqslant n_{2},\;\left\vert u_{n}-\ell'\right\vert <\varepsilon$ .\\
Posons $n_0=max(n_1,n_2)$, on a alors, $\forall n\geq n_0$ :\\
$\vert \ell-\ell' \vert\leq \vert \ell-u_n \vert+\vert u_n-\ell' \vert\leq 2.\varepsilon$.\\

\noindent Autrement dit, $\forall \varepsilon>0, \vert \ell-\ell' \vert\leq 2.\varepsilon$ d'où $\vert \ell-\ell'\vert =0\Leftrightarrow \ell=\ell'$.

\end{preuve}

\begin{thm}
Si la suite $\left( u_{n}\right) $ converge vers $\ell$, alors toute sous-suite
de $\left( u_{n}\right) $ converge aussi vers $\ell$.
\end{thm}

\begin{preuve}
Soit $\left( v_{n}\right) $ une sous-suite de $\left( u_{n}\right) $. Alors $%
v_{n}=u_{f(n)}$ où $f$ est une fonction strictement croissante de $\mathbb{N}$ vers $\mathbb{N}$.\\

\noindent La démonstration se fera en deux étapes :\\

\noindent \underline{Étape 1} : Montrons par récurrence sur $n$ que : $\forall
n\in \mathbb{N}$ on a : $f(n)\geqslant n$.


\textit{Initialisation} : la propriété est vraie 
à l'ordre $0$, car $f(0)\in \mathbb{N}$ donc $f(0)\geqslant
0$.\\
\textit{Hérédité} : Supposons la propriété vraie à l'ordre $n$ (hypothèse de récurrence), soit : $f(n)\geq n$.\\
Montrons qu'elle est vraie à l'ordre $n+1$.\\
Comme $f$ est strictement croissante : $n+1>n\Rightarrow f(n+1)>f(n)$.\\
Mais puisque : $f(n)\geqslant n$ on aura : $f(n+1)>n$, d'où : $f(n+1)\geqslant n+1$.\\
La propriété est donc vérifiée au rang $n+1$.\\
\textit{Conclusion} : $\forall n\in \mathbb{N}$ on a : $f(n)\geqslant n$.\\


\noindent \underline{Étape 2}: Montrons que $(v_n)$ converge vers
$\ell$.

$\lim u_{n}=\ell$ $\Rightarrow \forall \varepsilon>0
,\;\exists n_{0}\in \mathbb{N}$ tel que $\forall n\geq n_{0}, \left\vert u_{n}-\ell\right\vert <\varepsilon$.\\
Si $n\geqslant n_{0}$, alors on a : $f(n)\geqslant n\geqslant n_{0}$ et donc
: $\left\vert u_{f(n)}-\ell\right\vert <\varepsilon $, soit encore : $%
\left\vert v_{n}-\ell\right\vert <\varepsilon$.\\
Donc $\forall \varepsilon>0,\; \exists n_{0}\in \mathbb{N}$ tel que $\forall 
n\geqslant n_{0}$ on a : $\left\vert v_{n}-\ell\right\vert <\varepsilon $,
autrement dit : $\lim v_{n}=\ell$.
\end{preuve}



\begin{remarque}
Ce théorème est en particulier utile, sous sa forme contraposée,  pour montrer qu'une suite diverge.
\end{remarque}

\begin{exem}
Reprenons la suite $\left( u_{n}\right) $ définie par :
$u_{n}=\left(-1\right) ^{n}$.\\

\begin{etud}
  Montrons que $(u)$ diverge.
\end{etud}

\begin{prof}
  La sous-suite $\left( u_{2n}\right) $ est constante, égale à $1$ donc converge vers $1$. \\
  La sous-suite $\left( u_{2n+1}\right) $ est également constante,
  égale à $-1$ donc
  converge vers $-1$.\\
  On en déduit que $(u_n)$ diverge car si elle convergeait vers une
  limite $\ell$, toutes ses sous-suites \\convergeraient vers la même
  limite, ce qui n'est pas le cas.
\end{prof}
\end{exem}
\begin{thm}
$(u_n)$ converge vers $\ell$ $\Leftrightarrow$ les sous-suites $(u_{2n})$ et $(u_{2n+1})$ convergent vers $\ell$.
\end{thm}

\begin{preuve}$\quad$

\fbox{$\Rightarrow$} C'est en fait une conséquence du théorème précédent.\\
\fbox{$\Leftarrow$}
Soit $p\in\N$, comme $(u_{2n})$ tend vers $\ell$ on peut écrire:
\begin{equation*}
\exists n_{1}\in \mathbb{N} \text{ tel que } \forall n\geqslant n_{1},\left\vert  u_{2n}-\ell\right\vert <\varepsilon.
\end{equation*}

De même le fait que $(u_{2n+1})$ tend vers $\ell$ implique

\begin{equation*}
\exists n_{2}\in \mathbb{N} \text{tel que  }\forall n\geqslant n_{2},\left\vert u_{2n+1}-\ell\right\vert <\varepsilon.
\end{equation*}


\noindent Posons $n_{0}=\max \left( 2n_{1},2n_{2}+1\right) $, on a bien évidemment
:  $\forall n\geqslant n_{0},\left\vert u_{n}-\ell\right\vert <\varepsilon$.\\
Et donc  $(u_{n})$converge vers $\ell$.
\end{preuve}
 

\subsubsection{Limite des deux suites de référence}
\begin{pro}
 Soit $(u_n)$ une suite arithmétique de raison $r$ et de premier terme $u_0$.
\begin{enumerate}
 \item[-] Si $r=0$, alors la suite $(u_n)$ est constante et converge vers $u_0$
 \item[-] Si $r>0$, alors $\displaystyle \lim_{n\to +\infty} u_n=+\infty$
 \item[-] Si $r<0$, alors $\displaystyle \lim_{n\to +\infty} u_n=-\infty$
\end{enumerate}
\end{pro}

\begin{preuve}[fac]
Il suffit d'exprimer le terme général de la suite sous la forme $u_n=u_0+nr$...
\end{preuve}

Le cas de l'éventuelle limite d'une suite géométrique se règle grâce à la
proposition suivante:

\begin{pro}
 \begin{enumerate}
\item[]
  \item[-] Si $q>1$,  alors $\displaystyle \lim_{n\to +\infty} q^n=+\infty$
  \item[-] Si $q=1$, alors la suite $(q^n)$ est constante égale à $1$ et converge vers $1$
  \item[-] Si $-1<q<1$,  alors $\displaystyle \lim_{n\to +\infty} q^n=0$
  \item[-] Si $q\leq -1$,  alors la suite $(q^n)$ n'a pas de limite
 \end{enumerate}

\end{pro}

\begin{preuve}[fac]
 Démontrons le premier et le dernier point :\\
 \begin{enumerate}
\item[-] Si $q>1$, on a alors $q^n=e^{n\ln q}$ avec $\ln q>0$.\\
Donc   $n\ln q\to +\infty$ et $e^{n\ln q}\to +\infty$, d'où $\displaystyle \lim_{n\to +\infty} q^n=+\infty$.
\item[-] Si $q\leq -1$, les suites extraites $(q^{2n})$ et $(q^{2n+1})$ ont des limites distinctes donc la suite $(q^n)$ n'a pas de limite.
Plus précisement :\\
si $q=-1$, $q^{2n}=1$ et $q^{2n+1}=-1$ donc les suites $(q^{2n})$ et $(q^{2n+1})$ sont constantes et convergent respectivement vers $1$ et $-1$.\\
si $q<-1$, $q^{2n}=(q^2)^n$ avec $q^2>1$ donc, d'après ce qui a été vu précedemment, on a\\ $q^{2n}=(q^2)^n \to +\infty$.\\
D'autre part $q^{2n+1}=q(q^2)^n$ avec $q^2>1$ et $q<0$ donc  $q^{2n+1}=q(q^2)^n \to -\infty$.\\

\noindent Les suites  $(q^{2n})$ et $(q^{2n+1})$ ayant des limites distinctes, on peut affirmer que  la suite $(q^n)$ n'a pas de limite.
 \end{enumerate}
\end{preuve}


\subsection{Opérations sur les limites}
\noindent  $(u_n)$ et $(v_n)$ sont deux suites numériques convergentes ou tendant vers l'infini. On s'intéresse dans ce paragraphe au comportement asymptotique des suites $(u_n+v_n)$, $(u_n.v_n)$, $\left(\dfrac{1}{u_n} \right)$ \ldots 
\subsubsection{Somme de deux suites }
\noindent  Les résultats concernant la limite de $(u_n+v_n)$ sont
résumés dans le tableau suivant:


\begin{center}
  \begin{tabular}{|c|c|c|c|}
    \hline
    \backslashbox{  $\lim v_n$}{$\lim u_n$}    & $-\infty$ & $\ell\in \R$& $+\infty$  \\
   
   
    \hline
    $-\infty$   & $-\infty$ & $-\infty$&$\text{?}$\\
    \hline
    $\ell' \in \R$   & $-\infty$ & $\ell+\ell'$& $+\infty$\\
    \hline
    $+\infty$  & $\text{?}$ & $+\infty$&$+\infty$ \\
     \hline
  \end{tabular}
\end{center}


\noindent Les points d'interrogations représentent les cas où on ne peut pas  conclure de manière générale.\\
 On parle alors de \textbf{formes indéterminées}.

\subsubsection{Produit de deux suites}

\noindent Les résultats concernant la limite de $(u_n\times v_n)$ sont résumés dans le tableau suivant:
\begin{center}
  \begin{tabular}{|c|c|c|c|}
    \hline
    \backslashbox{  $\lim v_n$}{$\lim u_n$}  & $0$ & $\ell$ & $\infty$  \\\hline
       $0$     & $0$ & $0$ & ?\\
    \hline
    $\ell'$ & 0 &  $\ell\ell'$ & $\infty$\\
    \hline
   $\infty$&?&$\infty$&$\infty$\\
    \hline
  \end{tabular}
\end{center}
\begin{remarque}
  Le signe de la limite se déduit de la règle des signes.
\end{remarque}
\begin{remarque}
En prenant pour $(v_n)$ une suite constante, on peut déduire de ce tableau la limite de la suite $(\lambda u_n)$ en fonction de $(u_n)$. 
\end{remarque}


\begin{exem}
  Calculer la limite de la suite définie par:

  \begin{equation*}
   v_n=(-n^2+1)\exp(n) 
  \end{equation*}
\end{exem}
\begin{exem}
Considérons la suite $\left( u_{n}\right) $ définie par:
$u_{n}=2n^2-3n+1$\\

\begin{etud}
  Quelle est la limite de la suite $(u_n)$?
\end{etud}

\begin{prof}
  On a $\lim n^2=+\infty$ donc $\lim 2n^2=+\infty$ et $\lim -3n=-\infty$.\\
  En calculant la limite de $(u_n)$, on aboutit à \textquotedblright $+\infty -\infty $\textquotedblright  qui est une forme indéterminée.\\
  Nous verrons plus tard comment résoudre ce problème.
\end{prof}

\end{exem}

\subsubsection{Inverse d'une suite}

\noindent Les résultats concernant la limite de $\left( \dfrac{1}{u_n}\right) $ sont résumés dans le tableau suivant  :\\


\begin{center}
  \begin{tabular}{|c|c|c|c|}
    \hline
    $u_n$ &$0$ & $\ell$&$\infty$\\
    \hline
    
    $\frac{1}{u_n}$ & $\infty$&$\frac{1}{\ell}$&0\\\hline
    
  \end{tabular}
\end{center}


\begin{remarque}
Dans le cas d'un quotient, on peut voir $\dfrac{u_n}{v_n}$ comme le produit $u_n \times \dfrac{1}{v_n}$.\\
 La limite de
 $\dfrac{u_n}{v_n}$ peut être obtenue en utilisant les deux tableaux ci-dessus.  
\end{remarque}



\begin{exem}
Considérons la suite $\left( u_{n}\right) $ définie par:
$u_{n}=\dfrac{1}{-2n+5}$.

\begin{prof}
  \noindent On sait que $\lim n=+\infty$ donc $\lim -2n+5=+\infty$.

  \noindent En appliquant alors les résultats précédents on en déduit
  que $\lim u_{n}=0$.
\end{prof}

\end{exem}

\subsubsection{Limite de suite et fonction}

On s'intéresse ici à une suite $v$ définie par $v_n=f(u_n)$ où $u$ est
une suite numérique et $f$ une fonction d'une variable réelle. Il y a
de multiple cas de figure, voici un exemple de résultat.


\begin{pro}
  Soit $f$ une fonction définie sur un intervalle $I=]a;b[\subset \R$ et une
  suite $u$ à valeur dans $I$.

  Si $\lim u_n=b $ et $\lim\limits_{x\to b}f(x)=\ell\in\overline{R}$
  alors $\lim f(u_n)=\ell$
\end{pro}


\begin{exem}
Considérons la suite $\left( v_{n}\right) $ définie par : $v_{n}=\sqrt{n^{3}+5n+1}$.

\begin{etud}
  Calculons $\lim v_{n}$.
\end{etud}
\begin{prof}
  On a : $\lim n^3+5n+1=+\infty$ donc $\lim v_{n}=+\infty$.
\end{prof}

\end{exem}

\subsubsection{Formes indéterminées}

\noindent On appelle formes indéterminées les expressions faisant intervenir
des limites pour lesquelles les tableaux précédents ne
permettent pas de conclure. Il y en a de quatre sortes:


\begin{itemize}
\item Pour l'addition : <<$+\infty -\infty $>> 
\item Pour le produit : <<$0\times \infty $ >>
\item Pour le quotient : <<$\dfrac{\infty }{\infty }$>> et <<$\dfrac{0}{0}$>>.
\end{itemize}


Une indétermination ne signifie en aucune manière que la limite
n'existe pas.\ Cela signifie simplement que  l'on ne peut pas la déterminer directement, c'est à
dire en utilisant les résultats précédents. Pour calculer la limite, il faut  <<lever
l'indétermination>> en transformant l'expression de départ en une expression équivalente dont
on sait calculer la limite. Il convient toujours de bien identifier le
type de forme indéterminée afin de choisir la bonne méthode pour
calculer la limite.


\noindent Voici trois exemples de formes indéterminées du type <<$\dfrac{\infty }{\infty }$>> qui aboutissent à trois résultats différents :


\begin{exem}
Considérons la suite $\left( u_{n}\right) $ définie par :
$u_{n}=\dfrac{n^{3}+2n-1}{n^{4}}$.


\begin{etud}
  Cherchons la limite de la suite $(u)$.
\end{etud}
\begin{prof}



\noindent D'une part, on a : $\lim (n^{4})=+\infty$ et d'autre part :
$\lim \left( n^{3}+2n-1\right) =+\infty $ .\\
On se trouve en présence d'une indétermination du type $"\dfrac{\infty }{\infty }"$.\\
On lève cette indétermination en écrivant $u_n$ de la manière suivante :\\ $u_{n}=\dfrac{n^{3}\left(1+\dfrac{2}{n}-\dfrac{1}{n^2}\right) }{n^{4}}=\dfrac1n.\left(1+\dfrac{1}{n}+\dfrac{1}{n^2}\right) $.\\
On voit alors que : $\lim\left(1+\dfrac{1}{n}+\dfrac{1}{n^2}\right) =1$ et donc $\lim u_{n}=0$.
\end{prof}
\end{exem}


\begin{exem}
\label{lfi}
Considérons la suite $\left( u_{n}\right) $ définie par : $u_{n}=\dfrac{n^{3}+n+1}{n^2}$.

\begin{etud}
  Cherchons la limite de la suite $(u)$.
\end{etud}
\begin{prof}
  \noindent Pour lever l'indétermination, on écrit : $u_{n}=n+\dfrac{1}{n}+\dfrac{1}{n^{2}}$.\\
  Comme $\lim n=+\infty $ et que
  $\lim \left( \dfrac{1}{n}+\dfrac{1}{n^{2}}\right)=0$, on obtient le
  résultat : $\lim u_{n}=+\infty $.

\end{prof}
\end{exem}
\begin{exem}
Considérons la suite $\left( u_{n}\right) $ définie par : $u_{n}=\dfrac{2n^{3}+n+1}{n^3}$.


 Par la méthode développée dans l'exemple \ref{lfi}, on trouve $\lim u_{n}=2$.


\end{exem}
\begin{remarque}
\textbf{Dans tous les cas de quotients de polyn\^{o}mes, la limite en
  l'infini est celle du quotient des termes de plus haut degré}.
\end{remarque}
\begin{exem} 
Soit $(u_n)$, $(v_n)$ et $(w_n)$ les suites définies par
$u_n=n^3-2n+1$, $v_n=\ln\left(\dfrac{n+3}{n^2+1}\right)$ et $w_n=(3n+2)\e^{-n}$. 

\begin{etud}
  Cherchons les limites de ces trois suites.
\end{etud}

\begin{prof} On a:
  $\displaystyle\lim_{n\to+\infty} u_n=+\infty$.\\
  $\displaystyle\lim_{n\to+\infty} u_n=-\infty$.\\
  $\displaystyle\lim_{n\to+\infty} u_n=0$.\\
\end{prof}
\end{exem}




\subsection{Théorèmes de comparaison}
\subsubsection{Pour les suites convergentes}
\begin{thm}
Soit $(u_n)$ une suite convergente de limite $\ell$.
$$\text{Si }\forall n\in\N \text{ , }u_{n}\geqslant 0\text{ alors } \ell\geqslant 0$$
\end{thm}

\begin{preuve}
Supposons que $\ell<0$, alors on peut trouver $\varepsilon>0$ tel que $\ell<-\varepsilon<0$.
\begin{figure}[!ht]
\begin{center}
 \includegraphics[width=8cm,height=2.5cm]{images/neg.pdf} 
\end{center}
\end{figure}

\noindent Comme $u_n\to \ell$, $\exists n_0\in \N$ tel que $\forall n\geq n_0, \; \vert u_n-\ell\vert <\varepsilon$.\\
Donc  $\forall n\geq n_0, \; \ell-\varepsilon<u_n <\ell+\varepsilon$. Or   $\ell<-\varepsilon$ donc $\ell+\varepsilon<0$.\\
On en déduit que $\forall n\geq n_0, \; u_n<0$...ce qui est contraire à l'hypothèse  $u_{n}\geqslant 0$.\\
Donc, on a nécessairement $\ell\geq 0$ !
\end{preuve}

\begin{bclogo}[logo = \bcdanger,arrondi=0.1]{Attention }

 $\text{Si }\forall n\in\N \text{ , }u_{n}>0$, on ne peut pas affirmer que $\lim u_n> 0$.\\
Par exemple, pour $u_n=\dfrac1n$, on a $\forall n\in\N^* \text{ , }u_{n}> 0$ et $\lim u_n= 0$.\\
\end{bclogo}
\begin{cor}
Soient $(u_n)$ et $(v_n)$ deux suites convergeant respectivement vers $\ell$ et $\ell'$.
$$\text{Si }\forall n\in\N \text{ , }u_{n}\geqslant v_{n}\text{ alors } \ell\geqslant \ell'$$
\end{cor}

\begin{preuve}[fac]
Il suffit d'appliquer le théorème précédent à la suite $(w_n)$ définie par $\forall n\in\N, \; w_n=u_n-v_n$.\\
Alors : $(w_n)$ est une suite convergente (de limite $\ell-\ell'$) et qui vérifie $ \forall n\in\N \text{ , }w_{n}\geqslant 0$.\\
Donc  $\ell-\ell'\geqslant 0 \Leftrightarrow \ell\geqslant \ell'$
\end{preuve}

\begin{remarque}
Ce théorème s'énonce encore en disant que les inégalités \textbf{larges} sont conservées par passage à la limite.
\end{remarque}

\begin{thm}\textnormal{(Théorème d'encadrement)}\\
Si  
$\left. 
\begin{array}{r}
\forall n\in\N, u_{n}\leqslant
v_{n}\leqslant w_{n} \\ 
\text{et }\lim u_{n}=\lim w_{n}=\ell%
\end{array}%
\right\} $ alors la suite $\left( v_{n}\right) $  converge vers $\ell$.
\end{thm}

\begin{preuve}
 Soit $\varepsilon>0$.\\
 $u_n\to \ell$ donc $\exists n_1\in \N$ tel que $\forall n\geq n_1, \; \vert u_n-\ell\vert <\varepsilon\Leftrightarrow \ell-\varepsilon<u_n<\ell+\varepsilon$.\\
 $w_n\to \ell$ donc $\exists n_2\in \N$ tel que $\forall n\geq n_2, \; \vert w_n-\ell\vert <\varepsilon\Leftrightarrow \ell-\varepsilon<w_n<\ell+\varepsilon$.\\

\noindent Posons $n_0=max\{n_1,n_2\}$.\\
$\forall n\geq n_0,\; \ell-\varepsilon<u_n\leq v_n\leq w_n<\ell+\varepsilon$ donc $\forall n\geq n_0,\;\ell-\varepsilon< v_n<\ell+\varepsilon\Leftrightarrow \vert v_n-\ell\vert <\varepsilon$.\\
On a donc bien $v_n\to \ell$.
\end{preuve}


\begin{cor}$\quad$

Si $\left. 
\begin{array}{l}
\lim v_{n}=0 \\ 
\forall n\geqslant n_{0},\; \left\vert u_{n}-\ell\right\vert
\leqslant v_{n}%
\end{array}%
\right\} $ alors  la suite $(u_n)$ converge vers $\ell$.
\end{cor}

\subsubsection{Pour les suites tendant vers l'infini}

\begin{thm}$\quad$

Si $\left\{ 
\begin{array}{l}
\lim v_{n}=+\infty\\
\forall n\in\N,\; u_{n}\geqslant v_{n}
\end{array} \right.$
alors $\lim u_n=+\infty$.
\end{thm}

\begin{preuve} Soit $A>0$.\\
$v_n\to +\infty$ donc $\exists n_0\in\N$ tel que $\forall n\geq n_0,\; v_n>A$.\\
Or $u_{n}\geqslant v_n$ donc $\forall n\geq n_0,\; u_n\geq v_n>A$ d'où $u_n\to +\infty$.
\end{preuve}

\begin{exem}
\end{exem}
Soit $(u_n)$ la suite définie par $u_n=\displaystyle \sum_{k=1}^{n} \dfrac{1}{\sqrt{k}}, \; \forall n\in\N^*$.\\
Soit $n\in\N^*$. Pour tout $k$ compris entre $1$ et $n$, on a $\dfrac{1}{\sqrt{k}}\geq \dfrac{1}{\sqrt{n}}$ donc $u_n\geq \dfrac{n}{\sqrt{n}}$.\\
On a donc $u_n\geq \sqrt{n}$ et $\sqrt{n}\to +\infty$ donc $u_n\to +\infty$.




\subsection{Convergence monotone}
\begin{remarque}
Si la suite $(u_n)$ est convergente alors elle est bornée.\\
Mais la réciproque de cette propriété est fausse...\\
Nous allons voir cependant que, sous certaines hypothèses, une suite bornée converge.
\end{remarque}

\subsubsection{Théorème fondamental}
\begin{thm} (Admis)
Soit $(u_n)$ une suite \textbf{croissante}.

\begin{enumerate} 
 \item[$1)$] Si $(u_n)$ est  majorée (par $M$) alors converge vers $\ell$ et on a $\ell \leq M$.
\item[$2)$]  Si $(u_n)$ n'est pas majorée alors elle tend vers $+\infty $.
\end{enumerate}

\end{thm}


\begin{bclogo}[logo = \bcdanger,arrondi=0.1]{Attention }

 La limite de la suite, quand elle existe, n'est pas égale au majorant. On peut juste affirmer qu'elle est inférieure ou égale à celui-ci.
\end{bclogo}

\begin{exem}
Soit $(u_n)$ la suite définie par: 


\begin{equation*}
  \sys{u_0=1}{u_{n+1}=\frac{1}{2}u_n+3} \quad \forall n\in \N
\end{equation*}
\begin{etud}
  Déterminons la limite de cette suite.
\end{etud}

\begin{prof}

\noindent On peut démontrer par récurrence sur $n$ que $\forall n\in \N, u_n\leq 6$.\\
De plus, $\forall n \in \N,u_{n+1}-u_n=-\dfrac{1}{2}u_n+3\geq 0$ donc la suite est croissante.\\

\noindent  $(u_n)$ est croissante et majorée donc elle converge et sa limite vérifie  $\ell\leq6$.\\

\noindent  Démontrons à présent que si $(u_n)$   converge, alors $\lim u_n=6$ : \\
$u_n\to \ell$ donc toute suite extraite de $(u_n)$ tend vers $\ell$. En particulier $u_{n+1}\to \ell$.\\
D'autre part, par opération sur les limites, on peut dire que $u_{n+1}=\dfrac{1}{2}u_n+3 \to \dfrac{1}{2}\ell+3$.\\
Par unicité de la limite, on en déduit que $\ell=  \dfrac{1}{2}\ell+3 \Leftrightarrow \ell=6$.\\

\noindent Conclusion : $u_n\to 6$

\end{prof}
\end{exem}

\begin{remarque}
Nous avons pu démontrer que $(u_n)$ converge et trouver sa limite, sans exprimer le terme général en fonction de n.
\end{remarque}


\begin{exem}
Reprenons la suite définie par : $u_0=1$ et $u_{n+1}=\sqrt{u_n+6}$.\end{exem}
Nous avons vu précédemment que $(u_n)$ est bornée (minorée par $0$ et majorée par $3$).\\



\begin{etud}
  Déterminons la limite de cette suite.
\end{etud}
\begin{prof}
  Démontrons que cette suite est croissante, en étudiant le signe de $u_{n+1}-u_n$ :\\
$\forall n\in \N$, $u_{n+1}-u_n=\sqrt{u_n+6}-u_n=\dfrac{u_n+6-u_n^2}{\sqrt{u_n+6}+u_n}$.\\
Le dénominateur étant positif, $u_{n+1}-u_n$ est du signe de $-u_n^2+u_n+6$.\\
Or $\forall x\in [-2,3]$, $-x^2+x+6\geq 0$ (étude du signe d'un polynôme de degré $2$) et $0\leq u_n\leq 3$ donc $-u_n^2+u_n+6\geq 0$.\\
On en déduit que $u_{n+1}-u_n\geq 0$ et donc $(u_n)$ est croissante.\\
  
\noindent $(u_n)$ est croissante, majorée donc  converge vers $\ell$ avec $0\leq\ell\leq 3$.\\

\noindent On sait à présent que $(u_n)$ converge vers $\ell$ donc la suite $(u_{n+1})$ qui est une suite extrtaite de $(u_n)$ converge 
vers la même limite.\\
Par ailleurs, d'après les propriètés sur limites, $u_{n+1}\to \sqrt{\ell+6}$.\\

\noindent  Par unicité de la limite, on obtient : $\ell=\sqrt{\ell+6}$.\\
Cette équation admet deux solutions : $-2$ et $3$.\\
Or $0\leq\ell\leq 3$ donc $\ell=3$.
\end{prof}


\begin{exem}
Considérons la suite $\left( u_{n}\right) $ définie par : $u_{n}=%
\overset{n}{\underset{p=0}{\sum }}\;\dfrac{1}{p!}$.

\begin{etud}
  Montrons que cette suite est convergente.
\end{etud}

\begin{prof}
  Cette suite est bien évidemment croissante (car : $u_{n+1}-u_{n}=\dfrac{1}{(n+1)!}>0$).

  On peut démontrer par récurrence sur $p$ que :
  $\forall p\in \mathbb{%
    N}^{\ast }$ on a : $\dfrac{1}{p!}\leqslant \dfrac{1}{2^{p-1}}$.

  Par suite :

  \begin{equation*}
u_{n}=\overset{n}{\underset{p=0}{\sum }}\;\dfrac{1}{p!}=1+\overset{n}{%
  \underset{p=1}{\sum }}\dfrac{1}{p!}\leqslant
1+\overset{n}{\underset{p=1}{%
    \sum }}\dfrac{1}{2^{p-1}}=1+\overset{n-1}{\underset{p=0}{\sum
  }}\left( \dfrac{1}{2}\right) ^{p}=1+\dfrac{1-\left(
    \dfrac{1}{2}\right) ^{n}}{1-\dfrac{%
    1}{2}}=1+2-\left( \dfrac{1}{2}\right) ^{n-1}
\end{equation*}


\noindent soit encore :
$u_{n}\leqslant 3-\dfrac{1}{2^{n-1}}\leqslant 3$. Par consé%
quent, la suite $\left( u_{n}\right) $ est majorée par $3$.

\noindent On en déduit que $(u_n)$ converge vers $\ell$ avec
$\ell\leq 3$. (en réalité : $\ell=e$).


\end{prof}




\end{exem}

\noindent On peut énoncer un théorème analogue pour les suites décroissante.
\begin{thm} (Admis)
Soit $(u_n)$ une suite \textbf{décroissante}.

\begin{enumerate} 
 \item[$1)$] Si $(u_n)$ est  minorée (par $m$) alors converge vers $\ell$ et on a $\ell \geq m$.
\item[$2)$]  Si $(u_n)$ n'est pas minorée alors elle tend vers $-\infty $.
\end{enumerate}

\end{thm}

\subsubsection{Suites adjacentes}
\begin{Def}
Deux suites $\left( u_{n}\right) $ et $\left( v_{n}\right) $ sont adjacentes
si : 

\begin{Itemize}
  
\item $\left( u_{n}\right)$  est croissante et $\left( v_{n}\right)$
  est décroissante;
\item $\lim \left( v_{n}-u_{n}\right) =0$.

\end{Itemize}

\end{Def}


\begin{remarque} La définition implique que $\forall n\geq 0$,
  $u_n\leq v_n$.  En effet, si $\left( u_{n}\right) $ est
croissante et $\left( v_{n}\right) $ est décroissante alors $\left(
u_{n}-v_{n}\right) $ est croissante et comme elle tend vers $0$, alors elle
est négative donc : $\forall n\in \mathbb{N}$ on a : $u_{n}\leqslant
v_{n}$.
\end{remarque}


\begin{pro}
Si $\left( u_{n}\right) $ et $\left( v_{n}\right) $ sont deux suites adjacentes
alors elles convergent vers une même limite $\ell$ vérifiant : $\forall n\in 
\mathbb{N},u_{n}\leqslant \ell\leqslant v_{n}$.
\end{pro}

\begin{preuve}
La suite $\left( u_{n}\right) $ est croissante et majorée par $v_{0}$,
donc elle converge vers une limite $\ell$ et : $\forall n\in $ $\mathbb{N}$ on a : $u_{n}\leqslant
\ell$.\\
De même, $\left( v_{n}\right) $ est décroissante et minorée par $%
u_{0}$, donc elle converge vers une limite $\ell'$ et : $\forall n\in $ $\mathbb{N}$ on a : $\ell' \leqslant v_{n}$.\\
De plus,$ \lim \left(v_{n}-u_{n}\right) =0\text{ et } \lim \left(
v_{n}-u_{n}\right) =\ell'-\ell$, donc, par unicité de la limite  $\ell'-\ell=0 \Leftrightarrow\ell=\ell'$.\\

\noindent Les suites $\left( u_{n}\right) $ et $\left( v_{n}\right) $ convergent donc vers une même limite vérifiant la double inégalité annoncée.
\end{preuve}

\begin{exem}
Montrons que les suites définies sur $\mathbb{N}^{\ast }$ par $u_{n}=\overset{n}{\underset{p=0}{\sum }}\dfrac{1}{p!}$ et $v_{n}=u_{n}+\dfrac{1}{n!}$ sont adjacentes.

\begin{prof}
  \begin{itemize}
  \item Bien évidemment, la suite $\left( u_{n}\right) $ est
    croissante.

  \item De plus, $\forall n\in \mathbb{N}^*$ on a :\\
    $v_{n+1}-v_{n}=\left( u_{n+1}+\dfrac{1}{\left( n+1\right)
        !}\right) -\left(
      u_{n}+\dfrac{1}{n!}\right) =\dfrac{2}{\left( n+1\right) !}-\dfrac{1}{n!}=\dfrac{1-n}{\left( n+1\right) !}\leqslant 0$.\\
    Donc la suite $\left( v_{n}\right) $ est décroissante.

  \item Comme $u_{n}-v_{n}=\dfrac{1}{n!}$, on déduit que :
    $\lim\left( v_{n}-u_{n}\right) =0$.

  \item Par suite on peut dire que $\left( u_{n}\right) $ et
    $\left(v_{n}\right) $ sont adjacentes.
  \end{itemize}
  Les deux suites convergent donc vers une même limite $\ell$. On pourrait démontrer que $\ell=\e$.\\
  L'
 
\end{prof}
\end{exem}
\begin{remarque}
  L'inégalité $\forall n\in\N,\; u_n\leq e\leq v_n$ nous permet de trouver des approximations successives du nombre $\e$.\\

  \begin{figure}[!ht]
    \begin{center}
      \includegraphics[width=9cm,height=5cm]{images/adj.pdf}
      \caption{$(u_n)$ croit vers $\e$ et $(v_n)$ décroit vers $\e$}
    \end{center}

  \end{figure}
 

  \noindent au rang $n=1$ : $u_1=2$ et $v_1=3$ donc $2\leq \e\leq 3$,\\
  au rang $n=2$ : $u_2=\dfrac52$ et $v_2=3$ donc $2,5\leq \e\leq 3$...\\
  au rang $n=7$ : $u_7=2,7183$ et $v_7=2,7185$ donc
  $2,7183\leq \e \leq 2,7185$.

\end{remarque}


\end{document}
