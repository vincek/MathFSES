%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Les_suites_numeriques.tex]{subfiles}

%\etudiant=1
%\nosolout=1
\begin{document}
\setcounter{section}{4}
%\soltexte{fghjk}
%\input{pagedegarde}
\section{Théorème du point fixe et applications}
\label{sec:theoreme-du-point}




\noindent Soient $I$ un intervalle de $\R$ et $f: I\longmapsto I$ une fonction continue.
On considère une suite $(u_n)$ associée à $f$. C'est à dire une suite définie de la manière suivante:
$$u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$$

\noindent Dans le chapitre précédent, nous avons vu que, \textbf{si
}$(u_n)$ converge $\ell$ \textbf{alors} $\ell$ est un point d'équilibre de $f$.

\noindent Nous allons, dans ce chapitre, répondre aux questions suivantes:
\begin{itemize}
\item Existe-t-il toujours un point d'équilibre? 
\item Si oui, la suite  $(u_n)$ converge-t-elle systématiquement vers ce point d'équilibre?
\end{itemize}






\subsection{Introduction}
\subsubsection{Théorème du point fixe}
\begin{thm}
Soit $f$ une fonction continue sur l'intervalle $I=[a,b]$ telle que $f(I)\subset I$.
$f$ admet au moins un point fixe dans l'intervalle $I=[a,b]$.
\end{thm}
\begin{preuve}

  
Posons $g(x)=x-f(x)$.
Puisque $\forall x\in I$ on a $f(x)\in I$ on en déduit que: $\forall
x\in I$ on a: $a\leqslant f(x)\leqslant b$.
En particulier
pour $x=a$ on obtient  $a-f(a)\leqslant0$, soit encore $g(a)\leqslant0$
pour $x=b$ on obtient  $b-f(b)\geqslant0$, soit encore $g(b)\geqslant0$.
Comme la fonction $g$ est la somme de deux fonctions continues sur $I$, on en déduit que $g$ est continue sur $I$.
Par application du théorème des valeurs intermédiaires, on en déduit
que $g(I)$ est un intervalle.

$g(I)$ est un intervalle $g(a)$  et $g(b)$  sont éléments de $g(I)$
avec $g(a)\leqslant 0 \leqslant g(b)$, donc $0\in g(I)$.

Par définition de $g(I)$, il existe donc un élément $c\in g(I)$ tel que $g(c)=0$ soit encore $f(c)=c$.
\end{preuve} 


\begin{bclogo}[logo=\bcdanger,arrondi=0.1]{Attention}
  Ce théorème ne s'applique que dans le cas où $I$ est un intervalle  \textbf{fermé, borné}.
\end{bclogo}


\begin{exem}\label{exem:1}
On considère la suite $(u_n)$ définie par $u_0=1$ et $u_{n+1}=\sqrt{1+u_n^2},\; \forall n\in \N$.
Soit $f$ la fonction définie sur $I=[0,+\infty[$ par $f(x)=\sqrt{1+x^2}$.

\noindent L'intervalle $I$ est stable par $f$ donc $(u_n)$ est bien définie.
De plus, $f$ est croissante sur $I$ donc $(u_n)$ est monotone. Comme $u_0=1$ et $u_1=\sqrt2$, $(u_n)$ est croissante.

\noindent Il y a donc deux cas de figure possibles:
soit $(u_n)$ converge (vers un point d'équilibre de $f$),
soit $(u_n)$ diverge et tend vers $+\infty$.

\noindent Or il est facile de vérifier que $\forall x\geq 0,\; \sqrt{1+x^2}>x$. La fonction $f$ n'admet donc aucun point d'équilibre.
On en déduit que $(u_n)$ tend vers $+\infty$.
\end{exem}

\noindent Dans cet exemple, le théorème du point fixe est mis en défaut car l'intervalle $I$ n'est pas borné.
\begin{figure}[!h]
\begin{center}
\includegraphics[width=6cm,height=5cm]{images/nonconv.pdf} 
\caption{Exemple \ref{exem:1}, construction  de la suite $(u_n)$ qui tend vers $+\infty$}
\end{center} 
\end{figure}

\subsubsection{Un contre-exemple}
\noindent Malheureusement, l'existence d'un (ou plusieurs)  point(s) d'équilibre ne signifie pas que la suite converge...
\begin{exem}
On considère la suite $(u_n)$ définie par $u_0=1$ et $u_{n+1}=1-u_n^2,\; \forall n\in \N$.
Soit $f$ la fonction définie sur $I=[0,1]$ par $f(x)=1-x^2$.

\noindent L'intervalle $I$ est stable par $f$ donc $(u_n)$ est bien définie. 
De plus $f$ étant continue sur $I$, $f$ admet un point d'équilibre.

\noindent Cependant la suite $(u_n)$ est divergente. En effet, on montre facilement que:
$\forall n\in\N, \; u_{2n}=1$ et $ u_{2n+1}=0$.
\end{exem}



\subsection{Fonctions  contractantes}
\subsubsection{Définition}
\begin{Def}
Soit $f$ une fonction définie sur un intervalle $I$ de $\R$.
$f$ est dite contractante sur $I$ si il existe $k\in]0,1[$ tel que: 
\begin{equation}
\forall (x,y)\in I^2,\; |f(x)-f(y)|\leq k|x-y|\label{eq:1}
\end{equation}
\end{Def}


\begin{remarque}
 Une application contractante sur $I$ est continue sur $I$.
 Soit, en effet, $f$ une fonction contractante  sur $I$ et $a\in I$. Démontrons que $f$ est continue en $a$:
 $\forall x\in I$,  $|f(x)-f(a)|\leq k|x-a|$. 
 Si $x\to a$, $|x-a|\to 0$ et donc $|f(x)-f(a)|\to 0$, ce qui signifie que $f$  est continue en $a$.
\end{remarque}

\begin{exem}
La fonction $f$ définie sur $\R$ par $f(x)=-\dfrac{1}{2} x+1$ est contractante (et $k=\dfrac{1}{2}$).
\begin{equation*}
\forall (x,y)\in \R^2,\; |f(x)-f(y)|=|(-\dfrac12 x+1)-(-\dfrac12 y+1)|=|-1/2||x-y|=1/2|x-y|.
\end{equation*}

La fonction $f$ est $1/2$-contractante sur $\R$.
\end{exem}

\begin{pro}
Soit $f$ une fonction définie sur un intervalle $I$ de $\R$.
Si $f$ est dérivable sur $I$ et si il existe $k\in]0,1[$ tel que: $\forall x\in I,\; |f'(x)|\leq k$,
alors $f$ est contractante sur $I$ 
 \end{pro}

\begin{preuve}
Soient $x$ et $y$ dans $I$.
$f$ étant continue sur l'intervalle $[x,y]$, dérivable sur $]x,y[$, on peut appliquer le théorème des 
accroissements finis.
On en déduit que: $\exists c\in]x,y[$ tel que $f(x)-f(y)=f'(c)(x-y)$.
Or $|f'(c)|\leq k$ donc $|f(x)-f(y)|\leq k|x-y|$. 

\noindent $f$ est donc contractante sur $I$.
\end{preuve}

\begin{exem}
Soit $f$ la fonction définie sur $I=[0,+\infty[$ par $f(x)=\sqrt{1+x}$.
$f$ est dérivable sur $I$ et $\forall x\in I, \; f'(x)=\dfrac{1}{2\sqrt{1+ x}}$.
On a donc  $\forall x\geq 0,\; 0\leq f'(x)\leq \dfrac12$ d'\`ou $|f'(x)|\leq \dfrac12<1$.

\noindent $f$ est donc contractante sur  $I=[0,+\infty[$.

\end{exem}


\subsubsection{Convergence de $(u_n)$}
\begin{thm}\label{thm:cont}
Soient un intervalle fermé de $\R$ et $f: I\longmapsto I$.
Si $f$ est contractante sur $I$ alors:
\begin{enumerate}
 \item[i)] $f$ admet un unique point d'équilibre $\ell$ dans $I$.
\item[ii)] Toute suite  $(u_n)$ définie par 
$u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$ converge vers $\ell$
\end{enumerate}
\end{thm}

\begin{remarque}
 Ce théorème s'applique  même si l'intervalle $I$ n'est pas borné et assure l'unicité du point fixe.
\end{remarque}

\begin{preuve}
\begin{enumerate}
\item[i)] Si $I$ est borné, l'existence d'un point d'équilibre est évidente (c'est le théorème du 
point fixe appliqué à $f$ qui est contractante donc continue).
Si $I$ n'est pas borné, nous admettrons l'existence d'un point d'équilibre. 

\noindent Démontrons l'unicité de ce point d'équilibre:
Supposons que $f$ admette dans $I$ deux points d'équilibre $\ell$ et $\ell'$. 
$f$ étant contractante sur $I$, il existe $k\in]0,1[$ tel que:
$|f(\ell)-f(\ell')|\leq k|\ell-\ell'|$ donc $|\ell-\ell'|\leq k|\ell-\ell'|$ car  $\ell$ et $\ell'$ sont deux points d'équilibre.
D'où $(1-k)|\ell-\ell'|\leq 0$. Or $1-k>0$ donc $|\ell-\ell'|=0\Leftrightarrow \ell=\ell'$.
$f$ admet donc bien un unique point d'équilibre $\ell$ dans $I$.


\item[ii)] Démontrons à présent que toute suite  $(u_n)$ définie par 
$u_0\in I \text{ et } u_{n+1}=f(u_n),\; \forall n\in\N$ converge vers $\ell$.

\noindent $f$ étant contractante sur $I$,  il existe $k\in]0,1[$ tel que:
\begin{equation*}
 \forall n\in \N,\; |f(u_n)-f(\ell)|\leq k|u_n-\ell| \Leftrightarrow |u_{n+1}-\ell|\leq k|u_n-\ell| \text{ car } f(\ell)=\ell
\end{equation*}


\noindent  Un petit raisonnement par récurrence permet alors de démontrer que: $\forall n\in\N,\; |u_n-\ell|\leq k^n|u_0-\ell|$.
Or $|k|<1$ donc $k^n\to 0$ et donc $u_n\to \ell$.
La suite $(u_n)$ converge bien vers $\ell$.
\end{enumerate}
\end{preuve}




\begin{exem}\label{exem:racine}
Soit $(u_n)$ la suite  définie par $u_0\geq 0$ et
$u_{n+1}=\sqrt{1+u_n},\; \forall n\in\N$. Nous avons démontré que  la
fonction $f$  définie sur $I=[0,+\infty[$ par $f(x)=\sqrt{1+x}$ et
contractante avec $k=\dfrac12$. Elle admet donc un unique point d'équilibre $\ell$ et $(u_n)$ converge vers $\ell$.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=7cm,height=6cm]{images/convor.pdf} 
\caption{Construction des premiers termes de la suite $(u_n)$ de
  l'exemple \ref{exem:racine}.}
\end{center} 
\end{figure}

\noindent On peut d'ailleurs calculer $\ell$ en résolvant dans $I$ l'équation $f(\ell)=\ell$:
$f(\ell)=\ell \Leftrightarrow \sqrt{1+\ell}=\ell \Leftrightarrow \ell^2-\ell-1=0 \Leftrightarrow \ell=\dfrac{1+\sqrt{5}}{2}$ car $\ell\geq 0$.

\end{exem}
\begin{remarque}
 Ce nombre est bien connu des mathématicien. C'est le ``nombre d'or''.
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=6cm,height=6cm]{images/or3.pdf} 
\caption{Rectangle d'or}
\end{center} 
\end{figure}
\end{remarque}
Cette figure montre comment, à partir d'un carré de c\^oté $1$, on construit un <<\href{https://fr.wikipedia.org/wiki/Nombre_d'or#Rectangle_et_spirale_d.27or}{rectangle d'or}>>. C'est à dire, un rectangle  de longueur $\ell=\dfrac{1+\sqrt{5}}{2}$.



\subsubsection{Majoration de l'erreur}
\noindent On reprend les notations du paragraphe précédent.

\noindent Le calcul des termes successifs de la suite $(u_n)$ nous permet d'obtenir une valeur approchée de $\ell$ de plus en plus précise.
La question qui se pose alors est de savoir à partir de quand on estime que cette approximation est <<bonne>>. Pour cela, il faut majorer la quantité 
$|u_n-\ell|$.

\noindent Dans la démonstration précédente, nous avons obtenu la majoration suivante:

\begin{equation}
\forall n\in\N,\; |u_n-\ell|\leq k^n|u_0-\ell|\;\label{eq:2}
\end{equation}

L'inconvénient est que cette majoration fait intervenir $\ell$... que l'on cherche à approcher !

\noindent Décomposons $u_0-\ell$ de la manière suivante: $u_0-\ell=u_0-u_1+u_1-\ell$.
On a alors
\begin{equation*}
|u_0-\ell|\leq |u_0-u_1|+|u_1-\ell|\leq |u_0-u_1|+k|u_0-\ell|,
\end{equation*}

d'où $(1-k)|u_0-\ell|\leq |u_0-u_1|$ or $1-k>0$ donc on obtient $|u_0-\ell|\leq \dfrac{1}{1-k}|u_0-u_1|$. En introduisant dans l'inégalité \eqref{eq:2}, on a donc $\forall n\in\N,\; |u_n-\ell|\leq \dfrac{k^n}{1-k}|u_0-u_1|$.

\noindent Si on veut, par exemple, une valeur approchée de $\ell$ à $10^{-3}$ près, il suffit donc de choisir $n$ tel que $\dfrac{k^n}{1-k}|u_0-u_1|\leq 10^{-3}$.

\begin{exem}
Reprenons l'exemple \ref{exem:racine}:
Soit $(u_n)$ la suite  définie par $u_0\geq 0$ et $u_{n+1}=\sqrt{1+u_n},\; \forall n\in\N$.
Nous avons démontré que  la fonction $f$  définie sur $I=[0,+\infty[$ par $f(x)=\sqrt{1+x}$ et contractante avec $k=\dfrac12$ et que la suite $(u_n)$ 
converge vers $\ell=\dfrac{1+\sqrt{5}}{2}$.


\noindent On a donc $\forall n\in\N,\; |u_n-\ell|\leq \dfrac{k^n}{1-k}|u_0-u_1|$ soit $ |u_n-\ell|\leq \dfrac{1}{2^{n-1}} |u_0-u_1|$.
Fixons, par exemple $u_0=3$. On a $u_1=\sqrt{3+1}=2$, d'où $ |u_n-\ell|\leq \dfrac{1}{2^{n-1}}$.


\noindent Pour avoir  une valeur approchée de $\ell$ à $10^{-3}$ près, il suffit donc de choisir $n$ tel que $\dfrac{1}{2^{n-1}}\leq 10^{-3}$:
 $\dfrac{1}{2^{n-1}}\leq 10^{-3} \Leftrightarrow 2^{n-1}\geq 10^3 \Leftrightarrow (n-1)\ln(2)\geq 3\ln(10) \Leftrightarrow n\geq 1+3\dfrac{\ln(10)}{\ln(2)}$.
$1+3\dfrac{\ln(10)}{\ln(2)}\simeq 10,96$ donc $n=11$ convient. Le calcul de $u_{11}$ donne $\ell\simeq 1,618$.

\noindent Bien sûr, si on veut une approximation plus précise du nombre d'or, il faudra aller plus loin dans le calcul des termes de la suite !
\end{exem}

\subsection{Nature des équilibres}

\noindent D'après ce que nous venons de voir, si $f$ est contractante sur un intervalle $I$ fermé alors elle admet un unique point d'équilibre et toute suite 
associée à $f$ converge vers cet équilibre.
Malheureusement, toutes les fonctions ne sont pas contractantes...
En général, si $f$ possède un point d'équilibre, nous serons amenés à faire une étude au voisinage de ce point.

\noindent Dans la suite du cours, $f$ désigne une fonction de classe $\mathcal{C}_1$ sur un intervalle $I$, stable par $f$.
On considère une suite $(u_n)$ associée à $f$.
On suppose que $f$ possède un point d'équilibre noté $\ell$.


\subsubsection{Équilibre stable}
\begin{Def}
 $\ell$ est \textbf{un équilibre stable} (ou point fixe attractif) si
 on peut trouver un intervalle $J\subset I$ contenant $\ell$ tel que
 toute suite $(u_n)$ définie par $u_0\in J \text{ et } \forall
 n\in\N,\; u_{n+1}=f(u_n),\; $ converge vers $\ell$. On dit que $J$
 est un bassin d'attraction pour $\ell$.
\end{Def}

\begin{pro}
 Si $|f'(\ell)|<1$ alors $\ell$ est un point d'équilibre stable.
\end{pro}

\begin{preuve}
 Supposons  $|f'(\ell)|<1$, $f'$ étant continue sur $I$, on va pouvoir trouver un intervalle $J=[\ell-\alpha, \ell+\alpha]$ (avec $\alpha>0$) tel que:
$\forall x\in J,\; |f'(x)|<k<1$.
Autrement dit, $f$ sera contractante sur $J$. Nous allons donc pouvoir appliquer le théorème du point fixe sur l'intervalle $J$. Il reste juste à vérifier que $J$ est stable par $f$.

\noindent $J=[\ell-\alpha, \ell+\alpha]$, donc  $ x\in J \Leftrightarrow |x-\ell|\leq\alpha$.
Le théorème des accroissement finis nous donne: $\forall x\in J, \;\exists c\in J \text{ tel que } f(x)-f(\ell)=f'(c)(x-\ell)$.
Or $f(\ell)=\ell$ et $|f'(c)|\leq k$ donc  $|f(x)-\ell|\leq k|x-\ell|\leq k\alpha<\alpha$.
On en déduit que $f(x)\in J$ (car $ |f(x)-\ell|<\alpha$).
Cela signifie que $J$ est bien stable par $f$.

\noindent D'après le théorème du point fixe:
$J$ est stable par $f$,
$f$ est contractante sur $J$,
donc toute suite associée à $f$ avec $u_0\in J$ converge vers $\ell$.
\end{preuve}

\begin{remarque}
Cela signifie que toute suite associée à $f$ convergera vers $\ell$, à condition de choisir $u_0$ ``suffisamment proche'' de $\ell$.
La difficulté sera de réussir à quantifier le  ``suffisamment proche'' ...
\end{remarque}

\noindent \textbf{Cas particulier: $f'(\ell)=0$:}
Supposons que $f$ est de classe $\mathcal{C}_2$ sur $J$ et que $f''$ est bornée sur $J$ (c'est à dire: $\exists M\in\R$ tel que $\forall x\in J, \; |f''(x)|<M$).
En appliquant la formule de Taylor à l'ordre $2$, on obtient:
$$\forall x\in J,\; \exists c\in J \text{ tel que } f(x)=f(\ell)+f'(\ell)(x-\ell)+\dfrac{f''(c)}{2}(x-\ell)^2$$

\noindent Or $f(\ell)=\ell$ et $f'(\ell)=0$, on obtient donc: $f(x)=\ell+\dfrac{f''(c)}{2}(x-\ell)^2$ d'où $|f(x)-\ell|\leq \dfrac{M}{2}(x-\ell)^2$.

\noindent Revenons à la suite $(u_n)$ définie par $u_0\in J$ et $\forall n\in\N,\; u_{n+1}=f(u_n) $:
on a vu que cette suite converge vers $ \ell$ et on a $|u_{n+1}-\ell|\leq \dfrac{M}{2}(u_n-\ell)^2$.


\noindent Si $|u_n-\ell|<10^{-p}$ alors  $|u_{n+1}-\ell|\leq \dfrac{M}{2}10^{-2p}$.

\noindent Ce qui signifie qu'à chaque itération, on double le nombre de décimales exactes dans l'approximation de $\ell$.
La convergence de $(u_n)$ vers $\ell$ est dite quadratique (c'est une
convergence très rapide).

\noindent C'est pourquoi, dans le cas où $f'(\ell)=0$, on dira que $\ell$ est \textbf{un point fixe super-attractif}. 

\begin{exem}
Soit  l'intervalle $I=]-1,1[$. On considère la suite $(u_n)$ définie par: $u_0\in I$ et $\forall n\in\N,\; u_{n+1}=u_n^2$.
Introduisons la fonction $f$ définie sur $I$ par $f(x)=x^2$.

\noindent $\forall x\in I,\; f(x)\in [0,1[$ donc $f(I)\subset I$.
$f(x)=x \iff x(x-1)=0 \iff x=0 \text{ ou } x=1$: l'unique point fixe de $f$ dans $I$ est $\ell=0$.
$\forall x\in I,\; f'(x)=2x$ donc $f'(0)=0$: $0$ est un point fixe super-attractif.

\noindent La suite $(u_n)$ va donc converger (rapidement) vers $0$.
\end{exem}

\subsubsection{Équilibre instable}
\begin{Def}
 $\ell$ est \textbf{un équilibre instable} (ou point fixe répulsif) si on peut trouver un intervalle $J\subset I$ contenant $\ell$ tel que toute suite $(u_n)$ définie 
par $u_0\in J\backslash\{\ell\} \text{ et } \forall n\in\N,\; u_{n+1}=f(u_n),\; $ ne converge pas vers $\ell$.
\end{Def}

\begin{pro}
 Si $|f'(\ell)|>1$ alors $\ell$ est un point d'équilibre instable.
\end{pro}

\begin{preuve}
L'esprit de la preuve est le même que pour un équilibre stable.
 Supposons  $|f'(\ell)|>1$, $f'$ étant continue sur $I$, on va pouvoir trouver un intervalle $J=[\ell-\alpha, \ell+\alpha]$ (avec $\alpha>0$) tel que:
$\forall x\in J,\; |f'(x)|\geq k>1$.


\noindent 
Le théorème des accroissement finis nous donne: $\forall x\in J, \;\exists c\in J \text{ tel que } f(x)-f(\ell)=f'(c)(x-\ell)$.
Or $f(\ell)=\ell$ et $|f'(c)|\geq k$ donc  $|f(x)-\ell|\geq k|x-\ell|$.

\noindent Supposons que $(u_n)$ converge vers $\ell$, alors, à partir d'un certain rang, tous les termes de la suite sont ``proches`` de $\ell$ 
donc dans $J$.
Autrement dit: $\exists N\in \N \text{ tel que } \forall n\geq N,\; u_n\in J$,
quitte à ré-indexer la suite, nous pouvons supposer que: $\forall n\in \N,\; u_n\in J$.
On a alors $\forall n\in \N,\; |u_{n+1}-\ell|\geq k|u_n-\ell|$ donc $\forall n\in \N,\; |u_{n}-\ell|\geq k^{n}|u_0-\ell|$.
Or $k>1$ donc $k^{n}\to +\infty$, donc nécessairement $\forall n\in \N,\;u_n=\ell$.
Ce qui signifie que si la suite $(u_n)$ converge vers $\ell$, elle est stationnaire à partir d'un certain rang.

\noindent Par contra-position: Si $(u_n)$ n'est pas stationnaire, elle ne converge pas vers $\ell$.
Cependant, $(u_n)$ peut converger vers un autre point fixe...
\end{preuve}


\begin{exem}
Soit l'intervalle $I=[1,+\infty[$.On considère la suite $(u_n)$ définie par: $u_0\in I$ et $\forall n\in\N,\; u_{n+1}=u_n^2$.


Introduisons la fonction $f$ définie sur $I$ par $f(x)=x^2$.

\noindent $\forall x\in I,\; f(x)\in [1,+\infty[$ donc $f(I)\subset I$.
$f(x)=x \iff x(x-1)=0 \iff x=0 \text{ ou } x=1$: l'unique point fixe de $f$ dans $I$ est $\ell=1$.
$\forall x\in I,\; f'(x)=2x$ donc $f'(1)=2$: $1$ est un point fixe répulsif.

\noindent Étudions la suite $(u_n)$:
Si $u_0=1$, la suite est stationnaire donc converge vers $\ell=1$.
Si $u_0> 1$, $f$ est croissante sur $I$ donc $(u_n)$ est monotone. De plus $u_1-u_0=u_0(u_0-1)>0$ donc $(u_n)$ est (strictement) croissante.
Comme $u_0>1$, la suite ne converge par vers $1$. $(u_n)$ tend vers $+\infty$ (Théorème de convergence monotone)
\end{exem}

\begin{remarque}
Dans les deux exemples précédents, on a étudié la même suite mais on a changé la valeur de $u_0$. 
Dans un cas, la suite converge. Dans l'autre, elle diverge. Cela
montre bien que la valeur de $u_0$ conditionne le comportement de la
suite...

\begin{center}
  \includegraphics[width=8cm]{images/chapitre5_fig3} \includegraphics[width=8cm]{images/chapitre5_fig4}
\end{center}
\end{remarque}

\subsubsection{Cas particulier: $|f'(\ell)|=1$}
\noindent Le cas $|f'(\ell)|=1$ est un cas pour lequel on ne peut pas
énoncer de résultat général. Il faudra alors faire une étude plus complète de la suite afin de connaître son comportement. Dans certains cas, elle convergera vers $\ell$. Dans d'autres cas, on aura une suite divergente.

\noindent Voir les différents exemples dans le polycopié d'exercices.

\subsection{La suite logistique}
\label{sec:la-suite-logistique}

La \href{https://fr.wikipedia.org/wiki/Suite_logistique}{suite
  logistique} permet de  modéliser l'évolution d'une population.

\begin{equation*}
  x_{n+1}=\mu x_n(1-x_n) \qquad \text{ où }\mu>0
\end{equation*}


Il s'agit d'une suite définie par récurrence et la fonction associée
est \fonction{f_{\mu}}{[0;1]}{\R^+}{x}{\mu x(1-x)}

\begin{remarque}
  Sans le facteur $(1-x_n)$ il s'agit d'une suite géométrique. Le
  facteur $(1-x_n)$ est un facteur qui limite la croissance de $x_n$.
\end{remarque}

L'étude générale de cette suite est hors de portée dans le cadre de ce
cours, nous nous contenterons de l'étude de quelques cas particuliers
sous forme d'exercices.



\begin{exercice}
  \begin{enumerate}
  \item Étudier la fonction $f_{\mu}$. En déduire que $\mu\in[0;4]$.
  \item Quels sont les points fixes de $f_{\mu}$?
  \item Quelle est la nature de chacun des points fixes?
  \item Montrer que la suite est convergente lorsque $\mu<1$.
  \item Même question pour $\mu=1$.
  \end{enumerate}
  
  \begin{sol}
    \begin{enumerate}
    \item $\quad$
      

  \begin{TV*}{103}
    TV([0,1],[],"f","x",3*x*(1-x),1,n,\tv);
 \end{TV*}


L'intervalle $[0;1]$ est stable par $f$ si et seulement si
$[0;\frac{\mu}{4}]\subset [0;1]$. Donc la suite n'est définie que
lorsque $\mu\in]0;4]$.

  \item Soit $x\in [0;1]$.
    \begin{eqnarray*}
      f(x)=x\\
      \mu x (1-x)=x\\
      x\times (-\mu x+\mu-1)=0\\
      x=0\quad \text{ou} \quad x=\frac{\mu}{\mu-1}=1-\frac{1}{\mu}
    \end{eqnarray*}

    Donc $0$ est un point fixe de $f$ sur $[0;1]$. En revanche,
    $1-\frac{1}{\mu}\in [0;1]\Leftrightarrow \mu \geq 1$, donc lorsque
    $\mu\geq 1$, $f$ a deux points fixes sur $[0;1]$.
    
  \item $f'(x)=\mu(-2x+1)$
    \begin{Itemize}
      
    \item $f'(0)=\mu$
    \item $f'(1-\frac{1}{\mu})=\mu(\frac{2}{\mu}-1)=2-\mu$
    \end{Itemize}

    Donc 0 est un point fixe attractif si et seulement si $\mu<1$
    (Dans ce cas c'est l'unique point fixe). Et
    $1-\frac{1}{\mu}$ est un point fixe attractif si et seulement si
    $1<\mu<3$.

    Lorsque $\mu=1$, $f'(0)=f'(1-\frac{1}{\mu})=1$.
  
  \item $\quad$

    \begin{center}
      \includegraphics[width=12cm]{images/chapitre5_fig5}
    \end{center}

    \begin{Itemize}
      
    \item Si $x_0=0$ la suite est stationnaire.
    \item Si $x_0=1$, alors $x_1=0$ et la suite est stationnaire à
      partir du rang 1.
    \item Si $x_0\in ]0;\frac{1}{2}]$. L'intervalle $[0;\frac{1}{2}]$
      est stable par $f_{\mu}$  et $f_{\mu}$
      est croissante sur cet intervalle (\textit{cf}. tableau de
      variation). Donc la suite $(x_n)$ est monotone.

      La suite $(x_n)$ est monotone, bornée et $f_{\mu}$ est continue sur
      $[0;1]$, donc $(x_n)$ converge vers l'unique point fixe de $f_{\mu}$
      sur $[0;1]$, c'est à dire 0.
    \item Si $x_0\in ]\frac{1}{2};1]$, le maximum de $f$ étant
      $\frac{\mu}{4}\leq \frac{1}{4}$, $x_1\in[0;\frac{1}{2}[$ et l'on
      peut conclure que $(x_n)$ tend en décroissant vers 0 d'après le
      cas précédent.

      % On a $f_{\mu}(x)-x=x\times (-\mu x+\mu-1)$, comme $x$ est
      % positif, le signe de $f_{\mu}(x)-x$ ne dépend que de celui de
      % $(-\mu x+\mu-1)$ or cette quantité est positive sur $[0;1]$ car
      % $\mu<1$.

      % Donc sur $[0;1]$, $f(x)\leq x$. Donc $u_1\leq u_0$, la suite
      % $(x_n)$ est donc décroissante.
    \end{Itemize}


    On peut également se servir du théorème \ref{thm:cont}.

    La fonction $f_{\mu}$ est de classe $\mathcal{C}_1$ sur $[0;1]$ et
    $|f_{\mu}'(x)|=\mu|-2x+1|\leq \mu<1$, donc la fonction $f$ est
    contractante. D'après le théorème \ref{thm:cont} quelque soit
    $x_0$, la suite $(x_n)$ converge vers l'unique point fixe de $f$
    sur $[0;1]$ c'est à dire 0.
  \item Considérons la suite récurrente définie par
    $x_{n+1}=x_n(1-x_n)$.

 \begin{center}
      \includegraphics[width=12cm]{images/chapitre5_fig6}
    \end{center}


    Le graphique nous permet d'émettre la conjecture suivante: $(x_n)$
    converge encore vers 0 (mais plus lentement).

    Pour démontrer cette conjecture, il suffit de reprendre le
    raisonnement de la question précédente (où l'on distingue quatre
    cas). En revanche, on ne peut pas utiliser le théorème
    \ref{thm:cont} car l'application n'est pas contractante
    $f_{\mu}'(0)=1\geq 1$.
          \end{enumerate}
  \end{sol}
\end{exercice}


\begin{exercice}
  Considérons la suite récurrente définie par
  $x_{n+1}=2,5x_n(1-x_n)$. On pose: \fonction{f}{[0;1]}{\R^+}{x}{2,5x(1-x)}


  \begin{enumerate}
  \item Quels sont les points fixes de $f$, quelle est leur nature?
  \item Quelle est la nature de la suite $(x_n)$?
  \end{enumerate}

  \begin{sol}
    \begin{enumerate}
    \item La fonction $f$ admet deux points fixes $0$ et
      $\frac{3}{5}$. $f'(0)=\frac{5}{2}>1$, donc $0$ est un point fixe
      répulsif. $f'(\frac{3}{5})=-\frac{1}{2}\in]-1;1[$, $f$ est un
      point fixe attractif.
    \item Si $x_0=0$ ou $x_0=1$ la suite est stationnaire au moins à
      partir d'un certain rang. Dans la suite prenons $x_0\in]0;1[$.

      D'après l'exercice précédent
      $f'(x)=\frac{5}{2}(-2x+1)$. Cherchons un intervalle où $f$ est
      contractante.

      \begin{eqnarray*}
        -1< \frac{5}{2}(-2x+1)<1\\
        -\frac{2}{5}<-2x+1<\frac{2}{5}\\
        -\frac{7}{5}<-2x<\frac{-3}{5}\\
        \frac{3}{10}<x<\frac{7}{10}\\
      \end{eqnarray*}
      
En dressant le tableau de variation:
\begin{TVIex}
TVIex([0,1],[],"f","x",5*x*(1-x)/2,1,21/40,n,\tv)
\end{TVIex}

On remarque que l'intervalle $[0,3;0,7]$ est stable par
$f$. $f([0,3\,;\,0,7])=[\frac{21}{40}\,;\,\frac{5}{8}]\subsetneqq [0,3\,;\,0,7]$.

Donc en utilisant le théorème \ref{thm:cont} on peut montrer que si
$x_0\in[0,3\,;\,0,7]$ alors la suite converge vers
$\frac{3}{5}$. Reste deux cas de figure à explorer $x_0\in]0;0,3[$ et
$x_0\in]0,7;1[$.

Étudions le signe de $\delta(x)=f(x)-x=\frac{1}{2}x(3-5x)$.


\begin{TS}
TS("@delta",[x,3-5*x],[0,1],n,\tv)
\end{TS}

\paragraph{Cas $x_0\in]0;0,3[$}
Comme sur $[0;\frac{1}{2}]$, $f$ est croissante et $\delta(x)$ est
positif, si $x_0\in]0;0,3[$ la suite $(x_n)$ est croissante tant que
$x_n\leq \frac{1}{2}$. Nécessairement, il existe un rang $n_0$ tel que $0,3<x_{n_0}<\frac{5}{8}$.

En effet, si $x_n<0,3$ alors la suite serait bornée et convergerait
vers un nombre $\ell\in ]0;0,3]$ ce qui est impossible car $f$ est
continue et ne possède pas de point fixe sur cet intervalle. Donc il
existe un rang à partir duquel $x_n>0,3$, prenons pour $n_0$ le plus
petit rang pour lequel $x_n>0,3$. Donc $x_{n_0-1}\leq 0,3$ d'après le
tableau de variation $x_{n_0}\leq \frac{5}{8}$.


Donc il existe un rang à partir duquel $x_n$ appartient au bassin
d'attraction du point fixe $\frac{3}{5}$, la suite $(x_n)$ converge
vers $\frac{3}{5}$.

\paragraph{Cas $x_0\in]0,7;1[$}

D'après le tableau de variation $x_1\in]0;\frac{21}{40}[$

Si $x_1\in ]0,3;\frac{21}{40}[$ alors $x_1$ appartient au bassin d'attraction de
$\frac{3}{5}$ sinon  $0<x_1\leq0,3$ et $x_n$ appartient au bassin d'attraction de
$\frac{3}{5}$ à partir d'un certain rang (d'après précédemment).

Dans tous les cas, la suite $(x_n)$ converge
vers $\frac{3}{5}$.
    \end{enumerate}
  \end{sol}
\end{exercice}
\begin{exercice}
   Considérons la suite récurrente définie par
  $x_{n+1}=3,2x_n(1-x_n)$. On pose:
  \fonction{f}{[0;1]}{\R^+}{x}{3,2x(1-x)}

  \begin{enumerate}
  \item Quels sont les points fixes de $f$, quelle est leur nature?
  \item Quelle est la nature de la suite $(x_n)$?
  \end{enumerate}
  \begin{sol}
    \begin{enumerate}
    \item Les deux points fixes 0 et $\frac{11}{16}$ sont répulsifs.
    \item Soit la suite est stationnaire, c'est à dire qu'il existe
      $k$ tel que $f^{k}(x_0)=0$ ou $f^{k}(x_0)=\frac{11}{16}$; soit la
      suite diverge.
    \end{enumerate}

    \begin{center}
      \begin{tabular}[h]{cc}
        \includegraphics[width=8cm]{images/chapitre5_fig7}& \includegraphics[width=8cm]{images/chapitre5_fig8}\\
        La suite diverge, elle possède deux valeurs d'adhérence.& La
                                                                  suite
                                                                 est stationnaire.
      \end{tabular}
\end{center}
\end{sol}

\end{exercice}
\end{document}
