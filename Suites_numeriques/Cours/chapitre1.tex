%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Les_suites_numeriques.tex]{subfiles}


\begin{document}

%\maketitle



\section{Généralités sur les suites}
\label{sec:generalites-sur-les}


\subsection{Définitions}
\subsubsection{Suites numériques}
\begin{Def}
Une \textbf{suite réelle} $u$ est une application de $\N \text{ (ou d'une partie }I \text{  de } \N$) dans $\R$.\\
$$u:\left\lbrace \begin{array}{rll} 
\N~ (\text{ou } I)&\longrightarrow &\R \\
n &\longmapsto & u_n
\end{array} \right.$$
\end{Def}

Le nombre réel  $u_{n}$ est appelé  terme de rang $n$ de la suite $u$.\\

Traditionnellement, la suite $u$ est notée $(u_{n})_{n\in I}$
ou encore $(u_{n})$.

\begin{remarque}
Ne pas confondre, dans l'écriture, le terme $u_n$ et la suite $(u_n)$.
\end{remarque}


\begin{remarque}
Toutes les suites ne sont pas définies à partir du rang 0.
\end{remarque}
%\begin{Def}
%La \textbf{représentation graphique} de la suite $(u_{n})_{n\in \mathbb{N
%}}$ est l'ensemble des points $M_{n}$ du plan de coordonnées $\left(
%n,u_{n}\right) $ obtenus lorsque $n$ décrit l'ensemble $\mathbb{N}$.
%\end{Def}

Une suite $(u_n)$ peut être définie de deux manières différentes:
\begin{itemize}
\item Par la donnée du terme général en fonction de n $\left(\forall n\in \N^*, u_n=\dfrac{n^2+1}{n}\right)$.
\item Par une relation de récurrence et la donnée du premier terme ($\forall n\in \N,u_{n+1}=3u_n^2-5$ et $u_0=1$).\\
Chaque terme se calcule alors à l'aide du précédent.
\end{itemize}

\subsubsection{Le raisonnement par récurrence}
\noindent Certaines propriétés peuvent être démontrées de manière directe.\\
(Par exemple : `` Vérifier que $\forall n\in \N, u_n=\dfrac{n+1}{n^2}$ est positif'').\\ Parfois, cela n'est pas possible, en particulier quand
 la suite $(u_n)$ est définie par une relation de récurrence. On utilise alors un raisonnement par récurrence.\\[0.2cm]

\noindent\textbf{Principe} :\\
Pour démontrer qu'une propriété est vraie pour tout entier
naturel $n\geqslant n_{0}$ :

\begin{itemize}
\item on montre qu'elle est vraie au rang $n_{0}$ (c'est à dire pour $%
n=n_{0}$);

\item on suppose qu'elle est vraie au rang $n$ (avec $n$ fixé, $n\geq n_{0}$) et
on montre alors (en utilisant cette hypothèse) qu'elle est vraie au rang 
$n+1$.

\item on peut alors conclure que le résultat est vrai $\forall n\geq n_0$. 
\end{itemize}

\begin{remarque}
La première phase de la démonstration est souvent appelée
initialisation et la seconde hérédité.
\end{remarque}

\begin{exem}
Soit $(u_n)$ la suite définie par: $\forall n\in \N, u_{n+1}=\dfrac{1}{2}(u_n+1)$ et $u_0=2$\\
Démontrons que $\forall n\in \N, u_n\geq 1$.
\end{exem}

\begin{itemize}
\item Initialisation:
 Pour $n=0$, $u_0=2$ donc on a bien $u_0\geqslant 1$. La propriété est vraie au rang 0.
\item Hérédité:
Soit $n\in \N$, on suppose que $u_n \geq 1$. Vérifions qu'alors $u_{n+1}\geqslant 1$.\\
$$u_n \geq 1 \Leftrightarrow u_n+1 \geq 2 \Leftrightarrow \frac{1}{2}(u_n+1) \geq 1 \Leftrightarrow u_{n+1} \geq 1$$
La propriété est donc héréditaire.
\item Conclusion :
$\forall n\in \N, u_n\geq 1$
\end{itemize}

\subsection{Deux exemples fondamentaux}
\subsubsection{Suites arithmétiques}
\begin{Def}
La suite $(u_{n})$ est dite \textbf{arithmétique} de premier terme $u_{0}$ et
de raison $r$ si : 
\begin{equation*}
\forall n\in \mathbb{N},\;u_{n+1}=u_{n}+r
\end{equation*}

\end{Def}

\begin{remarque}
\item Une telle suite est donc définie par récurrence.
 On passe d'un terme au suivant en ajoutant à chaque fois la même quantité $r$.
\item On notera $S_n$ la somme des n+1 premiers termes de la suite
 ($S_n=u_0+u_1+\ldots+u_n=\underset{p=0}{\overset{n}{\sum }}u_{p}$).
\end{remarque}

\begin{pro}
Soit $(u_{n})$ la suite arithmétique de premier terme $u_{0}$ et de
raison $r$ alors $\forall (n,p)\in \mathbb{N}^2$ on a:
\begin{equation*}
u_{n}=u_{p}+r\times (n-p)\;\text{ et }\; S_{n}=\dfrac{(n+1)(u_0+u_n)}{2}
\end{equation*}

\end{pro}


\begin{preuve}[facultatif]$\quad$

  \begin{itemize}
\item Montrons  par récurrence sur $n$ la relation suivante
  \begin{equation*}
u_n=u_0+nr.
\end{equation*}


Initialisation : on a : $u_{0}+0r=u_{0}$, donc la propriété est vraie au rang $n=0$.

Hérédité : Supposons que pour $n\in \mathbb{N}$ fixé quelconque on a : $u_{n}=u_{0}+nr$ et montrons qu'alors on a : $u_{n+1}=u_{0}+(n+1)r$.

On a : $u_{n+1}=u_{n}+r=\left( u_{0}+nr\right) +r=u_{0}+(n+1)r$.

Conclusion : $\forall n\in \mathbb{N}$ on a : $u_{n}=u_{0}+nr$.


Et $u_n-u_p=(u_{0}+nr)-(u_{0}+pr)=r\times (n-p)$.

\item Montrons maintenant le second résultat.

On a : $\left\{ 
\begin{array}{l}
S_{n}=u_{0}+u_{1}+...+u_{n} \\ 
S_{n}=u_{n}+u_{n-1}+...+u_{0}
\end{array}
\right. $.

Ajoutons membre à membre ces deux égalités; nous obtenons :

$2S_{n}=\left( u_{0}+u_{n}\right) +\left( u_{1}+u_{n-1}\right) +...+\left(
u_{n}+u_{0}\right) $.

Puisque : $u_{0}+u_{n}=u_{1}+u_{n-1}=...=u_{n}+u_{0}$, la somme précédente est la somme de $\left( n+1\right) $ termes identiques.

Nous en déduisons alors : $2S_{n}=\left( n+1\right) \left(u_{0}+u_{n}\right) $ et par suite : $S_{n}=\dfrac{\left( n+1\right) \left(
u_{0}+u_{n}\right) }{2}$.
\end{itemize}
\end{preuve}

\noindent \textbf{Cas particulier}:\\
Si on considère la suite arithmétique de premier terme $u_0=0$ et de raison $r=1$, on obtient:
$$0+1+\ldots+n=\frac{n(n+1)}{2}$$

\begin{exem}: \textbf{Placement à interêts simples}\\
Un capital $C_0$ est placé sur un compte rémunéré à $5\%$ avec interêts simples. 
Cela signifie que, chaque année,
 le montant des intérêts est égal à $5\%$ du capital de départ.\\
Si on note $C_n$ le capital après n 	années de placement. On a alors $C_{n+1}=C_n+0,05.C_0$.\\
La suite $(C_n)$ est donc une suite arithmétique de premier terme $C_0$ et de raison $0,05.C_0$.\\
Question : Combien d'années faut-il attendre pour doubler le capital de départ ?
\end{exem}


\subsubsection{Suites géométriques}
\begin{Def}
La suite $(u_{n})$ est dite \textbf{géométrique} de premier terme $u_{0}$
et de raison $q$ si :
 $$\forall n\in \mathbb{N},\; u_{n+1}=qu_{n}$$
\end{Def}

\begin{remarque}: 
\item Une telle suite est donc définie par récurrence.
 On passe d'un terme au suivant en multipliant à chaque fois la même quantité $q$.
\item Si $q=1$, la suite est tout simplement la suite constante égale
  à $u_0$.
  
\item Si $q=0$, la suite est nulle à partir du rang 1.
\end{remarque}

\begin{pro}


  Si $(u_{n})$ est une suite géométrique  de premier terme $u_0$ et de raison $q$ ($q\neq 1$ et
  $q\neq 0$) alors $\forall (n,p)\in \mathbb{N}^2$ on a :

\begin{equation*}
u_{n}=u_p\times q^{n-p} \;\text{ et } S_{n}=u_0.\frac{1-q^{n+1}}{1-q}.
\end{equation*}


\end{pro}


\begin{preuve}[facultatif]$\quad$

  
  \begin{itemize}

    
\item Montrons  par récurrence sur $n$ que $u_{n}=q^{n}u_{0}$.

Initialisation : on a : $q^{0}u_{0}=1u_{0}=u_{0}$, donc la propriété est vraie au rang $n=0$.

Hérédité : Supposons que pour $n\in \mathbb{N}$ fixé quelconque on a : $u_{n}=q^{n}u_{0}$ et montrons qu'alors on a : $u_{n+1}=q^{n+1}u_{0}$.

On a : $u_{n+1}=qu_{n}=q\left( q^{n}u_{0}\right) =q^{n+1}u_{0}$.

Conclusion : $\forall n\in \mathbb{N}$ on a : $u_{n}=q^{n}u_{0}$
(si $q\neq 0$).


Enfin, si $u_0$ est nul tous les termes sont nuls et $u_{n}=u_p\times
q^{n-p}$ sinon, on obtient cette relation en divisant $u_n$ par $u_p$.


\item Montrons maintenant le second résultat.

Soit $q\neq 1$

Remarquons tout d'abord que : $\left( 1-q\right) \left(
1+q+q^{2}+...+q^{n}\right) =1-q^{n+1}$.

Par conséquent nous en déduisons que :

\begin{equation*}
1+q+q^{2}+...+q^{n}=\frac{1-q^{n+1}}{1-q}\text{.}
\end{equation*}

Par suite : $S_{n}=u_{0}+qu_{0}+q^{2}u_{0}+...+q^{n}u_{0}=u_{0}\left(
1+q+q^{2}+...+q^{n}\right) =u_{0}\dfrac{1-q^{n+1}}{1-q}$.
\end{itemize}
\end{preuve}

\begin{exem}: \textbf{Placement à interêts composés}\\
Un capital $C_0$ est placé sur un compte rémunéré à $3,5\%$ avec
interêts composés. Si on note $C_n$ le capital après $n$ années de
placement. On a alors $C_{n+1}=C_n+0,035\times C_n=1,035\times
C_n$. La suite $(C_n)$ est donc une suite géométrique de premier terme
$C_0$ et de raison $1,035$. Dans cette situation, en combien de temps
le capital est-il multiplié par 2?
\end{exem}

\subsubsection{Quelques remarques}
\label{sec:retenir}

\begin{bclogo}[logo = \bcbook,arrondi=0.1]{ À retenir }
Pour les suites arithmétiques et les suites géométriques on a deux
façons de les définir:

\begin{itemize}
\item une formule de récurrence;
\item une formule explicite.
\end{itemize}
 



\renewcommand{\arraystretch}{1.7}

\begin{center}
  \begin{tabular}[h]{|>{\centering}p{3cm}|>{\centering}p{6cm}|>{\centering}p{6cm}|}
    \hline
    
    &Suite arithmétique de raison $r$&Suite géométrique de raison
                                       $q$\tabularnewline\hline
                                       
    Formule de récurrence & $u_{n+1}=u_n+r$&$u_{n+1}=u_n\times
                                             q$\tabularnewline\hline
  Formule explicite  &$u_n=u_p+r(n-p)$&$u_n=u_p\times q^{n-p}$\tabularnewline\hline
  Représentation graphique
    & \includegraphics[width=5cm]{images/suite_arith2}& \includegraphics[width=5cm]{images/suite_geo2}\tabularnewline

& Les points sont alignés& Les points appartiennent à une courbe
                           exponentielle
                           $(y=v_0q^x)$.\tabularnewline\hline
       
  \end{tabular}
\end{center}

\end{bclogo}



  De manière générale une suite peut être définie de manière explicite à
  l'aide d'une fonction définie sur $\R^+$ par $u_n=f(n)$ ou par
  récurrence à l'aide d'une fonction définie sur $\R$ par
  $u_{n+1}=f(u_n)$. La formule explicite permet de calculer n'importe
  quel terme directement, mais l'obtention d'une formule explicite à
  partir d'une formule de récurrence  n'est pas toujours
  possible... En ce sens, les suites arithmétiques et géométriques
  sont remarquables.


\begin{bclogo}[logo = \bcbook,arrondi=0.1]{ À retenir }
Il est souhaitable de  retenir les formules de sommation  sous la forme
suivante:


Somme des termes d'une suite arithmétique:
\begin{equation*}
(\text{nombre de termes})\times\frac{\text{premier terme +dernier terme}}{2}.
\end{equation*}



Somme des termes d'une suite géométrique de raison différente de 1:
\begin{equation*}
\text{Premier terme} \times \frac{1-q^{\text{nombre de
 termes}}}{1-q}.
\end{equation*}
\end{bclogo}

  

\subsection{Comportement global d'une suite}
\subsubsection{Monotonie d'une suite}
\begin{Def}
\item La suite $(u_{n})$ est \textbf{croissante} si : $\forall n\in \mathbb{N}$ on
a : $u_{n+1}\geqslant u_{n}$.

\item La suite $(u_{n})$ est \textbf{décroissante} si : $\forall n\in \mathbb{N}$ on a : $u_{n+1}\leqslant u_{n}$.

\item La suite $(u_{n})$ est \textbf{monotone} si elle est croissante ou décroissante.
\end{Def}

\begin{remarque}

\begin{itemize}
\item Si les inégalités précédentes sont strictes, nous dirons que la
  suite est strictement croissante, strictement décroissante ou
  strictement monotone.
\item Étudier la monotonie d'une suite, c'est dire si elle est
  croissante, décroissante ou ni l'un ni l'autre.
\end{itemize}
\end{remarque}

\begin{remarque}
Toutes les défintions précédentes peuvent être considérées à partir d'un certain rang.\\
Par exemple, la suite $(u_{n})$ est dite \textbf{croissante à partir d'un certain rang} si 

$$
 \exists n_0 \in \N\text{ tel que : }\forall n\geq n_0\text{ on a
: }u_{n+1}\geq u_n\text{.}
$$
\end{remarque}



\noindent Pour étudier la monotonie d'une suite, on utilise, généralement deux méthodes selon le type de suites.
\begin{itemize}
\item[-] Soit on  cherche à déterminer le signe de $u_{n+1}-u_n$.
\item[-] Soit, si $\forall n\geq n_0, u_n>0$, on étudie le quotient $\dfrac{u_{n+1}}{u_n}$
\end{itemize}
\noindent On peut aussi être amené à faire une démonstration par récurrence.


\begin{exem}
Considérons la suite $(u_{n})$ définie par : $\forall n\in \mathbb{N}
$ , $u_{n}=2n-5$.\\
Puisque : $\forall n\in \mathbb{N}$ on a : $u_{n+1}-u_{n}=(2(n+1)-5)-(2n-5)=2$ donc $u_{n+1}-u_{n}>0$.\\
On en déduit que la suite $\left( u_{n}\right) $ est croissante (même strictement).
\end{exem}


\begin{exem}
Considérons la suite $(u_{n})$ définie par  : $u_0>0$ et $\forall n\in \mathbb{N}, u_{n+1}=\dfrac{2u_n}{u_n+3}$.\\
Remarquons tout d'abord que : $\forall n\in \mathbb{N}$ , $u_{n}>0$ (récurrence).\\
On peut alors regarder le quotient $\dfrac{u_{n+1}}{u_n}=\dfrac{2u_n}{u_n(u_n+3)}=\dfrac{2}{(u_n+3)}$.\\
Or $\; u_n+3>3>2$ (car $u_n>0$) donc $\dfrac{u_{n+1}}{u_n}<1$.\\
 On en déduit que la suite $(u_n)$ est décroissante.
\end{exem}

\begin{exem}
 Soit $(u_n)$ la suite définie par $u_0=2$ et $u_{n+1}=\sqrt{u_n}$, $\forall n\in \N$.\\
Démontrons par récurrence que  :  $\forall n\in \N, \; 0\leq u_{n+1}\leq u_n$\\
Initialisation: $u_1=\sqrt{2}$ donc $0\leq u_{1}\leq 2 \Leftrightarrow 0\leq u_1\leq u_0$

\noindent Hérédité : Soit $n\in\N$, on suppose que $0\leq u_{n+1}\leq u_n$.\\
On a alors, par croissance de la fonction racine carrée,  $\sqrt{0}\leq \sqrt{u_{n+1}}\leq \sqrt{u_n}\Leftrightarrow 0\leq u_{n+2}\leq u_{n+1}$.\\
La proprièté est donc bien héréditaire.

\noindent Conclusion :  $\forall n\in \N, \; 0\leq u_{n+1}\leq u_n$. En particulier, la suite $(u_n)$ est décroissante.
\end{exem}


\subsubsection{Suites bornées}
\begin{Def}$\quad$
\begin{enumerate}
\item[-] La suite $(u_{n})$ est \textbf{majorée} si l'ensemble de ses termes est
majoré, soit encore :%
\begin{equation*}
\exists M\in \mathbb{R}\text{ tel que  }\forall n\in \mathbb{N}\text{ on a
: }u_{n}\leqslant M\text{.}
\end{equation*}
(M est alors un \textbf{majorant} de la suite $(u_n)$)

\item[-] La suite $(u_{n})$ est \textbf{minorée} si l'ensemble de ses termes est
minoré, soit encore :%
\begin{equation*}
\exists m\in \mathbb{R}\text{ tel que  }\forall n\in \mathbb{N}\text{ on a
: }u_{n}\geqslant m\text{.}
\end{equation*}
(m est alors un \textbf{minorant} de la suite $(u_n)$)
\item[-] La suite $(u_{n})$ est \textbf{bornée }si elle est à la fois major%
ée et minorée, soit encore :%
\begin{equation*}
\exists (M,m)\in \mathbb{R}^2 \text{ tels que  }\forall n\in \mathbb{N}\text{ on
a : }m\leqslant u_{n}\leqslant M\text{.}
\end{equation*}
\end{enumerate}
\end{Def}

\begin{remarque}: Il n'y a pas unicité du majorant (ou du minorant) d'une suite.\\
En effet, soit M un majorant de la suite $(u_n)$, si $M'\geq M$ alors $\forall n\in \N, u_n\leq M\leq M'$ et donc M' est 
aussi un majorant de la suite.
\end{remarque}

\begin{remarque}
\item[-] Une suite croissante est minorée par son premier terme.
\item[-] Une suite décroissante est majorée par son premier terme
\end{remarque}

\begin{exem}
\end{exem}
Soit $(u_n)$ la suite définie par $u_0=1$ et $u_{n+1}=\sqrt{u_n+6},\; \forall n\in\N$.\\
Démontrons que $(u_n)$ est bornée (minorée par $0$, majorée par $3$) :\\
\begin{enumerate}
 \item[-] Pour $n=0$, $u_0=1$ donc $0\leq u_0\leq 3$,
\item[-] Soit $n\in\N$, on suppose que $0\leq u_n\leq 3$, on a alors $6\leq u_n+6\leq 9$ donc $0\leq u_{n+1}\leq 3$,
\item[-] Conclusion : $\forall n\in\N,\; 0\leq u_n\leq 3$ ce qui signifie que $(u_n)$ est bornée.
\end{enumerate}

\subsubsection{Suites extraites}

\begin{Def}
Une suite $\left( v_{n}\right) $ est appelée \textbf{suite extraite}, ou
encore sous-suite, d'une suite $\left( u_{n}\right) $ s'il existe une
fonction $f$, strictement croissante de $\mathbb{N}$ dans $\mathbb{N}$ vé%
rifiant : $v_{n}=u_{f(n)}$.
\end{Def}

\begin{exem}
Si $f(n)=2n$, alors la suite $\left( v_{n}\right) $ définie par : $%
v_{n}=u_{2n}$ est la sous-suite de  $\left( u_{n}\right) $ formé%
e des termes de rang pair.
\end{exem}

\begin{exem}
Si $f(n)=2n+1$, alors la suite $\left( w_{n}\right) $ définie par : $%
w_{n}=u_{2n+1}$ est la sous-suite de  $\left( u_{n}\right) $ formée des termes de rang impair.
\end{exem}

\begin{exem}
Si $u_{n}=\left( -1\right) ^{n}$, alors $v_{n}=u_{2n}=(-1)^{2n}=1$ et $w_{n}=u_{2n+1}=(-1)^{2n+1}=-1$.\\
Ces deux sous-suites sont constantes.
\end{exem}

\begin{pro}
 Si la suite $(u_n)$ est croissante, alors toute suite extraite de $(u_n)$ est croissante.
\end{pro}

\begin{preuve}
Supposons que la suite $(u_n)$ est croissante et considèrons une suite $(v_n)$ extraite de $(u_n)$.\\
Il existe une fonction $f$, strictement croissante de $\mathbb{N}$ dans $\mathbb{N}$ telle que $v_{n}=u_{f(n)}$, $\forall n\in\N$.\\
On veut démontrer que  $\forall n\in\N$, $v_{n+1}\geq v_n$ :\\
On sait que $n+1> n$ et que $f$ est strictement croissante donc $f(n+1)> f(n)$,\\
or la suite $(u_n)$ est croissante donc $u_{f(n+1)}\geq u_{f(n)}$ c'est à dire $v_{n+1}\geq v_n$.\\

\noindent La suite $(v_n)$ est donc elle aussi croissante.
\end{preuve}

\begin{remarque}
 La réciproque est fausse !\\
 Pour s'en convaincre, il suffit de prendre la suite $(u_n)$ définie si ci-dessus par $u_{n}=\left( -1\right) ^{n}$, $\forall n\in\N$.
\end{remarque}

\begin{remarque}
 La proposition ci-dessus est encore vraie pour une suite décroissante, minorée ou majorée.
\end{remarque}






\end{document}


