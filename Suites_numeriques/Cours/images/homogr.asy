// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
// code figure
import graph;
unitsize(10cm,10cm);
xlimits(0,1);
ylimits(0,1);
real F(real x) {return (2*x+3)/(x+4);}
real s_abs=0.05;
real s_ord=0;
string nomsuite="u";
for (int i = 0; i <3; ++i)
{
s_ord=F(s_abs);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+white+solid);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+gray+dashed);
if (i>0)
{
draw((s_abs,s_abs)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,s_abs)--(s_abs,s_ord),linewidth(1.2pt)+gray+dashed,Arrow(SimpleHead,3mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+gray+dashed,Arrow(SimpleHead,3mm));
}
else
{
draw((s_abs,0)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,0)--(s_abs,s_ord),linewidth(1.2pt)+gray+dashed,Arrow(SimpleHead,3mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+gray+dashed,Arrow(SimpleHead,3mm));
}
s_abs=s_ord;
}
draw(graph(new real(real x){return x;},0,1),linewidth(1pt)+heavygray+solid);
draw(graph(F,0,1,n=400),linewidth(1pt)+black+solid);
xlimits(0,1,Crop);
ylimits(0,1,Crop);
xaxis(axis=YEquals(0),xmin=0,xmax=1,Ticks("%",NoZero,Step=1,Size=1mm),p=linewidth(1pt)+black,true);
yaxis(axis=XEquals(0),ymin=0,ymax=1,Ticks("%",NoZero,Step=1,Size=1mm),p=linewidth(1pt)+black,true);
labelx(Label("$O$",NoFill), 0, SW);
draw(Label("$\vec{\imath}$",NoFill), (0,0)--(1,0),S,Arrow(2mm));
draw(Label("$\vec{\jmath}$",NoFill), (0,0)--(0,1),W,Arrow(2mm));
dot((0,0));
if (nomsuite!="rien")
{
s_abs=0.05;
s_ord=0;
for (int i = 0; i <3; ++i)
{
if (i>0) label("$"+nomsuite+"_{"+(string) i+"}$",(0,s_abs),4*W);
label("$"+nomsuite+"_{"+(string) i+"}$",(s_abs,0),4*S);
s_ord=F(s_abs);
s_abs=s_ord;
}
label("$"+nomsuite+"_{3}$",(0,s_abs),4*W);
label("$"+nomsuite+"_{3}$",(s_abs,0),4*S);
}
// code supplémentaire

shipout(bbox(0.1cm,0.1cm,white));


