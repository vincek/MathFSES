// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
settings.tex="pdflatex";

// code figure
import graph;
unitsize(3cm,3cm);
xlimits(-1.5,3);
ylimits(-1,3);
real F(real x) {return sqrt(x+2);}
real s_abs=0;
real s_ord=0;
string nomsuite="u";
for (int i = 0; i <3; ++i)
{
s_ord=F(s_abs);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+white+solid);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+red+dotted);
if (i>0)
{
draw((s_abs,s_abs)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,s_abs)--(s_abs,s_ord),linewidth(1.2pt)+red+dotted,Arrow(SimpleHead,2mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+red+dotted,Arrow(SimpleHead,2mm));
}
else
{
draw((s_abs,0)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,0)--(s_abs,s_ord),linewidth(1.2pt)+red+dotted,Arrow(SimpleHead,3mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+red+dotted,Arrow(SimpleHead,3mm));
}
s_abs=s_ord;
}
draw(graph(new real(real x){return x;},-2,5),linewidth(1pt)+heavygray+solid);
draw(graph(F,-2,5,n=400),linewidth(1pt)+black+solid);
xlimits(-1.5,3,Crop);
ylimits(-1,3,Crop);
xaxis(axis=YEquals(0),xmin=-1.5,xmax=3,Ticks("%",NoZero,Step=1,Size=1mm),p=linewidth(1pt)+black,true);
yaxis(axis=XEquals(0),ymin=-1,ymax=3,Ticks("%",NoZero,Step=1,Size=1mm),p=linewidth(1pt)+black,true);
labelx(Label("$O$",NoFill), 0, SW);
draw(Label("$\vec{\imath}$",NoFill), (0,0)--(1,0),S,Arrow(2mm));
draw(Label("$\vec{\jmath}$",NoFill), (0,0)--(0,1),W,Arrow(2mm));
dot((0,0));
if (nomsuite!="rien")
{
s_abs=0;
s_ord=0;
for (int i = 0; i <3; ++i)
{
if (i>0) label("$"+nomsuite+"_{"+(string) i+"}$",(0,s_abs),4*W);
label("$"+nomsuite+"_{"+(string) i+"}$",(s_abs,0),4*S);
s_ord=F(s_abs);
s_abs=s_ord;
}
label("$"+nomsuite+"_{3}$",(0,s_abs),4*W);
label("$"+nomsuite+"_{3}$",(s_abs,0),4*S);
}
// code supplémentaire

shipout(bbox(0.1cm,0.1cm,white));

