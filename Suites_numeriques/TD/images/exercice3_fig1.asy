// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
// code figure
import graph;
unitsize(0.05cm,0.05cm);
xlimits(0,100);
ylimits(0,100);
real F(real x) {return x^2+2/x;}
real s_abs=1;
real s_ord=0;
string nomsuite="u";
for (int i = 0; i <3; ++i)
{
s_ord=F(s_abs);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+white+solid);
draw((s_abs,0)--(s_abs,s_ord)--(0,s_ord)--(s_ord,s_ord)--(s_ord,0),linewidth(0.5pt)+gray+dotted);
if (i>0)
{
draw((s_abs,s_abs)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,s_abs)--(s_abs,s_ord),linewidth(1.2pt)+gray+dotted,Arrow(SimpleHead,3mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+gray+dotted,Arrow(SimpleHead,3mm));
}
else
{
draw((s_abs,0)--(s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+white+solid,Arrow(SimpleHead,3mm));
draw((s_abs,0)--(s_abs,s_ord),linewidth(1.2pt)+gray+dotted,Arrow(SimpleHead,3mm));
draw((s_abs,s_ord)--(s_ord,s_ord),linewidth(1.2pt)+gray+dotted,Arrow(SimpleHead,3mm));
}
s_abs=s_ord;
}
draw(graph(new real(real x){return x;},1,100),linewidth(1pt)+heavygray+solid);
draw(graph(F,1,100,n=400),linewidth(1pt)+black+solid);
xlimits(0,100,Crop);
ylimits(0,100,Crop);
xaxis(axis=YEquals(0),xmin=0,xmax=100,Ticks("%",NoZero,Step=10,Size=1mm),p=linewidth(1pt)+black,true);
yaxis(axis=XEquals(0),ymin=0,ymax=100,Ticks("%",NoZero,Step=10,Size=1mm),p=linewidth(1pt)+black,true);
labelx(Label("$O$",NoFill), 0, SW);
draw(Label("$\vec{\imath}$",NoFill), (0,0)--(1,0),S,Arrow(2mm));
draw(Label("$\vec{\jmath}$",NoFill), (0,0)--(0,1),W,Arrow(2mm));
dot((0,0));
if (nomsuite!="rien")
{
s_abs=1;
s_ord=0;
for (int i = 0; i <3; ++i)
{
if (i>0) label("$"+nomsuite+"_{"+(string) i+"}$",(0,s_abs),4*W);

s_ord=F(s_abs);
s_abs=s_ord;
}
label("$"+nomsuite+"_{3}$",(0,s_abs),4*W);
label("$"+nomsuite+"_{3}$",(s_abs,0),4*S);
}
// code supplémentaire
settings.tex="pdflatex";
shipout(bbox(0.1cm,0.1cm,white));

