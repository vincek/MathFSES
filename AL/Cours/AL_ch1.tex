%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Cours,12pt]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\titre{L'espace vectoriel $\R^n$}
\auteur{M. Pelini, V. Ledda}
\reference{Chapitre 1}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 SIAD \\
Algèbre linéaire}

\motsclefs{espace vectoriel, base, dimension finie, famille libre,
  famille génératrice}




\etudiant=0

\begin{document}

\maketitle
\tableofcontents
\newpage


\section{Structure de $\mathbb{R}^n$}

\subsection{Exemples}
\label{sec:exemples}

\begin{exem}
  Plaçons-nous dans un plan muni d'un repère et considérons une droite
  passant par l'origine. Cette droite est dirigé par un vecteur non
  nul, en réalité il existe une infinité de vecteurs qui dirige cette
  droite. Considérons $V$, l'ensemble des vecteurs directeurs de cette
  droite auquel on ajoute le vecteur nul. 

  \begin{itemize}
  \item Si l'on prend 2 vecteurs de cet ensemble, leur somme est soit
    nulle, soit un vecteur directeur de la droite. Donc le vecteur
    somme appartient à $V$, on dit que $V$ est stable pour la somme.
  \item La multiplication d'un vecteur de $V$ par un nombre réel est
    également un vecteur de $V$. On dit que $V$ est stable par la
    multiplication par un réel.
  \end{itemize}

  L'ensemble $V$ est un espace vectoriel.

  \begin{center}
    \includegraphics[width=8cm]{images/sommevec}
  \end{center}
\end{exem}


\begin{exem}
  De la même manière les plans de l'espace (muni d'un repère) qui passent par l'origine fournissent
  des exemples d'espace vectoriel.

  \begin{enumerate}
  \item Le plan $y0z$ d'équation $x=0$
  \item Le plan d'équation $x+y+z=0$ <<dirigé>> par les vecteurs
    $\vec{u}\coord{1}{-1}{0}$ et   $\vec{v}\coord{1}{}{-1}$.
  \end{enumerate}

  \begin{center}
    \includegraphics[width=8cm]{images/palnvec}
  \end{center}
  
\end{exem}

\subsection{Définitions}
\begin{Def}
Soit $n\in \mathbb{N}^*$.\\ On appelle  $\mathbb{R}^n$  l'ensemble des $n$-uplets $(x_1,\ldots ,x_n)$ avec $x_1,\ldots ,x_n$ réels.\\
Le n-uplet $\underbrace{(0,\ldots,0)}_{n \text{ fois}}$ sera noté $0_n$.
\end{Def}



\noindent Nous allons munir $\mathbb{R}^n$ des lois suivantes:
\begin{enumerate}
\item[$i)$] L'addition notée ``$+$'': 
Soit $x=(x_1,\ldots ,x_n)$ et $y=(y_1,\ldots ,y_n)$ dans $\mathbb{R}^n$,\\
 on définit $x+y$ de la manière suivante $$x+y=(x_1,\ldots ,x_n)+(y_1,\ldots ,y_n)=(x_1+y_1,\ldots ,x_n+y_n)$$
\item[$ii)$] La multiplication externe notée `` $\cdot$ '': 
Soit $x=(x_1,\ldots ,x_n)$  dans $\mathbb{R}^n$ et $\lambda\in\mathbb{R}$,\\
 on définit $\lambda\cdot x$ de la manière suivante $$\lambda\cdot x=(\lambda x_1,\ldots ,\lambda x_n)$$.
\end{enumerate}

\begin{remarque}
On voit que:\\
La somme de deux éléments de $\mathbb{R}^n$ est encore un élément de $\mathbb{R}^n$, c'est pourquoi on dira que l'addition est une loi de composition interne sur $\mathbb{R}^n$ (ou encore que $\mathbb{R}^n$ est stable par ``$+$'').\\
La multiplication d'un élément de $\mathbb{R}^n$ par un nombre réel est encore un élément de $\mathbb{R}^n$, c'est pourquoi on dira que la multiplication par un réel est une loi de composition externe sur $\mathbb{R}^n$ (ou encore que $\mathbb{R}^n$ est stable par `` $\cdot$ '').\\
\end{remarque}



\subsection{Propriétés}
\noindent Soit $n\in\mathbb{N}^*$, dans la suite du chapitre, on notera $E=\mathbb{R}^n$.

\begin{pro}
\begin{enumerate}
\

 \item[] La loi ``$+$'' vérifie les propriétés suivantes:
\begin{enumerate}
\item[$1$-] La loi $+$ est associative: Pour tout triplet $(x,y,z)$ de $E$, $(x+y)+z=x+(y+z)$.
\item[$2$-]  $\forall x \in E, x+0_n=0_n+x=x$.\\
L'élément $0_n$ est appelé élément neutre pour la loi $+$.
\item[$3$-] Pour tout élément $x$ de $E$, il existe $y\in E$ tel que $x+y=y+x=0_n$.\\
Cet élément est appelé symétrique (ou encore opposé) de $x$. On le note généralement $-x$.
\item[$4$-] La loi $+$ est commutative: Pour tout couple $(x,y)$ de $E$, $x+y=y+x$.
\end{enumerate}
\item[]  La loi `` $\cdot$ '' vérifie les propriétés suivantes:
\begin{enumerate}
\item[$5$-] $\forall(\lambda,\mu)\in \mathbb{R}^2,~ \forall x\in E, \lambda\cdot (\mu\cdot x)=(\lambda\mu)\cdot x$,
\item[$6$-] $\forall x\in E, 1\cdot x=x$,
\item[$7$-] $\forall(\lambda,\mu)\in \mathbb{R}^2, ~\forall x\in E,(\lambda +\mu)\cdot x=\lambda\cdot x+\mu\cdot x$,\\
(l'addition des réels est distributive par rapport à $\cdot$)
\item[$8$-] $\forall\lambda\in \mathbb{R}, ~\forall (x,y)\in E^2, \lambda\cdot(x+y)=(\lambda\cdot x)+(\lambda\cdot y)$.\\
(l'addition des éléments de $E$ est distributive par rapport à $\cdot$)
\end{enumerate}
\end{enumerate}
\end{pro}

\begin{remarque}
le symétrique de chaque élément de $E$ est  unique.\\
Soit $x\in E,\; x=(x_1,\ldots,x_n)$ on a alors $-x=(-x_1,\ldots,-x_n)=(-1).x$
\end{remarque}


\begin{pro}(Règles de calculs) $\forall (\lambda,\mu) \in \mathbb{R}^2,~\forall (x,y)\in E^2$, on a: 
\begin{enumerate}
\item[(i)] $0\cdot x=0_n  \text{ et } \lambda\cdot 0_n=0_n$.
\item[(ii)] $\lambda\cdot x=0_n~ \Leftrightarrow \lambda=0 \text{ ou } x=0_n$
\item[(iii)] $\lambda\cdot x=\lambda\cdot y~ \Leftrightarrow x=y$ (si $\lambda\neq 0$)
\item[(iv)] $\lambda\cdot x=\mu\cdot x~ \Leftrightarrow \lambda=\mu$  (si $x\neq 0_n$)
\item[(v)] $(-\lambda)\cdot x=\lambda \cdot(-x)=-(\lambda x)$
\end{enumerate}
\end{pro}

\subsection{($\mathbb{R}^n,+,\cdot)$: un espace vectoriel}
\noindent Les huit propriétés ci-dessus étant vérifiées, on dira que l'ensemble $\mathbb{R}^n$ muni des lois ``$+$'' et `` $\cdot$ '' est un \textbf{ espace vectoriel } sur $\mathbb{R}$.\\
Les éléments de $\mathbb{R}^n$ sont appelés des \textbf{vecteurs} et $0_n=(0,\ldots,0)$ est le \textbf{vecteur nul}.\\
Les éléments de $\mathbb{R}$ sont appelés des \textbf{scalaires}.\\

\noindent Nous travaillerons très souvent dans les ensembles suivants: \\
$\mathbb{R}^2$ l'ensemble des couples $\left( x,y\right)$ où $x$ et $y$ sont des réels.\\
$\mathbb{R}^{3}$ l'ensemble des triplets $\left( x,y,z\right)$ où $x$, $y$ et $z$  sont des réels.

\begin{remarque}
  Il existe d'autre espace vectoriel. Par exemple l'ensemble des
  polynômes est un espace vectoriel. L'ensemble des matrices de taille
  $(n;m)$ est également un espace vectoriel.
\end{remarque}

\subsection{Contre-exemples}
\noindent L'ensemble $\mathbb{N}$ des entiers naturels n'est pas un espace vectoriel sur $\mathbb{R}$.\\
En effet, $1$ (par exemple) ne possède pas de symétrique dans $\mathbb{N}$.\\
Le symétrique de $1$ pour la loi ``$+$'' est $-1$ qui n'est pas dans $\mathbb{N}$.\\

\noindent Cela nous conduit tout naturellement à considérer l'ensemble $\mathbb{Z}$ des entiers relatifs.\\
Mais $\mathbb{Z}$ lui non plus n'est pas un espace vectoriel sur $\mathbb{R}$.\\
En effet, $1\in \mathbb{Z}$ et $1,5\in \mathbb{R}$ mais $1,5 \cdot 1=1,5$ n'est pas dans $\mathbb{Z}$.\\
Autrement dit, $\mathbb{Z}$ n'est pas stable par `` $\cdot$ ''

\begin{remarque}
 On peut aussi montrer que $\mathbb{R}^n$ muni de certaines lois n'est plus un espace vectoriel.
\end{remarque}


\section{Sous-espaces vectoriels}
\noindent Dans ce paragraphe, $E$ désigne l'espace vectoriel  $\mathbb{R}^n$, (avec $n\in\mathbb{N}^*$).
\
\subsection{Combinaisons linéaires de $p$ vecteurs}
\begin{Def}
Soient $(u_1,\ldots ,u_p)$ $p$ vecteurs de $E$.\\
$x$ est une combinaison linéaire (CL) de $u_1,\ldots ,u_p$ s'il existe $p$ réels $\lambda_1,\ldots , \lambda_p$ tels que:
$$x=\lambda_1 u_1+\ldots +\lambda_p u_p=\sum_{i=1}^p \lambda_i u_i$$
\end{Def}

\begin{exem}
\begin{itemize}
\item[-] Dans $\mathbb{R}^2$, le vecteur $(1,-3)$ est une CL des vecteurs $(2,1)$, $(1,1)$ et $(-2,1)$. En effet, 
$$(1,-3) =-5(2,1)+5(1,1)-3(-2,1), \text{ ou encore }(1,-3)= 4(2,1)-7(1,1)+0(-2,1)$$
\item[-] Dans $\mathbb{R}^3$, le vecteur $(-1,0,4)$ est une CL des vecteurs $(3,-2,2)$ et $(-2,1,1)$. En effet, 
$$(-1,0,4)=(3,-2,2)+2(-2,1,1)$$
\end{itemize}
\end{exem}

\subsection{Sous-espace vectoriel de $E$}
\begin{Def}
Soit $F$ un sous-ensemble de $E$. $F$ est un sous-espace vectoriel (sev) de $E$ s'il vérifie: 
\begin{itemize}
\item[-] $F$ est non vide,
\item[-] $F$ est stable par $+$ (i.e. $\forall (u,v)\in F^2, u+v\in F$),
\item[-] $F$ est stable par $\cdot$ (i.e. $\forall \lambda\in \mathbb{R},~ \forall u\in F, \lambda\cdot u\in F$).
\end{itemize}
\end{Def}

\begin{remarque}
Si $F$ est un sous-espace vectoriel de $E$, alors $0_n\in F$.\\
En effet, comme $F$ n'est pas vide, il existe $u\in F$, alors $0\cdot u\in F$ et donc $0_n\in F$.
\end{remarque}
Par exemple, le sous-ensemble $F$ de $\mathbb{R}^3$ défini par $F=\lbrace (x,y,1),\text{ avec } x \text{ et } y \text{ réels}\rbrace$ n'est pas un sous-espace vectoriel de $\mathbb{R}^3$ car il ne contient pas le vecteur nul.

\begin{exem}
$E$ et $\{0_n\}$ sont deux sous-espaces vectoriels de $E$
\end{exem}

\begin{remarque}
Lorsque $F$ est un sous-espace vectoriel de $E$, on constate que: \\
$+$ est une loi de composition interne dans $F$ (car $F$ stable par $+$) et $\cdot$ est une loi de composition externe dans $F$ 
(car $F$ stable par $\cdot$),\\
toutes les propriétés de l'addition et de la multiplication par un réel, valables dans $E$, sont encore valables dans le sous ensemble $F$,\\
tout vecteur $u\in F$ possède un opposé \textbf{dans $F$} (car $-u=(-1)\cdot u$ et $F$ est stable par $\cdot$).\\
Donc $F$ est un espace vectoriel.
\end{remarque}
Cette remarque est très utile dans la pratique. En effet, pour montrer qu'un ensemble est un espace vectoriel, nous montrerons souvent que c'est en fait un sous-espace vectoriel d'un espace vectoriel connu.




\subsection{Caractérisation}
\begin{pro}
Soit $F$ un sous-ensemble de $E$.\\
\begin{center}
  $F$ est un sous-espace vectoriel de $E$ si et seulement si $\left\{ \begin{array}{l}
\textit{i}) \; 0_n \in F \\
\textit{ii}) ~\forall (\lambda,\mu)\in \mathbb{R}^2,~ \forall (u,v)\in F^2,~ \lambda\cdot u+\mu\cdot v \in F
\end{array}
\right.$
\end{center}

\end{pro}

\begin{remarque}
\

\noindent Dans la pratique, pour montrer que $F$ est non vide, on vérifie que $F$ contient le vecteur nul.\\
Pour le $ii)$, on dira que $F$ est stable par combinaisons linéaires.
\end{remarque}

\begin{preuve}

$\Rightarrow$
Supposons que $F$ soit un sous-espace vectoriel de $E$.\\
Par définition, $F$ est non vide car contient $0_E$.\\
De plus, si $u$ et $v$ sont des vecteurs de $F$ et si $\lambda$ et $\mu$ sont  des réels alors, $F$ étant stable par $\cdot$, $\lambda\cdot u$ et $\mu\cdot v$ sont dans $F$.\\
Enfin, $F$ étant stable par $+$, $\lambda\cdot u+\mu\cdot v$ est aussi dans $F$.

$\Leftarrow$
Supposons que $F$ soit non vide et stable par combinaisons linéaires.\\
En prenant $\lambda=1$ dans la définition, on obtient que $F$ est stable par $+$,\\
en prenant $\mu =0$ dans la définition, on obtient que $F$ est stable par $\cdot$.\\
Donc $F$ est bien un sous-espace vectoriel de $E$.
\end{preuve}

\begin{exem}
Soit $F$ le sous-ensemble de $\mathbb{R}^2$ défini par $F=\left\{(x,y) \in \mathbb{R}^2 /~3x-4y=0 \right\}$.
Montrons que $F$ est un sous-espace vectoriel de $\mathbb{R}^2$.

\label{ex:droite}
\begin{prof}
  $3\cdot 0-4\cdot 0=0$ donc $(0,0) \in F$.\\
  Soient $u=(x,y)$ et $u'=(x',y')$ des vecteurs de $F$ et soient $\lambda$ et $\mu$ des réels.\\
  On pose $v=\lambda u+ \mu u'=(\lambda(x,y)+\mu(x',y')=(\lambda x+\mu x',\lambda y+\mu y')$.\\
  Vérifions que $v\in F$: \\
  $3(\lambda x+\mu x')-4(\lambda y+\mu y')=\lambda(3 x-4 y)+\mu(3 x'-4 y')=\lambda\cdot 0-\mu \cdot 0=0$, donc $F$ est bien stable par combinaisons linéaires.\\
  $F$ est bien un sous-espace vectoriel de $\mathbb{R}^2$.
\end{prof}


\end{exem}
\begin{exem}
L'ensemble $\mathcal{S}$ des solutions de l'équation homogène $(E): ax+by+cz=0$ est un sous-espace vectoriel de $\mathbb{R}^3$.\\
\begin{prof}
  En effet $0_3\in \mathcal{S}$.\\
  De plus, si  $X=(x,y,z)$ et $X'=(x',y',z')$ sont deux éléments de $\mathcal{S}$, $\lambda X+\mu X'$ est aussi un élément de $\mathcal{S}$:\\
  $a(\lambda x+\mu x')+b(\lambda y+\mu y')+c(\lambda z+\mu z')=\lambda\underbrace{(ax+by+cz)}_{=0}+\mu\underbrace{(ax'+by'+cz')}_{=0}=\lambda\cdot 0+\mu\cdot 0=0$.\\
  Donc $\mathcal{S}$ est non vide et stable par combinaisons linéaires.\\
  On en déduit que c'est bien un sous-espace vectoriel de
  $\mathbb{R}^3$.
\end{prof}



\end{exem}


\subsection{Sous-espace vectoriel engendré par $p$ vecteurs}

\noindent Soit $p\in \mathbb{N}^*$ et soient $u_1$, $\ldots$, $u_p$ $p$ vecteurs de $E$.\\
On note $F$ le sous-ensemble de $E$ formé de toutes les CL de $u_1$, $\ldots$, $u_p$: 
$$F=\lbrace \lambda_1 u_1+\ldots +\lambda_p u_p ,~\lambda_1,\ldots ,\lambda_p \text{ réels }\rbrace$$

\begin{pro}
L'ensemble $F$ ainsi défini est un sous-espace vectoriel de $E$.
\end{pro}
\begin{preuve}
\begin{enumerate}
\item[]$0_E\in F$ (prendre $\lambda_1=\ldots=\lambda_p=0$) donc $F$ est non vide.
\item[]Soient $v=\lambda_1 u_1+\ldots +\lambda_p u_p$ et $v'=\lambda'_1 u_1+\ldots +\lambda'_p u_p$ deux éléments de $F$ et 
$\alpha$ et $\beta$ deux réels.\\
$\alpha v+\beta v'= (\alpha\lambda_1+\beta\lambda'_1) u_1+\ldots +(\alpha\lambda_p+\beta\lambda'_p)u_p$ donc $\alpha v+\beta v' \in F$.\\

\noindent $F$ est stable par combinaisons linéaires.
\item[]Donc $F$ est bien un sous-espace vectoriel de $E$.
\end{enumerate}
\end{preuve}

\begin{Def}
Ce sous-espace vectoriel est appelé \textnormal{sous-espace vectoriel engendré par $u_1$, $\ldots$, $u_p$}.\\
On le note $F=\Vect\lbrace u_1, \ldots, u_p\rbrace $.
$$F=\Vect\lbrace u_1, \ldots, u_p\rbrace=\lbrace \lambda_1 u_1+\ldots +\lambda_p u_p ,~\lambda_1,\ldots ,\lambda_p \text{ réels }\rbrace$$
\end{Def}



\begin{exem} Reprenons l'exemple \ref{ex:droite},\\
soit $F$ le sous-ensemble de $\mathbb{R}^2$ défini par $F=\left\{(x,y)
  \in \mathbb{R}^2 /~3x-4y=0 \right\}$.


Nous avons déjà démontré que $F$ est un sous-espace vectoriel de $\mathbb{R}^2$. Plus précisément,
 montrons que $F$ est le sous-espace vectoriel de $\mathbb{R}^2$
 engendré par le vecteur $u=\left(4 ; 3 \right)$.

 
 \begin{prof}
   \noindent $F=\left\{(x,y) \in \mathbb{R}^2 /~3x-4y=0 \right\}=\left\{(\dfrac43 y,y),\;  y\in \mathbb{R}  \right\}$.\\
   On a donc
   $F=\Vect\{\left(\dfrac43,1\right)\}=\Vect\{\left(4 ; 3\right)\}$.
 \end{prof}
 
\end{exem}
\begin{exem}

Grâce à cette propriété, on dispose d'une méthode simple pour démontrer qu'un ensemble est un sous-espace vectoriel de $\mathbb{R}^n$.\\
Nous allons démontrer que $F=\left\{(x,y,z ) \in \mathbb{R}^3/ x=2y+3z\right\}$ est un sous-espace vectoriel de $\mathbb{R}^3$.\\
 En effet
$F=\left\{
(2y+3z, y, z),~ (y,z)\in \mathbb{R}^2\right\}=
\left\{
y\left(2,1,0\right)+
z\left(3,0,1 \right), (y,z)\in \mathbb{R}^2 \right\}$\\

\vspace{0,1cm}

\noindent $F$ est en fait l'ensemble des CL des vecteurs $u=\left( 2,1,0 \right)$ et 
$v=\left(3,0,1\right)$\\
Donc $F$ est le sous-espace vectoriel engendré par les vecteurs $u$ et $v$.\\

\end{exem}
\begin{remarque}
Si $u_1$, $\ldots$, $u_p$ sont dans $\Vect(v_1, \ldots, v_k)$ alors  $\Vect(u_1, \ldots, u_p)\subset \Vect(v_1, \ldots, v_k)$.
\end{remarque}

\subsection{Intersection de deux sous-espaces vectoriels de $E$}
\begin{thm}
Si $F$ et $G$ sont deux sous-espaces vectoriels de $E$ alors $F\cap G$
est un sous-espace vectoriel de $E$.
\label{th:inter}
\end{thm}

\begin{preuve}
\begin{enumerate}
\item[] $F$ et $G$ sont deux sous-espaces vectoriels de $E$, en particulier ils contiennent le vecteur nul donc $0\in F\cap G$. On en déduit que $F\cap G$ est non vide.
\item[] Soient $u$ et $v$ dans $F\cap G$ et soient $\lambda$ et $\mu$ des réels.\\
$u$ et $v$ sont  dans $F$ et $F$ est un sous-espace vectoriel de $E$ donc stable par combinaisons linéaires d'où $\lambda\cdot u+\mu\cdot v \in F$. De même, $\lambda\cdot u+\mu\cdot v \in G$ donc $\lambda\cdot u+\mu\cdot v \in F\cap G$.\\
$F\cap G$ est bien stable par combinaisons linéaires.
\item[] $F\cap G$ est un sous-espace vectoriel de $E$.
\end{enumerate}

\end{preuve}

\begin{remarque}
Ce résultat est faux pour le réunion de deux sous-espaces vectoriels de $E$.\\
On peut montrer que: $F\cup G$ est un sous-espace vectoriel de $E$ si et seulement si $F\subset G$ ou $G\subset F$. 
\end{remarque}


Le théorème \ref{th:inter} permet de montrer que l'ensemble des solutions d'un système linéaire homogène à $n$ inconnues est un sous-espace vectoriel de $\mathbb{R}^n$.\\
En effet, considérons par exemple un système de 3 équations à trois inconnues.



\begin{equation*}
\mathcal{E}~ 
\left\{
\begin{array}{lc}
a_1 x+b_1 y+c_1 z=0 & (E_1) \\
a_2 x+b_2 y+c_2 z=0 & (E_2)\\
a_3 x+b_3 y+c_3 z=0 & (E_3)
\end{array}
\right.
\end{equation*}

Notons $S_i$ l'ensemble des solutions de  l'équation $(E_i)$, pour $i\in\left\{1,2,3\right\}$ et $S$ celui de $\mathcal{E}$.\\
On a alors $$\mathcal{S}=\bigcap_{i=1}^3 \mathcal{S}_i$$
Or chaque $\mathcal{S}_i$ est un sous-espace vectoriel de $\mathbb{R}^3$ (non vide et stable par combinaison linéaire) et donc $\mathcal{S}$ est aussi un sous-espace vectoriel de $\mathbb{R}^3$.
\section{Somme de sous-espaces vectoriels}
\subsection{Définition et premières propriétés}
\begin{Def}
Soient $F_1$ et $F_2$ des sous-espace vectoriel de $E$.\\
On appelle somme de $F_1$ et $F_2$, on note $F_1+F_2$ l'ensemble des vecteurs $v$ de $E$ qui peuvent s'écrire $v=v_1+v_2$, avec $v_1\in F_1$ et $v_2\in F_2$. 
\end{Def}
$$F_1+F_2=\lbrace v\in E |\quad \exists (v_1,v_2)\in F_1\times F_2 \text{ tels que } v=v_1+v_2 \rbrace$$

\begin{pro}
Si $F_1$ et $F_2$ sont des sous-espace vectoriel de $E$ alors $F_1+F_2$ est un sous-espace vectoriel de $E$.
\end{pro}
\begin{preuve}
\begin{itemize}
\item[]
$F_1$ et $F_2$ sont des sous-espace vectoriel de $E$ donc $0\in F_1$ et $0\in F_2$ d'où $0+0=0\in F_1+F_2$.
\item[] Soient $u$ et $v$ dans $F_1+F_2$ et $\lambda$ et $\mu$ des réels.\\
Par définition,\\
$\exists (u_1,u_2)\in F_1\times F_2 \text{ tels que } u=u_1+u_2$\\
$\exists (v_1,v_2)\in F_1\times F_2 \text{ tels que } v=v_1+v_2 $\\
Donc $\lambda u+\mu v=\lambda (u_1+u_2)+\mu (v_1+v_2 )=(\lambda u_1+\mu v_1)+(\lambda u_2+\mu v_2)$,\\
or $F_1$ et $F_2$ sont stables par combinaisons linéaires donc $\lambda u_1+\mu v_1\in F_1$ et $\lambda u_2+\mu v_2\in F_2$ donc $\lambda u+\mu v\in F_1+F_2$.\\
$F_1+F_2$ est stable par combinaisons linéaires.
\item[]Donc $F_1+F_2$ est un sous-espace vectoriel de $E$.
\end{itemize}
\end{preuve}

\begin{exem}

Soit $F_1$ et $F_2$ deux sous-espaces vectoriels de $\mathbb{R}^2$ définis par: 
$F_1=\Vect\{(1 , 1) \}$ et $F_2=\Vect\{(1 , -1) \}$.\\
On sait que $F_1+F_2$ est un sous-espace vectoriel de $\mathbb{R}^2$. En fait, on a même $F_1+F_2=\mathbb{R}^2$.\\
En effet, Soit $X=( x , y )$ un vecteur de $\mathbb{R}^2$, alors: \\
$X=\dfrac{x+y}{2}(1 , 1)+\dfrac{x-y}{2}(1 , -1)$ donc $X\in F_1+F_2$. D'où $\mathbb{R}^2\subset F_1+F_2$ et donc $F_1+F_2=\mathbb{R}^2$.
\end{exem}
\subsection{Somme directe}
\begin{Def}
Soient $F_1$ et $F_2$ deux sous-espaces vectoriels de $E$.\\
On dit  que la somme $F_1+F_2$ est directe si tout vecteur $v$ de $F_1+F_2$ peut s'écrire de manière \textbf{ unique } sous 
la forme $v=v_1+v_2$, avec $v_1\in F_1$ et $v_2\in F_2$.\\
Soit encore: $\forall v\in F, \; \exists ! (v_1,v_2)\in F_1\times F_2 \text{ tel que } v=v_1+v_2$.\\

On  note alors la somme $F_1\oplus F_2$.
\end{Def}

\begin{pro}
La somme $F_1+F_2$ est directe si et seulement si $F_1\cap F_2 =\{0\}$
\end{pro}
\begin{preuve}

$\Rightarrow$ 
Supposons que la somme soit directe.\\
Soit $v\in F_1\cap F_2$ alors on peut écrire:\\
$v=v+0$ avec $v\in F_1$ et $0\in F_2$ et $v=0+v$ avec $0\in F_1$ et $v\in F_2$,\\
d'où, par unicité de la décomposition dans $F_1\oplus F_2$, $v=0$.\\
On a donc bien $F_1\cap F_2 =\{0\}$.

$\Leftarrow$ 
Supposons que $F_1\cap F_2 =\{0\}$.\\
Soit $v\in F_1+F_2$ tel que $v=v_1+v_2=v'_1+v'_2$ avec $v_1$ et $v'_1$ dans $F_1$ et $v_2$ et $v'_2$ dans $F_2$.\\
On a donc $v_1-v'_1=v'_2-v_2$, or $v_1-v'_1\in F_1$ et $v'_2-v_2\in F_2$.\\
 Comme $F_1\cap F_2 =\{0\}$, on en déduit que $v_1-v'_1=v'_2-v_2=0$ et donc $v_1=v'_1$ et $v'_2=v_2$.\\
La décomposition de $v$ est donc unique et la somme $F_1+F_2$ est directe.
\end{preuve}

\begin{exem}

Reprenons l'exemple précédent:\\
On peut montrer que $F_1=\Vect\{(1 , 1) \}$ et $F_2=\Vect\{(1 , -1) \}$ sont en somme directe dans $\mathbb{R}^2$.
\end{exem}
\subsection{Sous-espaces supplémentaires dans $E$}
\begin{Def}
Soient $F_1$ et $F_2$ deux sous-espaces vectoriels de $E$.\\
$F_1$ et $F_2$ sont dits supplémentaires dans $E$ si tout vecteur de $E$ se décompose de manière unique comme somme d'un élément de $F_1$ 
et d'un élément de $F_2$.\\
Ce qui signifie que leur somme est directe et égale à $E$.\\
(c'est à dire: $F_1$ et $F_2$ sont dits supplémentaires dans $E$ si $F_1\oplus F_2=E$)
\end{Def}

\begin{pro}
$F_1$ et $F_2$ sont  supplémentaires dans $E$ si et seulement si 
$\left\{ \begin{array}{l}
F_1\cap F_2 =\{0\}\\
F_1+F_2=E
\end{array}
\right.$
\end{pro}


\begin{remarque}
$F_1$ et $F_2$ étant deux sous-espaces vectoriels de $E$, $F_1+F_2$ est aussi un sous-espace vectoriel de $E$.\\
Dans la pratique, on se contentera de vérifier que $E\subset F_1+F_2$, c'est à dire que tout vecteur de $E$ se décompose comme somme d'un vecteur de $F_1$ et d'un vecteur de $F_2$.
\end{remarque}
\begin{exem}

D'après ce qui a été fait plus haut, on peut dire que $F_1=\Vect \{(1 , 1) \}$ et $F_2=\Vect \{(1 , -1) \}$ sont supplémentaires dans $\mathbb{R}^2$.
\end{exem}


\begin{exem}

Soient $F_1=\left\{
\left( x,y, z \right) \in \mathbb{R}^3| x=2y+3z
\right\}$ et $F_2=\left\{
\left( x,0, 0 \right) 
\text{ où } x\in\mathbb{R}
\right\}$.\\
Nous avons déjà démontré que $F_1$ est le sous-espace vectoriel de $\mathbb{R}^3$ engendré par les vecteurs $\left(  2, 1, 0 \right)$ et $\left(  3, 0, 1  \right)$.\\
De la même manière $F_2$ est le sous-espace vectoriel de
$\mathbb{R}^3$ engendré par le vecteur $\left( 1, 0, 0 \right)$.\\


Montrons que $F_1$ et $F_2$ sont supplémentaires dans $\mathbb{R}^3$.


\begin{prof}
  \begin{enumerate}
  \item[-]Soit $v\in F_1\cap F_2$, alors $v=\left( 2y+3z, y, z \right)=\left( x, 0, 0 \right)$.\\
    On en déduit que $y=z=0$ et donc $x=2y+3z=0$. Par suite, $v=0$.\\
    D'où $F_1\cap F_2=\{0\}$.
  \item[-]Soit à présent $v\in \mathbb{R}^3$, $v=\left(x, y, z  \right)$. On cherche $u_1\in F_1$ et $u_2\in F_2$ tels que $v=u_1+u_2$.\\
    Donc $\left(  x, y, z \right)=\left(  2y_1+3z_1, y_1,z_1  \right)+\left( x_2,0, 0 \right)=\left( x_2+2y_1+3z_1, y_1,z_1  \right)$.\\


    On choisit donc $y_1=y$, $z_1=z$ et $x_2=x-2y_1-3z_1=x-2y-3z$.


    On obtient $v=\left( x-2y-3z, 0,0 \right)+\left(  2y+3z, y,z \right)$. Donc $v$ peut s'écrire comme somme d'un vecteur de
    $F_1$ et d'un vecteur de $F_2$.
  \item[-] On a donc bien $F_1\oplus F_2=\mathbb{R}^3$.
  \end{enumerate}
\end{prof}

\begin{figure}[!ht]
\begin{center}

\includegraphics{images/supplem.pdf}  

\end{center}
\end{figure}
\end{exem}
\end{document}
