%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Cours,11pt]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}

\titre{Matrice inversible et déterminants}
\auteur{M. Pelini, V. Ledda}
\reference{Chapitre 5}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 SIAD \\
Algèbre linéaire}

\motsclefs{déterminant, inverse, application linéaire, matrice, rang}


\usepackage{tikz}
\usetikzlibrary{matrix}



\usepackage[tikz]{bclogo}
\usepackage{tabularx}
\etudiant=1
\begin{document}

\maketitle
\tableofcontents

\newpage





\section{Matrices carrées inversibles et endomorphismes bijectifs}
\noindent $E$ désigne un espace vectoriel sur $\R$, de dimension  $n$ ($n\in\N^*$).

\subsection{Définitions et premières propriétés}
\begin{Def}
Soit $M\in\mathcal{M}_n$
\item[-] $M$ est inversible si il existe $N\in\mathcal{M}_n$ telle que $MN=NM=I_n$.
\item[-] On note $GL_n(\R)$, (ou, plus simplement, $GL_n$) l'ensemble des matrices inversibles.
\end{Def}

\begin{pro}
La matrice $N$, quand elle existe, est alors unique.\\ On l'appelle matrice inverse de $M$, on la note $M^{-1}$. 
\end{pro}

\begin{preuve}
 Supposons qu'il existe $N$ et $N'$ dans $\mathcal{M}_n$ telles que $MN=NM=I_n$ et $MN'=N'M=I_n$.\\
On a, en particulier, $MN=I_n$.\\
 Donc, en multipliant à gauche par $N'$, $N'MN=N'I_n$ soit $N=N'$ (car $N'M=I_n$).
 
\end{preuve}

\begin{remarque}
 Soit $M\in\mathcal{M}_n$, on peut montrer que : \\
\begin{tabular}{lclcl}
$M$ est inversible & $\iff$ & $M$ est inversible \` a droite & $\iff$ & $M$ est inversible à gauche \\
                   & $\iff$ & il existe $N\in\mathcal{M}_n$ telle que $MN=I_n$ & $ \iff$ & il existe $N'\in\mathcal{M}_n$ telle que $N'M=I_n$
\end{tabular}
On a alors $N=N'=M^{-1}$.
\end{remarque}

\begin{exem}
Soit $A=\begin{pmatrix} 0 & 1\\ 1 & 0 \end{pmatrix}$. On $A^2=I_2$ donc $A$ est inversible et $A^{-1}=A$.
\end{exem}

\begin{pro}
Soit $M\in\mathcal{M}_n$, $\mathcal{B}$ une base de $E$ et $u\in\mathcal{L}(E)$ tel que $Mat(u,\mathcal{B})=M$.\\
$$M \text{ est inversible si et seulement si  } u \text{ est bijectif}$$
On a alors $M^{-1}=Mat(u^{-1},\mathcal{B})$.
\end{pro}

\begin{preuve}

\begin{tabular}{lll}
$u$ est bijectif  & si et seulement si  & il existe $v\in\mathcal{L}(E)$ tel que $u\circ v=v\circ u=Id_E$\\
                  & si et seulement si  & il existe $N\in\mathcal{M}_n$ tel que $MN=NM=I_n$, où $N=Mat(v,\mathcal{B})$\\
                  & si et seulement si  & M est inversible
\end{tabular}\\
De plus, on a $v=u^{-1}$ et $N=M^{-1}$ donc $M^{-1}=Mat(u^{-1},\mathcal{B})$.
\end{preuve}
\begin{pro}
Soient $M$ et $N$ dans $GL_n$
\item[i)] $M^{-1}$ est inversible et $(M^{-1})^{-1}=M$
\item[ii)] $MN$ est inversible et $(MN)^{-1}=N^{-1}M^{-1}$
\item[iii)] $\forall p\in\N,\; M^p$ est inversible et $(M^p)^{-1}=(M^{-1})^p$
\end{pro}

\begin{preuve}
La démonstration de ces résultats n'est pas trés difficile (il suffit de l'écrire\ldots).\\
Nous allons, par exemple démontrer ii).\\
$M$ et $N$  sont inversibles donc $M^{-1}$ et $N^{-1}$  existent.  On
a alors :\\
$MNN^{-1}M^{-1}=MI_nM^{-1}=MM^{-1}=I_n$ donc $MN$ est inversible d'inverse $N^{-1}M^{-1}$.
\end{preuve}
\subsection{Caractérisation des matrices inversibles}
\textbf{Rappel:}

Soit $u\in\mathcal{L}(E)$, les affirmations suivantes sont équivalentes:

\begin{enumerate}[i)]
\item $u$ est bijectif
\item $u$ est injectif
\item  $u$ est surjectif
\item  $\ker(u)=\{0\}$
\item $\rg(u)=n$
\end{enumerate}
 
Ces équivalences sont transposables dans le cas des matrices
\begin{pro} Soit $M\in\mathcal{M}_n$
  \begin{equation*}
M\in GL_n\Leftrightarrow \ker(M)=\{0\} \Leftrightarrow \rg(M)=n
\end{equation*}

\end{pro}


\begin{exem}

\begin{enumerate}
\item[-] Soit $A=\begin{pmatrix} 1 & 0  & -1 \\2 & 4 & 6\\3 & -1 & 3 \end{pmatrix}$. On peut montrer que $\rg(A)=3$  et donc $A$ est inversible.
\item[-]  Soit $B=\begin{pmatrix}  0 &1 & -1 \\3 & 0 & 1\\1 & 2& 0  \end{pmatrix}$. On peut montrer que $\ker(B)=\{0\}$  et donc $B$ est inversible.
\end{enumerate}
\end{exem}


\section{Matrices semblables}
\
$E$ désigne un espace vectoriel sur $\R$ de dimension $n$ avec $n$ entier naturel non nul.

\subsection{Changement de bases}
\begin{Def}
 Soient $\mathcal{B}=(e_1,\ldots,e_n)$ une base de $E$ et $\mathcal{B}'=(f_1,\ldots,f_n)$ une famille de $n$ vecteurs de $E$.\\
On appelle matrice de passage de $\mathcal{B}$ à  $\mathcal{B}'$, on note $P_{\mathcal{B}\mathcal{B}'}$ la matrice suivante :
$$ P_{\mathcal{B}\mathcal{B}'}=\Mat_{\mathcal{B}}(\mathcal{B}')$$
(La $j^{\text{ème}}$ colonne de cette matrice représente donc les coordonnées du vecteur $f_j$ dans la base $\mathcal{B}$)
\end{Def}

\begin{pro}
$ P_{\mathcal{B}\mathcal{B}'} $ est inversible si et seulement si $\mathcal{B}'$ est une base de $E$.
\end{pro}

\begin{preuve}
 $\rg( P_{\mathcal{B}\mathcal{B}'})=\rg(\mathcal{B}')$.\\
$P$ est inversible si et seulement si $\rg(
P_{\mathcal{B}\mathcal{B}'})=n$ si et seulement si
$\rg(\mathcal{B}')=n$ si et seulement si $\mathcal{B}'$ est une base de $E$.
\end{preuve}

\begin{remarque}
 On a en fait  $P_{\mathcal{B}\mathcal{B}'}=\Mat(Id_E,\mathcal{B}',\mathcal{B})$
\end{remarque}

\begin{exem}
On note $\mathcal{B}=(e_1,e_2,e_3)$ une base  de $\R^3$  et $\mathcal{B}'=(f_1,f_2,f_3)$ la famille définie par :\\
$f_1=e_1,\quad f_2=e_1+e_2 \quad \text{et}\quad f_3=e_1+e_2+e_3$
$$\text{On a alors } P=P_{\mathcal{B}\mathcal{B}'}=\begin{pmatrix}
                          1&1&1\\0&1&1\\0&0&1
                         \end{pmatrix}$$

\end{exem}

\begin{exem}
 La matrice $P$ ci-dessus est inversible $(\text{ en effet }\det(P)=1)$ donc $\mathcal{B}'=(f_1,f_2,f_3)$ est une base de $E$ 
et on peut démontrer que  $P^{-1}=\begin{pmatrix}
                          ~1&-1&~0\\~0&~1&-1\\~0&~0&~1
                         \end{pmatrix}$.\\
Exprimons d'autre part la matrice de passage de $\mathcal{B}'$ à $\mathcal{B}$ :\\
$\left\{\begin{array}{l}
 f_1=e_1\\ f_2=e_1+e_2 \\ f_3=e_1+e_2+e_3
\end{array}\right.
\Leftrightarrow \left\{\begin{array}{l}
 e_1=f_1\\ e_2=f_2-f_1 \\ e_3=f_3-f_2
\end{array}\right.$\\

\vspace{0,1cm}

\noindent Donc $P_{\mathcal{B}'\mathcal{B}}=\begin{pmatrix}
                          ~1&-1&~0\\~0&~1&-1\\~0&~0&~1
                         \end{pmatrix}$.\\

\vspace{0,1cm}

\noindent On constate que $P^{-1}=(P_{\mathcal{B}\mathcal{B}'})^{-1}=P_{\mathcal{B}'\mathcal{B}}$

\end{exem}

\begin{pro}
Soient $\mathcal{B}$ et $\mathcal{B}'$ deux bases de $E$, alors $P$ est inversible et $(P_{\mathcal{B}\mathcal{B}'})^{-1}=P_{\mathcal{B}'\mathcal{B}}$ .
\end{pro}

\begin{preuve}
Soit $Q=P_{\mathcal{B}'\mathcal{B}}$, on a alàrs $PQ=\Mat(Id_E,\mathcal{B}',\mathcal{B})\Mat(Id_E,\mathcal{B},\mathcal{B}')=\Mat(Id_E\circ Id_E,\mathcal{B},\mathcal{B})=I_n$.\\
On en déduit donc que $Q=P^{-1}$, soit $(P_{\mathcal{B}\mathcal{B}'})^{-1}=P_{\mathcal{B}'\mathcal{B}}$ .
\end{preuve}



\begin{thm}
 Soient $\mathcal{B}$ et $\mathcal{B}'$ deux bases de $E$ , $P=P_{\mathcal{B}\mathcal{B}'}$ et $x$ un vecteur de $E$.\\
 On note $X$ et $X'$ les coordonnées de $x$ respectivement dans les bases $\mathcal{B}$ et $\mathcal{B}'$.\\
On a alors $X=PX'$.
\end{thm}

\begin{preuve}
 En effet $P=\Mat(Id_E,\mathcal{B}',\mathcal{B})$ donc en faisant le produit $PX'$, on obtient les coordonnées de $x$ dans la base $\mathcal{B}$ d'où $X=PX'$.
\end{preuve}

\begin{exem}
 Reprenons l'exemple précédent :\\
Soit $x$ le vecteur de coordonnées $X'=\begin{pmatrix} 1\\2\\1 \end{pmatrix}$ dans la base $\mathcal{B}'$.\\ 
On obtient ses coordonnées $X$ dans la base $\mathcal{B}$ en faisant le produit $PX'$.\\ 
Donc $X=\begin{pmatrix}
                          1&1&1\\0&1&1\\0&0&1
                         \end{pmatrix}\begin{pmatrix} 1\\2\\1 \end{pmatrix}=\begin{pmatrix} 4\\3\\1 \end{pmatrix}$.\\
Soit $y$ le vecteur de coordonnées $Y=\begin{pmatrix} 0\\1\\-1 \end{pmatrix}$ dans la base $\mathcal{B}$.\\
 On obtient ses coordonnées $Y'$ dans la base $\mathcal{B}'$ en faisant le produit $P^{-1}Y$.\\
Donc $Y'=\begin{pmatrix}
                          ~1&-1&~0\\~0&~1&-1\\~0&~0&~1
                         \end{pmatrix}\begin{pmatrix} 0\\1\\-1 \end{pmatrix}=\begin{pmatrix} -1\\2\\-1 \end{pmatrix}$.
\end{exem}

\begin{thm}
 Soient $\mathcal{B}$ et $\mathcal{B}'$ deux bases de $E$ , $P=P_{\mathcal{B}\mathcal{B}'}$ et $u\in\mathcal{L}(E)$.\\
 On note $A=\Mat(u,\mathcal{B})$ et $A'=\Mat(u,\mathcal{B}')$ . \\
On a alors $A'=P^{-1}AP$.
\end{thm}

\begin{preuve}
Soit $x$ un vecteur de $E$ et $y=u(x)$.\\
Si  $X$ et $X'$ représentent  les coordonnées de $x$ respectivement dans les bases $\mathcal{B}$ et $\mathcal{B}'$,\\
si $Y$ et $Y'$ représentent  les coordonnées de $y$ respectivement dans les bases $\mathcal{B}$ et $\mathcal{B}'$,\\
on a alors $Y=AX$ et $Y'=A'X'$,\\
d'autre part on sait que $Y=PY'$ et $X=PX' \Leftrightarrow X'=P^{-1}X$\\
 donc $AX=PA'X'\Leftrightarrow AX=PA'P^{-1}X$.\\
Cette égalité matricielle étant vraie $\forall X\in\R^n$, on en déduit que $A=PA'P^{-1} \Leftrightarrow A'=P^{-1}AP$ 
\end{preuve}

\subsection{Matrices semblables}
\begin{Def}
 Soient $A$ et $B$ dans $\mathcal{M}_n$.\\
On dit que $A$ et $B$ sont semblables si $\exists P\in GL_n \text{ telle que } B=P^{-1}AP$
\end{Def}

\begin{pro}
 Deux matrices sont semblables si et seulement sielles représentent le même endomorphisme dans des bases différentes.
\end{pro}
\begin{preuve}
 C'est une application directe du théorème ci-dessus.
\end{preuve}


\begin{pro}
 Soient  $A$ et $B$ dans $\mathcal{M}_n$ semblables et $P\in GL_n \text{ telle que } B=P^{-1}AP$.\\
Alors $\forall k\in \N$, $A^k$ et $B^k$ sont semblables et on a $B^k=P^{-1}A^kP$
\end{pro}

\begin{preuve}
 Démontrons ce résultat par récurrence sur $k$.\\
Pour $k=0$, on a $B^0=I_n$ et $P^{-1}A^0P=P^{-1}I_nP=I_n$. Donc $B^0=P^{-1}A^0P$.\\
Soit $k\in\N$, on suppose que $B^k=P^{-1}A^kP$, on a alors 
 $$B^{k+1}=B\cdot B^k= (P^{-1}A^kP)\cdot (P^{-1}AP)=P^{-1}(A^k\cdot A)P=P^{-1}A^{k+1}P$$
Conclusion :  $\forall k\in \N$, $A^k$ et $B^k$ sont semblables et on a $B^k=P^{-1}A^kP$.
\end{preuve}




\section{Déterminants}
 $E$ désigne un espace vectoriel sur $\R$ et $n$ un entier naturel non nul.

\subsection{Forme multilinéaire, forme alternée}
\begin{Def} Soit $ L:E^n\longmapsto \R$.
\begin{enumerate}
\item[-] On dit que $L$ est multi-linéaire sur $E$ si $L$ est linéaire par rapport à chaque vecteur, c'est  à dire :\\
$L(\lambda v_1+\lambda'v'_1, v_2, \ldots,v_n)=\lambda L(v_1, v_2, \ldots,v_n)+\lambda'L(v'_1, v_2, \ldots,v_n)$, pour la linéarité par rapport au premier vecteur, par exemple.
\item[-] On dit que $L$ est alternée si $L$ est changée en son opposé quand on permute 2 vecteurs, c'est à dire : 
$L(v_1,v_2,\ldots,v_n)=-L(v_2,v_1,\ldots ,v_n)$, par exemple.
\end{enumerate}
\end{Def}

\begin{pro}
Soit $L$ une forme alternée.\\
Si dans le n-uplet $(v_1,\ldots,v_n)$ deux vecteurs sont égaux alors $L(v_1,\ldots,v_n)=0$.
\end{pro}

\begin{preuve}
Soient $(v_1,\ldots,v_n)$ $n$ vecteurs de $E$.\\ On suppose que deux des vecteurs sont égaux, par exemple $v_1=v_2$ (pour simplifier les notations). \\On a alors :
$L(v_2,v_1,\ldots,v_n)=L(v_1,v_2,\ldots,v_n)$ car $v_1=v_2$.\\ 
Mais aussi : $L(v_2,v_1,\ldots,v_n)=-L(v_1,v_2,\ldots,v_n)$ car $L$ est alternée.\\ 
Donc $L(v_1,v_2,\ldots,v_n)=-L(v_1,v_2,\ldots,v_n) \Rightarrow L(v_1,v_2,\ldots,v_n)=0$.
\end{preuve}

\begin{pro}
Soit $L$ une forme  multilinéaire alternée.\\
Si  les vecteurs $v_1,\ldots,v_n$ forment une famille liée alors $L(v_1,\ldots,v_n)=0$.
\end{pro}

\begin{preuve}
Soient $(v_1,\ldots,v_n)$ $n$ vecteurs de $E$ formant une famille liée.\\
 Alors l'un des vecteurs (par exemple $v_1$) peut s'écrire comme combinaison linéaire des autres.\\
$\exists (\lambda_2,\ldots,\lambda_n)\in\R^{n-1} \text{ tel que } v_1=\lambda_2v_2+\ldots+\lambda_nv_n$, donc par multi-linéarité, on obtient :\\
$L(v_1,v_2,\ldots,v_n)=L(\lambda_2v_2+\ldots+\lambda_nv_n,v_2,\ldots,v_n)=\lambda_2 L(v_2,v_2,\ldots,v_n)+\ldots+\lambda_n L(v_n,v_2,\ldots,v_n)=0$ d'aprés la proposition précédente.
\end{preuve}

\subsection{Déterminant de n vecteurs de $\R^n$}
\begin{thdef}
Soit $\mathcal{B}$ une base de $\R^n$.\\
Il existe une unique forme multi-linéaire alternée sur $\R^n$ telle que $L(\mathcal{B})=1$.\\
Cette forme est appelée déterminant dans la base $\mathcal{B}$.
\end{thdef}
\begin{preuve}
Nous allons faire la démonstration dans le cas particulier $n=2$.\\
Notons $\mathcal{B}=(e_1,e_2)$ une base  de $\R^2$ et  $v_1={\begin{pmatrix} x_1\\y_1\end{pmatrix}}_{\mathcal{B}}$ , $v_2={\begin{pmatrix} x_2\\y_2\end{pmatrix}}_{\mathcal{B}}$ des vecteurs de $\R^2$.\\
\textit{Unicité de $L$} :\\
Soit $L$ multi-linéaire alternée telle que $L(e_1,e_2)=1$. On a alors :\\


\begin{tabular}{rll}
 $L(v_1,v_2)$ & $= L(x_1e_1+y_1e_2 ,x_2e_1+y_2e_2)$ &  \\
              & $= x_1 L(e_1,x_2e_1+y_2e_2)+y_1 L(e_2,x_2e_1+y_2e_2)$ & car $L$ linéaire/1\up{er} vecteur\\
              & $= x_1x_2 L(e_1,e_1)+x_1y_2 L(e_1,e_2)+y_1x_2 L(e_2,e_1)+y_1y_2 L(e_2,e_2)$ & car $L$ linéaire/2\up{ème} vecteur\\
              & $=x_1y_2 L(e_1,e_2)+y_1x_2 L(e_2,e_1)$ & car $L(e_1,e_1)=L(e_2,e_2)=0$\\
              & $=x_1y_2 -y_1x_2 $ & car  $L(e_2,e_1)=-L(e_1,e_2)=-1$\\
\end{tabular}


Donc on obtient que $L$ est unique et définie par $L(v_1,v_2)=x_1y_2 -y_1x_2$.\\



\noindent
\textit{Existence de $L$} :\\
Il suffit de concidérer $L$ définie ci-dessus. On vérifie aisément que c'est bien une forme multilinéaire alternée sur $\R^2$ et que $L(e_1,e_2)=1$.
\end{preuve}


\begin{remarque}
 Si $\mathcal{B}$ est la base canonique de $\R^n$, on parlera du déterminant, sans préciser la base.
\end{remarque}

\noindent NOTATION : \\
Soient $v_1,\ldots,v_n$ n vecteurs de $\R^n$, on notera $\det_{\mathcal{B}}(v_1,\ldots,v_n)$ (ou plus simplement $\det(v_1,\ldots,v_n)$) le déterminant des vecteurs $v_1,\ldots,v_n$ dans la base $\mathcal{B}$.\\
Une autre notation, souvent utilisée (surtout lors des calculs) est la suivante 
\begin{center}
$\det_{\mathcal{B}}(v_1,\ldots,v_n)=\begin{vmatrix}
 x_{11} & x_{12} & \cdots & \cdots & x_{1n}\\
x_{21} & x_{22} & \cdots & \cdots & x_{2n}\\
\vdots & \vdots &        &        & \vdots\\
x_{n1} & x_{n2} & \cdots & \cdots & x_{nn}\\
\end{vmatrix}$
\end{center}
où les colonnes représentent les coordonnées des vecteurs $v_1,\ldots,v_n$ dans la base $\mathcal{B}$.\\
En particulier $\det_{\mathcal{B}}(v_1,v_2)=\begin{vmatrix}
 x_{1} & x_2\\
y_1 & y_2
\end{vmatrix}=x_1y_2-y_1x_2$ (Régle du gamma).

\begin{remarque} D'aprés la définition, $\det_{\mathcal{B}}(\mathcal{B})=1$.\end{remarque}

\begin{exem}\end{exem}
Soit $v_1$,$v_2$ et $v_3$ les vecteurs de $\R^3$ dont les coordonnées dans la base canonique sont :\\ $\begin{pmatrix} 1\\2\\3 \end{pmatrix}$, $\begin{pmatrix} 6\\8\\4 \end{pmatrix}$ et $\begin{pmatrix} 3\\3\\6 \end{pmatrix}$.\\
On a alors $\det(v_1,v_2,v_3)=\begin{vmatrix} 1 & 6 & 3\\2 & 8 & 3\\3& 4 & 6\\
\end{vmatrix}=2\times \begin{vmatrix} 1 & 3 & 3\\2 & 4 & 3\\3& 2 & 6\\
\end{vmatrix}=2\times 3\times \begin{vmatrix} 1 & 3 & 1\\2 & 4 & 1\\3& 2 & 2\\
\end{vmatrix}$ ,
(car le déterminant est une application multilinéaire).

\subsection{Déterminant d'une matrice}
\begin{Def}
Soit $A\in\mathcal{M}_n$, $A=\left(a_{ij}\right)$.\\
On appelle déterminant de la matrice $A$, on note $\det(A)$ le déterminant de ces vecteurs colonnes,\\
 c'est à dire le déterminant des vecteurs $v_1,\ldots,v_n$ tels que $v_j$ a pour coordonnées $\begin{pmatrix} a_{1j} \\ a_{2j} \\ \vdots \\ a_{nj} \end{pmatrix}$ dans la base canonique de $\R^n$.
$$\det(A)=\det(v_1,\ldots,v_n)=\begin{vmatrix}
 a_{11} & a_{12} & \cdots & \cdots & a_{1n}\\
a_{21} & a_{22} & \cdots & \cdots & a_{2n}\\
\vdots & \vdots &        &        & \vdots\\
a_{n1} & a_{n2} & \cdots & \cdots & a_{nn}\\
\end{vmatrix}$$
\end{Def}
\begin{remarque} D'aprés la définition, $\det(I_n)=1$.\end{remarque}

\begin{pro}
Si $A\in\mathcal{M}_2$, $A=\begin{pmatrix} a & b\\ c & d  \end{pmatrix}$ alors $\det(A)=ad-bc$.
\end{pro}
\begin{preuve}
Ce résultat découle de la démonstration faite précédemment pour l'existence du déterminant quand $n=2$.
\end{preuve}

\begin{pro} Soit $A\in\mathcal{M}_n$\\
Si les colonnes de $A$ sont linéairement dépendantes, alors $\det(A)=0$.
\end{pro}

\begin{pro} (admise)\\
Soient  $A$ et $B$ dans $\mathcal{M}_n$  alors $\det(AB)=\det(A)\det(B)$.
\end{pro}

\noindent \textbf{ATTENTION :}\\
Si $A\in\mathcal{M}_n$ et $\lambda\in\R$ alors $\det(\lambda
A)=\lambda^n\det(A)$. (Car $\det$ est une application multilinéaire)


\subsection{Calcul pratique du déterminant }

\begin{pro}\textbf{Développement par rapport à une colonne}\\
Soit $A\in\mathcal{M}_n$ et $j\in\{1,\ldots,n\}$.\\
On peut développer le déterminant de $A$ selon la $j^{\text{ème}}$ colonne avec  la formule suivante 
$$\det(A)=\underbrace{\begin{vmatrix}
a_{11} & a_{12} & \cdots & \cdots & a_{1n}\\
a_{21} & a_{22} & \cdots & \cdots & a_{2n}\\
\vdots & \vdots &        &        & \vdots\\
a_{n1} & a_{n2} & \cdots & \cdots & a_{nn}\\
\end{vmatrix}}_{\text{déterminant d'ordre n}}=\underset{i=1}{\overset{n}{\sum}}(-1)^{i+j}a_{ij}\underbrace{\det(A_{ij})}_{\text{dét. d'ordre n-1}}$$
où $A_{ij}$ est la matrice carrée d'ordre $n-1$ obtenue en barrant la $i^{\text{ème}}$ ligne et la $j^{\text{ème}}$ colonne de $A$.
\end{pro}


On notera l'alternance des signes devant chaque coefficient:

\begin{equation*}
  \begin{pmatrix}
    +&-&+&-&+&-\\
    -& +&-&+&-&+\\
      +&-&+&-&+&-\\
      -& +&-&+&-&+\\
        +&-&+&-&+&-\\
    -& +&-&+&-&+\\
  \end{pmatrix}
\end{equation*}


\begin{exem}

\begin{tabular}{rcl}
$\begin{vmatrix} 1 & 6 & 3\\2 & 8 & 3\\3& 4 & 6\\
\end{vmatrix}$ & = & $1\begin{vmatrix} 8 & 3 \\ 4& 6 \end{vmatrix}+(-2)\begin{vmatrix}
6 & 3 \\4& 6 \end{vmatrix}+3\begin{vmatrix} 6 & 3 \\ 8& 3 \end{vmatrix}$\\
              & = & $1\times(48-12)+(-2)\times(36-12)+3\times(18-24)$\\
              & = & $36-48-18=-30$
\end{tabular}\\
(en développant par rapport à la première colonne)
\end{exem}
\begin{remarque}
\item Cette proposition permet de passer d'un déterminant de taille $n$ à $n$ déterminants de taille $n-1$. 
\item Cette proposition est particulièrement intéressante quand l'une des colonnes contient plusieurs $0$. Nous allons d'ailleurs voir, par la suite, comme faire apparaitre ces précieux zéros.
\end{remarque}

\begin{exem}\end{exem}
$\begin{vmatrix} 1 & 2 & 0\\3 & -1 & 0\\2& 4 & 1\\
\end{vmatrix}=1\times \begin{vmatrix} 1 & 2\\3 & -1\end{vmatrix}=-1-6=-7$, en développant par rapport à la troisième  colonne.



\begin{cor}
  Le déterminant d'une matrice triangulaire est égal au produit des
  termes diagonaux.
$$\begin{vmatrix}
  a_{11} & a_{12} & \cdots & \cdots & a_{1n}\\
  0      & a_{22} & \cdots & \cdots & a_{2n}\\
  0      & 0 & a_{33} & \cdots & a_{3n}\\
  \vdots & \vdots &        &        & \vdots\\
  0 & 0 & \cdots & 0& a_{nn}\\
\end{vmatrix}=a_{11}a_{22}\cdots a_{nn}$$
\end{cor}



En effet, il suffit de développper une première fois par rapport à la première colonne puis de recommencer avec le déterminant obtenu \ldots et ainsi de suite.


\begin{cor}[Régle de Sarrus]$\quad$.

  
En développant le déterminant d'une matrice de $\mathcal{M}_3$ et en
regroupant les termes précédés d'un signe "+" et ceux précédés d'un
signe "-", on obtient:
\begin{figure}[h]\centering
  
  \tikzset{node style ge/.style={circle}} $\det(M)= \left|
    \begin{matrix}
      a_{11} & a_{12} & a_{13}  \\
      a_{21} & a_{22} & a_{23}  \\
      a_{31} & a_{32} & a_{33}  \\
    \end{matrix}%
  \right|$
  =$\big(a_{11}a_{22}a_{33}+a_{21}a_{32}a_{13}+a_{31}a_{12}a_{23}\big)-\big(a_{13}a_{22}a_{31}+a_{23}a_{32}a_{11}+a_{33}a_{12}a_{21}\big)$


  \begin{tikzpicture}[baseline=(A.center)]
    \tikzset{BarreStyle/.style = {opacity=.4,line width=4 mm,line
        cap=round,color=#1}} \tikzset{SignePlus/.style = {above
        left,,opacity=1,circle,fill=#1!50}} \tikzset{SigneMoins/.style
      = {below left,,opacity=1,circle,fill=#1!50}}
    % les matrices
    \matrix (A) [matrix of math nodes, nodes = {node style ge},,column
    sep=0 mm]
    { a_{11} & a_{12} & a_{13}  \\
      a_{21} & a_{22} & a_{23}  \\
      a_{31} & a_{32} & a_{33}  \\
      a_{11} & a_{12} & a_{13} \\
      a_{21} & a_{22} & a_{23}\\
    };

    \draw [BarreStyle=red] (A-1-1.north west) node[SignePlus=red]
    {$+$} to (A-3-3.south east) ; \draw [BarreStyle=red] (A-2-1.north
    west) node[SignePlus=red] {$+$} to (A-4-3.south east) ; \draw
    [BarreStyle=red] (A-3-1.north west) node[SignePlus=red] {$+$} to
    (A-5-3.south east) ; \draw [BarreStyle=blue] (A-3-1.south west)
    node[SigneMoins=blue] {$-$} to (A-1-3.north east); \draw
    [BarreStyle=blue] (A-4-1.south west) node[SigneMoins=blue] {$-$} to
    (A-2-3.north east); \draw [BarreStyle=blue] (A-5-1.south west)
    node[SigneMoins=blue] {$-$} to (A-3-3.north east);
  \end{tikzpicture}
  
  \caption[Sarrus]{Calcul pratique du déterminant avec la règle de
    Sarrus}
  \label{fig:sarrus}
\end{figure}

\end{cor}

\begin{exem}
Calculer
$\begin{vmatrix}
  1&3&4\\-2&3&2\\1&-1&5
\end{vmatrix}$ à l'aide de la règle de Sarrus.
\end{exem}

\begin{pro}
Le déterminant d'une famille de vecteurs ne change pas si on ajoute à l'un des vecteurs une combinaison linéaire des autres.
\end{pro}

\begin{preuve}
Soient $v_1,v_2,\ldots,v_n$ n vecteurs de $\R^n$.\\
Posons $v'_1=v_1+\lambda_2v_2+\ldots+\lambda_nv_n$ vérifions que $\det(v'_1,v_2,\ldots,v_n)=\det(v_1,v_2,\ldots,v_n)$.\\
\begin{tabular}{lll}
$\det(v'_1,v_2,\ldots,v_n)$ & $=\det(v_1+\lambda_2v_2+\ldots+\lambda_nv_n,v_2,\ldots,v_n)$\\
                            & $=\det(v_1,v_2,\ldots,v_n)+\lambda_2 \det(v_2,v_2,\ldots,v_n)+\ldots+\lambda_n \det(v_n,v_2,\ldots,v_n)$\\
                            & $=\det(v_1,v_2,\ldots,v_n)$ (car les autres termes sont nuls)
\end{tabular}
                            
\end{preuve}

\begin{remarque} 
C'est cette propriété que nous allons utiliser pour "faire apparaitre des zéros".
\end{remarque}



\begin{pro} (admise)\\
Soit $A\in\mathcal{M}_n$ alors $\det(A)=\det({}^t A)$.
\end{pro}


\begin{remarque}
Cela signifie que l'on peut travailler sur les lignes de $A$ aussi bien que sur ses colonnes.
\end{remarque}

\begin{exem}\end{exem}
\begin{enumerate}
\item $\begin{vmatrix}
1 & -3 & 2\\
1 & 7 & -3\\
2& 4 & -1\\
\end{vmatrix}=0$ car $L_2=L_3-L_1$.
 \item $\begin{vmatrix}
1 & -3 & 2\\
0 & 0 & 2\\
2& 4 & -1\\
\end{vmatrix}=(-2)\begin{vmatrix}
1 & -3 \\2& 4 \end{vmatrix}=(-2)\times10 =-20$, en développant par rapport à la deuxième ligne.
\end{enumerate}

\begin{exem}

  Calculer

  \begin{equation*}
    \begin{vmatrix} 1 & 1 & 1\\2 & 4 & 8\\ 3& 6 & 9 \end{vmatrix}
  \end{equation*}
  \begin{prof}
  
$\begin{vmatrix} 1 & 1 & 1\\2 & 4 & 8\\ 3& 6 & 9 \end{vmatrix}=\begin{vmatrix} 1 & 0 & 0\\2 & 2 & 6\\ 3& 3 & 6 \end{vmatrix}=1\times \begin{vmatrix} 2 & 6\\ 3& 6  \end{vmatrix}=2\times 6-3\times 6=-6$.\\


\noindent
Nous avons gardé $C_1$ et nous l'avons utilisée pour modifier les
autres colonnes en utilisant le fait que
$\det(C_1,C_2,C_3)=\det(C_1,C_2-C_1,C_3-C_1)$, ensuite nous avons
développé par rapport à la première ligne.
\end{prof}
\end{exem}
\begin{bclogo}[logo = \bcdanger,arrondi=0.1]{Attention}
Il faut donc être prudent : toutes les opérations ne sont pas autorisées.
Par exemple
\begin{equation*}
  \begin{array}{lll}
  \begin{vmatrix} 2&1&0\\1&-1&3\\3&2&1\end{vmatrix} &\neq\begin{vmatrix} 2&0&0\\1&-3&3\\3&1&1\end{vmatrix} & C_2\gets 2C_2-C_1\\
  \end{array}
\end{equation*}
Pourquoi?
  
\end{bclogo}


\subsection{Petits exercices}
Calculer, le plus simplement possible, les déterminants des matrices suivantes :\\
\begin{tabular}{ll}
$A=\begin{pmatrix} 1 & 3 & 4 \\-2 & 2 & 0\\5 & -1 & 4 \end{pmatrix}$ & réponse : $\det(A)=0$\\
\\
$B=\begin{pmatrix} 1 & 0 & 3 \\4 & 2 & 1\\ -1 & 0 & 1 \end{pmatrix}$  & réponse : $\det(B)=8$\\
\\
$C=\begin{pmatrix} -1 & 2 & -1 & 3 \\2 & 3 & 1 & 2\\ 1 & 3 & 4 & -2\\ 3 & 1 & 2 & -3 \end{pmatrix}$  & réponse : $\det(C)=52$
\end{tabular}



\subsection{Déterminant d'un endomorphisme}
\begin{pro}
 Si $A$ et $B$ sont deux matrices semblables alors $\det(A)=\det(B)$.
\end{pro}

\begin{preuve}
 En effet comme $A$ et $B$ sont semblabes $\exists P\in GL_n \text{ telle que } B=P^{-1}AP$,\\
d'où  $\det(B)=\det(P^{-1})\det(A)\det(P)$. Or $\det(P^{-1})=\dfrac{1}{\det(P)}$ donc $\det(A)=\det(B)$.
\end{preuve}

\begin{Def}
 Soit $u\in\mathcal{L}(E)$.\\
On appelle déterminant de $u$, on note $\det(u)$ le déterminant de $A$ où $A$ est la matrice de $u$ dans une base quelconque de $E$.
\end{Def}

\begin{remarque}
 Il est important de remarquer que $\det(u)$ ne dépend pas de la base dans laquelle on choisit d'exprimer $u$.\\
En effet, si $A$ et   $B$ sont deux matrices représentant $u$ dans des  bases différents alors elles sont semblables et donc $\det(A)=\det(B)$. 
\end{remarque}

\subsection{Condition nécéssaire et suffisante d'inversibilité}
\noindent Pour savoir si une matrice est (ou non) inversible, nous avons vu qu'il est possible de calculer son rang ou de déterminer son noyau.\\
Cependant, il existe une autre caractérisation :

\begin{pro}
Soit $M\in\mathcal{M}_n$, $M$ est inversible si et seulement si  $\det(M)\neq 0$.
 \end{pro}
 
\begin{preuve}
\begin{enumerate}
\item[$\Rightarrow $] Si $M\in GL_n$ alors il existe $N\in GL_n$ telle que $MN=I_n$.
En particulier $\det(MN)=\det(M)\det(N)=1$ et donc $\det(M)\neq 0$.\\
\item[$\Leftarrow$] Si $M\notin GL_n$ alors $\rg(M)<n$ et les colonnes de $M$ sont linéairement dépendantes donc $\det(M)=0$.
\end{enumerate}
\end{preuve} 

\begin{exem}
\begin{large}\end{large}Reprenons les matrices $A$ et $B$ du paragraphe précédent.\\
On a $\det(A)=32(\neq 0)$ et $\det(B)=-5(\neq 0)$ donc $A$ et $B$ sont inversibles. 
\end{exem}

\begin{pro}
Si $M\in GL_n$ alors $\det(M)\neq 0$ et $\det(M^{-1})=\dfrac{1}{\det(M)}$.
\end{pro}

\begin{preuve}
Si $M\in GL_n$ alors $\det(M)\neq 0$ (voir ci-dessus) et  on a \\
$\det(MM^{-1})=\det(I_n)$ donc $\det(M)\det(M^{-1})=1$ et $\det(M^{-1})=\dfrac{1}{\det(M)}$.\\
\end{preuve}


\begin{exem}
Soit $A(m)=\begin{pmatrix} 2m & 3m+1 & 3m-1\\ m+1 & 2(m+1) & m+1\\2m & 3m+1 & 2m \end{pmatrix}$,  $m\in\R$.\\


  
\noindent Cherchons les valeurs de $m$ pour lesquelles $A(m)$ est inversible. Pour cela, calculons $\det(A(m))$.


  
\begin{tabular}{lll}
$\det(A(m))$ & $=\begin{vmatrix} 2m & 3m+1 & 3m-1\\ m+1 & 2(m+1) & m+1\\2m & 3m+1 & 2m \end{vmatrix}$ & \\
             \\
             & $=\begin{vmatrix} 2m & -m+1 & m-1\\ m+1 & 0 &0\\2m & -m+1 & 0 \end{vmatrix}$ & en faisant $C_2\gets C_2-2C_1$ et $C_3\gets C_3-C_1$\\
             \\
             & $=(m-1)\begin{vmatrix} m+1 & 0 &\\ 2m & -m+1 \end{vmatrix}$ & en développant par rapport à $C_3$\\
             \\
             & $=(m-1)(m+1)(-m+1)=-(m-1)^2(m+1)$ & 
             \end{tabular}


            
\noindent            
Donc  $A(m)$ est inversible si et seulement si   $\det(A(m))\neq 0$ si et seulement si  $m\neq 1$ et $m\neq -1$.            

\end{exem}




\subsection{Calcul pratique de l'inverse}

\paragraph{a)}{Méthode générale}
\noindent Rappel : Soit $u\in\mathcal{L}(E)$,
$u$ est bijectif si et seulement si  $\forall y\in E,\; \exists! x\in E$ tel que $u(x)=y$.

\begin{pro} Soit $M\in\mathcal{M}_n$,\\
$M$ est inversible si et seulement si  $\forall Y\in\R^n$, l'équation $MX=Y$ admet une unique solution. 
On a alors $X=M^{-1}Y$.
\end{pro}
\begin{remarque}
Cela signifie que pour calculer $M^{-1}$, nous allons "inverser" le système $MX=Y$.
\end{remarque}

\begin{exem}
Soit $M=\begin{pmatrix} 1 & 2  & -1 \\0 & 1 & 4\\0 & 0 & 1 \end{pmatrix}$. On a $\det(M)=1^3=1$ donc $M$ est inversible.\\
Calculons $M^{-1}$ :\\



\begin{tabular}{rlr}
$MX=Y$ & $\Leftrightarrow
\left\{ 
\begin{array}{rcr}
x_1+2x_2-x_3& = & y_1\\
x_2+4x_3 & = &y_2\\
x_3 & = & y_3
\end{array}\right.$ & 
$\Leftrightarrow
\left\{ 
\begin{array}{rcr}
x_1+2x_2& = & y_1+y_3\\
x_2& = &y_2-4y_3 \\
x_3 & = & y_3
\end{array}\right.$\\
 & & \\
 & $\Leftrightarrow
\left\{ 
\begin{array}{rcr}
x_1& = & y_1+y_3-2(y_2-4y_3 )\\
x_2& = &y_2-4y_3 \\
x_3 & = & y_3
\end{array}\right.$ & 
$\Leftrightarrow
\left\{ 
\begin{array}{rcr}
x_1& = & y_1-2y_2+9y_3\\
x_2& = &y_2-4y_3 \\
x_3 & = & y_3
\end{array}\right.$
\end{tabular}\\



Donc  $M^{-1}=\begin{pmatrix} 1 & -2  & 9 \\0 & 1 & -4\\0 & 0 & 1 \end{pmatrix}$
\end{exem}

\begin{exem}
Soit $N=\begin{pmatrix}
 1 & 1\\
 2 & 1 
 \end{pmatrix}$. On a $\det(N)=1-2=-1$ donc $N$ est inversible.\\
Calculons $N^{-1}$ :\\
On peut  présenter les calculs de la manière suivante, en faisant les mêmes opérations élémentaires sur les lignes des deux matrices , jusqu'à obtenir l'identité à gauche et donc $N^{-1}$ à droite.\\ 



\begin{equation*}
\left(\begin{array}{cc|cc}
 1&1&1&0\\ 2&1&0&1
 \end{array}\right),  
 \left(\begin{array}{cc|cc}
 -1&0&1&-1\\ 2&1&0&1
 \end{array}\right), 
 \left(\begin{array}{cc|cc}
 -1&0&1&-1\\ 0&1&2&-1
 \end{array}\right),  
\left(\begin{array}{cc|cc}
 1&0&-1&1\\ 0&1&2&-1
\end{array}\right)
\end{equation*}

 


\noindent Donc $N^{-1}=\begin{pmatrix}
 -1 & 1\\
 2 & -1 
 \end{pmatrix}$
\end{exem}
\begin{exem}
 Reprenons la matrice $B=\begin{pmatrix}  0 &1 & -1 \\3 & 0 & 1\\1 & 2& 0  \end{pmatrix}$. \\
On sait que $B$ est inversible et on peut montrer que $B^{-1}=-\dfrac{1}{5}\begin{pmatrix}  -2 &-2 & 1 \\1 & 1 & -3\\6 & 1& -3  \end{pmatrix}$.
\end{exem}

\begin{exercice}
Pour quelles valeurs de $a$ la matrice $C=\begin{pmatrix} 1 & 1  & 1 \\2 & a & 1\\1 & 0 & 2 \end{pmatrix}$ est-elle inversible ?\\



\noindent Réponse : $\det(C)=a-3$ donc $C$ est inversible si et seulement si  $a\neq 3$.

\end{exercice}

\noindent On a alors $C^{-1}=\dfrac{1}{a-3}\begin{pmatrix} 2a & -2  & 1-a \\-3 & 1 & 1\\-a & 1 & a-2 \end{pmatrix}$.

\paragraph{b)}
{Cas d'une matrice $2\times 2$ }
\begin{exem} Soit $A=\begin{pmatrix} 1 & 3\\ 0 & 2 \end{pmatrix}$.
On a $\det(A)=2$ donc $A$ est inversible et $\det(A^{-1})=\dfrac{1}{2}$.\\
On peut même montrer que $A^{-1}=\begin{pmatrix} 1 & -3/2\\ 0 & 1/2  \end{pmatrix}=\dfrac{1}{2}\begin{pmatrix} 2 & -3\\ 0 & 1 \end{pmatrix}$.
\end{exem}

\noindent {Plus généralement :}
Soit $A=\begin{pmatrix} a & b\\ c & d \end{pmatrix}$ telle que $ad-bc\neq 0$.\\
Alors $A$ est inversible et on peut même montrer que $A^{-1}=\dfrac{1}{ad-bc}\begin{pmatrix} d & -b\\ -c & a  \end{pmatrix}$.

\paragraph{c)}{Matrice des cofacteurs}
\begin {Def} Soit $M\in\mathcal{M}_n$, on appelle comatrice de $M$, on note $\Com(M)$, la matrice dont les coefficients $c_{ij}$ sont définis par :
 $$c_{ij}=(-1)^{i+j}\det(M_{ij})$$
où $M_{ij}$ est la matrice carrée d'ordre $n-1$ obtenue en barrant la $i^{\text{ème}}$ ligne et la $j^{\text{ème}}$ colonne de $M$.
\end{Def}

\begin{pro}
 Si $M\in GL_n$ alors $M^{-1}=\dfrac{1}{\det(M)}\quad {}^t \Com(M)$
\end{pro}

\begin{remarque}
\item[-] Cette proposition est une généralisation de la formule donnée dans le paragraphe b).
\item[-] Elle est utilisable pour $n=2$ voire $n=3$, au delà, elle n'est pas trés pratique. \\
En effet, pour $n=4$, cela revient à calculer $16$ déterminants d'ordre $3$ \ldots
\end{remarque}

\begin{exem}
 Reprenons la matrice $A=\begin{pmatrix} 1 & 0  & -1 \\2 & 4 & 6\\3 & -1 & 3 \end{pmatrix}$.\\
On a vu que $\det(A)=32$, il nous reste à déterminer $\Com(A)$ :\\
$$\Com(A)=\begin{pmatrix}
   +\begin{vmatrix} 4&6\\-1&3 \end{vmatrix} & -\begin{vmatrix} 2&6\\3&3 \end{vmatrix} & +\begin{vmatrix} 2&4\\3&-1 \end{vmatrix} \\ \\
  -\begin{vmatrix} 0&-1\\-1&3 \end{vmatrix} & +\begin{vmatrix} 1&-1\\3&3 \end{vmatrix} & -\begin{vmatrix} 1&0\\3&-1 \end{vmatrix}  \\ \\
   +\begin{vmatrix} 0&-1\\4&6 \end{vmatrix} & -\begin{vmatrix} 1&-1\\2&6 \end{vmatrix} & +\begin{vmatrix} 1&0\\2&4 \end{vmatrix}  \\ \\
         \end{pmatrix}=
\begin{pmatrix} 18&12&-14 \\ 1&6&1 \\ 4&-8&4 \end{pmatrix}$$

Donc $A^{-1}=\dfrac{1}{32}  \begin{pmatrix}
                             18&1&4 \\ 12&6&-8 \\-14&1&4
                            \end{pmatrix}$



\end{exem}

\end{document}