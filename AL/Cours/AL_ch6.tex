%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Cours,11pt]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}

\titre{Réduction des endomorphismes}
\auteur{M. Pelini, V. Ledda}
\reference{Chapitre 5}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 SIAD \\
Algèbre linéaire}

\motsclefs{diagonalisation, vecteurs propres, valeurs propres}


\usepackage{tikz}
\usetikzlibrary{matrix}



\usepackage[tikz]{bclogo}
\usepackage{tabularx}
\etudiant=1
\begin{document}

\maketitle
\tableofcontents
\newpage




\section{Éléments propres }
$E$ désigne un espace vectoriel sur $\R$ de dimension $n$ avec $n$ entier naturel non nul.

\subsection{Valeurs propres d'un endomorphisme et vecteurs propres associés}
\begin{Def}
 Soient $u\in\mathcal{L}(E)$ et $\lambda\in\R$.
\begin{enumerate}
\item[-] $\lambda$ est une valeur propre de $u$ si et seulement si $\exists x\in E, x\neq O_E \text{ tel que } u(x)=\lambda x$.
\item[-] un tel vecteur $x$ est alors appelé vecteur propre de $u$ associé à la valeur propre $\lambda$.

\end{enumerate}
\end{Def}

\begin{exem}
 Soient $(e_1,e_2)$ une base de $\R^2$ et $u\in\mathcal{L}(\R^2)$ défini par $u(e_1)=e_1$ et $u(e_2)=-e_1$.\\
$u(e_1)=e_1$ donc $1$ est valeur propre de $u$ et $e_1$ est un vecteur propre associé à cette valeur propre.\\
$u(e_1+e_2)=0$ donc $0$ est valeur propre de $u$ et $e_1+e_2$ est un vecteur propre associé à cette valeur propre.\\

\end{exem}





\subsection{Sous-espaces propres}

Si $\lambda\in\R$, on note  $E_{\lambda}$ le sous-ensemble de $E$ défini par :
$$E_{\lambda}=\{ x\in E \text{ tel que } u(x)=\lambda x\}$$ 

\begin{pro}

$E_{\lambda}$ est un sous-espace vectoriel de E.
\end{pro}

\begin{preuve}

  
\begin{tabular}{rll}
$E_{\lambda}$ & $=\{ x\in E \text{ tel que } u(x)=\lambda x\}$ \\
          & $=\{ x\in E \text{ tel que } (u-\lambda Id)(x)=0\}$\\
	  & $=\ker(u-\lambda Id)$
\end{tabular}\\
En particulier $E_{\lambda}$ est un sous-espace vectoriel de E.
\end{preuve}


\begin{thm}
 Soient $u\in\mathcal{L}(E)$ et $\lambda\in\R$.\\
Les propriétés suivantes sont équivalentes :
\begin{enumerate}
 \item[i)] $\lambda$ est une valeur propre de $u$.
\item[ii)] $E_{\lambda}\neq\{0_E\}$.
\item[iii)] $\rg(u-\lambda Id_E)<n$.

\end{enumerate}
 
\end{thm}
\begin{preuve}
 \begin{enumerate}
 \item[$i) \Rightarrow ii)$] Si $\lambda$ est une valeur propre de $E$ alors $\exists x\in E, x\neq O_E \text{ tel que } u(x)=\lambda x$.\\
c'est à dire $\exists x\in E, x\neq O_E \text{ tel que } x\in E_{\lambda}$. Donc $E_{\lambda}\neq\{0_E\}$.
\item[$ii) \Rightarrow iii)$] Supposons que $E_{\lambda}\neq\{0_E\}$.\\
$E_{\lambda}\neq\{0_E\} \Leftrightarrow \ker(u-\lambda Id)\neq\{0_E\}\Leftrightarrow u-\lambda Id \text{ non injectif }$.\\
Or $u-\lambda Id_E$ est un endomorphisme de $E$, s'il n'est pas injectif, il n'est pas surjectif.\\
En particulier $\rg(u-\lambda Id_E)<n$.
\item[$iii)\Rightarrow i)$] Si $u-\lambda Id$ n'est pas surjectif alors $u-\lambda Id$ n'est pas injectif (car $u-\lambda Id\in\mathcal{L}(E)$) \\
donc $\exists x\neq 0_E \text{ tel que } x\in \ker(u-\lambda Id)$.\\
Autrement dit  $\exists x\neq 0_E \text{ tel que } u(x)=\lambda x$, On en déduit que $\lambda$ est bien une valeur propre de $u$.
\end{enumerate}
\end{preuve}


\begin{Def}
Si $\lambda$ est valeur propre de $u$ alors $E_{\lambda}$ appelé sous-espace propre de $u$ associé à  $\lambda$
\end{Def}



\subsection{Cas d'une matrice}
Soit $A\in\mathcal{M}_n$, par analogie avec ce qui a été vu précédemment, on peut définir les valeurs propres de $A$, les vecteurs propres et les sous-espaces propres associés.\\
\begin{Def}
 Soient $A\in\mathcal{M}_n$ et $\lambda\in\R$.\\
\begin{enumerate}
\item[-] $\lambda$ est une valeur propre de $A$ si $\exists X\in \mathcal{M}_{n1}, X\neq O_n \text{ tel que } AX=\lambda X$.
\item[-] un tel vecteur $X$ est alors appelé vecteur propre de $A$ associé à la valeur propre $\lambda$.
\item[-] On note $E_{\lambda}=\{ X\in \mathcal{M}_{n1} \text{ tel que } AX=\lambda X\}=\ker(A-\lambda I)$
\end{enumerate}
\end{Def}

L'ensemble des propriétés vues ci-dessus peuvent être traduites en language matriciel...

\subsection{Polyn\^ome caractéristique}

\begin{Def}
 Soit $A\in\mathcal{M}_n$, $\forall \lambda\in\R, \text{ on pose } P_{A}(\lambda)=\det(A-\lambda I)$.\\
Alors $P_{A}$ est un polyn\^ome de degré $n$ en $\lambda$ appelé  \textnormal{polyn\^ome caractéristique } de la matrice $A$.
\end{Def}

\begin{pro} Soit $\lambda\in\R$,
 $\lambda$ est valeur propre de $A$ si et seulement si $P_A(\lambda)=0$
\end{pro}

\begin{preuve}
 On a vu précédemment que \\
\begin{tabular}{rcl}
 $\lambda$ est valeur propre de $A$  & si et seulement si& $A-\lambda I$ n'est pas inversible\\
                                 & si et seulement si& $\det(A-\lambda I)= 0$\\
                                 & ssi& $P_A(\lambda)=0$
 ×
\end{tabular}
\end{preuve}

\begin{exem}
 Les valeurs propres d'une matrice diagonale ou triangulaire sont ses termes diagonaux.
\end{exem}

\begin{remarque}
 Une matrice de taille $n$ admet au plus $n$ valeurs propres (distinctes ou non).
\end{remarque}
\begin{thm}
Soient $A$ et $A'$ deux éléments de $\mathcal{M}_n$.\\
Si $A$ et $A'$ sont semblables alors $P_{A}=P_{A'}$
\end{thm}

\begin{preuve}
 Si $A$ et $A'$ sont semblables alors $\exists P\in GL_n \text{ telle que } A'=P^{-1}AP$.\\
On a alors $\forall \lambda\in \R$,\\ 

\begin{tabular}{rl}
$ P_{A'}(\lambda)$ & $=\det(A'-\lambda I) =\det(P^{-1}AP-\lambda I)$\\
               & $ =\det(P^{-1}(A-\lambda I)P) =\det(P^{-1})\det(A-\lambda I)\det(P)$ \\
               & $=\det(A-\lambda I)=P_A(\lambda)$
\end{tabular}\\
On a donc bien $P_{A}=P_{A'}$
\end{preuve}


\begin{remarque} On en déduit en particulier que deux matrices semblables ont les mêmes valeurs propres (puisqu'elles ont le même polyn\^ome caractéristique).

\end{remarque}
\begin{Def}
Soit $u\in\mathcal{L} (E)$ et $\mathcal{B}$ une base de $E$.\\
On appelle polyn\^ome caractéristique de $u$, on note $P_u$ le polyn\^ome $P_A$ avec $A=\Mat(u,\mathcal{B})$.
\end{Def}

\begin{remarque}
\item[-] $P_u$ ne dépend pas de la base choisie pour exprimer $u$.\\
En effet, si $\mathcal{B}$ et $\mathcal{B}'$ sont deux bases de $E$ et si  on pose $A=\Mat(u,\mathcal{B})$ et $A'=\Mat(u,\mathcal{B}')$ alors $A$ et $A'$ sont semblables.\\
En particulier, $P_{A'}=P_{A}(=P_u)$
\item[-] $A$ et $u$ ont les mêmes valeurs propres.
\end{remarque}






\begin{exem}


Soit $A=\begin{pmatrix}
         2&~4\\1&-1
        \end{pmatrix}$.\\



\textbf{Recherchons les valeurs propres de $A$:}\\
Soit $\lambda\in\R$, $$P_A(\lambda)=\det(A-\lambda I)=\begin{vmatrix} 2-\lambda & ~4\\1&-1-\lambda \end{vmatrix}=(2-\lambda)(-1-\lambda)-4=\lambda^2-\lambda-6$$.
$\Delta= 1+4\times6=25(>0)$ donc $P_A$ admet deux racines distinctes qui sont :
$$\lambda_1=\dfrac{1+5}{2}=3 \quad  \text{et} \quad \lambda_2=\dfrac{1-5}{2}=-2$$
Conclusion: La matrice $A$ admet deux valeurs propres $-2$ et $3$.\\


 

\noindent \textbf{Déterminons les sous-espaces propres associés:}\\
Pour $E_3$, résolvons le système $AX=3X$\\
$$AX=3X \Leftrightarrow \left\{\begin{array}{l}
2x+4y=3x\\
x-y=3y
  \end{array}\right. \Leftrightarrow x=4y$$

Donc $E_3=\Vect \left\{\begin{pmatrix}
                       4\\1
                      \end{pmatrix}\right\}$.\\
Pour $E_{-2}$, résolvons le système $AX=-2X$\\
$$AX=-2X \Leftrightarrow \left\{\begin{array}{l}
  2x+4y=-2x\\
x-y=-2y
  \end{array}\right. \Leftrightarrow x+y=0$$

Donc $E_{-2}=\Vect \left\{ \begin{pmatrix}
                       ~1\\-1
                      \end{pmatrix}\right\}$.\\

\noindent On peut aussi chercher le noyau de $A-3I_2$ et de $A+2I_2$ (en général, c'est plus rapide \ldots).
\end{exem}
\begin{remarque}
 Soit $u\in\mathcal{L}(\R^2)$ tel que $\Mat(u,bc)=A$.\\
Posons $f_1=(4,1)$ et $f_2=(1,-1)$. $(f_1,f_2)$ est une famille libre de $\R^2$ donc c'est une base. \\
Notons $\mathcal{B}$ cette base et déterminons $A'=\Mat(u,\mathcal{B})$ :\\
d'après ce qui précéde, on a $u(f_1)=3f_1$ et $u(f_2)=-2f_2$ donc $A'=\begin{pmatrix}
         ~3&~0\\~0&-2
        \end{pmatrix}$.\\
Nous avons donc trouvé une base dans laquelle la matrice de $u$ est diagonale.\\
On a même $A'=P^{-1}AP$ avec $P=P_{bc,\mathcal{B}}=\begin{pmatrix}
                                   ~4&~1\\~1&-1
                                  \end{pmatrix}$

\end{remarque}







\section{Diagonalisation}
$E$ désigne un espace vectoriel sur $\R$ de dimension $n$, $n$ entier naturel non nul.
\subsection{Somme de sous-espaces propres}
\begin{pro}
 Soit $u\in\mathcal{L}(E)$,\\
si $\lambda_1$,\ldots,$\lambda_k$ sont $k$ valeurs propres distinctes de $u$ et si $x_1$,\ldots,$x_k$ sont $k$ vecteurs propres associés respectivement à $\lambda_1$,\ldots,$\lambda_k$\\
alors la famille $(x_1,\ldots,x_k)$ est libre.
\end{pro}

\begin{preuve}

  
Démontrons ce résultat par récurrence sur $k$ :
Si $k=1$, $x_1\neq 0$ (car c'est un vecteur propre) et donc $(x_1)$ est une famille libre.\\
Soit $k\in\N^*$, on suppose que la famille $(x_1,\ldots,x_k)$ est libre et on regarde la famille $(x_1,\ldots,x_{k+1})$.\\
Soit $(a_1,\ldots,a_{k+1})\in\R^{k+1}$, tel que $a_1x_1+\ldots+a_{k+1}x_{k+1}=0$ (*).\\
On a donc $a_{k+1}x_{k+1}=-a_{1}x_1-\ldots-a_kx_k$.\\
D'autre part, en appliquant $u$ à (*), on obtient $a_1\lambda_1x_1+\ldots+a_{k+1}\lambda_{k+1}x_{k+1}=0$.\\
En rempla\c cant $x_{k+1}$, on en déduit $a_1(\lambda_1-\lambda_{k+1})x_1+\ldots+a_{k}(\lambda_{k}-\lambda_{k+1})x_{k}=0$.\\
Or la famille $(x_1,\ldots,x_k)$ est libre et toutes les valeurs propres sont distinctes donc
\begin{equation*}
a_1\underbrace{(\lambda_1-\lambda_{k+1})}_{\neq 0}=\ldots=a_k\underbrace{(\lambda_k-\lambda_{k+1})}_{\neq 0}=0\quad  \Rightarrow \quad a_1=\ldots=a_{k}=0
\end{equation*}

En remplaçant dans (*), on en déduit que l'on a aussi $a_{k+1}=0$ (car
$x_{k+1}\neq0$) et donc la famille $(x_1,\ldots,x_{k+1})$ est libre. Conclusion : Pour tout $k\in\N$,  si $\lambda_1$,\ldots,$\lambda_k$
sont $k$ valeurs propres distinctes de $u$ et si $x_1$,\ldots,$x_k$
sont $k$ vecteurs propres associés respectivement à
$\lambda_1$,\ldots,$\lambda_k$ alors la famille $(x_1,\ldots,x_k)$ est libre.
\end{preuve}

\begin{pro}
 Soit $u\in\mathcal{L}(E)$,\\
si $\lambda_1$,\ldots,$\lambda_k$ sont $k$ valeurs propres distinctes de $u$ et $E_1$,\ldots,$E_k$ les sous-espaces propres associés,\\
 alors $E_1$,\ldots,$E_k$ sont en somme directe.
\end{pro}

\begin{preuve}
  
\noindent Rappel : $E_1$,\ldots,$E_k$ sont en somme directe si et seulement sitout vecteur de $E_1+\ldots+E_k$ admet une unique décomposition comme somme de vecteurs de $ E_1\times\ldots \times E_k$.\\
ou encore \\
$E_1$,\ldots,$E_k$ sont en somme directe si et seulement si $\forall i\neq j$, $E_i\cap E_j=\{0_E\}$.\\

 
\noindent Soit donc  $\forall i\neq j$ et $x\in E$,\\
 $x\in E_i\cap E_j$ si et seulement si $x\in E_i$ et $x\in E_j$. On a alors $u(x)=\lambda_i x$ et  $u(x)=\lambda_j x$ donc $\lambda_i x=\lambda_j x$.\\
Or $\lambda_i\neq \lambda_j$ donc $(\lambda_i-\lambda_j)x=0_E\Rightarrow x=0_E$.\\
On en déduit que $E_i\cap E_j=\{0_E\}$.
\end{preuve}

\begin{pro}
 Soit $u\in\mathcal{L}(E)$ et $P_u$ son polyn\^ome caractéristique.\\
Si $\lambda$ est racine d'ordre $\alpha(\lambda)$ de $P_u$ alors $\dim(E_{\lambda})\leq \alpha(\lambda)$.
\end{pro}

\begin{preuve}
 Posons $p=\dim(E_{\lambda})$.\\
Soit $(e_1,\ldots,e_p)$ une base de $E_{\lambda}$, alors c'est une famille libre de $E$ que l'on peut compléter en une base $\mathcal{B}=(e_1,\ldots,e_n)$ de $E$.\\
On a alors $A=mat(u,\mathcal{B})=\left( 
\begin{array}{ccc|c} \lambda &        &     & \vdots \\
                          & \ddots &     & \vdots \\
                          &        & \lambda &  \vdots \\
\hline 
                      0   & \ldots & 0 & Q
 \end{array}\right)$ avec $Q\in\mathcal{M}_{n-p}$.\\

\vspace{0,1cm}

\noindent On en déduit que $P_u(X)=P_A(X)=\det(A-XI_n)=(\lambda-X)^p\cdot\det(Q-XI_{n-p})$ et donc $\lambda$ est racine de $P_u$ d'ordre au moins $p=\dim(E_{\lambda})$.\\
On a donc bien  $\dim(E_{\lambda})\leq \alpha(\lambda)$.
\end{preuve}

\begin{remarque}
 Si $\lambda$ est racine d'ordre $1$ de $P_u$ alors $\dim(E_{\lambda})=1$.\\
En effet, on sait que $\lambda$ est alors une valeur propre de $u$ donc $E_{\lambda}\neq\{0\}$ et donc $\dim(E_{\lambda})\geq 1$.\\
Mais on sait aussi que $\dim(E_{\lambda})\leq 1$ d'aprés la propriété ci-dessus.\\ On en déduit que $\dim(E_{\lambda})=1$.
\end{remarque}

%\begin{pro}
% Soit $u\in\mathcal{L}(E)$, on a $\underset{\lambda\in sp(u)}{\sum} \dim(E_{\lambda})\leq n$.
%\end{pro}

%\begin{preuve}
%Soit $ \lambda\in sp(u)$, on note $\alpha(\lambda)$ la multiplicité de $\lambda$ comme racine de $P_u$.\\
%On a alors ${P_u(X)}= \underset{\lambda\in sp(u)}{\prod}(X-\lambda)^{\alpha(\lambda)} \cdot {Q(X)}$
%et donc $$\underbrace{d^o P_u}_{= n}= \underset{\lambda\in sp(u)}{\sum}\alpha(\lambda) + \underbrace{d^o Q}_{\geq 0}$$
%D'où $ \underset{\lambda\in sp(u)}{\sum}\alpha(\lambda)\leq n$.
% Or  $\forall \lambda\in sp(u),\; \dim(E_{\lambda})\leq \alpha(\lambda)$.\\
%On obtient donc l'inégalité souhaitée :  
%$$\underset{\lambda\in sp(u)}{\sum} \dim(E_{\lambda})\leq \underset{\lambda\in sp(u)}{\sum} \alpha(\lambda)\leq n$$
%\end{preuve}



\subsection{Endomorphismes et matrices diagonalisables}
\begin{Def}
Soit $u\in\mathcal{L}(E)$ et $A\in\mathcal{M}_n$,
 \begin{enumerate}
  \item[-]  $u$ est diagonalisable si il existe une base de $E$ formée de vecteurs propres pour $u$.\\
(Dans cette base, la matrice de $u$ sera donc diagonale \ldots )
\item[-] La matrice $A$ est diagonalisable si elle est semblable à une matrice diagonale.
 \end{enumerate}
\end{Def}

\begin{remarque}
$A$ est diagonalisable si et seulement si $A$ est la matrice d'un endomorphisme diagonalisable exprimé dans une base quelconque de $E$.\\
Cela signifie que toutes les propriétés qui seront énoncées pour les endomorphismes pourront être traduites dans le domaine matriciel.
\end{remarque}



\begin{exem}
Reprenons l'exemple traité précédemment :\\
Soit $A=\begin{pmatrix}
         2&~4\\1&-1
        \end{pmatrix}$.\\
Nous avons montré que $A$ est semblable à $A'=\begin{pmatrix}
         ~3&~0\\~0&-2
        \end{pmatrix}$. \\
Donc $A$ est diagonalisable (et l'endomorphisme $u$ canoniquement associé à $A$ aussi).
\end{exem}

\begin{remarque}
 Si $A$ est diagonalisable alors $A$ est semblable à une matrice diagonale $D$ et les termes diagonaux de $D$ sont les valeurs propres de $A$.\\
En effet, on sait que $A$ et $D$ ont les mêmes valeurs propres et que les valeurs propres de $D$ sont ses termes diagonaux. 
\end{remarque}

\begin{pro}
 Si $u$ posséde $n$ valeurs propres distinctes alors $u$ est diagonalisable.
\end{pro}

\begin{preuve}
 Notons $\lambda_1$,\ldots , $\lambda_n$ les $n$ valeurs propres distinctes de $u$.\\
$\forall i\in\{1,\ldots,n\}$, on choisit $e_i \in E \text{ tel que } e_i\neq 0  \text{ et } u(e_i)=\lambda_i e_i$.\\
Les $e_i$ étant $n$ vecteurs propres associés à des valeurs propres distinctes, ils forment une famille libre.\\
Or $n=\dim(E)$ donc c'est une base. On en déduit que $u$ est diagonalisable.
\end{preuve}

\begin{exem}
 Soit $E$ un espace vectoriel de dimension $2$ et $\mathcal{B}=(e_1,e_2)$ une base de $E$.\\
On considère $u\in\mathcal{L}(E)$ défini par $u(e_1)=u(e_2)=e_1+e_2$.\\
On a alors :\\
$u(e_1)=u(e_2)\Leftrightarrow u(e_1-e_2)=0$ donc $0$ est valeur propre de $u$ \\(et $e_1-e_2$ est un vecteur propre associé à $0$).\\
$u(e_1)+u(e_2)=(e_1+e_2)+(e_1+e_2)\Leftrightarrow u(e_1+e_2)=2(e_1+e_2)$ donc $2$ est valeur propre de $u$ \\(et $e_1+e_2$ est un vecteur propre associé à $2$).\\
$\dim(E)=2$ et $u$ possède $2$ valeurs propres distinctes donc $u$ est diagonalisable. \\
De plus, $\mathcal{B}'=(e_1-e_2 ; e_1+e_2)$ est une base $E$ formée de vecteurs propres pour $u$.\\

\noindent Regardons l'aspect matriciel :\\
Soit $A=\Mat(u,\mathcal{B})=\begin{pmatrix} 1 & 1\\1&1 \end{pmatrix}$.\\
$A$ est semblable à la matrice $A'=\Mat(u,\mathcal{B}')=\begin{pmatrix} 0 & 0\\0&2 \end{pmatrix}$.\\
On a même $A'=P^{-1}AP$ avec $P=P_{\mathcal{B}\mathcal{B}'}=\begin{pmatrix} 1 & 1\\-1&1 \end{pmatrix}$
\end{exem}

\begin{thm}
 Soient $\lambda_1$,\ldots,$\lambda_k$ les valeurs propres (distinctes) de $u$.\\
Les propositions suivantes sont équivalents :
\begin{enumerate}
\item[i)] $u$ est diagonalisable 
\item[ii)] $E=E_{\lambda_1}\oplus \ldots \oplus E_{\lambda_k}$
\item[iii)] $\underset{i=1}{\overset{k}{\sum }}\dim(E_{\lambda_i})=n$
\end{enumerate}

\end{thm}

\begin{preuve}
\begin{enumerate}
\item[$i)\Rightarrow ii)$] Supposons que  $u$ est diagonalisable, alors il existe une base de $E$ formée de vecteurs propres pour $u$. 
Tout vecteur de $E$ peut donc s'écrire comme combinaison linéaire de vecteurs propres. D'où $E=E_{\lambda_1} +\ldots +E_{\lambda_k}$.\\
Or les $\lambda_i$ étant distinctes, on a vu que les sous-espaces propres associés sont en somme directe.\\
On a donc bien $E=E_{\lambda_1}\oplus \ldots \oplus E_{\lambda_k}$
\item[$ii)\Rightarrow iii)$] (trivial) \\
Si $E=E_{\lambda_1}\oplus \ldots \oplus E_{\lambda_n}$, on a bien $\underset{i=1}{\overset{k}{\sum }}\dim(E_{\lambda_i})=n$
\item[$iii) \Rightarrow i)$] Supposons que $\underset{i=1}{\overset{k}{\sum }}\dim(E_{\lambda_i})=n$.\\
$\forall i\in\{1,\ldots,k\}$, on choisit $\mathcal{B}_i$ une base de $E_{\lambda_i}$.\\
Comme les valeurs propres sont distinctes, on sait les sous-espaces propres associés sont en somme directe.\\
Or  $\underset{i=1}{\overset{k}{\sum }}\dim(E_{\lambda_i})=n=\dim(E)$ donc  $E=E_{\lambda_1}\oplus \ldots \oplus E_{\lambda_k}$.\\
On en déduit que $\mathcal{B}=\mathcal{B}_1 \cup \ldots  \cup\mathcal{B}_k$ est une base de $E$. Comme elle est formée de vecteurs propres pour $u$, $u$ est bien diagonalisable.

\end{enumerate} 
\end{preuve}

\subsection{Utilisation de la trace}
\noindent Rappel concernant la trace d'une matrice :
\begin{Def}
Si $A\in\mathcal{M}_n$.\\
On appelle trace de $A$, on note $Tr(A)$ la somme des éléments diagonaux de $A$.
\end{Def}

\begin{exem}
 \end{exem}

\noindent Soit $A=\begin{pmatrix}
                          1&2&1\\4&3&1\\0&0&1
                         \end{pmatrix}$, on a $Tr(A)=1+3+1=5$\\

 \noindent Soit $B=\begin{pmatrix}
                          1&1\\2&5
                         \end{pmatrix}$, on a $Tr(B)=1+5=6$

\begin{pro}(admise)
 Soient $A$ et $B$  dans $\mathcal{M}_n$, alors $Tr(AB)=Tr(BA)$
\end{pro}

\noindent En particulier, si $A$ et $B$ sont semblables, alors $Tr(A)=Tr(B)$.\\
En effet, si $A $ et $B$ sont semblables, il existe $P$ matrice inversible telle que $B=P^{-1}AP$ alors
$$Tr(B)=Tr(P^{-1}AP)=Tr(APP^{-1})=Tr(A)$$

\noindent Si la matrice $A$ est diagonalisable, alors $A$ est semblable à une matrice diagonale $D$.\\
On a alors : $Tr(A)=Tr(D)=\sum \text{termes diagonaux de }D=\sum \text{ valeurs propres de } A$

\begin{remarque}
 Cette propriété nous sera utile afin de vérifier la cohérence des calculs.
\end{remarque}

\begin{exem}
Reprenons l'exemple traité précédemment :\\
Soit $A=\begin{pmatrix}
         2&~4\\1&-1
        \end{pmatrix}$.\\
Nous avons montré que $A$ est diagonalisable  et $A$ admet  $-2$ et $3$ comme valeurs propres.\\

\noindent Vérification : $\sum \; vp= -2+3=1$ et $Tr(A)=2-1=1$.\\
A priori, pas d'erreur de calcul\ldots
\end{exem}


\subsection{Étude de quelques exemples}

\begin{exem}

Notons $\mathcal{B}=(e_1,e_2,e_3)$ une base  de $\R^3$.
Soit $u\in\mathcal{L}(\R^3)$ défini par $\left\{\begin{array}{l}
u(e_1)=3e_1+e_2+e_3\\
u(e_2)=e_1+3e_2+e_3\\
u(e_3)=e_1+e_2+3e_3\\
                                                \end{array}\right.$\\
                                              

Est-ce que $u$ est diagonalisable, le cas échéant déterminer une
base dans laquelle $u$ est diagonal.
\begin{prof}
  On a alors
  $A=\Mat(u,\mathcal{B})=\begin{pmatrix} 3&1&1\\1&3&1\\1&1&3
  \end{pmatrix}$.\\
  Déterminons les valeurs propres de $u$ (et de $A$) :\\
  On pose $P_A(X)=\det(A-X I)$\\
  \begin{align*}
    P_A(X)& =\begin{vmatrix} 3-X&1&1\\1&3-X&1\\1&1&3-X\end{vmatrix}\\
          & =(5-X)\begin{vmatrix} 1&1&1\\1&3-X&1\\1&1&3-X\end{vmatrix}=(5-X)\begin{vmatrix} 1&1&1\\0&2-X&0\\0&0&2-X\end{vmatrix}\\
          &= (5-X)(2-X)^2
  \end{align*}
  $\lambda$ est valeur propre de $A$ si et seulement si $P_A(\lambda)=0$ \\
  donc  $A$ admet 2 valeurs propres distinctes : $2$ (vp d'ordre 2) et $5$ (vp d'ordre 1).\\

  \noindent Vérification : $\sum \; vp=2+2+5=9$ et $Tr(A)=3+3+3=9$.\\

  \begin{enumerate}
  \item[-] Déterminons $E_5=\ker(A-5I_3)$ :
    $A-5I_3=\begin{pmatrix} -2&1&1\\1&-2&1\\1&1&-2\end{pmatrix}$. \\
    On remarque que $C_1+C_2+C_3=0$  donc $e_1+e_2+e_3=\begin{pmatrix}1\\1\\1  \end{pmatrix}_{\mathcal{B}}$ est un vecteur (non nul) de  $E_5$, or $\dim(E_5)=1$ donc c'est une base de $E_5$.\\

  \item[-] Déterminons $E_2=\ker(A-2I_3)$ :
    $A-2I_3=\begin{pmatrix} 1&1&1\\1&1&1\\1&1&1\end{pmatrix}$. \\
    On remarque que $C_1-C_2=0$ et que $C_1-C_3=0$ donc
    $e_1-e_2=\begin{pmatrix}~1\\-1\\~0 \end{pmatrix}_{\mathcal{B}}$ et
    $e_1-e_3=\begin{pmatrix}
      ~1\\~0\\-1  \end{pmatrix}_{\mathcal{B}}$ sont deux vecteurs linéairement indépendants de $E_2$.\\
    De plus, on sait que $\dim(E_2)\leq 2$ donc
    $\begin{pmatrix}~1\\-1\\~0 \end{pmatrix}_{\mathcal{B}}$ et
    $\begin{pmatrix}
      ~1\\~0\\-1  \end{pmatrix}_{\mathcal{B}}$ forment une base de $E_2$.\\
  \end{enumerate}
  \noindent On a donc $\dim(E_2)+\dim(E_5)=2+1=3=\dim(\R^3)$. On en déduit que $u$ et $A$ sont diagonalisables.\\

  \noindent $\mathcal{B'}=\lbrace e_1-e_2, e_1-e_3, e_1+e_2+e_3\rbrace$ est une base de $\R^3$ formée de vecteurs propres pour $u$.\\
  De plus, $A$ est semblable à la matrice $A'=\Mat(u,\mathcal{B}')=\begin{pmatrix} 2 & 0&0\\0&2&0\\0&0&5 \end{pmatrix}$.\\
  On a même $A'=P^{-1}AP$ avec
  $P=P_{ \mathcal{B}\mathcal{B}'}=\begin{pmatrix} 1 &
    1&1\\-1&0&1\\0&-1&1 \end{pmatrix}$.
\end{prof}
\end{exem}

\begin{exem}

Soit $B=\begin{pmatrix}
         ~4&-3&-1\\~4&-3&-2\\-1&~1&~2
       \end{pmatrix}$\\


      La matrice $B$ est-elle diagonalisable?
       \begin{prof}
         Déterminons les valeurs propres de $B$ :\\
         On pose $P_B(X)=\det(B-X I_3)$\\

\begin{align*}
  P_B(X)& =\begin{vmatrix}  4-X&-3&-1\\~4&-3-X&-2\\-1&~1&2-X\end{vmatrix} =\begin{vmatrix}  1-X&-3&-1\\1-X&-3&-2\\~0&~1&~2\end{vmatrix}\\
        & =(1-X)\begin{vmatrix}  ~1&-3&-1\\~0&-X&-1\\0&~1&2-X\end{vmatrix}\\
        &= (1-X)(-X(2-X)+1)=(1-X)^3
\end{align*}

\noindent donc  $B$ admet 1 valeur propre: $1$ (vp d'ordre 3).\\
Vérification : $\sum \; vp=1+1+1=3$ et $Tr(B)=4-3+2=3$.\\

\noindent On peut donc affirmer que $B$ n'est pas diagonalisable : en
effet, si $B$ était diagonalisable, alors $B$ serait semblable à la
matrice $I_3$ autrement dit $B$ serait égale à $I_3$\ldots ce qui
n'est pas le cas !
\end{prof}

\end{exem}
\begin{exem}

Soit $C=\begin{pmatrix}
         ~2&~1&~1\\~1&~3&~0\\-1&~0&~3
       \end{pmatrix}$\\
       
   Diagonaliser $C$, si c'est possible.
   \begin{prof}
     Déterminons les valeurs propres de $C$ :\\
     On pose $P_C(X)=\det(C-X I_3)$\\
     \begin{align*}
       P_C(X)&=\begin{vmatrix} 2-X&1&1\\1&3-X&0\\-1&0&3-X\end{vmatrix}=\begin{vmatrix} 2-X&1&0\\1&3-X&X-3\\-1&0&3-X\end{vmatrix}\\
             &=(3-X)\begin{vmatrix} 2-X&1&0\\1&3-X&-1\\-1&0&1\end{vmatrix}=(3-X)\begin{vmatrix} 2-X&1&0\\0&3-X&0\\-1&0&1\end{vmatrix}\\
             &=-(2-X)(3-X)^2
     \end{align*}

     \noindent donc  $C$ admet 2 valeurs propres distinctes : $2$ (vp d'ordre 1) et $3$ (vp d'ordre 2).\\
     \noindent Vérification : $\sum \; vp=2+3+3=8$ et $Tr(C)=2+3+3=8$.\\

     \begin{enumerate}
     \item[-] Déterminons $E_2=\ker(C-2I_3)$ :
       $C-2I_3=\begin{pmatrix} 0&1&1\\1&1&0\\-1&0&1\end{pmatrix}$. \\
       On remarque que $C_1+C_3=C_2$  donc $\begin{pmatrix}1\\-1\\1  \end{pmatrix}$ est un vecteur (non nul) de  $E_2$, or $\dim(E_2)=1$ donc c'est une base de $E_2$.\\

     \item[-] Déterminons $E_3=\ker(C-3I_3)$ :
       $C-3I_3=\begin{pmatrix} -1&1&1\\1&0&0\\-1&0&0\end{pmatrix}$. \\

       \noindent On remarque que $C_2=C_3$ donc $\begin{pmatrix}~0\\~1\\-1  \end{pmatrix}$ est  un vecteur non nul de $E_3$.\\
       De plus, $(C_1,C_2)$ est une famille libre donc $rg(C-3I_3)=2$ et $\dim(\ker(C-3I_3))=3-2=1$.\\
       On en déduit que $\begin{pmatrix}~0\\~1\\-1  \end{pmatrix}$ est une base de $E_3$.\\
     \end{enumerate}
     \noindent On a donc $\dim(E_2)+\dim(E_3)=1+1=2\neq\dim(\R^3)$. On en déduit que $C$ n'est pas diagonalisable.\\



\begin{remarque}
  On vient de voir que $C$ n'est pas diagonalisable mais on pourrait
  montrer que $C$ est semblable à une matrice triangulaire.  On dira
  alors que $C$ est trigonalisable.
\end{remarque}
\end{prof}
\end{exem}


\end{document}
