%%% Ce document ainsi que tous les documents qui y sont liés sont
%%% soumis à la licence CC NC BY SA
%%% (https://creativecommons.org/licenses/by-nc-sa/4.0/)


% rubber: module biblatex

\documentclass[Cours,11pt]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}

\titre{Matrice d'une application linéaire}
\auteur{M. Pelini, V. Ledda}

\reference{Chapitre 4}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 SIAD \\
Algèbre linéaire}

\motsclefs{application linéaire,matrice, rang}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing}


\usepackage[backend=biber,   style=science,    sortlocale=fr_FR, natbib=true,    url=false,     doi=false,    eprint=true]{biblatex}

\addbibresource{../../biblio.bib}
\usepackage[tikz]{bclogo}

\etudiant=1
\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Définitions}
\subsection{L'ensemble $\mathcal{M}_{n,p}$}
\begin{Def}
 On appelle matrice réelle à $n$ lignes, $p$ colonnes un ensemble de $n\times p$ nombres réels donnés sous la forme d'un tableau.\\
Une telle matrice est dite de taille $(n,p)$.
\end{Def}
\begin{center}
 \begin{tabular}{rcl}
 $M=$ & $\left( \begin{array}{ccccc}
 a_{11} & \cdots & a_{1j} & \ldots & a_{1n}\\
\vdots &     & \vdots  &         & \vdots \\
\vdots &     & a_{ij}  &         & \vdots \\
\vdots &     & \vdots  &         & \vdots \\
 a_{n1} & \cdots & a_{nj} & \ldots & a_{nn}\\
    \end{array}\right) $ & $\gets i$ ème ligne \\
     & $\uparrow$ &   \\
     &$j$  ème colonne &             
\end{tabular}
\end{center}

\begin{remarque}
On note $a_{ij}$ le terme situé à l'intersection de la ligne $i$ et de le colonne $j$.\\
On donne toujours l'indice de ligne puis l'indice de colonne. 
\end{remarque}

\noindent La matrice $M$ sera noté de manière abrégée 
$M=(a_{ij})_{ 1\leq i\leq n  ,  1\leq j\leq p }$ ou, plus simplement, $ M=(a_{ij})_{n,p}$.

\begin{Def}
 On note $\mathcal{M}_{n,p}$ l'ensemble des matrices de tailles $(n,p)$.
\end{Def}

\begin{exem}
 $M=\begin{pmatrix}
     1 & -4 & \dfrac35 \\
     -2 & 0,5 & 10
    \end{pmatrix}$ est une matrice de taille $(2,3)$ donc $M\in \mathcal{M}_{2,3}$

\end{exem}

Cas Particuliers : 
\begin{enumerate}
 \item[-] Si $n=1$, \\
$\mathcal{M}_{1,p}$ désigne l'ensemble des matrices à une ligne et $p$ colonnes.\\
On les appellent \textbf{ matrices lignes}.
$L=\begin{pmatrix}
    a_{11} & a_{12} & \ldots & a_{1p}
   \end{pmatrix}$

\item[-] Si $p=1$, \\
$\mathcal{M}_{n,1}$ désigne l'ensemble des matrices à $n$ lignes et une colonne.\\
On les appellent \textbf{ matrices colonnes}.
$C=\begin{pmatrix}
    a_{11} \\ a_{21} \\ \vdots \\ a_{n1}
   \end{pmatrix}$

\item[-] Si $n=p$, \\
On parlera de \textbf{matrice carrée} d'ordre $n$. L'ensemble $\mathcal{M}_{n,n}$ se note $\mathcal{M}_n$.\\
Les termes en position $(i,i)$ (les $a_{ii}$, $1\leq i\leq n$) sont alors appêlés \textbf{termes diagonaux}.
\end{enumerate}

\begin{Def}
 Soit $M\in \mathcal{M}_n$, $M=\left( a_{ij}\right) $.\\
On appelle \textbf{trace} de $M$, on note $Tr(M)$, la somme des termes diagonaux de la matrice.
$$ tr(M)=\sum_{i=1}^n a_{ii}$$
\end{Def}

\begin{exem}
 Soit $A=\begin{pmatrix}
          1&1&1\\1&1&2\\1&0&5
         \end{pmatrix}$\\
$A$ est une matrice carrée d'ordre $3$ et on a $Tr(A)=1+1+5=7$.
\end{exem}

\textbf{La matrice identité} \\
\begin{tabular}{lr}
\begin{tabular}{l}
On appelle matrice identité d'ordre $n$ \\la matrice carrée de taille $n$,\\ ayant des $1$ sur la diagonale\\ et des $0$ partout ailleurs.\\ Elle est notée $I_n$.\end{tabular} & $I_n=\begin{pmatrix}
  1&0& \ldots &\ldots &0\\
  0&1&        &       &  \\
  \vdots &\vdots &\ddots  &       &\vdots\\
  \vdots & \vdots&        & \ddots&\vdots \\
  0&0&\ldots  & \ldots      & 1                                                                                                               \end{pmatrix}$
 
\end{tabular}

\vspace{0.2cm}

\textbf{La matrice nulle} \\
\begin{tabular}{lr}
\begin{tabular}{l}
On appelle matrice nulle de taille $(n,p)$  \\la matrice dont tous les coefficients sont nuls .\\ Elle est notée $O_{n,p}$.\end{tabular} & $O_{n,p}=\begin{pmatrix}
  0& \ldots&0\\
 \vdots &\ddots &\vdots\\
0&\ldots&0        \end{pmatrix}$
 
\end{tabular}

\subsection{Matrice d'une application linéaire}
\noindent Dans la suite du chapitre, $E$ désigne un espace vectoriel de dimension $p$ et $F$ un espace vectoriel de dimension $n$ (avec $n$ et $p$ des entiers naturels non nuls).\\
Soient $\mathcal{B}=\{e_1,\ldots,e_p\}$ une base de $E$ et $\mathcal{B}'=\{f_1,\ldots,f_n\}$ une base de $F$.\\
On considère $u$ une application linéaire de $E$ dans $F$ ($u\in \mathcal{L}(E,F)$) et $x$ un vecteur quelconque de $E$.\\
$x$ se décompose de manière unique dans la base $\mathcal{B}$ : $x=x_1e_1+\ldots+x_pe_p=\underset{j=1}{\overset{p}{\sum }} x_je_j$.\\
On a alors : $u(x)=u(x_1e_1+\ldots+x_pe_p)=x_1u(e_1)+\ldots+x_pu(e_p)=\underset{j=1}{\overset{p}{\sum }} x_ju(e_j)$, car $u$ est linéaire.\\
Donc $u$ est entièrement déterminée par $u(e_1)$, \ldots,$u(e_p)$.\\



\noindent Or les $u(e_j)$ sont des vecteurs de $F$ donc ils se décomposent de manière unique dans la base $\mathcal{B}'$.\\
$u(e_1)=a_{11}f_1+\ldots+a_{n1}f_n=\begin{pmatrix}  a_{11}\\ \vdots \\ a_{n1} \end{pmatrix}_{\mathcal{B}'}$ ,\\
\vdots \\
$u(e_j)=a_{1j}f_1+\ldots+a_{nj}f_n=\begin{pmatrix}  a_{1j}\\ \vdots \\ a_{nj} \end{pmatrix}_{\mathcal{B}'}$ ,\\
\vdots \\
$u(e_p)=a_{1p}f_1+\ldots+a_{np}f_n=\begin{pmatrix}  a_{1p}\\ \vdots \\ a_{np} \end{pmatrix}_{\mathcal{B}'}$ .\\

\noindent 
En rangeant dans un tableau les $p$ colonnes obtenues, représentant les coordonnées des vecteurs $u(e_1)$,\ldots,$u(e_p)$ dans la base $\mathcal{B}'$, nous obtenons la matrice suivante :\\
\begin{center}
 \begin{tabular}{rcl}
 $M=$ & $\left( \begin{array}{cccccc}
 a_{11} & a_{12} & \ldots & a_{1j} & \ldots & a_{1p}\\
a_{21}  & a_{22} & \ldots & a_{2j} & \ldots & a_{2p} \\
\vdots  & \vdots &        & \vdots &         & \vdots \\
a_{i1}  & a_{i2} & \ldots & a_{ij} &  \ldots & a_{ip} \\
\vdots  & \vdots &        & \vdots &         &  \vdots \\
a_{n1}  & a_{n2} & \ldots & a_{nj} & \ldots & a_{np}\\
    \end{array}\right) $ & \begin{tabular}{l} $\gets f_1$\\$\gets f_2$ \\ \\$\gets f_i$ \\ \\$\gets f_n$ \end{tabular}\\

     & \begin{tabular}{lccrcr}$\uparrow$ & & &$\uparrow$& & $\uparrow$ \\
                              $u(e_1)$ & & &$u(e_j)$& & $u(e_p)$ \end{tabular} &   
\end{tabular}
\end{center}
Le terme $a_{ij}$ représente la $i$ ème coordonnée de $u(e_j)$ dans la base $\mathcal{B}'$.

\begin{Def}
La matrice obtenue est appelée matrice de $u$ relativement aux bases $\mathcal{B}$ et $\mathcal{B}'$.\\
On la note $M(u,\mathcal{B},\mathcal{B}')$.
\end{Def}
Reprenons notre vecteur  $x=x_1e_1+\ldots+x_pe_p$, on a vu que \\

$\begin{aligned}
 u(x) & =x_1u(e_1)+\ldots+x_pu(e_p)=\underset{j=1}{\overset{p}{\sum }} x_ju(e_j)\\
                    & = \underset{j=1}{\overset{p}{\sum }}x_j\left(\underset{i=1}{\overset{n}{\sum }}a_{ij} f_i\right) =\underset{j=1}{\overset{p}{\sum }}\underset{i=1}{\overset{n}{\sum }}a_{ij}x_j f_i \\
                    &=\underset{i=1}{\overset{n}{\sum }}\left(\underset{j=1}{\overset{p}{\sum }}a_{ij}x_j\right) f_i
 \end{aligned}$.\\

\noindent Posons $y=u(x)$, $y$ est un vecteur de $F$ et donc se décompose de manière unique dans la base $\mathcal{B}'$ :  $y=y_1f_1+\ldots+y_nf_n=\underset{i=1}{\overset{n}{\sum }} y_if_i$.\\
En comparant cette écriture à celle de $u(x)$ obtenue précédemment, on en déduit (unicité des coordonnées) que : 
$\forall i\in\{1,\ldots,n\},\; y_i=\underset{j=1}{\overset{p}{\sum }}a_{ij}x_j$, ou encore :\\

$ \left\lbrace \begin{array}{cc}
y_1&=a_{11}x_1+a_{12}x_2+\ldots+a_{1p}x_p\\
y_2&=a_{21}x_1+a_{22}x_2+\ldots+a_{2p}x_p\\
\vdots & \\
y_n&=a_{n1}x_1+a_{n2}x_2+\ldots+a_{np}x_p\\
\end{array}\right.$

\begin{Def}
 Ces équations sont appelées équations de $u$ relativement aux bases $\mathcal{B}$ et $\mathcal{B}'$.\\
Elles donnent les coordonnées $y_i$, dans la base $\mathcal{B}'$, de l'image d'un vecteur quelconque de $E$, connu par ses coordonnées $x_j$, dans la base $\mathcal{B}$.
\end{Def}




\subsection{Exemples}
\begin{exem}
$ $\\
$M(Id_E, \mathcal{B},\mathcal{B})=I_p$\\
$M(0_{EF},\mathcal{B},\mathcal{B}')=0_{np}$
\end{exem}

\begin{exem}
\noindent Prenons :\\
$E=\mathbb{R}^2$ muni de la  base canonique $\mathcal{B}=\left( e_1,e_2 \right)$ et
$F=\mathbb{R}^3$ muni de la  base canonique  $\mathcal{B}'=\left( f_1,f_2,f_3 \right)$.

\noindent Soit $u\in\mathcal{L}(E,F)$ définie par $u(x,y)=(2x+y, -3x+2y, -x+8y)$,\\
Nous allons déterminer la matrice de $u$ relativement aux bases $\mathcal{B}$ et $\mathcal{B}'$.\\
 Elle aura $3$ lignes et $2$ colonnes donc $M(u,\mathcal{B},\mathcal{B}')\in\mathcal{M}_{32}$.\\
Calculons l'image des vecteurs de base, on obtient :\\
$u(e_1)=u(1,0)=(2,-3,-1)=\begin{pmatrix} 2\\-3\\-1\end{pmatrix}_{\mathcal{B}'}$ et 
$u(e_2)=u(0,1)=(1,2,8)=\begin{pmatrix} 1\\2\\8\end{pmatrix}_{\mathcal{B}'}$.\\

\noindent On obtient donc la matrice 
\begin{center}
\begin{tabular}{rcl}
$M(u,\mathcal{B},\mathcal{B}')=$ & $\left( \begin{array}{cc} 2&1\\-3&2\\-1&8 \end{array}\right) $ & \begin{tabular}{l} $\gets f_1$\\$\gets f_2$ \\ $\gets f_3$ \end{tabular}\\

     & \begin{tabular}{rl}$\uparrow$ & $\uparrow$ \\
                          $u(e_1)$   & $u(e_2)$ \end{tabular} &   
\end{tabular}
\end{center}
\end{exem}
 

\subsection{Que se passe-t-il si on change de base ?}
\noindent Reprenons l'exemple ci-dessus :\\
Choisissons comme base de $E$ la famille $\mathcal{B}_0=(\varepsilon_1,\varepsilon_2)$ où $\varepsilon_1=e_1+e_2$ et $\varepsilon_2=e_2$.\\
On a alors $u(\varepsilon_1)=u(e_1)+u(e_2)=\begin{pmatrix} 2\\-3\\-1\end{pmatrix}_{\mathcal{B}'}+\begin{pmatrix} 1\\2\\8\end{pmatrix}_{\mathcal{B}'}=\begin{pmatrix} 3\\-1\\7\end{pmatrix}_{\mathcal{B}'}$ et $u(\varepsilon_2)=\begin{pmatrix} 1\\2\\8\end{pmatrix}_{\mathcal{B}'}$.\\
La matrice de $u$ relatrivement aux bases $\mathcal{B}_0$ et $\mathcal{B}'$ est donc \\
\begin{center}
\begin{tabular}{rcl}
$M(u,\mathcal{B}_0,\mathcal{B}')=$ & $\left( \begin{array}{cc} 3&1\\-1&2\\7&8 \end{array}\right) $ & \begin{tabular}{l} $\gets f_1$\\$\gets f_2$ \\ $\gets f_3$ \end{tabular}\\

     & \begin{tabular}{rl}$\uparrow$ & $\uparrow$ \\
                          $u(\varepsilon_1)$   & $u(\varepsilon_2)$ \end{tabular} &   
\end{tabular}
\end{center}
On constate que cette matrice est différente de celle obtenue précédemment.\\

Si on change de base, la matrice de l'application linéaire change elle aussi.


\subsection{Bijection entre $\mathcal{L}(E,F)$ et $\mathcal{M}_{n,p}$} 
\noindent $\mathcal{B}$ et $\mathcal{B}'$ sont deux bases fixées de $E$ et $F$.\\
Soit $M\in\mathcal{M}_{np}$, on peut construire une application linéaire $u$ de $E$ dans $F$ de la manière suivante :\\
Pour tout  $j\in\{1,\ldots,p\}$, $u(e_j)$ a pour coordonnées dans la bases $\mathcal{B}'$, la $j$ème colonne de $M$.\\
L'application $u$ est ainsi entièrement déterminée et unique. De plus $M(u,\mathcal{B},\mathcal{B}')=M$.\\

\noindent \textbf{À toute matrice $M$ de $\mathcal{M}_{np}$, on peut associer une unique application linéaire $u$ de $E$ dans $F$ telle que $M(u,\mathcal{B},\mathcal{B}')=M$.}\\

\noindent Réciproquement, on vient de voir qu'à toute application linéaire $u$ de $E$ dans $F$, on associe une unique matrice $M$ telle que $M(u,\mathcal{B},\mathcal{B}')=M$.\\

\noindent L'application $\left\{ \begin{array}{ccl} \mathcal{L}(E,F) & \longrightarrow & \mathcal{M}_{n,p} \\
                                                    u         & \longmapsto     & M(u,\mathcal{B},\mathcal{B}')
                                 \end{array}\right.\;$ est donc une bijection.

\section{L'espace vectoriel $\mathcal{M}_{n,p}$}
\noindent Dans ce paragraphe, $E$ et $F$ désignent des espaces vectoriels de dimension $p$ et $n$.\\
 $\mathcal{B}=(e_1,...,e_p)$ est une base de $E$.\\
 $\mathcal{B}'=(f_1,...,f_n)$ est une base de $F$.


\subsection{Addition de matrices}
\noindent Soient $A=(a_{ij})$ et $B=(b_{ij})$ des éléments de $\mathcal{M}_{np}$.\\

\noindent On note :\\
$u$ l'unique application linéaire de $E$ dans $F$ telle que $A=M(u,\mathcal{B},\mathcal{B}')$\\
$v$ l'unique application linéaire de $E$ dans $F$ telle que $B=M(v,\mathcal{B},\mathcal{B}')$ 

  
\begin{Def}
On définit la matrice $A+B$ de la manière suivante : $A+B=M(u+v,\mathcal{B},\mathcal{B}')$
\end{Def}

\noindent Recherchons les coefficients de cette matrice. Pour cela, il faut exprimer $(u+v)(e_j)$ ($\forall j\in\{1,\ldots,p\}$) dans la base $\mathcal{B}'$.\\
Par définition de $A$ et $B$, on a $u(e_j)=\underset{i=1}{\overset{n}{\sum }}a_{ij}f_i$ et $v(e_j)=\underset{i=1}{\overset{n}{\sum }}b_{ij}f_i$.\\

\noindent On en déduit que : $(u+v)(e_j)=u(e_j)+v(e_j)=\underset{i=1}{\overset{n}{\sum }}a_{ij}f_i+\underset{i=1}{\overset{n}{\sum}}b_{ij}f_i=\underset{i=1}{\overset{n}{\sum }}(a_{ij}+b_{ij})f_i$.\\

\noindent Donc $A+B$ est la matrice de terme général $a_{ij}+b_{ij}$.\\
Cela signifie, tout simplement, que l'addition de matrices se fait ``terme à terme''.

\begin{exem}

  Calculer:
  \begin{equation*}
    \begin{pmatrix} 3&1\\-1&2\\7&8 \end{pmatrix}+\begin{pmatrix} 1&1\\-1&2\\4&1 \end{pmatrix}
  \end{equation*}
  \begin{prof}
 $$\begin{pmatrix} 3&1\\-1&2\\7&8 \end{pmatrix}+\begin{pmatrix} 1&1\\-1&2\\4&1 \end{pmatrix}=\begin{pmatrix} 4&2\\-2&4\\11&9 \end{pmatrix}$$
\end{prof}

\end{exem}



\subsection{Multiplication par un réel}
\noindent Soient $A=(a_{ij})$ un élément de $\mathcal{M}_{np}$ et $\lambda$ un réel.\\

\noindent On note $u$ l'unique application linéaire de $E$ dans $F$ telle que $A=M(u,\mathcal{B},\mathcal{B}')$.

\begin{Def}
On définit la matrice $\lambda \cdot A$ de la manière suivante : $\lambda \cdot A=M(\lambda u,\mathcal{B},\mathcal{B}')$
\end{Def}

\noindent Recherchons les coefficients de cette matrice. Pour cela, il faut exprimer $(\lambda u)(e_j)$ ($\forall j\in\{1,\ldots,p\}$) dans la base $\mathcal{B}'$.\\
Par définition de $A$ , on a $u(e_j)=\underset{i=1}{\overset{n}{\sum }}a_{ij}f_i$.\\

\noindent On en déduit que : $(\lambda u)(e_j)=\lambda u(e_j)=\lambda\left( \underset{i=1}{\overset{n}{\sum }}a_{ij}f_i\right) =\underset{i=1}{\overset{n}{\sum }}(\lambda a_{ij})f_i$.\\

\noindent Donc $\lambda\cdot A$ est la matrice de terme général $\lambda a_{ij}$.\\
Cela signifie, tout simplement, que la multiplication d'une matrice par un réel se fait ``terme à terme''.

\begin{exem}

  Calculer:
  \begin{equation*}
    3\cdot \begin{pmatrix}2&5\\-1&8\end{pmatrix}
  \end{equation*}
  
  \begin{prof}
 $$3\cdot \begin{pmatrix}2&5\\-1&8\end{pmatrix}=\begin{pmatrix}6&15\\-3&24\end{pmatrix}$$
\end{prof}


\end{exem}

\subsection{L'espace vectoriel $\mathcal{M}_{n,p}$}
\noindent On peut démontrer que l'ensemble $\mathcal{M}_{np}$ muni des
lois $+$ et $\cdot$  est un espace vectoriel sur $\mathbb{R}$ (Il
suffit de vérifier les huit propriétés de la définition).

\noindent Soient $i\in\{1,\ldots,n\}$ et $j\in\{1,\ldots,p\}$, on note $\Delta_{ij}$ la matrice ayant un $1$ en position $(i,j)$ et des $0$ ailleurs.\\
La famille formée des matrices $\Delta_{ij}$ est une famille libre et génératrice, donc une base de $\mathcal{M}_{np}$. On en déduit que $\mathcal{M}_{np}$ est de dimension $n\times p$.\\

\noindent De plus, par définition des lois $+$ et $\cdot$, l'application $M : \left\{ \begin{array}{ccl} \mathcal{L}(E,F) & \longrightarrow & \mathcal{M}_{np} \\
                                                    u         & \longmapsto     & M(u,\mathcal{B},\mathcal{B}') \end{array}\right.\;$ est linéaire (En effet $M(u+v)=M(u)+M(v)$ et $M(\lambda u)=\lambda M(u)$).\\

\noindent Or nous avons vu précédemment qu'elle est bijective. C'est donc un isomorphisme d'espaces vectoriels.\\
On  en déduit, en particulier, que $\dim \mathcal{L}(E,F)=n\times p=\dim(E)\times \dim(F)$.


\section{Multiplication de matrices}
\subsection{Définition}
\noindent Soient $A=(a_{ij})_{np}$ un élément de $\mathcal{M}_{np}$ et $B=(b_{jk})_{pq}$ un élément de $\mathcal{M}_{pq}$.\\

\noindent On note :\\
$u$ l'unique application linéaire de $\mathbb{R}^p$ dans $\mathbb{R}^n$ telle que $A=M(u,\mathcal{B},\mathcal{B}')$\\
$v$ l'unique application linéaire de $\mathbb{R}^q$ dans $\mathbb{R}^p$ telle que $B=M(v,\mathcal{B}'',\mathcal{B})$\\
où :\\
$\mathcal{B}=(e_1,\ldots,e_p)$ est une base de $\mathbb{R}^p$,\\
$\mathcal{B}'=(f_1,\ldots,f_n)$ est une base de $\mathbb{R}^n$ ,\\ 
$\mathcal{B}''=(g_1,\ldots,g_q)$ est une base de $\mathbb{R}^q$.

\begin{Def}
On définit la matrice $A\cdot B$ de la manière suivante : $A\cdot B=M(u\circ v,\mathcal{B}'',\mathcal{B}')$
\end{Def}

\begin{remarque}
\
 \begin{enumerate}

  \item[-] Cela a bien un sens de considérer $u\circ v$. En effet, on a
$$ \mathbb{R}^q  \overset{v}{\longrightarrow}  \mathbb{R}^p \overset{u}{\longrightarrow}   \mathbb{R}^n  \text{ donc }u\circ v :\mathbb{R}^q  \longrightarrow  \mathbb{R}^n$$
 \item[-] La définition justifie, a posteriori, les tailles des matrices $A$ et $B$.\\
En effet, pour pouvoir définir le produit $AB$, il faut pouvoir composer les applications $u$ et $v$. Il faut donc que le nombre de colonnes de $A$ soit égal au nombre de lignes de $B$.
\item[-] Le produit d'une matrice de taille $(n,p)$ par une matrice de taille $(p,q)$ est une matrice de taille $(n,q)$
 \end{enumerate}

\end{remarque}

\subsection{Recherche des coefficients de la matrice produit}
\noindent On cherche à déterminer la matrice de $u\circ v$ relativement aux bases $\mathcal{B}''$ et $\mathcal{B}'$. Pour cela, il faut exprimer $u\circ v(g_k)$ ($\forall k\in\{1,\ldots,q\}$) dans la base $\mathcal{B}'$.\\
Par définition de $B$, on a $v(g_k)=\underset{j=1}{\overset{p}{\sum }}b_{jk}e_j$ et donc $u\circ v(g_k)=\underset{j=1}{\overset{p}{\sum }}b_{jk}u(e_j)$.\\
D'autre part, par définition de $A$, on a  $u(e_j)=\underset{i=1}{\overset{n}{\sum }}a_{ij}f_i$. D'où :
$$u\circ v(g_k)=\underset{j=1}{\overset{p}{\sum }}b_{jk}\left( \underset{i=1}{\overset{n}{\sum }}a_{ij}f_i\right) =\underset{i=1}{\overset{n}{\sum }}\left(\underset{j=1}{\overset{p}{\sum }} a_{ij}b_{jk}\right)f_i$$

Donc $AB$ est la matrice de terme général $c_{ik}=\underset{j=1}{\overset{p}{\sum }} a_{ij}b_{jk}=a_{i1}b_{1k}+\ldots+a_{ip}b_{pk}$.\\


En pratique, on dispose parfois les calculs de la manière suivante:%

  \begin{figure}[h!]\centering
     \input{../../Calcul_matriciel/Cours/falk.tex}
    \caption{Schéma de Falk}
    \label{fig:falk}
  \end{figure}




\subsection{Exemple}
\begin{exem}
  Calculer le produit $AB$ avec $A=\begin{pmatrix} 1&2&1\\-1&1&2\end{pmatrix}$ et $B=\begin{pmatrix} -1&2&3&-2\\2&0&1&1\\0&-1&1&1\end{pmatrix}$.\\
\end{exem}
\begin{prof}
  Vérifions tout d'abord la compatibilité : on a
  $A\in\mathcal{M}_{23}$ et $B\in\mathcal{M}_{34}$. Le nombre de
  colonnes de $A$ est bien égal au nombre de lignes de $B$ donc on
  peut faire le produit et on aura $AB\in\mathcal{M}_{24}$.

  \begin{tabular}{cc}
    \begin{tabular}{cl}
      & $\left. \begin{pmatrix} -1&2&3&-2\\2&0&1&1\\0&-1&1&1\end{pmatrix}\right\}=B$\\
      & \\
      $\underbrace{\begin{pmatrix} 1&2&1\\-1&1&2\end{pmatrix}}_{=A}$ & $\left.\begin{pmatrix}  ~3&~1&~6&~1\\~3&-4&~0&~5\end{pmatrix}\right\} =AB$                                                    \end{tabular}& \begin{tabular}{l} 
                                                                                                                                                                                                                      $c_{11}=-1.1+2.2+1.0=3$\\
                                                                                                                                                                                                                      \vdots\\
                                                                                                                                                                                                                      $c_{13}=1.3+2.1+1.1=6$ \\
                                                                                                                                                                                                                      \vdots\\
                                                                                                                                                                                                                      $c_{23}=-1.3+1.1+2.1=0$
                                                                                                                                                                                                                    \end{tabular}
  \end{tabular}
\end{prof}


\subsection{Propriétés}
\noindent Les propriétés suivantes découlent directement des propriétés vues précédemment concernant les applications linéaires.
\begin{pro}
  \begin{enumerate}
  \item[$i)$] Soient $A\in \mathcal{M}_{np}$, $B\in \mathcal{M}_{pq}$ et   $C\in \mathcal{M}_{qr}$, on a $(AB)\cdot C=A\cdot (BC)$.\\
    Autrement dit, le produit matriciel est \textbf{associatif}.
  \item[$ii)$] Soient $A\in \mathcal{M}_{np}$, $B\in \mathcal{M}_{pq}$ et   $C\in \mathcal{M}_{pq}$, on a $A\cdot (B+C)=AB+AC$.\\
    Autrement dit, le produit matriciel est \textbf{distributif} par
    rapport à l'addition.
  \item[$iii)$] Si $A\in \mathcal{M}_{np}$, alors
    $I_n\cdot A=A\cdot I_p=A$.
  \item[$iv)$] Si on multiplie une matrice $A$ quelconque par la
    matrice nulle, on obtient la matrice nulle.
  \item[$v)$] Soient $A\in \mathcal{M}_{np}$, $B\in \mathcal{M}_{pq}$
    et $\lambda$ et $\mu$ des réels, on
    $(\lambda \cdot A)(\mu\cdot B)=(\lambda\mu)\cdot (AB)$.
  \end{enumerate}
\end{pro}


\subsection{Puissance d'une matrice carrée}
\noindent Soient $A\in\mathcal{M}_n$ et $p\in \mathbb{N}$.\\
On définit $A^p$ par réccurrence, de la manière suivante : $\left\{ \begin{array}{l} A^0=I_n\\ \forall p\in\mathbb{N}, A^{p+1}=A\cdot A^p\end{array}\right.$\\
Autrement dit, $A^p=\underbrace{A\cdot A \ldots A}_{p \text{ fois }}$

\subsection{Le produit matriciel est-il commutatif ?}
\noindent Soient $A\in \mathcal{M}_{np}$, $B\in \mathcal{M}_{pq}$ alors $AB$ existe et on a $AB\in\mathcal{M}_{nq}$.\\
Si on veut pouvoir calculer $BA$, il faut que $q=n$ et on a alors $BA\in\mathcal{M}_{p}$.\\
De plus, si on veut comparer $AB$ et $BA$, il faut que $n=p=q$.\\
La question de savoir si le produit matriciel est commutatif ne se pose donc que dans le cas des matrices carrées.\\


\noindent Prenons, par exemple, $A=\begin{pmatrix} 1&2&1\\0&3&1\\-1&1&2\end{pmatrix}$ et $B=\begin{pmatrix}  -1&2&3\\2&0&1\\0&-1&1\end{pmatrix}$ 
deux matrice carrées d'ordre $3$.

 

 On a alors \\
\begin{tabular}{cc}
\begin{tabular}{cl}
  & $\left. \begin{pmatrix} -1&2&3\\2&0&1\\0&-1&1\end{pmatrix}\right\}=B$\\
  & \\
$\underbrace{\begin{pmatrix} 1&2&1\\0&3&1\\-1&1&2\end{pmatrix}}_{=A}$ & $\left.\begin{pmatrix}  3&1&6\\6&-1&4\\3&-4&0\end{pmatrix}\right\} =AB$ 
\end{tabular} &
\begin{tabular}{cl}
  & $\left.\begin{pmatrix} 1&2&1\\0&3&1\\-1&1&2\end{pmatrix} \right\}=A$\\
  & \\
$\underbrace{\begin{pmatrix}  3&1&6\\6&-1&4\\3&-4&0\end{pmatrix}}_{=B}$ & $\left.\begin{pmatrix} -3&15&16\\2&13&13\\3&-6&-1 \end{pmatrix}\right\} =BA$ 
\end{tabular} 
\end{tabular}\\

\noindent Il est donc clair que, généralement, on a $AB\neq BA$. \\
Autrement dit, le produit matriciel n'est \textbf{pas commutatif}.

\begin{remarque}
 On pouvait s'y attendre dans la mesure où, généralement, si $u$ et $v$ sont deux endomorphismes de $E$ alors $u\circ v\neq v\circ u$.
\end{remarque}

\begin{Def}
 Soient $A$ et $B$ deux matrices de $\mathcal{M}_n$.\\
Si $AB=BA$, on dit que les matrices \textbf{commutent}.
\end{Def}

\begin{exem}
 Soit $\lambda\in\mathbb{R}$, la matrice $\lambda\cdot I_n$ commute avec toute matrice de $\mathcal{M}_n$.
\end{exem}

\subsection{Écriture matricielle d'une application linéaire}
\noindent On peut remarquer que, dans les équations de $u$ vues précédemment, les coefficients sont ceux de la matrice $A=M(u,\mathcal{B},\mathcal{B}')$.\\
Soit $x$ un vecteur de $E$ de coordonnées $X=\begin{pmatrix} x_1\\ \vdots \\x_p \end{pmatrix}$ dans la base $\mathcal{B}$ .\\

\noindent Soit $y$ un vecteur de $F$ de coordonnées $Y=\begin{pmatrix} y_1\\ \vdots \\y_n \end{pmatrix}$ dans la base $\mathcal{B}'$.\\
On a alors :
$$y=u(x) \iff  \left\lbrace \begin{array}{cc}
y_1&=a_{11}x_1+a_{12}x_2+\ldots+a_{1p}x_p\\
y_2&=a_{21}x_1+a_{22}x_2+\ldots+a_{2p}x_p\\
\vdots & \\
y_n&=a_{n1}x_1+a_{n2}x_2+\ldots+a_{np}x_p\\
        
       \end{array}\right. \iff Y=M(u,\mathcal{B},\mathcal{B}') X\iff Y=AX$$

\begin{exem}
Reprenons l'exemple vu précédemment.\\
Soit $u$ l'application linéaire de $\mathbb{R}^2$ dans $\mathbb{R}^3$ dont la matrice relativement aux bases canoniques est :

$$A=M(u,\mathcal{B},\mathcal{B}')=\left( \begin{array}{cc} 2&1\\-3&2\\-1&8 \end{array}\right) $$

\noindent Soit $x=(-1,1)$, on veut calculer $u(x)$. Ses coordonnées dans la base canonique sont :
$$A\begin{pmatrix} -1\\1\end{pmatrix}=\left( \begin{array}{cc} 2&1\\-3&2\\-1&8 \end{array}\right)\begin{pmatrix} -1\\1\end{pmatrix}=\begin{pmatrix} -1\\5\\9\end{pmatrix}$$
Donc $u(x)=(-1,5,9)$\end{exem}


\section{Rang d'une matrice}
\subsection{Définition}
\begin{Def}
Soit $A\in\mathcal{M}_{np}$, $A=(C_1,\ldots,C_p)$,\\
on appelle \textbf{rang} de la matrice $A$, on note $\mathrm{rg}(A)$ le rang de la famille de vecteurs $C_1,\ldots,C_p$
\end{Def}

\begin{remarque}
Le $\mathrm{rg}(A)$ est donc égal au  nombre de colonnes de $A$ linéairement indépendantes ou encore à la dimension du sous-espace de $\mathbb{R}^n$ engendré par les vecteurs $ C_1,\ldots,C_p$.\\
On en déduit que $\mathrm{rg}(A)\leq \min(n,p)$.
\end{remarque}

\noindent Soit $A\in\mathcal{M}_{np}$ et $u$ l'application linéaire de $\mathbb{R}^p$ dans $\mathbb{R}^n$ dont la matrice relativement aux bases canoniques est $A$.\\
Par définition, $C_j$ représente les coordonnées du vecteur $u(e_j)$ dans la base canonique de $\mathbb{R}^n$.\\
on a donc 
$$ \mathrm{rg}(A)=\mathrm{rg}(C_1,\ldots,C_p)=\mathrm{rg}(u(e_1),\ldots,u(e_p))=\mathrm{rg}(u)=\dim(\mathrm{Im} u)$$

\subsection{Calcul du rang}
\noindent Calculer le rang d'une matrice revient donc à calculer le rang d'une famille de vecteurs. Les propriétés vues dans les chapitres précédents restent donc valables. En particulier :
\begin{pro}
\
\begin{enumerate}
\item[$i$)] On ne change pas le rang d'une matrice en permutant ses colonnes.
 \item[$ii)$] On ne change pas le rang d'une matrice en ajoutant à une colonne une combinaison linéaire des autres.
 \item[$iii)$] Soit $p\geq 2$.\\
  Si $C_1$ est combinaison linéaire de $C_2,\ldots,C_p$ alors  $\mathrm{rg}(A)=\mathrm{rg}(C_1,\ldots,C_p)=\mathrm{rg}(C_2,\ldots,C_p)$.\\
  Si $C_1$ n'est pas  combinaison linéaire de $C_2,\ldots,C_p$ alors  $\mathrm{rg}(A)=\mathrm{rg}(C_1,\ldots,C_p)=1+\mathrm{rg}(C_2,\ldots,C_p)$.
\end{enumerate}
\end{pro}



\begin{exem}Soit $A=\begin{pmatrix}
    1&2&1\\-1&3&1\\2&-1&0\end{pmatrix}$.
  Déterminer le rang de la   matrice $A$.\end{exem}
\begin{prof}

\noindent On a $\mathrm{rg}(A)\leq \min(3,3) \iff \mathrm{rg}(A)\leq 3$\\

$\begin{array}{lll}
   \mathrm{rg}(A)&=\mathrm{rg}\begin{pmatrix} 1&2&1\\-1&3&1\\2&-1&0\end{pmatrix}& \\
                 &&\\
                 &= \mathrm{rg}\begin{pmatrix} 5&2&1\\5&3&1\\0&-1&0\end{pmatrix}& C_1\gets C_1+2C_2  \\
                 &&\\
                 &=1+ \mathrm{rg}\begin{pmatrix} 5&1\\5&1\end{pmatrix}&   \text{ car } C_2  \text{ n'est pas CL de  } C_1 \text{ et } C_3\\
                 &&\\
                 &=1+1=2 & \text{ car } C_1=5C_3 \text{ et } C_2\neq O
 \end{array}$
\end{prof}
 
\begin{exem}Soit $B=\begin{pmatrix}1&0&1&-1\\a&1&2&0\\b&1&-1&3\end{pmatrix}$ avec $a$ et $b$ réels
 
Déterminer le rang de la matrice $B$.
  
\begin{prof}
  On a $\mathrm{rg}(B)\leq \min(3,4) \iff \mathrm{rg}(B)\leq 3$\\

  $\begin{array}{lll}
     \mathrm{rg}(B)&=\mathrm{rg} \begin{pmatrix}1&0&1&-1\\a&1&2&0\\b&1&-1&3\end{pmatrix}& \\
                   &&\\
                   &=\mathrm{rg} \begin{pmatrix}1&0&0&0\\a&1&2-a&a\\b&1&-1-b&3+b\end{pmatrix}& C_3\gets C_3-2C_1 \quad   C_4\gets C_4+C_1 \\
                   &&\\
                   &=1+\mathrm{rg} \begin{pmatrix}1&2-a&a\\1&-1-b&3+b\end{pmatrix}& \text{ car } C_1  \text{ n'est pas CL de  } C_2 \ldots C_4\\
                   &&\\
                   &=1+\begin{pmatrix}1&0&0\\1&a-b-3&3+b-a\end{pmatrix}& C_2\gets C_2-(2-a)C_1 \quad   C_3\gets C_3-aC_1\\
                   &&\\
                   &=1+1+\mathrm{rg} \begin{pmatrix}0&0\\a-b-3&3+b-a\end{pmatrix}&
                                                                                   \text{
                                                                                   car
                                                                                   }
                                                                                   C_1
                                                                                   \text{
                                                                                   n'est
                                                                                   pas
                                                                                   CL
                                                                                   de
                                                                                   }
                                                                                   C_2
                                                                                   \text{
                                                                                   et
                                                                                   }
                                                                                   C_3
   \end{array}$\\

   \noindent Si $a-b-3=0$, $C_2=C_3=O$ donc $\mathrm{rg}(C_2,C_3)=0$ et $\mathrm{rg}(B)=2$.\\
   Si $a-b-3\neq0$, $C_3=-C_2$ et $C_2\neq O$ donc
   $\mathrm{rg}(C_2,C_3)=1$ et $\mathrm{rg}(B)=3$.
 \end{prof}

\end{exem}
\subsection{Transposition de matrices}
\begin{Def}
Soit $A\in\mathcal{M}_{np}$, $A=(a_{ij})$.\\
On appelle \textbf{matrice transposée} de $A$, on note ${}^t A$ la matrice de $\mathcal{M}_{pn}$, de terme général $a'_{ij}=a_{ji}$.\\
(Cela signifie que la $j$ème colonne de $A$ devient la $j$ème ligne de ${}^t A$)
\end{Def}

\begin{exem}
 Soit $A=\begin{pmatrix}1&2&-4\\0&-1&3 \end{pmatrix}$. Alors ${}^t A=\begin{pmatrix}1&0\\2&-1\\-4&3 \end{pmatrix}$
\end{exem}

\begin{thm}[admis]
Soit $A\in\mathcal{M}_{np}, \mathrm{rg}(A)=\mathrm{rg}({}^t A)$
 \end{thm}

\begin{remarque}
Ce théorème est trés utile dans la pratique. En effet, cela signifie que pour calculer le rang d'une matrice, on peut travailler indifféremment sur les colonnes ou les lignes. 
\end{remarque}

\begin{exem}
Soit $A=\begin{pmatrix} 1&2&3&-1\\0&1&2&0\\ 0&0&0&0\end{pmatrix}$.
\end{exem}

\noindent En regardant les lignes de la matrice $A$, on voit tout de suite que $\mathrm{rg}(A)=2$.
\pagebreak

\nocite{Ayres1990,Bodin2016,Dodge2007,Hefferon2017,Meyer2001,Serre2001,Ferrier2007,Simon1998}

\printbibliography 
\end{document}
