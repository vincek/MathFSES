%%% Ce document ainsi que tous les documents qui y sont liés sont soumis à la licence CC NC BY SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
\documentclass[Cours,11pt]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}


\titre{Applications linéaires}
\auteur{M. Pelini, V. Ledda}
\reference{Chapitre 3}
\composante{Faculté des Sciences Économiques et Sociales - Université
  de Lille\\
Licence 3 SIAD \\
Algèbre linéaire}


\motsclefs{application linéaire, noyau, rang}



\usepackage[tikz]{bclogo}

\etudiant=1
\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Définitions et premières propriétés}

\subsection{Définition}
\begin{Def}
Soient $E$ et $F$ deux espaces vectoriels sur $\mathbb{R}$.\\
Une application $u$ de $E$ vers $F$ est \textnormal{linéaire} si 
\begin{enumerate}
\item[i)] $\forall (x,y)\in E^2,~ u(x+y)=u(x)+u(y)$.
\item[ii)] $\forall (\lambda,x)\in \mathbb{R}\times E,~ u(\lambda\cdot x)=\lambda\cdot u(x)$.
\end{enumerate}
L'ensemble des applications linéaires de $E$ vers $F$ est noté $\mathcal{L}(E,F)$.
\end{Def}

\begin{Def}
\item[]Une application linéaire de $E$ dans lui même est appelé un \textnormal{endomorphisme}.
\item[]L'ensemble des endomorphismes de $E$ est noté $\mathcal{L}(E)$. 
\end{Def}

\begin{exem}
  
\begin{enumerate}
\item[1.] L'application $Id_E ~:~ \left\{\begin{array}{lcl}
E & \longrightarrow & E\\
x & \longmapsto& x
\end{array} \right.$ est un endomorphisme de $E$.
\item[2.] L'application de  $\mathbb{R}[X]$ dans lui même qui \`a chaque polyn\^ome $P$ associe son polyn\^ome dérivé $P'$ est un endomorphisme de $\mathbb{R}[X]$.
\item[3.] Soit $u:\mathbb{R}^3\longrightarrow \mathbb{R}^2$ définie par $u(x_1,x_2,x_3)=(x_1-x_2,2x_3)$.\\
  Montrons que $u\in \mathcal{L}(\mathbb{R}^3,\mathbb{R}^2)$ :\\
  
\begin{prof}
  Soient $x =\left( x_1,x_2,x_3\right)$ et $y =\left( y_1,y_2,y_3\right)$ deux vecteurs de $\mathbb{R}^3$ et $\lambda$ un réel,\\
  \begin{align*}
    u(x+y) & =((x_1+y_1)-(x_2+y_2),2(x_3+y_3))\\
           & =((x_1-x_2)+(y_1-y_2),2x_3+2y_3)\\
           & =(x_1-x_2,2x_3)+(y_1-y_2,2y_3)\\
           & =u(x)+u(y)
  \end{align*}
$$u(\lambda x)=(\lambda x_1-\lambda x_2,2\lambda x_3)=(\lambda (x_1-x_2),\lambda (2x_3))=\lambda u(x)$$
donc $u$ est bien linéaire.
\end{prof}

\end{enumerate}
\end{exem}


\begin{remarque}
Si $u\in\mathcal{L}(E,F)$ alors $u(0_E)=0_F$.
\end{remarque}

\begin{preuve}
Soit $x\in E$,\\
on a d'une part, $~u(x+0_E)=u(x)$ et d'autre part, $u(x+0_E)=u(x)+u(0_E)$ car $u$ est linéaire.\\
D'où $u(x)=u(x)+u(0_E)$ et donc $u(0_E)=0_F$.
\end{preuve}

\begin{exem}
Soit $u:\mathbb{R}^2\longrightarrow \mathbb{R}^2$ définie par $u(x;y)=(4x+1;5y)$.\\
$u(0;0)=(1;0)$ donc $u$ n'est pas linéaire.
\end{exem}



\begin{pro}[Caractérisation des applications linéaires]

  
Soit $u$ une application de $E$ dans $F$, $u$ est linéaire si et seulement si :
$$\forall\lambda\in \mathbb{R},~\forall(x,y)\in E^2,~~ u(\lambda x+y)=\lambda u(x)+u(y)$$
\end{pro}

\begin{preuve}
\begin{enumerate}
\item[$\Rightarrow$] Soit $u\in \mathcal{L} (E,F)$,$\lambda\in \mathbb{R}$ et $(x,y)\in E^2$
\begin{align*}
u(\lambda x+ y) & =u(\lambda x)+u( y) \text{ d'après } i)\\
               & =\lambda u(x)+u(y) \text{ d'après } ii)
               \end{align*}
\item[$\Leftarrow$] Supposons que $\forall\lambda\in \mathbb{R},~\forall(x,y)\in E^2,~~ u(\lambda x+ y)=\lambda u(x)+ u(y)\; (*)$,\\
Remarquons tout d'abord que $u(0_E)=0_F$.\\
En effet, on a $u(0_E+0_E)=u(0_E)+u(0_E)$ soit $u(0_E)=2u(0_E)$ donc $u(0_E)=0_F$.\\
en prenant $\lambda=1$ dans $(*)$, on retrouve $i)$,\\
en prenant $y=0_E$ dans $(*)$, on retrouve $ii)$.\\
 En effet $ u(\lambda x+0_E)=\lambda u(x)+u(0_E)$, or $u(0_E)=0_F$ d'où $u(\lambda x)=\lambda u(x)$\\
Donc $u$ est bien linéaire de $E$ dans $F$
\end{enumerate}
\end{preuve}

\begin{pro}
Soit $u\in \mathcal{L} (E,F)$ et $n\in\mathbb{N}^*$\\
$$\forall(\lambda_1,\ldots,\lambda_n)\in \mathbb{R}^n,~\forall(x_1,\ldots,x_n)\in E^n,~~ u\left(\sum_{i=1}^n \lambda_i x_i\right)= \sum_{i=1}^n \lambda_i u(x_i)$$
\end{pro}

\begin{preuve} Cette égalité s'obtient par récurrence sur $n$ sans difficultés.
% \begin{enumerate}
%  \item[-] Initialisation :\\
% Si $n=1$, on a bien $ \forall (\lambda,x)\in \mathbb{R}\times E,~ u(\lambda x)=\lambda u(x)$ car $u$ est linéaire.
% \item[-] Hérédité :\\
% Soit $n\in\mathbb{N}^*$, on suppose que $\forall(\lambda_1,\ldots,\lambda_n)\in \mathbb{R}^n,~\forall(x_1,\ldots,x_n)\in E^n,~~ u\left(\underset{i=1}{\overset{n}{\sum }} \lambda_i x_i\right)=\underset{i=1}{\overset{n}{\sum }}\lambda_i u(x_i)$,\\
% soient $(\lambda_1,\ldots,\lambda_{n+1})\in \mathbb{R}^{n+1}$ et $(x_1,\ldots,x_{n+1})\in E^{n+1}$, on a alors\\
% \begin{align*}
% u\left(\underset{i=1}{\overset{n+1}{\sum }} \lambda_i x_i\right) & =u\left(\underset{i=1}{\overset{n}{\sum }} \lambda_i u(x_i)+\lambda_{n+1}x_{n+1}\right) &\\
%                                          & =u\left( \underset{i=1}{\overset{n}{\sum }} \lambda_i u(x_i)\right)+u(\lambda_{n+1}x_{n+1}) \quad \text{ car \textit{u} est linéaire}\\
%                                          & =\underset{i=1}{\overset{n}{\sum }} \lambda_i u(x_i)+\lambda_{n+1}u(x_{n+1}) \quad\text{ d'après l'hypothèse de récurrence  }\\
%                                          & =\underset{i=1}{\overset{n+1}{\sum }} \lambda_i u(x_i)\\
% \end{align*}
% Donc la propriété est bien héréditaire.
% \item[-] Conclusion :$$\forall(\lambda_1,\ldots,\lambda_n)\in \mathbb{R}^n,~\forall(x_1,\ldots,x_n)\in E^n,~~ u\left(\sum_{i=1}^n \lambda_i x_i\right)= \sum_{i=1}^n \lambda_i u(x_i)$$

% \end{enumerate}

\end{preuve}


\subsection{Image d'une application linéaire, applications linéaires surjectives}


\begin{Def} Soit $u\in \mathcal{L} (E,F)$, on appelle \textnormal{image} de $u$, on note $\mathrm{Im}(u)$,
 l'ensemble des images de tous les vecteurs $x$ de $E$.

\begin{align*}
\mathrm{Im}(u) & =\left\{u(x)~|~ x\in E\right\}\\
    \mathrm{Im}(u)  & =\left\{y\in F ~|~\exists x\in E \text{ tel que } y=u(x)\right\}      
\end{align*}
\end{Def}

\begin{pro} Si $u\in \mathcal{L} (E,F)$, $\mathrm{Im}(u)$ est un sous espace vectoriel  de $F$.
\end{pro}

\begin{preuve}
Par définition, $\mathrm{Im}(u)\subset F$.\\
De plus, nous avons vu que $u(0_E)=0_F$ donc $0_F \in \mathrm{Im}(u)$, en particulier $\mathrm{Im}(u)$ est non vide.\\
Vérifions \`a présent que $\mathrm{Im}(u)$ est stable par combinaisons linéaires :\\
Soient donc $y$ et $y'$ dans $\mathrm{Im}(u)$ et $\lambda$ et $\mu$ deux réels.\\
Par définition, $~\exists x\in E \text{ tel que } y=u(x)$ et $~\exists x'\in E \text{ tel que } y'=u(x')$, d'où\\
$\lambda y+\mu y' =\lambda u(x)+\mu u(x')=u(\lambda x)+u(\mu x')=u(\lambda x+\mu x')$ par linéarité de $u$. Donc $\lambda y+\mu y'\in \mathrm{Im}(u)$.\\
$\mathrm{Im}(u)$ est bien un sous espace vectoriel  de $F$.
\end{preuve}


\begin{bclogo}[logo = \bclampe,arrondi=0.1]{Rappel}

Une application $u$ de $E$ dans $F$ est surjective si tout élément de
$F$ admet au moins un antécédent par $u$.

Autrement dit
\[u \text{ est surjective} \iff \forall y\in F, ~~ \exists x\in E
\text{ tel que } y=u(x)\]
\end{bclogo}


\begin{pro}

  Soit $u\in \mathcal{L} (E,F)$, $u$ est surjective si et seulement si $\mathrm{Im}(u)=F$.
\end{pro}

\begin{preuve}
\begin{align*}
u \text{ est surjective } & \Leftrightarrow \text{ tout élément de $F$ admet un antécédent par } u\\
                          & \Leftrightarrow \forall y\in F ,~\exists x\in E \text{ tel que } y=u(x)\\
                          & \Leftrightarrow \mathrm{Im}(u)=F
\end{align*}
\end{preuve}

\begin{exem}

Soit $u:\mathbb{R}^3\mapsto \mathbb{R}^2$ définie par $u(x_1,x_2,x_3)=(x_1-x_2,2x_3)$.\\
Nous avons montré que $u\in \mathcal{L}(\mathbb{R}^3,\mathbb{R}^2)$ :\\
Vérifions que $ \mathrm{Im}(u)=\mathbb{R}^2$ : Soit $(x,y)\in\mathbb{R}^2$, on alors $(x,y)=u(x,0,\frac{1}{2}y)$ et donc $(x,y)\in\mathrm{Im}(u)$.\\
Donc $u$ est surjective.

\end{exem}

\begin{exem}
Soit $v\in\mathcal{L}(\mathbb{R}^2)$ définie par : $v(x,y) =( 2x-4y,-x+2y)$.\\
On admet que $v$ est bien linéaire.\\
$\forall (x,y)\in\mathbb{R}^2$, on a $v(x,y) =( 2x-4y , -x+2y)=(-x+2y) (-2,1)$.\\
On en déduit que $\mathrm{Im}(v)\subset\Vect\left\{ (-2,1)\right\}$ (on a même $\mathrm{Im}(v)=\Vect\left\{ (-2,1)\right\}$).\\
En particulier $\mathrm{Im}(v)\neq \mathbb{R}^2$ donc $v$ n'est pas surjective.
\end{exem}

\subsection{Noyau d'une application linéaire, applications linéaires injectives}


\begin{Def} Soit $u\in \mathcal{L} (E,F)$, on appelle \textnormal{noyau} de $u$, on note $\ker(u)$, l'ensemble des éléments de $E$ dont l'image est le vecteur nul. 

\[\ker(u)=\left\{x\in E |~u(x)=0_F\right\}\]
      
\end{Def}

\begin{remarque} $X\in\ker(u) \Leftrightarrow u(X)=0_F$ \end{remarque}
\begin{pro}

  Si $u\in \mathcal{L} (E,F)$, $\ker(u)$ est un sous espace vectoriel  de $E$.
\end{pro}

\begin{preuve}
Par définition, $\ker(u)\subset E$.\\
De plus, nous avons vu que $u(0_E)=0_F$ donc $0_E \in \ker(u)$, en particulier $\ker(u)$ est non vide.\\
Vérifions \`a présent que $\ker(u)$ est stable par combinaisons linéaires :\\
Soient donc $x$ et $x'$ dans $\ker(u)$ et $\lambda$ et $\mu$ deux réels.\\
$u(\lambda x+\mu x') =\lambda u(x)+\mu u(x')=\lambda 0_F+\mu 0_F= 0_F$, par linéarité de $u$. Donc $\lambda x+\mu x'\in \ker(u)$.\\
$\ker(u)$ est bien un sous espace vectoriel  de $E$.
\end{preuve}

\begin{bclogo}[logo = \bclampe,arrondi=0.1]{Rappel}
  Une application $u$ de $E$ dans $F$ est injective si deux  éléments distincts de $E$ ont deux images distinctes par $u$.\\
  Autrement dit
  $$u \text{ est injective} \iff (~\forall (x,x')\in E^2, x\neq x'
  \Rightarrow u(x)\neq u(x'))$$ Ou encore
  $$u \text{ est injective} \iff (~\forall (x,x')\in E^2, u(x)=u(x')
  \Rightarrow x=x')$$
\end{bclogo}

\begin{pro} Soit $u\in \mathcal{L} (E,F)$, $u$ est injective si et seulement si $\ker(u)=\left\{ 0_E\right\}$.
\end{pro}

\begin{preuve}
\begin{enumerate}
\item[$\Rightarrow$] Soit $u\in \mathcal{L} (E,F)$, supposons $u$ injective, c'est \`a dire : $\forall (x,x')\in E^2,~u(x)=u(x')\Rightarrow x=x'$.\\
Soit $x\in \ker(u)$, on alors $u(x)=0_F=u(0_E)$, donc, par injectivité de $u$, $x=0_E$.\\ D'où $\ker(u)=\left\{ 0_E\right\}$
\item[$\Leftarrow$] Soit $u\in \mathcal{L} (E,F)$, supposons que $\ker(u)=\left\{ 0_E\right\}$ et montrons que $u$ est injective.\\
Soient donc $x$ et $x'$ dans $E$ tels que $u(x)=u(x')$, on alors $u(x-x')=0_F$ donc $x-x'\in \ker(u)$.\\
Or $\ker(u)=\left\{ 0_E\right\}$ donc $x-x'=0_E \Leftrightarrow x=x'$ et donc $u$ est injective.
\end{enumerate}
\end{preuve}

\noindent Reprenons les exemples précédents :
\begin{exem}

Soit $u:\mathbb{R}^3\mapsto \mathbb{R}^2$ définie par $u(x_1,x_2,x_3)=(x_1-x_2,2x_3)$.\\
Nous avons montré que $u$ est surjective. Qu'en est-il de l'injectivité :\\
Soit $(x_1,x_2,x_3)\in\ker(u)$, on alors $u(x_1,x_2,x_3)=(0,0,0) \Leftrightarrow x_1=x_2$ et $x_3=0$.\\
Donc $\ker(u)=\Vect\{ (1,1,0)\}$ et $u$ n'est pas injective.

\end{exem}
\begin{exem}

  

Soit $v\in\mathcal{L}(\mathbb{R}^2)$ définie par : $v(x,y) =( 2x-4y,-x+2y)$.\\
Nous avons montré que $v$ n'est pas surjective. Qu'en est-il de l'injectivité :\\
Soit $(x,y)\in\ker(v)$, on a alors $v(x,y)=(0,0) \Leftrightarrow x=2y$ \\
Donc $\ker(v)=\Vect\{ (2, 1) \}$ et donc $v$ n'est pas injective.
\end{exem}
\subsection{Composée d'applications linéaires}
\noindent Rappel : Soient $E$, $F$ et $G$ des espaces vectoriels, $u\in\mathcal{L}(F,G)$ et $v\in\mathcal{L}(E,F)$.\\
L'application $u\circ v$ est définie de la manière suivante $u\circ v \left\{
\begin{array}{lclcl}
 E & \longrightarrow & F    & \longrightarrow & G\\
 x &    \longmapsto             & v(x) &        \longmapsto            & u\circ v(x)=u(v(x))
 \end{array}
 \right.$
\begin{pro}
Si $u\in\mathcal{L}(F,G)$ et $v\in\mathcal{L}(E,F)$ alors $u\circ v\in \mathcal{L}(E,G)$
\end{pro}
\begin{preuve}
\begin{enumerate}
\item[] $\forall (x,y)\in E^2,~ u\circ v(x+y)=u(v(x+y))=u(v(x)+v(y))=u(v(x))+u(v(y))=u\circ v(x)+u\circ v(y)$.\\
(car $u$ et $v$ sont linéaires).
\item[] $\forall (\lambda,x)\in \mathbb{R}\times E,~ u\circ v(\lambda x)=u(v(\lambda x))=u(\lambda v(x))=\lambda u(v(x))=\lambda u\circ v (x)$.\\
(car $u$ et $v$ sont linéaires).
\end{enumerate}
\end{preuve}

\begin{remarque}
\item[]Si $E=F=G$ alors <<$\circ$>> induit une loi de composition interne sur $\mathcal{L}(E)$.
\item[]On notera parfois $uv$ au lieu de  $u\circ v$.
\end{remarque}

\begin{pro}
\begin{enumerate}\item[]
\item[$1.$] Si $u\in\mathcal{L}(G,H)$, $v\in\mathcal{L}(F,G)$ et $w\in\mathcal{L}(E,F)$ alors $u\circ (v\circ w)=(u\circ v)\circ w$.\\
On dira que la composition d'applications linéaires est associative.
\item[$2.$]
 \begin{enumerate}
%\item[]
\item[$i)$] $\forall (u,v)\in (\mathcal{L}(F,G))^2$, $\forall w\in\mathcal{L}(E,F)$, $(u+v)\circ w=u\circ w + v\circ w$.
\item[$ii)$]$\forall u\in \mathcal{L}(F,G)$, $\forall (v,w)\in (\mathcal{L}(E,F))^2$, $u\circ (v+w)=u\circ v+u\circ w$.
%\noindent On dira que la composition d'applications linéaires est distributive par rapport \`a l'addition.
\end{enumerate}
On dira que la composition d'applications linéaires est distributive par rapport \`a l'addition.
\item[$3.$] $\forall (u,v)\in (\mathcal{L}(E))^2$, $\forall \lambda\in \mathbb{R}$, $u\circ (\lambda v)=(\lambda u)\circ v=\lambda (u\circ v)$.
\end{enumerate}

\end{pro}

\begin{preuve}
\begin{enumerate}
\item $\forall x\in E,~~u\circ (v\circ w)(x)=u((v\circ w (x)))=u(v(w(x)))=u\circ v (w(x))=(u\circ v)\circ w(x)$

\item Le vérifier
\item Le vérifier

\end{enumerate}
\end{preuve}


\subsection{Isomorphismes d'espaces vectoriels}
\begin{Def}
\item[] Une application $u$ de $E$ dans $F$ est \textnormal{bijective} si elle est injective et surjective.
\item[] Une application linéaire $u$ de $E$ dans $F$, bijective est appelé \textnormal{isomorphisme} de $E$ dans $F$.
\item[] Un isomorphisme de $E$ dans lui même est appelé un \textnormal{automorphisme} de $E$.
\end{Def}

\begin{remarque}
Cela peut se traduire de la manière suivante :\\
Une application $u$ de $E$ dans $F$ est bijective  $\iff \forall y\in F,\quad \exists! x\in E\quad  \text{ tel que } y=u(x)$.\\
L'existence de $x$ est liée \`a la surjectivité de $u$, l'unicité  \`a son injectivité.
\end{remarque}
\begin{exem}
Soit $u\in\mathcal{L}(\mathbb{R}^2)$ définie par : $u(x,y) =(y,x)$.\\
$u$ est un isomorphisme de $\mathbb{R}^2$.
\end{exem}
\begin{Def}
Les espaces vectoriels $E$ et $F$ sont dits \textnormal{isomorphes} s'il existe un isomorphisme $u$ de $E$ dans $F$.
\end{Def}

\begin{exem}
Tout plan vectoriel de $\R^3$ est isomorphe \`a $\R^2$.\end{exem}

\begin{exem}
 $\mathbb{R}^3$ et $\mathbb{R}_2[X]$ sont isomorphes. \\
En effet, l'application $u : \left\{\begin{array}{lll} ~\mathbb{R}^3 &\longrightarrow & \mathbb{R}_2[X]\\
                         (x,y,z) & \longmapsto & xX^2+yX+z
                         \end{array}
                         \right.$ est un isomorphisme de $\mathbb{R}^3$ dans $\mathbb{R}_2[X]$.

\end{exem}

\begin{pro}
Soit $u$ un isomorphisme de $E$ dans $F$.\\
L'application $v : \left\{\begin{array}{lll} F & \longrightarrow & E\\
                         y & \longmapsto & \text{l'unique } x\in E \text{ tel que } u(x)=y
                         \end{array}
                         \right.$ est linéaire de $F$ dans $E$.\\
                         
                       
                         
\noindent Elle vérifie $u\circ v=Id_F$ et $v\circ u=Id_E$.\\
                         On la note $v=u^{-1}$.
\end{pro}


\newpage
\section{Cas des espaces vectoriels de dimension finie}
\noindent Dans tout ce paragraphe, $E$ désigne un espace vectoriel de dimension $n$ ($n\in\mathbb{N}^*$) et $F$ désigne un 
espace vectoriel de dimension $p$ ($p\in\mathbb{N}^*$).

\subsection{Image d'une base de $E$}
\noindent On a supposé que $E$ est dimension finie et que $E\neq \lbrace 0 \rbrace$, on en déduit que $E$ admet des bases.\\
Soit  donc $(e_1,\ldots,e_n)$ une base de $E$ .\\

\noindent Soit $u\in\mathcal{L}(E,F)$,
par définition, \[\forall x\in E,~\exists ! (x_1,\ldots,x_n)\in\mathbb{R}^n \text{ tel que }~ x=x_1e_1+ \ldots+x_ne_n.\]
Alors, par linéarité de $u$, on obtient : $ u(x)=x_1u(e_1)+ \ldots+x_nu(e_n)$.\\
On en déduit que :


$\begin{array}{rl}\mathrm{Im}(u)&=\left\{u(x),~ x\in E\right\}\\
                                 &=\left\{x_1u(e_1)+ \ldots+x_nu(e_n), (x_1,\ldots,x_n)\in\mathbb{R}^n\right\}\\
&=\Vect\{u(e_1),\ldots,u(e_n)\}\end{array}$ 

\noindent $\im(u)$ est engendré par la famille $(u(e_1),\ldots,u(e_n))$.

\begin{remarque}
\item[-] Cela signifie que $u$ est entièrement définie par l'image d'une base de $E$.
\item[-] Pour démontrer l'égalité de deux applications linéaires, il suffit de prouver qu'elles sont égales sur une base de $E$.
\end{remarque}

\begin{exem}

Soit $f\in\mathcal{L}( \R^2)$ définie par : $f(1,0)=v_1$, $f(0,1)=v_2$
où $v_1=(1,1)$ et $v_2=(1,-1)$.

\end{exem}
\begin{enumerate}
\item Déterminer $f(X)$, pour tout $X\in \R^2$.
\item Calculer $f(v_1)$ et $f(v_2)$.
\item En déduire $f\circ f$.
\end{enumerate}

\begin{pro}
Soit $u\in\mathcal{L}(E,F)$.
\begin{itemize}
\item[$i)$] $u$ est injective si et seulement si  l'image d'une famille libre de $E$ est une famille libre de $F$.
\item[$ii)$] $u$ est surjective si et seulement si  l'image d'une famille génératrice de $E$ est une famille génératrice de $F$.
\item[$iii)$] $u$ est bijective si et seulement si  l'image d'une base de $E$ est une base de $F$.
\end{itemize}
\end{pro}

\begin{preuve}
\begin{itemize}
\item[i)] Démontrons que $u$ est injective si et seulement si  l'image d'une famille libre de $E$ est une famille libre de $F$.\\
$\Rightarrow$ Supposons $u$  injective.\\
Soit $(e_1,\ldots,e_k)$ une famille libre de $E$ (donc $k\leq n$).\\
Montrons que  $(u(e_1),\ldots,u(e_k))$ est une famille libre de $F$ :\\
Soient $(\lambda_1,\ldots,\lambda_k)\in\mathbb{R}^k \text{ tel que } \lambda_1u(e_1)+\ldots+\lambda_ku(e_k)=0$.\\
\begin{tabular}{lcl}
 $\lambda_1u(e_1)+\ldots+\lambda_ku(e_k)=0$ & $\iff$ & $u(\lambda_1e_1+\ldots+\lambda_ke_k)=0$\\
                                    & $\iff$ & $\lambda_1e_1+\ldots+\lambda_ke_k\in\ker(u)$\\
                                    & $\iff$ & $\lambda_1e_1+\ldots+\lambda_ke_k=0$, car $\ker(u)=\{0\}$\\
                                    & $\iff$ & $\lambda_1=\ldots=\lambda_k=0$, car $(e_1,\ldots,e_k)$ est libre.
\end{tabular}\\
On en déduit donc que $(u(e_1),\ldots,u(e_k))$ est une famille libre de $F$.\\

\noindent $\Leftarrow$ Supposons que l'image d'une famille libre de $E$ est une famille libre de $F$.\\
Soit $x\in\ker(u)$, montrons que $x=0$.\\
Si $(e_1,\ldots,e_n)$ est une base de $E$, alors il existe $(x_1,\ldots,x_n)\in\mathbb{R}^n$ tel que $x=x_1e_1+\ldots+x_ne_n$.
$$x\in\ker(u) \Rightarrow u(x)=0 \Rightarrow x_1u(e_1)+\ldots +x_nu(e_n)=0$$ 
Or $(e_1,\ldots,e_n)$ une famille libre  de $E$ donc $(u(e_1),\ldots,u(e_n))$ une famille libre  de $F$.\\
 On en déduit que $x_1=\ldots=x_n=0$ et par suite $x=0$. \\
 D'où $\ker(u)=\{0\}$ et donc  $u$ est injective.
\item[ii)] Démontrons que $u$ est surjective si et seulement si  l'image d'une famille génératrice de $E$ est une famille génératrice de $F$.\\
$\Rightarrow$ Supposons $u$  surjective.\\
Soient $(e_1,\ldots,e_k)$ une famille génératrice de $E$ (donc $k\geq n$) et  $y\in F$.\\
Comme $u$ est surjective, $\exists x\in E \text{ tel que } y=u(x)$.\\
Or  $(e_1,\ldots,e_k)$ est une famille génératrice de $E$ donc il existe $(x_1,\ldots,x_k)\in\mathbb{R}^k$ tel que\\ $x=x_1e_1+\ldots+x_ke_k$ et $y=x_1u(e_1)+\ldots+x_ku(e_k)$.\\
Cela signifie que $(u(e_1),\ldots,u(e_k))$ est bien une famille génératrice de $F$.\\

\noindent $\Leftarrow$ Supposons maintenant que l'image d'une famille génératrice de $E$ est une famille génératrice de $F$. \\
Soit $y\in F$, on cherche $x\in E$ tel que $u(x)=y$ :\\
Considérons $(e_1,\ldots,e_k)$ (avec $k\geq n$) une famille génératrice de $E$, $(u(e_1),\ldots,u(e_k))$ est alors une famille génératrice de $F$ donc $y=y_1u(e_1)+\ldots+y_ku(e_k)=u(y_1e_1+\ldots+y_ke_k)$.\\
Prenons $x=y_1e_1+\ldots+y_ke_k$, alors $x\in E$ et $u(x)=y$ donc $y\in \mathrm{Im}(u)$.\\
$u$ est bien surjective.
\item[iii)] Démontrons que $u$ est bijective si et seulement si  l'image d'une base de $E$ est une base de $F$.\\
$\Rightarrow$ Supposons $u$  bijective.\\
Soit $(e_1,\ldots,e_n)$ une base de $E$, montrons que $(u(e_1),\ldots,u(e_n))$ est une base de $F$.\\
Comme $u$ est bijective, $u$ est en particulier surjective et donc  $(u(e_1),\ldots,u(e_n))$ est une famille génératrice de $F$.\\
De plus $u$ est aussi injective donc  $(u(e_1),\ldots,u(e_n))$ est une famille libre de $F$.\\
 $(u(e_1),\ldots,u(e_n))$ est donc bien une base de $F$.\\

\noindent $\Leftarrow$ Supposons maintenant que l'image d'une base de $E$ est une base de $F$. \\
On veut montrer que $u$ est injective et surjective. \\
Soit $(e_1,\ldots,e_p)$ une famille libre de $E$ (donc $p\leq n$), on la complète en une base de $E$ $(e_1,\ldots,e_p,e_{p+1},\ldots,e_n)$.
Alors $(u(e_1),\ldots,u(e_p),u(e_{p+1}),\ldots,u(e_n))$ est une base de $F$, en particulier c'est une famille libre.\\
On en déduit que $(u(e_1),\ldots,u(e_p))$ est aussi une famille libre (car c'est une sous-famille d'une famille libre).\\
D'après $i)$, on a donc  $u$ injective.\\
Pour montrer que $u$ est surjective, on fait de même en prenant une famille génératrice dont on extrait une base \ldots
\end{itemize}
\end{preuve}


\begin{remarque} Supposons que $\dim(E)=n$ et $\dim(F)=p$.
\item[-] Si $u\in\mathcal{L}(E,F)$ est injective  alors $\dim(E)\leq \dim(F)$.\\
En effet, l'image d'une base de $E$ par une application injective est une famille libre de $F$ donc $n\leq p$. 
\item[-] Si $u\in\mathcal{L}(E,F)$ est surjective alors  $\dim(E)\geq \dim(F)$.\\ 
En effet, l'image d'une base de $E$ par une application surjective est une famille génératrice de $F$ donc $n\geq p$. \end{remarque}

\begin{remarque}
\item[-] Si $E$ est de dimension finie et si $F$ est isomorphe \`a $E$ alors $F$ est aussi de dimension finie et on a  $\dim(F)=\dim(E)$.
\item[-] En fait, on peut démontrer que $E$ et $F$ sont isomorphes si et seulement si  ils ont même dimension.
\end{remarque}

\subsection{Rang d'une application linéaire}


\begin{bclogo}[logo = \bclampe,arrondi=0.1]{Rappel}
  Si $u\in\mathcal{L}(E,F)$ alors $\mathrm{Im}(u)$ est un sous-espace
  vectoriel de $F$ engendré par l'image d'une base de $E$. En
  particulier, $\mathrm{Im}(u)$ admet une famille génératrice finie
  est donc c'est un espace vectoriel de dimension finie.
\end{bclogo}
\begin{Def}
Soit $u\in\mathcal{L}(E,F)$, on appelle \textnormal{rang} de $u$, on note $rg(u)$ la dimension de $\mathrm{Im}(u)$.
\end{Def}

\begin{remarque}
Si $(e_1,\ldots,e_n)$ est une base de $E$, d'après ce qui précède, on sait que $(u(e_1),\ldots,u(e_n))$ engendre $\mathrm{Im}(u)$ donc le rang de $u$ sera égal au rang de la famille de vecteurs $(u(e_1),\ldots,u(e_n))$. \\
Le calcul pratique du rang d'une application linéaire se fait donc comme le calcul du rang d'une famille de vecteurs.
\end{remarque}



\begin{pro}\textnormal{\textbf{(Caractérisation des applications linéaires  surjectives)}}\\
On suppose que $F$ est de dimension finie, égale \`a $p\in\mathbb{N}^*$.\\

\noindent Soit $u\in\mathcal{L}(E,F)$ , alors $u$ est surjective si et seulement si $rg(u)=p$.

\end{pro}

\begin{preuve}
 
\begin{tabular}{rcl}
$u$ surjective & $ \iff$ & $\mathrm{Im}(u)=F$ \\
 & $ \iff$ & $\dim(\mathrm{Im}(u))=\dim(F)$\\
 & $ \iff$ & $rg(u)=p$\\
\end{tabular}

\end{preuve}

\subsection{Théorème du rang}
\begin{thm}
Soit $u\in\mathcal{L}(E,F)$ où $E$ est dimension finie, égale \`a $n>0$. On a alors :
$$\dim(E)=\dim (Ker(u))+rg(u)$$ 
\end{thm}


\begin{preuve}
\begin{enumerate}
 \item[-]  Si $\ker(u)=\{0\}$.\\
Soit $(e_1,\ldots,e_n)$ une base de $E$. On sait que $\mathrm{Im}(u)=\Vect\{u(e_1),\ldots,u(e_n)\}$. \\
De plus la famille $(e_1,\ldots,e_n)$ est libre et $u$ est injective donc $(u(e_1),\ldots,u(e_n))$ est une famille libre de $F$.\\
On en déduit que c'est une base de $\mathrm{Im}(u)$. En particulier, $rg(u)=\dim(\mathrm{Im}(u))=n$.\\
Alors $rg(u)+\dim(\ker(u))=n+0=\dim(E)$.
\item[-] Si $\ker(u)\neq\{0\}$ alors $\ker(u)$ est un sous-espace
  vectoriel de $E$ donc de dimension finie $k$, non nulle.\\
Soit donc $(e_1,\ldots,e_k)$ une base de $\ker(u)$, c'est une famille libre  de $E$ que l'on complète en une base $(e_1,\ldots,e_n)$.\\
On sait alors que $\mathrm{Im}(u)$ est engendré par $(u(e_1),\ldots,u(e_n))$.\\
Or $u(e_1)=\ldots=u(e_k)=0$ donc $\mathrm{Im}(u)=\Vect\{u(e_{k+1}),\ldots,u(e_n)\}$.\\
 Vérifions que la famille $(u(e_{k+1}),\ldots,u(e_n))$ est libre :\\
soit $(\lambda_{k+1},\ldots,\lambda_n)\in\mathbb{R}^{n-k}$ tel que $\lambda_{k+1}u(e_{k+1})+\ldots+\lambda_nu(e_n)=0$,\\
 cela signifie que  $\lambda_{k+1}e_{k+1}+\ldots+\lambda_ne_n\in\ker(u)$ donc c'est une combinaison linéaire de $e_1,\ldots,e_k$, autrement dit $\exists (\lambda_1,\ldots,\lambda_k)\in\mathbb{R}^k \text{ tel que }~~ \lambda_{k+1}e_{k+1}+\ldots+\lambda_ne_n=\lambda_{1}e_{1}+\ldots+\lambda_ke_k$.\\
Or la famille $(e_1,\ldots,e_n)$ est libre donc $\lambda_1=\ldots=\lambda_n=0$ et donc, on a  en particulier \\$\lambda_{k+1}=\ldots=\lambda_n=0$.\\
On en déduit que la famille $(u(e_{k+1}),\ldots,u(e_n))$ est libre. \\
Comme elle engendre $\mathrm{Im}(u)$, c'est une base et $rg(u)=\dim(\mathrm{Im}(u))=n-k$. \\
On a donc bien $\dim (Ker(u))+rg(u)=k+(n-k)=n=\dim(E)$.
\end{enumerate}
\end{preuve}
\begin{exem}
Soit $u\in\mathcal{L}(\mathbb{R}^2,\mathbb{R}^3)$ définie par : $u(x,y) =( 2x+y,x-y,x-2y)$.\\
On voit que $(x,y)\in\ker(u)\iff x=y=0$ donc $\ker(u)=\{0\}$ et donc $rg(u)=\dim(E)=2$.


\end{exem}

\noindent \textbf{Cas des applications linéaires entres espaces de même dimension :}

\begin{thm}
Soit $u\in\mathcal{L}(E,F)$, on suppose que $\dim(E)=\dim(F)$. On a alors l'équivalence : 
\begin{center}
$u$ est injective $\iff$ $u$ est bijective $\iff$ $u$ est surjective
\end{center}
\end{thm}

\begin{preuve}
 Par définition, si $u$ est bijective alors $u$ est surjective et injective.\\
Il reste \`a montrer que :
\begin{enumerate}
 \item[-] Si $u$ est injective alors $u$ est bijective.\\
Si $u$ est injective alors $\ker(u)=\{0 \}$ et donc d'après le théorème du rang, $\dim(E)=rg(u)$.\\
Or $\dim(E)=\dim(F)$ donc $rg(u)=\dim(F)$ et $u$ est aussi surjective.\\
Donc $u$ est bien bijective.
\item[-] Si $u$ est surjective alors $u$ est bijective.\\
Si $u$ est surjective alors $rg(u)=\dim(F)$, or $\dim(E)=\dim(F)$ donc $rg(u)=\dim(E)$ et donc d'après le théorème du rang, $\dim(\ker(u))=0$.\\
On en déduit que $\ker(u)=\{0 \}$ donc  $u$ est aussi injective.\\
Donc $u$ est bien bijective.
\end{enumerate}

\end{preuve}



\end{document}
